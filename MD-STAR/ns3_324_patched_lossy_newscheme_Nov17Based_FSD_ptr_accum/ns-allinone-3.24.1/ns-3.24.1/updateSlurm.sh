#!/bin/bash

mkdir Throughput
mkdir Done

sed -i '/bin\/sh/{s/.*/&\n#SBATCH --partition=tmp_anvil\n#SBATCH --reservation=vuran/;:a;n;ba}' *Random.slurm 

for f in *Random.slurm
	do

		fileName=$(cat $f | grep 'mv' | cut -d/ -f3 | sed 's/.$//g')
		#echo $fileName	
		echo $'\n' >> $f	
		echo "bash FSDGatheringSimplified_Uplink_OtherNodes.sh $fileName" >> $f
		echo "bash FSDGatheringSimplified_Uplink_TX.sh $fileName" >> $f
		echo "bash DropAll.sh $fileName" >> $f
		echo "mv $fileName Done" >> $f		

	done
