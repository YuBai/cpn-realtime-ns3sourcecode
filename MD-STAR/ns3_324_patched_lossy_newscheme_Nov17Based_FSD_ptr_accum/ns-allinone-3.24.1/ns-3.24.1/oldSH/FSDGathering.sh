#!/bin/bash

ipPrefix="10\.1\.3\."

for f in *.err
	do

		echo logggFSD_{$f}
		cat "$f" | grep "FSD" > FSDResult/logggFSD_{$f}
		cat FSDResult/logggFSD_{$f} | grep "case1" | cut -d: -f5 > FSDResult/logggFSD_{$f}_0_0_case1
		cat FSDResult/logggFSD_{$f} | grep "case2" | cut -d: -f6 > FSDResult/logggFSD_{$f}_0_0_case2
		echo logggFSD_{$f} done. 

	done

for f in *.err
	do
		echo RX_{$f}
		cat "$f" | grep "TraceDelay: RX" > Throughput/"RX_{$f}"
		sed -e "s/TraceDelay: RX//g" -e "s/bytes from //g" -e "s/Sequence Number//g" -e "s/Uid//g" -e "s/TXtime//g" -e "s/RXtime//g" -e "s/ns//g" -e "s/Delay//g" -e "s/Node//g" -e "s/PacketTagger//g" -e "s/LastDelay//g" -e "s/Last Jitter//g" -e "s/Followup//g" -e "s/Last//g" < Throughput/"RX_{$f}" > Throughput/"RX_{$f}_tmp"
		

		for ((i=1;i<22;i++))
			do

				echo "$ipPrefix$i,"
				cat Throughput/"RX_{$f}_tmp" | grep "$ipPrefix$i," | sed -e "s/$ipPrefix$i,//g"  > Throughput/"RX_{$f}_tmp_$ipPrefix$i"
				echo "$ipPrefix$i done."
			done		

		
		echo RX_{$f} done.		

	done
