#!/bin/bash

max=35;


for f in *.err

	do

		cat $f | grep "With Queue Length" > QueueInfo/"$f"_QueueLenTmp.err

		for((i=1;i<=max;i++))
			do
				i=$(printf "0x%02x\n" $i)
				j=$(echo "00:00:00:00:00:"$i | sed "s/0x//g")
				echo $j

				cat QueueInfo/"$f"_QueueLenTmp.err | grep "$j" | sed -e "s/ns//g" -e "s/$j//g" -e "s/:/,/g" -e "s/-/,/g" -e "s/Current State is //g" -e "s/With Queue Length /,/g" -e "s/No Packet In Service\.//g" -e "s/With Packet Being Served As /,/g"  > QueueInfo/"$f"_QueueLenTmp_"$i"


			done

	done



