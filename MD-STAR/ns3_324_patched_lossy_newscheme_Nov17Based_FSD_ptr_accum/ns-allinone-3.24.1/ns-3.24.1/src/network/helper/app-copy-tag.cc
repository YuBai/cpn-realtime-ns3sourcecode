/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "app-copy-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to AppCopyform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class AppCopyTag : public Tag
{
public:
	AppCopyTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetAppCopy (void) const;
  void SetAppCopy (double AppCopy) ;

private:
  double m_AppCopy; //!< The time stored in the tag
};

AppCopyTag::AppCopyTag ()
  : m_AppCopy (0.0)
{
}

TypeId
AppCopyTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::AppCopyTag")
    .SetParent<Tag> ()
    .SetGroupName("AppCopyTag")
    .AddConstructor<AppCopyTag> ()
    .AddAttribute ("LastAppCopy",
                   "Last AppCopy Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&AppCopyTag::GetAppCopy),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
AppCopyTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
AppCopyTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
AppCopyTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_AppCopy);
}
void
AppCopyTag::Deserialize (TagBuffer i)
{
  m_AppCopy = i.ReadDouble ();
}
void
AppCopyTag::Print (std::ostream &os) const
{
  os << "m_AppCopy=" << m_AppCopy;
}
double
AppCopyTag::GetAppCopy (void) const
{
  return m_AppCopy;
}

void
AppCopyTag::SetAppCopy (double AppCopy)
{
	m_AppCopy=AppCopy;
}

AppCopyTagger::AppCopyTagger ()
{
	this->lastAppCopy=0.0;
}

void
AppCopyTagger::WriteAppCopyValue (Ptr<const Packet> packet, double tagValue)
{
  AppCopyTag tag;
  tag.SetAppCopy(tagValue);
  packet->AddByteTag (tag);
}
int
AppCopyTagger::RecordAppCopyValue (Ptr<const Packet> packet)
{
  AppCopyTag tag;
  lastAppCopy=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastAppCopy=tag.GetAppCopy ();
  return (int)lastAppCopy;
}

double
AppCopyTagger::GetLastAppCopy (void) const
{
  return lastAppCopy;
}

} // namespace ns3
