/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fifth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FifthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FifthMacTag : public Tag
{
public:
	FifthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFifthMac (void) const;
  void SetFifthMac (uint8_t FifthMac) ;

private:
  uint8_t m_FifthMac; //!< The time stored in the tag
};

FifthMacTag::FifthMacTag ()
  : m_FifthMac (0)
{
}

TypeId
FifthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FifthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fifthMacTag")
    .AddConstructor<FifthMacTag> ()
    .AddAttribute ("LastFifthMac",
                   "Last FifthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FifthMacTag::GetFifthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FifthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FifthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FifthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FifthMac);
}
void
FifthMacTag::Deserialize (TagBuffer i)
{
  m_FifthMac = i.ReadU8 ();
}
void
FifthMacTag::Print (std::ostream &os) const
{
  os << "m_FifthMac=" << m_FifthMac;
}
uint8_t
FifthMacTag::GetFifthMac (void) const
{
  return m_FifthMac;
}

void
FifthMacTag::SetFifthMac (uint8_t FifthMac)
{
	m_FifthMac=FifthMac;
}

FifthMacTagger::FifthMacTagger ()
{
	this->lastFifthMac=0.0;
}

void
FifthMacTagger::WriteFifthMacValue (Ptr<const Packet> packet)
{
  FifthMacTag tag;
  tag.SetFifthMac(this->lastFifthMac);
  packet->AddByteTag (tag);
}
void
FifthMacTagger::RecordFifthMacValue (Ptr<const Packet> packet)
{
  FifthMacTag tag;
  SetLastFifthMac(0);

  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFifthMac=tag.GetFifthMac ();

}

uint8_t
FifthMacTagger::GetLastFifthMac (void) const
{
  return lastFifthMac;
}

void
FifthMacTagger::SetLastFifthMac (uint8_t FifthMac)
{
	lastFifthMac=FifthMac;
}


} // namespace ns3
