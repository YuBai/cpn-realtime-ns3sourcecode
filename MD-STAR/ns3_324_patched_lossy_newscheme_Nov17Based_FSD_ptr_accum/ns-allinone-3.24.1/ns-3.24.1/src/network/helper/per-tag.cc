/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "per-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class PerTag : public Tag
{
public:
	PerTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetPer (void) const;
  void SetPer (double per) ;

private:
  double m_per; //!< The time stored in the tag
};

PerTag::PerTag ()
  : m_per (0.0)
{
}

TypeId
PerTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::PerTag")
    .SetParent<Tag> ()
    .SetGroupName("PerTag")
    .AddConstructor<PerTag> ()
    .AddAttribute ("LastPer",
                   "Last Per Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&PerTag::GetPer),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
PerTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
PerTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
PerTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_per);
}
void
PerTag::Deserialize (TagBuffer i)
{
  m_per = i.ReadDouble ();
}
void
PerTag::Print (std::ostream &os) const
{
  os << "m_per=" << m_per;
}
double
PerTag::GetPer (void) const
{
  return m_per;
}

void
PerTag::SetPer (double per)
{
	m_per=per;
}

PerTagger::PerTagger (double per)
{
	this->lastPer=per;
}

void
PerTagger::WritePerValue (Ptr<const Packet> packet)
{
  PerTag tag;
  tag.SetPer(this->lastPer);
  packet->AddByteTag (tag);
}
void
PerTagger::RecordPerValue (Ptr<const Packet> packet)
{
  PerTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastPer=tag.GetPer ();

}

double
PerTagger::GetLastPer (void) const
{
  return lastPer;
}

} // namespace ns3
