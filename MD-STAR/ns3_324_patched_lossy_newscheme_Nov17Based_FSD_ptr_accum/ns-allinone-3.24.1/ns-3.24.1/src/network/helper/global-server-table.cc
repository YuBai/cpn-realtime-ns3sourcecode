#include "global-server-table.h"

#define DEFAULT_GST_SIZE 1000

namespace ns3{

globalServerTable::globalServerTable()
{
	m_gst.resize(DEFAULT_GST_SIZE);
	initServerTable(DEFAULT_GST_SIZE);

}

globalServerTable::globalServerTable(int size)
{
	m_gst.resize(size);
	initServerTable(size);
}

void
globalServerTable::reSize(int size)
{
	m_gst.resize(size);
	initServerTable(size);
}

void
globalServerTable::fillEntries(int _nodeID, int _APID, Ipv4Address _targetIP)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::fillEntries(): _nodeID out of range!");
	}
	m_gst[_nodeID].ipAddrGroup[_APID]=_targetIP;

}

//void
//globalServerTable::copyAnotherGST(globalServerTable _m_gst)
//{
//
//	for(int i=0;i<m_gst.size();i++)
//	{
//
//		m_gst[i].currentIPIndex=_m_gst.getCurrentIPIndex(i);
//		m_gst[i].ipAddrGroup=_m_gst.
//
//	}
//
//}



void
globalServerTable::setCurrentIPIndex(int _nodeID, Ipv4Address _targetIP)
{
	for(int i=0;i<MAX_IP_ADDR;i++)
	{
		if(m_gst[_nodeID].ipAddrGroup[i] == _targetIP)
		{
			m_gst[_nodeID].currentIPIndex=i;
			return;
		}
	}
	NS_FATAL_ERROR("" << Simulator::Now() << ": Current IP Index failed to be set at node " << _nodeID);
}

void
globalServerTable::setIPAddr(int _nodeID, int _APID, Ipv4Address _targetIP)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::setIPAddr(): _nodeID out of range!");
	}
	m_gst[_nodeID].ipAddrGroup[_APID]=_targetIP;
}

void
globalServerTable::setCurrentCopyState(int _nodeID, int _currentCopyState)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::setCurrentCopyState(): _nodeID out of range!");
	}
	m_gst[_nodeID].currentCopyState=_currentCopyState;

}

int
globalServerTable::getCurrentCopyState(int _nodeID)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::getCurrentCopyState(): _nodeID out of range!");
	}
	return m_gst[_nodeID].currentCopyState;

}

int
globalServerTable::getCurrentIPIndex(int _nodeID)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::getCurrentIPIndex(): _nodeID out of range!");
	}
	return m_gst[_nodeID].currentIPIndex;
}

int
globalServerTable::getIPIndexByIP(int _nodeID, Ipv4Address _targetIP)
{
	for(int i=0;i<MAX_IP_ADDR;i++)
	{
		if(m_gst[_nodeID].ipAddrGroup[i] == _targetIP)
		{
			return i;
		}
	}
	NS_FATAL_ERROR("" << Simulator::Now() << ": IP Index failed to be fetched by at node " << _nodeID << " and input IP: " << _targetIP);

}
void
globalServerTable::setLastTriggerTime(int _nodeID, Time _time)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::setLastTriggerTime(): _nodeID out of range!");
	}
	m_gst[_nodeID].lastTriggerTime=_time;
}

Time
globalServerTable::getLastTriggerTime(int _nodeID)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::getLastTriggerTime(): _nodeID out of range!");
	}
	return m_gst[_nodeID].lastTriggerTime;
}

void
globalServerTable::initServerTable(int size)
{
	for(int i=0;i<size;i++)
	{
		m_gst[i].currentIPIndex=-1;
		for(int j=0; j<MAX_IP_ADDR;j++)
		{
			m_gst[i].ipAddrGroup[j]="255.255.255.255";
		}
		m_gst[i].lastTriggerTime=Seconds(7);//may be stupid here
	}
}


}


