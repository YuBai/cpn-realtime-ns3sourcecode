/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "second-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to SecondMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class SecondMacTag : public Tag
{
public:
	SecondMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetSecondMac (void) const;
  void SetSecondMac (uint8_t SecondMac) ;

private:
  uint8_t m_SecondMac; //!< The time stored in the tag
};

SecondMacTag::SecondMacTag ()
  : m_SecondMac (0)
{
}

TypeId
SecondMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::SecondMacTag")
    .SetParent<Tag> ()
    .SetGroupName("SecondMacTag")
    .AddConstructor<SecondMacTag> ()
    .AddAttribute ("LastSecondMac",
                   "Last SecondMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&SecondMacTag::GetSecondMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
SecondMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
SecondMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
SecondMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_SecondMac);
}
void
SecondMacTag::Deserialize (TagBuffer i)
{
  m_SecondMac = i.ReadU8 ();
}
void
SecondMacTag::Print (std::ostream &os) const
{
  os << "m_SecondMac=" << m_SecondMac;
}
uint8_t
SecondMacTag::GetSecondMac (void) const
{
  return m_SecondMac;
}

void
SecondMacTag::SetSecondMac (uint8_t SecondMac)
{
	m_SecondMac=SecondMac;
}

SecondMacTagger::SecondMacTagger ()
{
	this->lastSecondMac=0.0;
}

void
SecondMacTagger::WriteSecondMacValue (Ptr<const Packet> packet)
{
  SecondMacTag tag;
  tag.SetSecondMac(this->lastSecondMac);
  packet->AddByteTag (tag);
}
void
SecondMacTagger::RecordSecondMacValue (Ptr<const Packet> packet)
{
  SecondMacTag tag;
  lastSecondMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastSecondMac=tag.GetSecondMac ();

}

uint8_t
SecondMacTagger::GetLastSecondMac (void) const
{
  return lastSecondMac;
}

void
SecondMacTagger::SetLastSecondMac (uint8_t SecondMac)
{
	lastSecondMac=SecondMac;
}


} // namespace ns3
