/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 */

#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "packet-loss-counter.h"
#include "ns3/enqueue-time-compensate-tag.h"
#include "ns3/stop-traffic-tag.h"
#include "ns3/probe-tag.h"

#include "seq-ts-header.h"
#include "udp-server.h"
#include "udp-client.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("UdpServer");

NS_OBJECT_ENSURE_REGISTERED (UdpServer);


TypeId
UdpServer::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::UdpServer")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<UdpServer> ()
    .AddAttribute ("Port",
                   "Port on which we listen for incoming packets.",
                   UintegerValue (100),
                   MakeUintegerAccessor (&UdpServer::m_port),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("PacketWindowSize",
                   "The size of the window used to compute the packet loss. This value should be a multiple of 8.",
                   UintegerValue (32),
                   MakeUintegerAccessor (&UdpServer::GetPacketWindowSize,
                                         &UdpServer::SetPacketWindowSize),
                   MakeUintegerChecker<uint16_t> (8,256))
  ;
  return tid;
}

UdpServer::UdpServer ()
  : m_lossCounter (0),
	rcvPktLastSlot (0),
	rcvPktThisSlot (0),
//	currentTriggeringTime (-1),
//	currentProbeTrainLen (-1),
	currentTriggeringTime (MAX_FSD_CLIENT_ALLOWED),
	currentProbeTrainLen (MAX_FSD_CLIENT_ALLOWED),
	m_reportInterval (Seconds(0.1)),
	feedbackClone (MAX_FSD_CLIENT_ALLOWED),
	fakePkt1Clone (MAX_FSD_CLIENT_ALLOWED),
	fakePkt2Clone (MAX_FSD_CLIENT_ALLOWED),
	fakePkt1Clone_ACK (MAX_FSD_CLIENT_ALLOWED),
	fakePkt2Clone_ACK (MAX_FSD_CLIENT_ALLOWED),
	feedbackAddr (MAX_FSD_CLIENT_ALLOWED),
	//feedbackACKedOne (0),
	//feedbackACKedTwo (0),
	//feedbackACKedOne_Counter (0),
	//feedbackACKedTwo_Counter (0),
	//fakePktClone_Train_ACK (0),
	feedbackACKedOne (MAX_FSD_CLIENT_ALLOWED),
	feedbackACKedTwo (MAX_FSD_CLIENT_ALLOWED),
	feedbackACKedOne_Counter (MAX_FSD_CLIENT_ALLOWED),
	feedbackACKedTwo_Counter (MAX_FSD_CLIENT_ALLOWED),
	fakePktClone_Train_ACK (MAX_FSD_CLIENT_ALLOWED),
	m_gst(10),
	currentIndex(0)
//	feedbackAddr("")
{
  NS_LOG_FUNCTION (this);
  m_received=0;
//  memset(feedbackACKedOne,0,sizeof(feedbackACKedOne));
//  memset(feedbackACKedTwo,0,sizeof(feedbackACKedTwo));
//  memset(feedbackACKedOne_Counter,0,sizeof(feedbackACKedOne_Counter));
//  memset(feedbackACKedTwo_Counter,0,sizeof(feedbackACKedTwo_Counter));
//  memset(fakePktClone_Train_ACK,0,sizeof(fakePktClone_Train_ACK));
//  memset(currentTriggeringTime,0,sizeof(currentTriggeringTime));
//  memset(currentProbeTrainLen,0,sizeof(currentProbeTrainLen));
  m_probingPort=11;

}

UdpServer::~UdpServer ()
{
  NS_LOG_FUNCTION (this);
}

uint16_t
UdpServer::GetPacketWindowSize () const
{
  NS_LOG_FUNCTION (this);
  return m_lossCounter.GetBitMapSize ();
}

void
UdpServer::SetPacketWindowSize (uint16_t size)
{
  NS_LOG_FUNCTION (this << size);
  m_lossCounter.SetBitMapSize (size);
}

uint32_t
UdpServer::GetLost (void) const
{
  NS_LOG_FUNCTION (this);
  return m_lossCounter.GetLost ();
}

uint32_t
UdpServer::GetReceived (void) const
{
  NS_LOG_FUNCTION (this);
  return m_received;
}

globalServerTable&
UdpServer::GetGST(void)
{
	return m_gst;
}

void
UdpServer::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

void
UdpServer::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny (),
                                                   m_port);
      m_socket->Bind (local);
    }

  m_socket->SetRecvCallback (MakeCallback (&UdpServer::HandleRead, this));

  if (m_socket6 == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket6 = Socket::CreateSocket (GetNode (), tid);
      Inet6SocketAddress local = Inet6SocketAddress (Ipv6Address::GetAny (),
                                                   m_port);
      m_socket6->Bind (local);
    }

  m_socket6->SetRecvCallback (MakeCallback (&UdpServer::HandleRead, this));

  //if(m_node->GetId() == 0) //meaning it is server node - we do not need client to send periodic report to server
  //{
	  Simulator::Schedule(m_reportInterval, &UdpServer::ReportStatistics, this);
  //}

}

void
UdpServer::StopApplication ()
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0)
    {
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
    }
}

void
UdpServer::ReportStatistics()
{
	int tmp;
	//NS_LOG_UNCOND ("ReportStatistics ENTERED.");

	for(int _nodeID = 0; _nodeID < MAX_FSD_CLIENT_ALLOWED; _nodeID++)
	{

		if(feedbackClone[_nodeID] == 0)
		{
			//continue;
		}
		else //(feedbackClone != 0 && feedbackAddr != 0)
		{
			//check to see whether we need to resend fake packets
			/*if(fakePkt1Clone != 0 && feedbackACKedOne == 0 && feedbackACKedOne_Counter++%30==0)
			{
				NS_LOG_UNCOND ("fakePkt1Clone != 0 && feedbackACKedOne == 0 && feedbackACKedOne_Counter++30==0");
				tmp = m_socket->SendTo(fakePkt1Clone, 0, feedbackAddr);
				if(tmp != -1)
				{
					NS_LOG_UNCOND ("" << Simulator::Now() << ": Fake packet 1 resent successfully!");
					//fakePkt1Clone=0;
				}
			}*/
			/*if(fakePkt2Clone != 0 && feedbackACKedTwo == 0 && feedbackACKedTwo_Counter++%30==0)
			{
				NS_LOG_UNCOND ("fakePkt2Clone != 0 && feedbackACKedTwo == 0 && feedbackACKedTwo_Counter++30==0");
				tmp = m_socket->SendTo(fakePkt2Clone, 0, feedbackAddr);
				if(tmp != -1)
				{
					NS_LOG_UNCOND ("" << Simulator::Now() << ": Fake packet 2 resent successfully!");
					//fakePkt2Clone=0;
				}
			}*/
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Enter into UdpServer::ReportStatistics:else");
			if(fakePkt1Clone_ACK[_nodeID] != 0 && currentIndex == _nodeID) //original path case
			{
				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ACK for Fake packet 1 received.");
				//m_ht.WriteHopValue(fakePkt1Clone,1);
				tmp = m_socket->SendTo(fakePkt1Clone_ACK[_nodeID], 0, feedbackAddr[_nodeID]);
				if(tmp != -1)
				{
					NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake packet 1 with ACK tag resent successfully!");
					//fakePkt1Clone_ACK=0;
				}
				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ACK for Fake packet 1 received with m_finipt: " << m_finipt.GetLastFinishProbe());
			}
			if(fakePkt2Clone_ACK[_nodeID] != 0 && currentIndex == _nodeID)
			{
				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ACK for Fake packet 2 received.");
				//m_ht.WriteHopValue(fakePkt2Clone,1);
				//m_ht.RecordHopValue(fakePkt2Clone);
				tmp = m_socket->SendTo(fakePkt2Clone_ACK[_nodeID], 0, feedbackAddr[_nodeID]);
				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ACK for Fake packet 2 received with m_finipt: " << m_finipt.GetLastFinishProbe());
				if(tmp != -1)
				{
					NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake packet 2 with ACK tag resent successfully!");
					//fakePkt2Clone_ACK=0;
				}
			}

			if((m_gst.getCurrentCopyState(_nodeID) != 3 && m_gst.getCurrentCopyState(_nodeID) != 4)
					&& Simulator::Now() < Seconds(7)) //meaning we need appCopy - which one is depending on which server this is
			{
				Ptr<Packet>tmp = feedbackClone[_nodeID]->Copy();

				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Amount of Pkts Received During Time Interval " << m_reportInterval << " is " << rcvPktThisSlot-rcvPktLastSlot);
				m_stat.WriteStatisticsValue(tmp, rcvPktThisSlot-rcvPktLastSlot == 0 ? 1 : rcvPktThisSlot-rcvPktLastSlot);
				m_stat.RecordStatisticsValue(tmp);
				m_ptg.WritePortValue(tmp,m_port);
				rcvPktLastSlot = rcvPktThisSlot;
				//NS_LOG_UNCOND ("DONE.");
				m_socket->SendTo (tmp, 0, feedbackAddr[_nodeID]);
				NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": FEEDBACK CLONE SENT WITH M_STAT AS " << m_stat.GetLastStatistics() << " AND FEEDBACK ADDR " << InetSocketAddress::ConvertFrom (feedbackAddr[_nodeID]).GetIpv4 () << " AND PORT NUMBER " << m_port << " AND I AM " << m_node->GetId());
			}
			else
			{

				NS_LOG_UNCOND_YB("" << Simulator::Now() << ": packet with port " << m_port << " for node " << _nodeID << " has already copied. Report skipped.");
			}

		}
	}
	Simulator::Schedule (m_reportInterval, &UdpServer::ReportStatistics, this);
}

void
UdpServer::HandleRead (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Ptr<Packet> packet;
  Address from;

  IdTagger m_it_tmp (0.0);

  //NS_LOG_UNCOND ("HandleRead ENTERED.");
  if(m_gst.getCurrentIPIndex(3) != -1)
  {
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": I am " << GetNode()->GetId() << " and I have 3rd entry on m_gst not empty!");
  }

 while ((packet = socket->RecvFrom (from)))
    {
	  m_fpt.RecordFakePktValue(packet);
	  m_it_tmp.RecordIdValue(packet);

	  StopTrafficTagger m_stt_tmp;

	  m_stt_tmp.RecordStopTrafficValue(packet);

	  ProbeTagger m_pt;
	  m_pt.RecordProbeValue(packet);
	  //m_sht.RecordSecondHopValue(packet);
	  //NS_LOG_UNCOND ("LAST FPT:" << m_fpt.GetLastFakePkt() << " LAST SHT: " << m_sht.GetLastSecondHop());
	  /*if(m_sht.GetLastSecondHop() == 1)
	  {
	  	  NS_LOG_UNCOND ("END-OF-TRAIN-ACK RECEIVED.");
	  	  //fakePktClone_Train_ACK = packet->Copy();
	  	  fakePktClone_Train_ACK = 1;
	  }*/


	  m_finipt.RecordFinishProbeValue(packet);
	  if(m_finipt.GetLastFinishProbe() == 1)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": FEEDBACK PKT for One Received.");
		  //feedbackACKedOne = 1;
		  fakePkt1Clone_ACK[m_it_tmp.GetLastId()] = 0;
		  //fakePktClone_Train_ACK = 0;

	  }
	  else if(m_finipt.GetLastFinishProbe() == 2)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": FEEDBACK PKT for Two Received.");
		  //feedbackACKedTwo = 1;
		  fakePkt2Clone_ACK[m_it_tmp.GetLastId()] = 0;
		  //fakePktClone_Train_ACK = 0;

	  }
	  else
	  {
		  //feedbackACKedOne = 0;
		  //feedbackACKedTwo = 0;
		  //fakePkt1Clone_ACK = 0;
		  //fakePkt2Clone_ACK = 0;
		  //fakePktClone_Train_ACK = 0;
		  //NS_LOG_UNCOND ("" << Simulator::Now() << ": fakePkt1Clone_ACK and fakePkt2Clone_ACK Reset!");
	  }

	  //newly added to control whether downlink to specific user is stopped
	  if(m_stt_tmp.GetLastStopTraffic() != 0)
	  {
		  NS_LOG_UNCOND_HYB("" << Simulator::Now() << ": I Got A Packet with stop traffic tag " << m_stt_tmp.GetLastStopTraffic() << " and it comes from " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " and I am Node " << GetNode()->GetId());
		  int stopTrafficValue=m_stt_tmp.GetLastStopTraffic();
		  //m_qtRead.SetTid();
		  //packet->PeekPacketTag(m_qtRead);
	  	  m_qtRead.SetTid(7);

		  //NS_LOG_UNCOND("Done.");
	      packet->RemoveAllPacketTags ();
		  //NS_LOG_UNCOND("Done.");
	      packet->RemoveAllByteTags ();
		  //NS_LOG_UNCOND("Done.");

	      m_stt_tmp.WriteStopTrafficValue(packet,stopTrafficValue);
	      packet->AddPacketTag(m_qtRead);
	      //NS_LOG_UNCOND("Done.");

	      /*SeqTsHeader seqTsTmp;
	      seqTsTmp.SetSeq (m_sent);
	      packet->AddHeader (seqTsTmp);
	      if(m_socket->Send (packet) >= 0)
	      {
	    	  ++m_sent;
	    	  NS_LOG_UNCOND("" << Simulator::Now() << ": I sent a stop traffic packet with tag value " << stopTrafficValue);
	      }*/

	      int tmp;
		  //NS_LOG_UNCOND("Done.");
	      tmp=socket->SendTo (packet, 0, from);
		  //NS_LOG_UNCOND("Done.");
	      NS_LOG_UNCOND_YB("" << Simulator::Now() << ": I called SendTo() with Return Value " << tmp);  //Send Packet Back


	  }


	  if(m_fpt.GetLastFakePkt() != 0)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Fake Packet Detected!");

		  //Keep Necessary Tags - A Trick Played Here That For Packet Without Mac Tag I Added a New Tag Value with All 0s There
		  //m_fpt.RecordFakePktValue(packet);

		  double tempPrinting[MAX_PROBE_TRAIN_LENGTH];
		  m_dst_tmp.RecordDelaySamplesValue(packet,tempPrinting);
		  for(int x=0;x<INITIAL_PROBE_TRAIN_LENGTH;x++)
		  {
			  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": Check Value: FSD[" << x << "]: " << tempPrinting[x]);
		  }


		  m_fmt.RecordFakeMacValue(packet);
		  m_fdfb.RecordDelaySamples(packet);
		  m_apt.RecordAlterPathValue(packet);
		  m_ptt.RecordProbeTimeValue(packet);
		  m_ptlt.RecordProbeTrainLengthValue(packet);

		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "ServerSide: m_fmt: " << m_fmt.GetLastFakeMac() << " m_apt: " << m_apt.GetLastAlterPath() << " m_ptt: " << m_ptt.GetLastProbeTime() << " m_ptlt: " << m_ptlt.GetLastProbeTrainLength());

		  if(currentTriggeringTime[m_it_tmp.GetLastId()] == -1 && currentProbeTrainLen[m_it_tmp.GetLastId()] == -1)
		  {
			  currentTriggeringTime[m_it_tmp.GetLastId()] = m_ptt.GetLastProbeTime();
			  currentProbeTrainLen[m_it_tmp.GetLastId()] = m_ptlt.GetLastProbeTrainLength();
		  }

		  //Do some cleaninng work as what is done in udp-echo-server
		  //packet->PeekPacketTag(m_qtRead);
	  	  m_qtRead.SetTid(7);

		  packet->RemoveAllPacketTags ();
	      packet->RemoveAllByteTags ();

	      packet->AddPacketTag(m_qtRead);

	      m_fpt.WriteFakePktValue(packet,1); //Making sure that when following down someone still knows that this is the packet for probing feedback
	      m_fmt.WriteFakeMacValue(packet);
		  m_apt.WriteAlterPathValue(packet, m_apt.GetLastAlterPath());
		  m_fpt.RecordFakePktValue(packet);
		  m_ptt.WriteProbeTimeValue(packet, m_ptt.GetLastProbeTime());
		  m_ptlt.WriteProbeTrainLengthValue(packet, m_ptlt.GetLastProbeTrainLength());

		  m_fdfb.WriteDelaySamples(packet);
		  m_ht.WriteHopValue(packet,1);

		  int tmp;
		  //NS_LOG_UNCOND ("tmp=socket->SendTo(packet,0,from)");
	      tmp = socket->SendTo (packet, 0, from);  //Send Packet Back
	      NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake Packet Sent Through Socket with TAG: " << m_fmt.GetLastFakeMac() << " and m_apt: " << m_apt.GetLastAlterPath() << " and m_fpt: " << m_fpt.GetLastFakePkt() << " and tmp: " << tmp << " and from: " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " and isMatching(): " << InetSocketAddress::IsMatchingType (from) << " and UID: " << packet->GetUid());

	      //if(tmp == -1) //which means the feedback packet is not sent successfully
	      //{
	    	  if(m_apt.GetLastAlterPath() == 0)
	    	  {
	    		  //fakePkt1Clone = packet->Copy();
	    		  if(fakePkt1Clone_ACK[m_it_tmp.GetLastId()] != 0)
	    		  {
	    			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": fakePkt1Clone_ACK refreshed!");
	    		  }

	    		  fakePkt1Clone_ACK[m_it_tmp.GetLastId()] = packet->Copy();
	    		  fakePkt2Clone_ACK[m_it_tmp.GetLastId()] = 0;
	    		  //m_ht.WriteHopValue(fakePkt1Clone_ACK,1);
	    		  //m_ht.RecordHopValue(fakePkt1Clone_ACK);
	    		  //NS_LOG_UNCOND ("" << Simulator::Now() << ": Fake Pkt 1 copied due to socket failure.");
	    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake Pkt 1 and Fake Pkt 1 ACK copied.");
	    	  }
	    	  else
	    	  {
	    		  if(fakePkt2Clone_ACK[m_it_tmp.GetLastId()] != 0)
	    		  {
	    			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": fakePkt2Clone_ACK refreshed!");
	    		  }

	    		  //fakePkt2Clone = packet->Copy();
	    		  fakePkt2Clone_ACK[m_it_tmp.GetLastId()] = packet->Copy();
	    		  fakePkt1Clone_ACK[m_it_tmp.GetLastId()] = 0;
	    		  //m_ht.WriteHopValue(fakePkt2Clone_ACK,1);
	    		  //m_ht.RecordHopValue(fakePkt2Clone_ACK);
	    		  //NS_LOG_UNCOND ("" << Simulator::Now() << ": Fake Pkt 2 copied due to socket failure.");
	    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake Pkt 2 and Fake Pkt 2 ACK copied.");
	    	  }
	      //}
		currentIndex = m_it_tmp.GetLastId();
	      //Following Only For Mesh Network
	      //socket->SendTo (packet, 0, from);  //Send Packet Back
	      //NS_LOG_UNCOND ("Fake Packet Sent Through Socket AGAIN with TAG: " << m_fmt.GetLastFakeMac() << " and m_apt: " << m_apt.GetLastAlterPath());


	  }
	  else if (packet->GetSize () > 0)
        {
          SeqTsHeader seqTs;
          packet->RemoveHeader (seqTs);
          uint32_t currentSequenceNumber = seqTs.GetSeq ();

          m_udje.RecordRx(packet);
          m_fde.RecordDqTime(packet);
          m_st.RecordSeqValue(packet);
          m_ct.RecordChosenValue(packet);
          m_ptg.RecordPortValue(packet);

    	  IdTagger m_it (0);
    	  m_it.RecordIdValue(packet);

	  EnqueueTimeCompensateTagger m_etct;
	  m_etct.RecordEqTime(packet);

          if (InetSocketAddress::IsMatchingType (from))
            {
              NS_LOG_INFO ("TraceDelay: RX " << packet->GetSize () <<
                           " bytes from "<< InetSocketAddress::ConvertFrom (from).GetIpv4 () <<
                           " Sequence Number: " << currentSequenceNumber <<
                           " Uid: " << packet->GetUid () <<
                           " TXtime: " << seqTs.GetTs () <<
                           " RXtime: " << Simulator::Now () <<
                           " Delay: " << Simulator::Now () - seqTs.GetTs ());

               NS_LOG_UNCOND ("TraceDelay: RX" << packet->GetSize () <<
                           ",bytes from "<< InetSocketAddress::ConvertFrom (from).GetIpv4 () <<
                           ",Sequence Number" << currentSequenceNumber <<
                           ",Uid" << packet->GetUid () <<
                           ",TXtime" << seqTs.GetTs () <<
                           ",RXtime" << Simulator::Now () <<
                           ",Delay" << Simulator::Now () - seqTs.GetTs () <<
			   ",CompensateDelay" << m_etct.GetLastRecordedCompensateTime() <<
						   ",Node" << GetNode()->GetId() <<
						   ",PacketTagger" << m_st.GetLastSeq() <<
						   "," << packet->GetSize() <<
						   ",LastDelay" << m_udje.GetLastDelay() <<
						   ",Last Jitter" << m_udje.GetLastJitter() <<
						   ",Followup Delay" << m_fde.GetLastQDelay() <<
						   ",Local Port" << m_ptg.GetLastPort() <<
						   ",From Node: " << m_it_tmp.GetLastId() <<
						   " with first IP index " << m_gst.getCurrentIPIndex(m_it_tmp.GetLastId()));


              //Added by Y.B.
              //NS_LOG_UNCOND (InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << packet->GetSize() << ",FollowUp Delay," << m_fde.GetLastQDelay());
              //if(m_ct.GetLastChosen() == 1)
              //if(InetSocketAddress::ConvertFrom(from).GetIpv4() == "10.1.3.2" || InetSocketAddress::ConvertFrom(from).GetIpv4() == "10.1.4.2") //very dirty hack
               //if(false)
               //{
               //if(m_port == m_probingPort) //dirty hack - I assume application for port 11 is only for the purpose of probing
               if(GetNode()->GetId() < 3)
               {
               	   	  if(m_gst.getCurrentIPIndex(m_it_tmp.GetLastId()) == -1)
               	   	  {
               	   		  m_gst.setCurrentIPIndex(m_it_tmp.GetLastId(),InetSocketAddress::ConvertFrom(from).GetIpv4());
               	   	  }
            	  	  rcvPktThisSlot++;
            		  //packet->PeekPacketTag(m_qtRead);
            	  	  m_qtRead.SetTid(7);
            	  	  m_act.RecordAppCopyValue(packet);

            		  packet->RemoveAllPacketTags ();
            	      packet->RemoveAllByteTags ();
            	      packet->AddPacketTag(m_qtRead);
            	  	  feedbackClone[m_it_tmp.GetLastId()] = packet->Copy();
            	  	  feedbackAddr[m_it_tmp.GetLastId()] = from;

            	  	  m_gst.setCurrentCopyState(m_it_tmp.GetLastId(),m_act.GetLastAppCopy());
            	  	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Current copy state for node  " << m_it_tmp.GetLastId() << " has changed to " << m_act.GetLastAppCopy() << " with port " << m_port);
            	  	  //Simulator::Schedule (m_reportInterval, &UdpServer::ReportStatistics, this, m_it_tmp.GetLastId());
               }
            	  //}

            }
          else if (Inet6SocketAddress::IsMatchingType (from))
            {
              NS_LOG_INFO ("TraceDelay: RX " << packet->GetSize () <<
                           " bytes from "<< Inet6SocketAddress::ConvertFrom (from).GetIpv6 () <<
                           " Sequence Number: " << currentSequenceNumber <<
                           " Uid: " << packet->GetUid () <<
                           " TXtime: " << seqTs.GetTs () <<
                           " RXtime: " << Simulator::Now () <<
                           " Delay: " << Simulator::Now () - seqTs.GetTs ());
            }

          m_lossCounter.NotifyReceived (currentSequenceNumber);
          m_received++;
        }
	  //NS_LOG_UNCOND("HERE: " << m_gst.getLastTriggerTime(m_it_tmp.GetLastId()));

	  //if(InetSocketAddress::ConvertFrom (from).GetIpv4 () == "10.1.4.2" && Simulator::Now() > m_gst.getLastTriggerTime(m_it_tmp.GetLastId()))//lastTriggeredTime[m_it_tmp.GetLastId()])
	  NS_LOG_UNCOND_YB("HERE HERE HERE");
	  if(m_port != m_probingPort
			  && GetNode()->GetId() < 3 //dirty hack here
			  && m_gst.getCurrentIPIndex(m_it_tmp.GetLastId()) != m_gst.getIPIndexByIP(m_it_tmp.GetLastId(),InetSocketAddress::ConvertFrom(from).GetIpv4())
			  && m_gst.getLastTriggerTime(m_it_tmp.GetLastId()) < Simulator::Now())
	  {
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": This is node " << GetNode()->GetId() << " and I have detected existence of " << InetSocketAddress::ConvertFrom(from).GetIpv4());

		  //Ptr<Application>testt=GetNode()->GetApplication(2); //starts from 2 since 0 is for probing, 1 is udp server
		  Ptr<Application>testt=GetNode()->GetApplication(m_it_tmp.GetLastId()-1); //starts from 2 since 0 is for probing, 1 is udp server

		  Ptr<UdpClient> test=DynamicCast<UdpClient>(testt);
		  NS_LOG_UNCOND_YB("test now is " << testt);
		  Address tmp=test->GetRemotePeerAddr();
		  uint16_t tmpPort=test->GetPort();
		  //NS_LOG_UNCOND("IPV4 I get is " << tmp.GetIpv4() << " and port I get is " << tmp.GetPort());
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": IPv4 I get is " << Ipv4Address::ConvertFrom(tmp) << " with port number " << tmpPort << " with probe tag as " << m_pt.GetLastProbe());
		  //if(m_pt.GetLastProbe() == 0 && test->GetFlag() != 2) //meaning decision on using AP2 is made
		  if(m_pt.GetLastProbe() == 0 && test->GetFlag() != m_gst.getCurrentIPIndex(m_it_tmp.GetLastId())+1)
		  {
			  NS_LOG_UNCOND_YB("" << Simulator::Now() << "Set Flag on udp-server.");
			  Ipv4Address local;
			  //local.Set("10.1.4.2");
			  local.Set(InetSocketAddress::ConvertFrom(from).GetIpv4().Get());
			  //test->SetRemotePeerAddr(Address(local));
			  test->SetRemote(local,tmpPort);
			  test->SetFlag(m_gst.getCurrentIPIndex(m_it_tmp.GetLastId())+1);
			  //lastTriggeredTime = lastTriggeredTime+Seconds(60);
			  m_gst.setLastTriggerTime(m_it_tmp.GetLastId(),m_gst.getLastTriggerTime(m_it_tmp.GetLastId()));
			  m_gst.setCurrentIPIndex(m_it_tmp.GetLastId(),local); 
		  }

		  //newly added - whether this is correct or not can only be verified through a multiple run
		  feedbackClone[m_it_tmp.GetLastId()] = 0;
		  fakePkt1Clone_ACK[m_it_tmp.GetLastId()] = 0;
		  fakePkt2Clone_ACK[m_it_tmp.GetLastId()] = 0;
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": all feedback pkts are reset.");
//		  else if(m_pt.GetLastProbe() == 0 && test->GetFlag() != 1) //meaning decision on using AP2 is made
//		  {
//			  NS_LOG_UNCOND("" << Simulator::Now() << "Set Flag on udp-server.");
//			  Ipv4Address local;
//			  //local.Set("10.1.3.2");
//			  local.Set(InetSocketAddress::ConvertFrom(from).GetIpv4().Get());
//			  //test->SetRemotePeerAddr(Address(local));
//			  test->SetRemote(local,tmpPort);
//			  test->SetFlag(0);
//			  //lastTriggeredTime=lastTriggeredTime+Seconds(60);//Simulator::Now(); very stupid modification
//			  m_gst.setLastTriggerTime(m_it_tmp.GetLastId(),m_gst.getLastTriggerTime(m_it_tmp.GetLastId())+Seconds(60));
//
//		  }

	  }
//	  else if(InetSocketAddress::ConvertFrom (from).GetIpv4 () == "10.1.3.2" && Simulator::Now() > m_gst.getLastTriggerTime(m_it_tmp.GetLastId()))
//	  {
//
//		  NS_LOG_UNCOND("" << Simulator::Now() << ": This is node " << GetNode()->GetId() << " and I have detected existence of 10.1.3.2");
//		  Ptr<Application>testt=GetNode()->GetApplication(2); //starts from 2 since 0 is for probing, 1 is udp server
//		  Ptr<UdpClient> test=DynamicCast<UdpClient>(testt);
//		  NS_LOG_UNCOND("test now is " << testt);
//		  Address tmp=test->GetRemotePeerAddr();
//		  uint16_t tmpPort=test->GetPort();
//		  //NS_LOG_UNCOND("IPV4 I get is " << tmp.GetIpv4() << " and port I get is " << tmp.GetPort());
//		  NS_LOG_UNCOND("" << Simulator::Now() << ": IPv4 I get is " << Ipv4Address::ConvertFrom(tmp) << " with port number " << tmpPort << " with probe tag as " << m_pt.GetLastProbe());
//		  if(m_pt.GetLastProbe() == 0 && test->GetFlag() != 1) //meaning decision on using AP2 is made
//		  {
//			  NS_LOG_UNCOND("" << Simulator::Now() << "Set Flag on udp-server.");
//			  Ipv4Address local;
//			  local.Set("10.1.3.2");
//			  //test->SetRemotePeerAddr(Address(local));
//			  test->SetRemote(local,tmpPort);
//			  test->SetFlag(0);
//			  //lastTriggeredTime=lastTriggeredTime+Seconds(60);//Simulator::Now(); very stupid modification
//			  m_gst.setLastTriggerTime(m_it_tmp.GetLastId(),m_gst.getLastTriggerTime(m_it_tmp.GetLastId())+Seconds(60));
//
//		  }
//
//
//	  }
    }
}

} // Namespace ns3
