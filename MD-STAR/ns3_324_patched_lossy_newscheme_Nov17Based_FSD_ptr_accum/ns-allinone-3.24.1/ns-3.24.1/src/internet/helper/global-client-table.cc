#include "../../internet/helper/global-client-table.h"

#define DEFAULT_GCT_SIZE 1000
//#define DEFAULT_GAT_SIZE 2

namespace ns3{


globalClientTable::globalClientTable()
{
	m_gct.resize(DEFAULT_GCT_SIZE);
	initClientTable(DEFAULT_GCT_SIZE);

}

globalClientTable::globalClientTable(int size)
{
	m_gct.resize(size);
	initClientTable(size);
}

void
globalClientTable::reSize(int size)
{
	m_gct.resize(size);
	initClientTable(size);
}

void
globalClientTable::setMacAddr(int _nodeID, Mac48Address _macAddr)
{
	if((uint32_t)_nodeID > m_gct.size())
	{
		NS_FATAL_ERROR("globalClientTable::setMacAddr(): _nodeID out of range!");
	}
	m_gct[_nodeID].macAddr=_macAddr;
}

void
globalClientTable::setIPAddr(int _nodeID, int _APID, Ipv4Address _targetIP)
{
	if((uint32_t)_nodeID > m_gct.size())
	{
		NS_FATAL_ERROR("globalClientTable::setIPAddr(): _nodeID out of range!");
	}
	m_gct[_nodeID].ipAddrGroup[_APID]=_targetIP;
}

Mac48Address
globalClientTable::getMacAddrBynodeID(int _nodeID)
{
	if((uint32_t)_nodeID > m_gct.size())
	{
		NS_FATAL_ERROR("globalClientTable::getMacAddrBynodeID(): _nodeID out of range!");
	}
	return m_gct[_nodeID].macAddr;
}

Ipv4Address
globalClientTable::getIpAddrBynodeID(int _nodeID, int _APID)
{
	if((uint32_t)_nodeID > m_gct.size())
	{
		NS_FATAL_ERROR("globalClientTable::getIpAddrBynodeID(): _nodeID out of range!");
	}
	return m_gct[_nodeID].ipAddrGroup[_APID];
}

void
globalClientTable::initClientTable(int size)
{
	for(int i=0;i<size;i++)
	{
		m_gct[i].macAddr="ff:ff:ff:ff:ff:ff";
		for(int j=0;j<MAX_IP_ADDR;j++)
		{
			m_gct[i].ipAddrGroup[j]="255.255.255.255";
		}

	}
}

//globalAPTable::globalAPTable()
//{
//	m_gat.resize(DEFAULT_GAT_SIZE);
//	initAPTable(DEFAULT_GAT_SIZE);
//}
//
//globalAPTable::globalAPTable(int size)
//{
//	m_gat.resize(size);
//	initAPTable(size);
//}
//
//void
//globalAPTable::reSize(int size)
//{
//	m_gat.resize(size);
//	initAPTable(size);
//}
//
//void
//globalAPTable::setMacAddr(int _apID, Mac48Address _macAddr)
//{
//	m_gat[_apID].apAddr = _macAddr;
//}
//
//int
//globalAPTable::getAPIDByMacAddr(Mac48Address _macAddr)
//{
//	std::vector<apTable>::size_type i;
//	for (i = 0; i != m_gat.size(); i++)
//	{
//		if(m_gat.at(i).apAddr == _macAddr)
//		{
//			return i;
//		}
//	}
//	return -1;
//}
//
//void
//globalAPTable::initAPTable(int size)
//{
//	for(int i=0;i<size;i++)
//	{
//		m_gat[i].apAddr="ff:ff:ff:ff:ff:ff";
//		m_gat[i].apID=-1;
//	}
//}

}

