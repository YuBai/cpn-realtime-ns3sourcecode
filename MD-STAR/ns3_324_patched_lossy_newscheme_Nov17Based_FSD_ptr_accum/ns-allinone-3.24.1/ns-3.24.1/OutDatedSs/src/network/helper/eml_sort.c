/*
 * eml_sort.c
 *
 * Code generation for function 'eml_sort'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "FSDFunctionReference.h"
#include "eml_sort.h"

/* Function Declarations */
static void eml_sort_idx(const double x_data[30], const int x_size[1], int
  idx_data[30], int idx_size[1]);

/* Function Definitions */
static void eml_sort_idx(const double x_data[30], const int x_size[1], int
  idx_data[30], int idx_size[1])
{
  int k;
  int i;
  int idx0_data[30];
  int i2;
  int j;
  int pEnd;
  int p;
  int q;
  int qEnd;
  int kEnd;
  idx_size[0] = (signed char)x_size[0];
  for (k = 1; k <= x_size[0]; k++) {
    idx_data[k - 1] = k;
  }

  for (k = 1; k <= x_size[0] - 1; k += 2) {
    if (x_data[k - 1] <= x_data[k]) {
    } else {
      idx_data[k - 1] = k + 1;
      idx_data[k] = k;
    }
  }

  k = x_size[0];
  for (i = 0; i < k; i++) {
    idx0_data[i] = 1;
  }

  i = 2;
  while (i < x_size[0]) {
    i2 = i << 1;
    j = 1;
    for (pEnd = 1 + i; pEnd < x_size[0] + 1; pEnd = qEnd + i) {
      p = j;
      q = pEnd;
      qEnd = j + i2;
      if (qEnd > x_size[0] + 1) {
        qEnd = x_size[0] + 1;
      }

      k = 0;
      kEnd = qEnd - j;
      while (k + 1 <= kEnd) {
        if (x_data[idx_data[p - 1] - 1] <= x_data[idx_data[q - 1] - 1]) {
          idx0_data[k] = idx_data[p - 1];
          p++;
          if (p == pEnd) {
            while (q < qEnd) {
              k++;
              idx0_data[k] = idx_data[q - 1];
              q++;
            }
          }
        } else {
          idx0_data[k] = idx_data[q - 1];
          q++;
          if (q == qEnd) {
            while (p < pEnd) {
              k++;
              idx0_data[k] = idx_data[p - 1];
              p++;
            }
          }
        }

        k++;
      }

      for (k = 0; k + 1 <= kEnd; k++) {
        idx_data[(j + k) - 1] = idx0_data[k];
      }

      j = qEnd;
    }

    i = i2;
  }
}

void b_eml_sort(const double x_data[900], const int x_size[2], double y_data[900],
                int y_size[2])
{
  double vwork_data[30];
  int vwork_size[1];
  int i2;
  int i;
  int i1;
  int ix;
  int k;
  int iidx_size[1];
  int iidx_data[30];
  vwork_size[0] = (signed char)x_size[0];
  for (i2 = 0; i2 < 2; i2++) {
    y_size[i2] = x_size[i2];
  }

  i2 = 0;
  for (i = 1; i <= x_size[1]; i++) {
    i1 = i2;
    i2 += x_size[0];
    ix = i1;
    for (k = 0; k < x_size[0]; k++) {
      vwork_data[k] = x_data[ix];
      ix++;
    }

    eml_sort_idx(vwork_data, vwork_size, iidx_data, iidx_size);
    ix = i1;
    for (k = 0; k < x_size[0]; k++) {
      y_data[ix] = vwork_data[iidx_data[k] - 1];
      ix++;
    }
  }
}

void eml_sort(const double x_data[900], const int x_size[2], double y_data[900],
              int y_size[2])
{
  double vwork_data[30];
  int vwork_size[1];
  int i2;
  int i;
  int i1;
  int ix;
  int k;
  int iidx_size[1];
  int iidx_data[30];
  vwork_size[0] = (signed char)x_size[0];
  for (i2 = 0; i2 < 2; i2++) {
    y_size[i2] = x_size[i2];
  }

  i2 = 1;
  for (i = 1; i <= x_size[1]; i++) {
    i1 = i2 - 1;
    i2 += x_size[0];
    ix = i1;
    for (k = 0; k < x_size[0]; k++) {
      vwork_data[k] = x_data[ix];
      ix++;
    }

    eml_sort_idx(vwork_data, vwork_size, iidx_data, iidx_size);
    ix = i1;
    for (k = 0; k < x_size[0]; k++) {
      y_data[ix] = vwork_data[iidx_data[k] - 1];
      ix++;
    }
  }
}

/* End of code generation (eml_sort.c) */
