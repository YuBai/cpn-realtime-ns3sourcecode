/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "stop-traffic-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to StopTrafficform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class StopTrafficTag : public Tag
{
public:
	StopTrafficTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetStopTraffic (void) const;
  void SetStopTraffic (double StopTraffic) ;

private:
  double m_StopTraffic; //!< The time stored in the tag
};

StopTrafficTag::StopTrafficTag ()
  : m_StopTraffic (0.0)
{
}

TypeId
StopTrafficTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::StopTrafficTag")
    .SetParent<Tag> ()
    .SetGroupName("StopTrafficTag")
    .AddConstructor<StopTrafficTag> ()
    .AddAttribute ("LastStopTraffic",
                   "Last StopTraffic Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&StopTrafficTag::GetStopTraffic),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
StopTrafficTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
StopTrafficTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
StopTrafficTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_StopTraffic);
}
void
StopTrafficTag::Deserialize (TagBuffer i)
{
  m_StopTraffic = i.ReadDouble ();
}
void
StopTrafficTag::Print (std::ostream &os) const
{
  os << "m_StopTraffic=" << m_StopTraffic;
}
double
StopTrafficTag::GetStopTraffic (void) const
{
  return m_StopTraffic;
}

void
StopTrafficTag::SetStopTraffic (double StopTraffic)
{
	m_StopTraffic=StopTraffic;
}

StopTrafficTagger::StopTrafficTagger ()
{
	this->lastStopTraffic=0.0;
}

void
StopTrafficTagger::WriteStopTrafficValue (Ptr<const Packet> packet, double tagValue)
{
  StopTrafficTag tag;
  tag.SetStopTraffic(tagValue);
  packet->AddByteTag (tag);
}
int
StopTrafficTagger::RecordStopTrafficValue (Ptr<const Packet> packet)
{
  StopTrafficTag tag;
  lastStopTraffic=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastStopTraffic=tag.GetStopTraffic ();
  return (int)lastStopTraffic;
}

double
StopTrafficTagger::GetLastStopTraffic (void) const
{
  return lastStopTraffic;
}

} // namespace ns3
