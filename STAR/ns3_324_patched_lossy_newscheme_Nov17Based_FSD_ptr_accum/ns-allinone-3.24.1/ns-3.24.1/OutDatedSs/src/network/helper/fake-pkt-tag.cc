/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-pkt-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to FakePktform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakePktTag : public Tag
{
public:
	FakePktTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetFakePkt (void) const;
  void SetFakePkt (double FakePkt) ;

private:
  double m_FakePkt; //!< The time stored in the tag
};

FakePktTag::FakePktTag ()
  : m_FakePkt (0.0)
{
}

TypeId
FakePktTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakePktTag")
    .SetParent<Tag> ()
    .SetGroupName("fakePktTag")
    .AddConstructor<FakePktTag> ()
    .AddAttribute ("LastFakePkt",
                   "Last FakePkt Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&FakePktTag::GetFakePkt),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
FakePktTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakePktTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
FakePktTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_FakePkt);
}
void
FakePktTag::Deserialize (TagBuffer i)
{
  m_FakePkt = i.ReadDouble ();
}
void
FakePktTag::Print (std::ostream &os) const
{
  os << "m_FakePkt=" << m_FakePkt;
}
double
FakePktTag::GetFakePkt (void) const
{
  return m_FakePkt;
}

void
FakePktTag::SetFakePkt (double FakePkt)
{
	m_FakePkt=FakePkt;
}

FakePktTagger::FakePktTagger ()
{
	this->lastFakePkt=0.0;
}

void
FakePktTagger::WriteFakePktValue (Ptr<const Packet> packet, double tagValue)
{
  FakePktTag tag;
  tag.SetFakePkt(tagValue);
  packet->AddByteTag (tag);
}
int
FakePktTagger::RecordFakePktValue (Ptr<const Packet> packet)
{
  FakePktTag tag;
  lastFakePkt=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastFakePkt=tag.GetFakePkt ();
  return (int)lastFakePkt;
}

double
FakePktTagger::GetLastFakePkt (void) const
{
  return lastFakePkt;
}

} // namespace ns3
