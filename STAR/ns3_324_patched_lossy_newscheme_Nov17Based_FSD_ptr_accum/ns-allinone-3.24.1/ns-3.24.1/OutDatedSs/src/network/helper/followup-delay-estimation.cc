/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "followup-delay-estimation.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FollowupDelayEstimationTimestampTag : public Tag
{
public:
	FollowupDelayEstimationTimestampTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  Time GetTxTime (void) const;
private:
  uint64_t m_creationTime; //!< The time stored in the tag
};

FollowupDelayEstimationTimestampTag::FollowupDelayEstimationTimestampTag ()
  : m_creationTime (Simulator::Now ().GetTimeStep ())
{
}

TypeId
FollowupDelayEstimationTimestampTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FollowupDelayEstimationTimestampTag")
    .SetParent<Tag> ()
    .SetGroupName("followupDelayEstimation")
    .AddConstructor<FollowupDelayEstimationTimestampTag> ()
    .AddAttribute ("CreationTime",
                   "The time at which the timestamp was created",
                   StringValue ("0.0s"),
                   MakeTimeAccessor (&FollowupDelayEstimationTimestampTag::GetTxTime),
                   MakeTimeChecker ())
  ;
  return tid;
}
TypeId
FollowupDelayEstimationTimestampTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FollowupDelayEstimationTimestampTag::GetSerializedSize (void) const
{
  return 8;
}
void
FollowupDelayEstimationTimestampTag::Serialize (TagBuffer i) const
{
  i.WriteU64 (m_creationTime);
}
void
FollowupDelayEstimationTimestampTag::Deserialize (TagBuffer i)
{
  m_creationTime = i.ReadU64 ();
}
void
FollowupDelayEstimationTimestampTag::Print (std::ostream &os) const
{
  os << "CreationTime=" << m_creationTime;
}
Time
FollowupDelayEstimationTimestampTag::GetTxTime (void) const
{
  return TimeStep (m_creationTime);
}

FollowupDelayEstimation::FollowupDelayEstimation ()
  : m_previousRx (Simulator::Now ()),
    m_previousRxTx (Simulator::Now ()),
    m_delay (Seconds (0.0))
{
}
void
FollowupDelayEstimation::PrepareEqTime (Ptr<const Packet> packet)
{
  FollowupDelayEstimationTimestampTag tag;
  packet->AddByteTag (tag);
}
void
FollowupDelayEstimation::RecordDqTime (Ptr<const Packet> packet)
{
  FollowupDelayEstimationTimestampTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
	  //std::cerr << "FollowupDelayEstimation::RecordRX: Not Found.\n";
      return;
    }
  tag.GetTxTime ();

  Time delta = (Simulator::Now () - m_previousRx) - (tag.GetTxTime () - m_previousRxTx);
  m_previousRx = Simulator::Now ();
  m_previousRxTx = tag.GetTxTime ();
  m_delay = Simulator::Now () - tag.GetTxTime ();
}

Time 
FollowupDelayEstimation::GetLastQDelay (void) const
{
  return m_delay;
}

} // namespace ns3
