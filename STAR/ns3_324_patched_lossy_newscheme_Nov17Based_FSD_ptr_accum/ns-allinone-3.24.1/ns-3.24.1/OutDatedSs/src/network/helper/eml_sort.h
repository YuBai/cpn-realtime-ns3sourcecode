/*
 * eml_sort.h
 *
 * Code generation for function 'eml_sort'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

#ifndef __EML_SORT_H__
#define __EML_SORT_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "FSDFunctionReference_types.h"

/* Function Declarations */
extern void b_eml_sort(const double x_data[900], const int x_size[2], double y_data[900], int y_size[2]);
extern void eml_sort(const double x_data[900], const int x_size[2], double y_data[900], int y_size[2]);
#endif
/* End of code generation (eml_sort.h) */
