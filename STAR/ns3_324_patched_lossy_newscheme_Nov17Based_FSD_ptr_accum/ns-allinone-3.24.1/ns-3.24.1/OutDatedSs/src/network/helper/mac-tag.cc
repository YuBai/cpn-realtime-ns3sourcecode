/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"


namespace ns3 {

/**
 * Tag to Macform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */

MacTagger::MacTagger() :
m_firmt(),
m_secmt(),
m_thimt(),
m_foumt(),
m_fifmt(),
m_sixmt()
{
	m_firmt.SetLastFirstMac((uint8_t)0);
	m_secmt.SetLastSecondMac((uint8_t)0);
	m_thimt.SetLastThirdMac((uint8_t)0);
	m_foumt.SetLastFourthMac((uint8_t)0);
	m_fifmt.SetLastFifthMac((uint8_t)0);
	m_sixmt.SetLastSixthMac((uint8_t)0);

}

void
MacTagger::WriteMacValue (Ptr<const Packet> packet)
{
	m_firmt.WriteFirstMacValue(packet);
	m_secmt.WriteSecondMacValue(packet);
	m_thimt.WriteThirdMacValue(packet);
	m_foumt.WriteFourthMacValue(packet);
	m_fifmt.WriteFifthMacValue(packet);
	m_sixmt.WriteSixthMacValue(packet);

}
void
MacTagger::RecordMacValue (Ptr<const Packet> packet)
{

	lastMacArray[0]=0;
	lastMacArray[1]=0;
	lastMacArray[2]=0;
	lastMacArray[3]=0;
	lastMacArray[4]=0;
	lastMacArray[5]=0;

	m_firmt.RecordFirstMacValue(packet);
	m_secmt.RecordSecondMacValue(packet);
	m_thimt.RecordThirdMacValue(packet);
	m_foumt.RecordFourthMacValue(packet);
	m_fifmt.RecordFifthMacValue(packet);
	m_sixmt.RecordSixthMacValue(packet);

	lastMacArray[0]=m_firmt.GetLastFirstMac();
	lastMacArray[1]=m_secmt.GetLastSecondMac();
	lastMacArray[2]=m_thimt.GetLastThirdMac();
	lastMacArray[3]=m_foumt.GetLastFourthMac();
	lastMacArray[4]=m_fifmt.GetLastFifthMac();
	lastMacArray[5]=m_sixmt.GetLastSixthMac();

	lastMac.CopyFrom(lastMacArray);

}

Mac48Address
MacTagger::GetLastMac (void) const
{
  return lastMac;
}

void
MacTagger::SetLastMac (Mac48Address Mac)
{
	Mac.CopyTo(lastMacArray);
	m_firmt.SetLastFirstMac(lastMacArray[0]);
	m_secmt.SetLastSecondMac(lastMacArray[1]);
	m_thimt.SetLastThirdMac(lastMacArray[2]);
	m_foumt.SetLastFourthMac(lastMacArray[3]);
	m_fifmt.SetLastFifthMac(lastMacArray[4]);
	m_sixmt.SetLastSixthMac(lastMacArray[5]);
	lastMac=Mac;

}

void
MacTagger::ResetMac()
{
	lastMacArray[0]=0;
	lastMacArray[1]=0;
	lastMacArray[2]=0;
	lastMacArray[3]=0;
	lastMacArray[4]=0;
	lastMacArray[5]=0;

	lastMac.CopyFrom(lastMacArray);
}



} // namespace ns3
