#!/bin/bash

while read line; do


	cat $line | grep ",NoDom1," | sed -e 's/ns//g' -e 's/NoDom1,//g' > NoDom1_$line
	cat $line | grep ",Dom1," | sed -e 's/ns//g' -e 's/Dom1,//g' > Dom1_$line
	cat $line | grep ",NoDom," | sed -e 's/ns//g' -e 's/NoDom,//g' > NoDom_$line
	cat $line | grep ",Dom," | sed -e 's/ns//g' -e 's/Dom,//g' > Dom_$line


done < $1
