#include "ns3/global-ap-table.h"

#define DEFAULT_GAT_SIZE 2

namespace ns3{


globalAPTable::globalAPTable()
{
	m_gat.resize(DEFAULT_GAT_SIZE);
	initAPTable(DEFAULT_GAT_SIZE);
}

globalAPTable::globalAPTable(int size)
{
	m_gat.resize(size);
	initAPTable(size);
}

void
globalAPTable::reSize(int size)
{
	m_gat.resize(size);
	initAPTable(size);
}

int
globalAPTable::getSize(void)
{
	return m_gat.size();
}

void
globalAPTable::setMacAddr(int _apID, Mac48Address _macAddr)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::setMacAddr(): _apID out of range!");
	}
	m_gat[_apID].apAddr = _macAddr;
}

int
globalAPTable::getAPIDByMacAddr(Mac48Address _macAddr)
{
	std::vector<apTable>::size_type i;
	for (i = 0; i != m_gat.size(); i++)
	{
		if(m_gat.at(i).apAddr == _macAddr)
		{
			return i;
		}
	}
	return -1;
}

Mac48Address
globalAPTable::getMacAddrByAPID(int _apID)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::getMacAddrByAPID: _apID out of range!");
	}
	return m_gat[_apID].apAddr;
}

void
globalAPTable::setChannelNumber(int _apID, int _channelNumber)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::setChannelNumber(): _apID out of range!");
	}
	m_gat[_apID].channelNumber=_channelNumber;
}

int
globalAPTable::getChannelNumberByAPID(int _apID)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::getChannelNumberByAPID(): _apID out of range!");
	}
	return m_gat[_apID].channelNumber;
}

void
globalAPTable::setSsid(int _apID, Ssid _ssid)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::setSsid(): _apID out of range!");
	}
	m_gat[_apID].ssid=_ssid;
}

Ssid
globalAPTable::getSsidByAPID(int _apID)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::getSsidByAPID(): _apID out of range!");
	}
	return m_gat[_apID].ssid;
}

void
globalAPTable::setRoutingPlan(int _apID, int _routingPlan)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::setRoutingPlan(): _apID out of range!");
	}
	m_gat[_apID].routingPlan=_routingPlan;
}

int
globalAPTable::getRoutingPlanByAPID(int _apID)
{
	if((uint32_t)_apID > m_gat.size())
	{
		NS_FATAL_ERROR("globalAPTable::getRoutingPlanByAPID(): _apID out of range!");
	}
	return m_gat[_apID].routingPlan;
}

int
globalAPTable::getNext(int _apID)
{
	//currently this returns next apID in rorating manner
	return (_apID+1)%(m_gat.size());
}

void
globalAPTable::initAPTable(int size)
{
	for(int i=0;i<size;i++)
	{
		m_gat[i].apAddr="ff:ff:ff:ff:ff:ff";
	}
}

}

