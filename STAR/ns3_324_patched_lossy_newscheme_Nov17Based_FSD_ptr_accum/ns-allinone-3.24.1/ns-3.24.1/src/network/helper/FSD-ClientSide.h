/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef FSD_CLIENTSIDE_H_NEW
#define FSD_CLIENTSIDE_H_NEW

#define CHECK_MODE 0 //0: Check FSD Output 1: Check Avg Throughput
#define NumBitsPerChar	8U

#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/udp-delay-jitter-estimation.h"
#include "ns3/probe-seq-tag.h"
#include "ns3/delay-samples-tag.h"
#include "ns3/mac-tag.h"
#include "rtwtypes.h"
#include <limits.h>
#include "FSD-ClientSide.h"

namespace ns3 {


/* Type Definitions */

struct emxArray_int32_T_30
{
    int data[30];
    int size[1];
};

typedef struct emxArray_int32_T_30 emxArray_int32_T_30;

struct emxArray__common
{
  void *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

typedef struct emxArray__common emxArray__common;

struct emxArray_int32_T
{
  int *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};
typedef struct emxArray_int32_T emxArray_int32_T;


struct emxArray_real_T
{
    double *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};

typedef struct emxArray_real_T emxArray_real_T;

struct emxArray_real_T_1x30
{
    double data[30];
    int size[2];
};

typedef struct emxArray_real_T_1x30 emxArray_real_T_1x30;

struct emxArray_real_T_30
{
    double data[30];
    int size[1];
};

typedef struct emxArray_real_T_30 emxArray_real_T_30;

struct emxArray_real_T_30x30
{
    double data[900];
    int size[2];
};

typedef struct emxArray_real_T_30x30 emxArray_real_T_30x30;

typedef struct {
  struct {
    uint32_T wordH;
    uint32_T wordL;
  } words;
} BigEndianIEEEDouble;

typedef struct {
  struct {
    uint32_T wordL;
    uint32_T wordH;
  } words;
} LittleEndianIEEEDouble;

typedef struct {
  union {
    real32_T wordLreal;
    uint32_T wordLuint;
  } wordL;
} IEEESingle;
   class FSDDelayCalculator
   {


	   /*=======================================================================*
	    * Fixed width word size data types:                                     *
	    *   int8_T, int16_T, int32_T     - signed 8, 16, or 32 bit integers     *
	    *   uint8_T, uint16_T, uint32_T  - unsigned 8, 16, or 32 bit integers   *
	    *   real32_T, real64_T           - 32 and 64 bit floating point numbers *
	    *=======================================================================*/

	   typedef signed char int8_T;
	   typedef unsigned char uint8_T;
	   typedef short int16_T;
	   typedef unsigned short uint16_T;
	   typedef int int32_T;
	   typedef unsigned int uint32_T;
	   typedef long int64_T;
	   typedef unsigned long uint64_T;
	   typedef float real32_T;
	   typedef double real64_T;

	   /*===========================================================================*
	    * Generic type definitions: real_T, time_T, boolean_T, int_T, uint_T,       *
	    *                           ulong_T, ulonglong_T, char_T and byte_T.        *
	    *===========================================================================*/

	   typedef double real_T;
	   typedef double time_T;
	   typedef unsigned char boolean_T;
	   typedef int int_T;
	   typedef unsigned int uint_T;
	   typedef unsigned long ulong_T;
	   typedef unsigned long long ulonglong_T;
	   typedef char char_T;
	   typedef char_T byte_T;

	   /*===========================================================================*
	    * Complex number type definitions                                           *
	    *===========================================================================*/
	   #define CREAL_T
	      typedef struct {
	         real32_T re;
	         real32_T im;
	      } creal32_T;

	      typedef struct {
	         real64_T re;
	         real64_T im;
	      } creal64_T;

	      typedef struct {
	         real_T re;
	         real_T im;
	      } creal_T;

	      typedef struct {
	         int8_T re;
	         int8_T im;
	      } cint8_T;

	      typedef struct {
	         uint8_T re;
	         uint8_T im;
	      } cuint8_T;

	      typedef struct {
	         int16_T re;
	         int16_T im;
	      } cint16_T;

	      typedef struct {
	         uint16_T re;
	         uint16_T im;
	      } cuint16_T;

	      typedef struct {
	         int32_T re;
	         int32_T im;
	      } cint32_T;

	      typedef struct {
	         uint32_T re;
	         uint32_T im;
	      } cuint32_T;

	      typedef struct {
	         int64_T re;
	         int64_T im;
	      } cint64_T;

	      typedef struct {
	         uint64_T re;
	         uint64_T im;
	      } cuint64_T;


	   /*=======================================================================*
	    * Min and Max:                                                          *
	    *   int8_T, int16_T, int32_T     - signed 8, 16, or 32 bit integers     *
	    *   uint8_T, uint16_T, uint32_T  - unsigned 8, 16, or 32 bit integers   *
	    *=======================================================================*/

	   #define MAX_int8_T  	((int8_T)(127))
	   #define MIN_int8_T  	((int8_T)(-128))
	   #define MAX_uint8_T 	((uint8_T)(255))
	   #define MIN_uint8_T 	((uint8_T)(0))
	   #define MAX_int16_T 	((int16_T)(32767))
	   #define MIN_int16_T 	((int16_T)(-32768))
	   #define MAX_uint16_T	((uint16_T)(65535))
	   #define MIN_uint16_T	((uint16_T)(0))
	   #define MAX_int32_T 	((int32_T)(2147483647))
	   #define MIN_int32_T 	((int32_T)(-2147483647-1))
	   #define MAX_uint32_T	((uint32_T)(0xFFFFFFFFU))
	   #define MIN_uint32_T	((uint32_T)(0))
	   #define MAX_int64_T	((int64_T)(9223372036854775807L))
	   #define MIN_int64_T	((int64_T)(-9223372036854775807L-1L))
	   #define MAX_uint64_T	((uint64_T)(0xFFFFFFFFFFFFFFFFUL))
	   #define MIN_uint64_T	((uint64_T)(0UL))

	   /* Logical type definitions */
	   #if !defined(__cplusplus) && !defined(__true_false_are_keywords)
	   #  ifndef false
	   #   define false (0U)
	   #  endif
	   #  ifndef true
	   #   define true (1U)
	   #  endif
	   #endif

	   /*
	    * MATLAB for code generation assumes the code is compiled on a target using a 2's compliment representation
	    * for signed integer values.
	    */
	   #if ((SCHAR_MIN + 1) != -SCHAR_MAX)
	   #error "This code must be compiled using a 2's complement representation for signed integer values"
	   #endif

	   /*
	    * Maximum length of a MATLAB identifier (function/variable)
	    * including the null-termination character. Referenced by
	    * rt_logging.c and rt_matrx.c.
	    */
	   #define TMW_NAME_LENGTH_MAX	64


   public:
	   FSDDelayCalculator ();

	     /**
	      * \param packet the packet to send over a wire
	      *
	      * This method should be invoked once on each packet to
	      * record within the packet the tx time which is used upon
	      * packet reception to calculate the delay and jitter. The
	      * tx time is stored in the packet as an ns3::Tag which means
	      * that it does not use any network resources and is not
	      * taken into account in transmission delay calculations.
	      */
	    double CalculateFSDRelationship(double* p1, double* p2, int* _size, double _delta_x, double _alpha_F, double N, double* Q, int* _Qsize);


   private:
	   void b_eml_sort(const emxArray_real_T *x, emxArray_real_T *y);
	   void b_emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);
           void c_emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);
	   void eml_sort(const emxArray_real_T *x, emxArray_real_T *y);
	   void eml_sort_idx(const emxArray_real_T *x, emxArray_int32_T *idx);
	   void power(const emxArray_real_T *a, emxArray_real_T *y);



	   double b_sum(const emxArray_real_T *x);
	   void sum(const emxArray_real_T *x, emxArray_real_T *y);
	   void var(const emxArray_real_T *x, emxArray_real_T *y);
	   double rt_powd_snf(double u0, double u1);
	   void emxEnsureCapacity(emxArray__common *emxArray, int oldNumel, int elementSize);
	   void emxFree_int32_T(emxArray_int32_T **pEmxArray);
	   void emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions);
	   void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);
	   void emxFree_real_T(emxArray_real_T **pEmxArray);


	   double FSDFunctionWTFFork(const emxArray_real_T *data1, const emxArray_real_T *data2, double data1_len, double data2_len, double delta_x, double alpha_F, double N, const emxArray_real_T *Q);
	   void FSDFunctionWTFFork_initialize(void);
	   void FSDFunctionWTFFork_terminate(void);
	   emxArray_real_T *emxCreateND_real_T(int numDimensions, int *size);
	   emxArray_real_T *emxCreateWrapperND_real_T(double *data, int numDimensions, int *size);
	   emxArray_real_T *emxCreateWrapper_real_T(double *data, int rows, int cols);
	   emxArray_real_T *emxCreate_real_T(int rows, int cols);
	   void emxDestroyArray_real_T(emxArray_real_T *emxArray);
	   real_T rtGetInf(void);
	   real32_T rtGetInfF(void);
	   real_T rtGetMinusInf(void);
	   real32_T rtGetMinusInfF(void);
	   real_T rtGetNaN(void);
	   real32_T rtGetNaNF(void);
	   void rt_InitInfAndNaN(size_t realSize);
	   boolean_T rtIsInf(real_T value);
	   boolean_T rtIsInfF(real32_T value);
	   boolean_T rtIsNaN(real_T value);
	   boolean_T rtIsNaNF(real32_T value);
           void rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z);
	   //Newly Added for Avg Checkout
	   int AvgFunction(double* p1, double* p2, int Len);

	   real_T rtInf;
	   real_T rtMinusInf;
	   real_T rtNaN;
	   real32_T rtInfF;
	   real32_T rtMinusInfF;
	   real32_T rtNaNF;

	   emxArray_real_T* m_delays_p1;
    //UdpDelayJitterEstimation m_uje;
    //ProbeSeqTagger m_pst;
    //bool fullOrNot;
	   emxArray_real_T* m_delays_p2;
	   emxArray_real_T* quantiles;
    DelaySamplesTagger m_dst;
    MacTagger m_mt; //Determine which one is which one
    int defaultSize[1]; //default size of sample set
    int debuggingRetVal;

};

} // namespace ns3

#endif /* DELAY_JITTER_ESTIMATION_H */
