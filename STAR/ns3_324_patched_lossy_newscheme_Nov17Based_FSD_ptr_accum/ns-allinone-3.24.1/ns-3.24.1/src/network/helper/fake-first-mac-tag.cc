/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-first-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeFirstMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeFirstMacTag : public Tag
{
public:
	FakeFirstMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeFirstMac (void) const;
  void SetFakeFirstMac (uint8_t FakeFirstMac) ;

private:
  uint8_t m_FakeFirstMac; //!< The time stored in the tag
};

FakeFirstMacTag::FakeFirstMacTag ()
  : m_FakeFirstMac (0)
{
}

TypeId
FakeFirstMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeFirstMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeFirstMacTag")
    .AddConstructor<FakeFirstMacTag> ()
    .AddAttribute ("LastFakeFirstMac",
                   "Last FakeFirstMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeFirstMacTag::GetFakeFirstMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeFirstMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeFirstMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeFirstMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeFirstMac);
}
void
FakeFirstMacTag::Deserialize (TagBuffer i)
{
  m_FakeFirstMac = i.ReadU8 ();
}
void
FakeFirstMacTag::Print (std::ostream &os) const
{
  os << "m_FakeFirstMac=" << m_FakeFirstMac;
}
uint8_t
FakeFirstMacTag::GetFakeFirstMac (void) const
{
  return m_FakeFirstMac;
}

void
FakeFirstMacTag::SetFakeFirstMac (uint8_t FakeFirstMac)
{
	m_FakeFirstMac=FakeFirstMac;
}

FakeFirstMacTagger::FakeFirstMacTagger ()
{
	this->lastFakeFirstMac=0.0;
}

void
FakeFirstMacTagger::WriteFakeFirstMacValue (Ptr<const Packet> packet)
{
  FakeFirstMacTag tag;
  tag.SetFakeFirstMac(this->lastFakeFirstMac);
  packet->AddByteTag (tag);
}
void
FakeFirstMacTagger::RecordFakeFirstMacValue (Ptr<const Packet> packet)
{
  FakeFirstMacTag tag;
  SetLastFakeFirstMac (0);
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeFirstMac=tag.GetFakeFirstMac ();

}

uint8_t
FakeFirstMacTagger::GetLastFakeFirstMac (void) const
{
  return lastFakeFirstMac;
}

void
FakeFirstMacTagger::SetLastFakeFirstMac (uint8_t FakeFirstMac)
{
	lastFakeFirstMac=FakeFirstMac;
}


} // namespace ns3
