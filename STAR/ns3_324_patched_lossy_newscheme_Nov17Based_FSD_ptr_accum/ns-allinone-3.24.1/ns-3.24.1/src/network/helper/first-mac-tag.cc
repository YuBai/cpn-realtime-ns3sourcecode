/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "first-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FirstMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FirstMacTag : public Tag
{
public:
	FirstMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFirstMac (void) const;
  void SetFirstMac (uint8_t FirstMac) ;

private:
  uint8_t m_FirstMac; //!< The time stored in the tag
};

FirstMacTag::FirstMacTag ()
  : m_FirstMac (0)
{
}

TypeId
FirstMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FirstMacTag")
    .SetParent<Tag> ()
    .SetGroupName("firstMacTag")
    .AddConstructor<FirstMacTag> ()
    .AddAttribute ("LastFirstMac",
                   "Last FirstMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FirstMacTag::GetFirstMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FirstMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FirstMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FirstMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FirstMac);
}
void
FirstMacTag::Deserialize (TagBuffer i)
{
  m_FirstMac = i.ReadU8 ();
}
void
FirstMacTag::Print (std::ostream &os) const
{
  os << "m_FirstMac=" << m_FirstMac;
}
uint8_t
FirstMacTag::GetFirstMac (void) const
{
  return m_FirstMac;
}

void
FirstMacTag::SetFirstMac (uint8_t FirstMac)
{
	m_FirstMac=FirstMac;
}

FirstMacTagger::FirstMacTagger ()
{
	this->lastFirstMac=0.0;
}

void
FirstMacTagger::WriteFirstMacValue (Ptr<const Packet> packet)
{
  FirstMacTag tag;
  tag.SetFirstMac(this->lastFirstMac);
  packet->AddByteTag (tag);
}
void
FirstMacTagger::RecordFirstMacValue (Ptr<const Packet> packet)
{
  FirstMacTag tag;
  SetLastFirstMac (0);
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFirstMac=tag.GetFirstMac ();

}

uint8_t
FirstMacTagger::GetLastFirstMac (void) const
{
  return lastFirstMac;
}

void
FirstMacTagger::SetLastFirstMac (uint8_t FirstMac)
{
	lastFirstMac=FirstMac;
}


} // namespace ns3
