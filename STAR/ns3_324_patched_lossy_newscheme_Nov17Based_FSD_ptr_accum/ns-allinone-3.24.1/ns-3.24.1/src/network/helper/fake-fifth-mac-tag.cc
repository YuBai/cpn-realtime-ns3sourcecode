/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-fifth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeFifthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeFifthMacTag : public Tag
{
public:
	FakeFifthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeFifthMac (void) const;
  void SetFakeFifthMac (uint8_t FakeFifthMac) ;

private:
  uint8_t m_FakeFifthMac; //!< The time stored in the tag
};

FakeFifthMacTag::FakeFifthMacTag ()
  : m_FakeFifthMac (0)
{
}

TypeId
FakeFifthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeFifthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeFifthMacTag")
    .AddConstructor<FakeFifthMacTag> ()
    .AddAttribute ("LastFakeFifthMac",
                   "Last FakeFifthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeFifthMacTag::GetFakeFifthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeFifthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeFifthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeFifthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeFifthMac);
}
void
FakeFifthMacTag::Deserialize (TagBuffer i)
{
  m_FakeFifthMac = i.ReadU8 ();
}
void
FakeFifthMacTag::Print (std::ostream &os) const
{
  os << "m_FakeFifthMac=" << m_FakeFifthMac;
}
uint8_t
FakeFifthMacTag::GetFakeFifthMac (void) const
{
  return m_FakeFifthMac;
}

void
FakeFifthMacTag::SetFakeFifthMac (uint8_t FakeFifthMac)
{
	m_FakeFifthMac=FakeFifthMac;
}

FakeFifthMacTagger::FakeFifthMacTagger ()
{
	this->lastFakeFifthMac=0.0;
}

void
FakeFifthMacTagger::WriteFakeFifthMacValue (Ptr<const Packet> packet)
{
  FakeFifthMacTag tag;
  tag.SetFakeFifthMac(this->lastFakeFifthMac);
  packet->AddByteTag (tag);
}
void
FakeFifthMacTagger::RecordFakeFifthMacValue (Ptr<const Packet> packet)
{
  FakeFifthMacTag tag;
  SetLastFakeFifthMac(0);

  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeFifthMac=tag.GetFakeFifthMac ();

}

uint8_t
FakeFifthMacTagger::GetLastFakeFifthMac (void) const
{
  return lastFakeFifthMac;
}

void
FakeFifthMacTagger::SetLastFakeFifthMac (uint8_t FakeFifthMac)
{
	lastFakeFifthMac=FakeFifthMac;
}


} // namespace ns3
