/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "alter-path-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to AlterPathform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class AlterPathTag : public Tag
{
public:
	AlterPathTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetAlterPath (void) const;
  void SetAlterPath (double AlterPath) ;

private:
  double m_AlterPath; //!< The time stored in the tag
};

AlterPathTag::AlterPathTag ()
  : m_AlterPath (0.0)
{
}

TypeId
AlterPathTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::AlterPathTag")
    .SetParent<Tag> ()
    .SetGroupName("AlterPathTag")
    .AddConstructor<AlterPathTag> ()
    .AddAttribute ("LastAlterPath",
                   "Last AlterPath Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&AlterPathTag::GetAlterPath),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
AlterPathTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
AlterPathTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
AlterPathTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_AlterPath);
}
void
AlterPathTag::Deserialize (TagBuffer i)
{
  m_AlterPath = i.ReadDouble ();
}
void
AlterPathTag::Print (std::ostream &os) const
{
  os << "m_AlterPath=" << m_AlterPath;
}
double
AlterPathTag::GetAlterPath (void) const
{
  return m_AlterPath;
}

void
AlterPathTag::SetAlterPath (double AlterPath)
{
	m_AlterPath=AlterPath;
}

AlterPathTagger::AlterPathTagger ()
{
	this->lastAlterPath=0.0;
}

void
AlterPathTagger::WriteAlterPathValue (Ptr<const Packet> packet, double tagValue)
{
  AlterPathTag tag;
  tag.SetAlterPath(tagValue);
  packet->AddByteTag (tag);
}
int
AlterPathTagger::RecordAlterPathValue (Ptr<const Packet> packet)
{
  AlterPathTag tag;
  lastAlterPath=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastAlterPath=tag.GetAlterPath ();
  return (int)lastAlterPath;
}

double
AlterPathTagger::GetLastAlterPath (void) const
{
  return lastAlterPath;
}

} // namespace ns3
