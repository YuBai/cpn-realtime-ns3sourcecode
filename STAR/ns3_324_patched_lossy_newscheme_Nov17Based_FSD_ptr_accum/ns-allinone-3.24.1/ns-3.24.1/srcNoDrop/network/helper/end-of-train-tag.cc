/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "end-of-train-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to EndOfTrainform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class EndOfTrainTag : public Tag
{
public:
	EndOfTrainTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetEndOfTrain (void) const;
  void SetEndOfTrain (double EndOfTrain) ;

private:
  double m_EndOfTrain; //!< The time stored in the tag
};

EndOfTrainTag::EndOfTrainTag ()
  : m_EndOfTrain (0.0)
{
}

TypeId
EndOfTrainTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::EndOfTrainTag")
    .SetParent<Tag> ()
    .SetGroupName("EndOfTrainTag")
    .AddConstructor<EndOfTrainTag> ()
    .AddAttribute ("LastEndOfTrain",
                   "Last EndOfTrain Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&EndOfTrainTag::GetEndOfTrain),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
EndOfTrainTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
EndOfTrainTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
EndOfTrainTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_EndOfTrain);
}
void
EndOfTrainTag::Deserialize (TagBuffer i)
{
  m_EndOfTrain = i.ReadDouble ();
}
void
EndOfTrainTag::Print (std::ostream &os) const
{
  os << "m_EndOfTrain=" << m_EndOfTrain;
}
double
EndOfTrainTag::GetEndOfTrain (void) const
{
  return m_EndOfTrain;
}

void
EndOfTrainTag::SetEndOfTrain (double EndOfTrain)
{
	m_EndOfTrain=EndOfTrain;
}

EndOfTrainTagger::EndOfTrainTagger ()
{
	this->lastEndOfTrain=0.0;
}

void
EndOfTrainTagger::WriteEndOfTrainValue (Ptr<const Packet> packet, double tagValue)
{
  EndOfTrainTag tag;
  tag.SetEndOfTrain(tagValue);
  packet->AddByteTag (tag);
}
int
EndOfTrainTagger::RecordEndOfTrainValue (Ptr<const Packet> packet)
{
  EndOfTrainTag tag;
  lastEndOfTrain=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastEndOfTrain=tag.GetEndOfTrain ();
  return (int)lastEndOfTrain;
}

double
EndOfTrainTagger::GetLastEndOfTrain (void) const
{
  return lastEndOfTrain;
}

} // namespace ns3
