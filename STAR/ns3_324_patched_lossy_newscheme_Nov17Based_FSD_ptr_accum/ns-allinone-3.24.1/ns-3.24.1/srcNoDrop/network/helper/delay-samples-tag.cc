/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "delay-samples-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to DelaySamplesform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class DelaySamplesTag : public Tag
{
public:
	DelaySamplesTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  void CopyTo (double* _buffer);
  void CopyFrom (double* _buffer) ;

private:
  double m_DelaySamples[MAX_PROBE_TRAIN_LENGTH]; //!< The time stored in the tag
};

DelaySamplesTag::DelaySamplesTag ()
{
	memset(m_DelaySamples, 0, sizeof(m_DelaySamples));
}

TypeId
DelaySamplesTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::DelaySamplesTag")
    .SetParent<Tag> ()
    .SetGroupName("Network")
    .AddConstructor<DelaySamplesTag> ()
;
  return tid;
}
TypeId
DelaySamplesTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
DelaySamplesTag::GetSerializedSize (void) const
{
  //std::clog << "delay sample tag size is" << sizeof(double)*MAX_PROBE_TRAIN_LENGTH << "\n";
  return sizeof(double)*(MAX_PROBE_TRAIN_LENGTH);
}
void
DelaySamplesTag::Serialize (TagBuffer i) const
{
  int j=0;
  for(j=0;j<MAX_PROBE_TRAIN_LENGTH;j++)
  {
	  i.WriteDouble(m_DelaySamples[j]);
  }

}
void
DelaySamplesTag::Deserialize (TagBuffer i)
{
	int j=0;
	for(j=0;j<MAX_PROBE_TRAIN_LENGTH;j++)
	{
		m_DelaySamples[j]=i.ReadDouble();
	}

}
void
DelaySamplesTag::Print (std::ostream &os) const
{
}

void
DelaySamplesTag::CopyTo (double* _buffer)
{
	int j=0;
	for(j=0;j<MAX_PROBE_TRAIN_LENGTH;j++)
	{
		_buffer[j]=m_DelaySamples[j];
	}

}

void
DelaySamplesTag::CopyFrom (double* _buffer)
{
	int j=0;
	for(j=0;j<MAX_PROBE_TRAIN_LENGTH;j++)
	{
		m_DelaySamples[j]=_buffer[j];
	}

}

DelaySamplesTagger::DelaySamplesTagger ()
{
	memset(lastDelaySamples,0,sizeof(lastDelaySamples));
}

void
DelaySamplesTagger::WriteDelaySamplesValue (Ptr<const Packet> packet, double* _buffer)
{
  DelaySamplesTag tag;
  //std::clog << "delaySamplesTag Created.\n";
  tag.CopyFrom(_buffer);
  //std::clog << "delaySamplesTag CopyFrom Called.\n";
  packet->AddByteTag (tag);
  //std::clog << "delaySamplesTag Added.\n";
}
void
DelaySamplesTagger::RecordDelaySamplesValue (Ptr<const Packet> packet, double* _buffer)
{
  DelaySamplesTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  tag.CopyTo(_buffer);
}

} // namespace ns3
