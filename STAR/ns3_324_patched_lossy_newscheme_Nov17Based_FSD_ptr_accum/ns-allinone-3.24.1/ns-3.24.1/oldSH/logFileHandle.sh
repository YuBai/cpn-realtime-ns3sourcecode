#!\bin\bash

grep -H "FSD ORIGINAL" loggg* > logggFSDO
sed -e "s/logggTest//g" -e "s/_iIs//g" -e "s/_jIs/,/g" -e "s/_kIs/,/g" -e "s/_lIs/,/g" -e "s/_mIs/,/g" -e "s/_contentionVScontention.err:/,/g" -e "s/nsINITIAL_PROBE_TRAIN_LENGTH:/,/g" -e "s/INCREASE_STEP://g" -e "s/MAX_PROBE_TRAIN_LENGTH:/,/g" -e "s/START TIME:/,/g" -e "s/nsFSDTurn:/,/g" -e "s/-/,/g" -e "s/: ALTERNATIVE PATH/,/g" -e "s/NOT FSD ORIGINAL PATH/,-1,/g" -e "s/FSD ORIGINAL PATH/,1,/g" -e "s/WITH M_MACPATHRESULT:/,/g" -e "s/://g" < logggFSDO > logggFSDOResult
