#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/mac48-address.h"
#include "ns3/ipv4-address.h"
#include "ns3/simulator.h"
#include "ns3/log.h"

#ifndef GLOBAL_SERVER_TABLE
#define GLOBAL_SERVER_TABLE 1

#define MAX_IP_ADDR 10 //indicating there could be at most 10 APs

namespace ns3{

	class globalServerTable{

		public:

			typedef struct{

				int currentIPIndex;
				int currentCopyState;
				Time lastTriggerTime;
				Ipv4Address ipAddrGroup[MAX_IP_ADDR];

			}ServerTable;

			globalServerTable();
			globalServerTable(int size);
			void fillEntries(int _nodeID, int _APID, Ipv4Address _targetIP);
			void reSize(int size);
			//void copyAnotherGST(globalServerTable _m_gst);

			void setCurrentIPIndex(int _nodeID, Ipv4Address _targetIP);
			void setIPAddr(int _nodeID, int _APID, Ipv4Address _targetIP);

			void setCurrentCopyState(int _nodeID, int _currentCopyState);
			int getCurrentCopyState(int _nodeID);

			void setLastTriggerTime(int _nodeID, Time _time);
			Time getLastTriggerTime(int _nodeID);

			int getCurrentIPIndex(int _nodeID);
			int getIPIndexByIP(int _nodeID, Ipv4Address _targetIP);

		private:

			void initServerTable(int size);
			std::vector<ServerTable> m_gst;

	};


}

#endif
