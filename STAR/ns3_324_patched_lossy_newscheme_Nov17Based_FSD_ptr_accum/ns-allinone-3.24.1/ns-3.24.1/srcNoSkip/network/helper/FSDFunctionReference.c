/*
 * FSDFunctionReference.c
 *
 * Code generation for function 'FSDFunctionReference'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "FSDFunctionReference.h"
#include "eml_sort.h"
#include "sum.h"
#include "power.h"
#include "var.h"
#include "mean.h"

/* Function Declarations */
static double rt_powd_snf(double u0, double u1);
static double rt_roundd_snf(double u);

/* Function Definitions */
static double rt_powd_snf(double u0, double u1)
{
  double y;
  double d0;
  double d1;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    d0 = fabs(u0);
    d1 = fabs(u1);
    if (rtIsInf(u1)) {
      if (d0 == 1.0) {
        y = rtNaN;
      } else if (d0 > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

static double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

signed char FSDFunctionReference(const unsigned int data1[500], const unsigned
  int data2[500], unsigned int data1_len, unsigned int data2_len, double delta_x,
  double alpha_F, signed char replication)
{
  signed char Y;
  int data1_index;
  int data2_index;
  double d;
  double Selection_count_H1;
  double alpha_K;
  int Done;
  unsigned int data1_trunk_new[10];
  unsigned int data2_trunk_new[10];
  int i;
  unsigned int data1_trunk[30];
  unsigned int data2_trunk[30];
  signed char rep;
  double x_q[18];
  double GDomF_q[3];
  double Decision_q[3];
  int GDomF;
  int GNotDomF;
  int j;
  double x_data[900];
  double b_x_data[900];
  int x_size[2];
  int b_x_size[2];
  int q;
  static const double dv0[3] = { 0.7, 0.8, 0.9 };

  double r;
  double a_beta;
  int loop_ub;
  double c_x_data[30];
  double x_q_avg[6];
  double dv1[18];
  double x_q_sum_sqr[6];
  double x_q_var[6];
  double Sum_x_q[6];
  double a_alpha;
  double b_x_q[6];
  int c_x_size[2];

  /* #! /usr/bin/octave -qf */
  /* % */
  /* First Part of FSDFunction: Structure */
  /* function Y=FSDFunction(data1, data2, quantile_option, n_0) */
  /* data1=zeros(100,1); */
  /* data2=zeros(100,2); */
  /* i=0; */
  /* for i=1:1:100 */
  /*     data1(i,1)=0; */
  /* end */
  /* for i=1:1:100 */
  /*     data2(i,1)=10; */
  /* end */
  /* data1=csvread('/Users/zerdocross/Downloads/case1'); */
  /* data2=csvread('/Users/zerdocross/Downloads/case2'); */
  /* data1=csvread('/Users/zerdocross/Downloads/experiment data/timestamp files/6Node_Throughput.txt'); */
  /* data2=csvread('/Users/zerdocross/Downloads/experiment data/timestamp files/073014Case6_Throughput.txt'); */
  data1_index = 11;

  /*  Record Offset for data1_trunk */
  data2_index = 11;

  /*  Record Offset for data2_trunk */
  /*  number of systems */
  /* Q = [0.1 0.2 0.3];  */
  /* Q = [0.4 0.5 0.6]; */
  /*  have to be the same length as one with data1 and data2                 */
  /* delta_x = 30; */
  /* delta_x = 4294967295; */
  d = 3.0 * delta_x / 8.0;

  /*  H0: G NOT FSD F  */
  Selection_count_H1 = 0.0;

  /*  H1: G FSD F */
  /* alpha_F = 0.05;                                */
  alpha_K = alpha_F / 3.0;

  /*  number of average quantile estimates in the initial sample */
  /* replication = 1; */
  /* fprintf(fid,'Q set is: %1.4f ... %1.4f \n', Q(1), Q(length(Q))); */
  /* fprintf(fid,'delta_x is: %1.3f \n', delta_x); */
  /* fprintf(fid,'N is: %d \n', N); */
  /* fprintf(fid,'Number of replications is: %d \n', replication); */
  /* 1 means HD estimator, 2 means WA estimator */
  Done = 0;
  for (i = 0; i < 10; i++) {
    data1_trunk_new[i] = 0U;
    data2_trunk_new[i] = 0U;
  }

  for (i = 0; i < 30; i++) {
    data1_trunk[i] = data1[0];
    data2_trunk[i] = data2[0];
    data1_index = 12;
    data2_index = 12;
  }

  /*  Weight calculation for the HD estimator. */
  /*  Weight calculation for the WA estimator */
  /* (length(Q),1) vector */
  for (rep = 1; rep <= replication; rep++) {
    /* fprintf('Replication number: %d \n', rep);         */
    memset(&x_q[0], 0, 18U * sizeof(double));
    for (i = 0; i < 3; i++) {
      GDomF_q[i] = 0.0;

      /*  H1: G dominates F at q-quantile */
      /*  H0: G does NOT dominate F at q-quantile */
      Decision_q[i] = 0.0;
    }

    GDomF = 0;

    /*  H1: G dominates F at EVERY q-quantile */
    GNotDomF = 0;

    /*  H0: G does NOT dominate F at EVERY q-quantile */
    for (i = 0; i < 2; i++) {
      if (1 + i == 1) {
        for (j = 0; j < 30; j++) {
          x_data[j] = data1_trunk[j];
        }
      } else {
        for (j = 0; j < 30; j++) {
          x_data[j] = data2_trunk[j];
        }
      }

      x_size[0] = 10;
      x_size[1] = 3;
      for (j = 0; j < 30; j++) {
        b_x_data[j] = x_data[j];
      }

      eml_sort(b_x_data, x_size, x_data, b_x_size);
      for (q = 0; q < 3; q++) {
        r = (0.5 + (7.0 + (double)q)) - 10.0 * dv0[q];
        a_beta = (0.5 - (7.0 + (double)q)) + 10.0 * dv0[q];
        loop_ub = b_x_size[1];
        for (j = 0; j < loop_ub; j++) {
          c_x_data[j] = x_data[(q + b_x_size[0] * j) + 6] * r + x_data[(q +
            b_x_size[0] * j) + 7] * a_beta;
        }

        for (j = 0; j < 3; j++) {
          x_q[(i + (q << 1)) + 6 * j] = c_x_data[j];
        }
      }
    }

    mean(x_q, x_q_avg);

    /*  K by length(Q) */
    power(x_q, dv1);
    sum(dv1, x_q_sum_sqr);

    /*  K by length(Q) */
    var(x_q, x_q_var);

    /*  K by length(Q) */
    sum(x_q, Sum_x_q);

    /*  K by length(Q) */
    r = 1.0 / alpha_K;
    a_beta = 1.0 / alpha_K;
    for (i = 0; i < 2; i++) {
      for (j = 0; j < 2; j++) {
        if (1 + i < 1 + j) {
          for (q = 0; q < 3; q++) {
            if (Sum_x_q[q << 1] - Sum_x_q[1 + (q << 1)] < 3.0 * (delta_x - d) -
                (x_q_var[q << 1] + x_q_var[1 + (q << 1)]) * (r - 1.0) / (2.0 * d))
            {
              Decision_q[q] = 1.0;
            } else {
              if (Sum_x_q[q << 1] - Sum_x_q[1 + (q << 1)] > 3.0 * d + (x_q_var[q
                   << 1] + x_q_var[1 + (q << 1)]) * (a_beta - 1.0) / (2.0 * d))
              {
                GDomF_q[q] = 1.0;
                Decision_q[q] = 1.0;
              }
            }
          }

          if (b_sum(Decision_q) == 3.0) {
            Done = 1;
            if (b_sum(GDomF_q) == 3.0) {
              GDomF = 1;
            } else {
              GNotDomF = 1;
            }
          } else {
            Done = 0;
          }
        }
      }
    }

    r = 3.0;

    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     */
    while ((Done == 0) && ((unsigned int)data1_index < data1_len) && ((unsigned
             int)data2_index < data2_len)) {
      r++;
      a_beta = (rt_powd_snf(1.0 / alpha_K, 2.0 / (r - 1.0)) - 1.0) * ((r - 1.0) /
        2.0);
      a_alpha = (rt_powd_snf(1.0 / alpha_K, 2.0 / (r - 1.0)) - 1.0) * ((r - 1.0)
        / 2.0);
      for (j = 0; j < 6; j++) {
        b_x_q[j] = 0.0;
      }

      /* fprintf('data1_index now is: %.4f \n',data1_index); */
      /* fprintf('data2_index now is: %.4f \n',data2_index); */
      /* fprintf('the average sample size is: %.1f \n',average_sample_size); */
      for (i = 0; i < 2; i++) {
        /*              if i==1  */
        /*                %x = normrnd(mu1, sigma1, [N,1]); */
        /*                x = exprnd(mu1, [N,1]); */
        /*              elseif i==2 */
        /*                %x = normrnd(mu2, sigma2, [N,1]); */
        /*                x = exprnd(mu2, [N,1]); */
        /*              end */
        for (j = 0; j < 10; j++) {
          data1_trunk_new[j] = data1[0];
          data2_trunk_new[j] = data2[0];
          data1_index = 12;
          data2_index = 12;
        }

        if (1 + i == 1) {
          for (j = 0; j < 10; j++) {
            x_data[j] = data1_trunk_new[j];
          }
        } else {
          for (j = 0; j < 10; j++) {
            x_data[j] = data2_trunk_new[j];
          }
        }

        c_x_size[0] = 10;
        c_x_size[1] = 1;
        for (j = 0; j < 10; j++) {
          b_x_data[j] = x_data[j];
        }

        b_eml_sort(b_x_data, c_x_size, x_data, b_x_size);
        for (q = 0; q < 3; q++) {
          b_x_q[i + (q << 1)] = x_data[q + 6] * ((0.5 + (7.0 + (double)q)) -
            10.0 * dv0[q]) + x_data[7 + q] * ((0.5 - (7.0 + (double)q)) + 10.0 *
            dv0[q]);
        }
      }

      /*  K by length(Q) */
      b_power(b_x_q, x_q_var);
      for (j = 0; j < 6; j++) {
        x_q_avg[j] = ((r - 1.0) * x_q_avg[j] + b_x_q[j]) / r;
        x_q_sum_sqr[j] += x_q_var[j];
      }

      /*  K by length(Q) */
      b_power(x_q_avg, x_q_var);
      for (j = 0; j < 6; j++) {
        /*  K by length(Q)                 */
        x_q_var[j] = (x_q_sum_sqr[j] - r * x_q_var[j]) / (r - 1.0);
        Sum_x_q[j] += b_x_q[j];
      }

      /*  K by length(Q) */
      /* This part will be commented out if you want to stop comparing for */
      /* a particular q as soon as the decision is made for that q. */
      for (i = 0; i < 3; i++) {
        GDomF_q[i] = 0.0;

        /*  H1: G dominates F at q-quantile */
        /*  H0: G does NOT dominate F at q-quantile */
        Decision_q[i] = 0.0;
      }

      for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
          if (1 + i < 1 + j) {
            for (q = 0; q < 3; q++) {
              /* if Decision_q(q) == 0 */
              if (Sum_x_q[q << 1] - Sum_x_q[1 + (q << 1)] < r * (delta_x - d) -
                  (x_q_var[q << 1] + x_q_var[1 + (q << 1)]) * a_beta / (2.0 * d))
              {
                Decision_q[q] = 1.0;
              } else {
                if (Sum_x_q[q << 1] - Sum_x_q[1 + (q << 1)] > r * d + (x_q_var[q
                     << 1] + x_q_var[1 + (q << 1)]) * a_alpha / (2.0 * d)) {
                  GDomF_q[q] = 1.0;
                  Decision_q[q] = 1.0;
                }
              }

              /* end */
            }

            if (b_sum(Decision_q) == 3.0) {
              Done = 1;
              if (b_sum(GDomF_q) == 3.0) {
                GDomF = 1;
              } else {
                GNotDomF = 1;
              }
            } else {
              Done = 0;
            }
          }
        }
      }
    }

    if ((GNotDomF == 1) || (!(GDomF == 1))) {
      /* fprintf('H0 Selected: %d %d', data1_index, data2_index); */
      /* fprintf('Done == 0: Running Out of Samples: %d %d', data1_index, data2_index); */
    } else {
      Selection_count_H1++;

      /* fprintf('H1 Selected: %d %d', data1_index, data2_index); */
    }
  }

  /* average_sample_size = total_sample_size/replication; */
  /* fprintf('the percentage of H0 selection is: %.4f \n',Selection_count_H0/replication); */
  /* fprintf('the percentage of H1 selection is: %.4f \n',Selection_count_H1/replication); */
  /* fprintf('the average sample size is: %.1f \n',average_sample_size); */
  /* fprintf(fid, 'the percentage of H0 selection is: %.4f \n',Selection_count_H0/replication); */
  /* fprintf(fid, 'the percentage of H1 selection is: %.4f \n',Selection_count_H1/replication); */
  /* fprintf(fid, 'the average sample size is: %.1f \n',average_sample_size); */
  /* fclose(fid); */
  r = rt_roundd_snf(Selection_count_H1 / (double)replication);
  if (r < 128.0) {
    if (r >= -128.0) {
      Y = (signed char)r;
    } else {
      Y = MIN_int8_T;
    }
  } else if (r >= 128.0) {
    Y = MAX_int8_T;
  } else {
    Y = 0;
  }

  return Y;
}

/* End of code generation (FSDFunctionReference.c) */
