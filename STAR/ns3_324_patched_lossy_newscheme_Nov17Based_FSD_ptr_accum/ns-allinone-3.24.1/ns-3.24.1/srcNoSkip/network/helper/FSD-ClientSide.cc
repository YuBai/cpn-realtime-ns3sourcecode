#include "FSD-ClientSide.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE ("FSDClientSide");

//Generated Files - START
/* Function Declarations */
/* Function Definitions */
/* Function Definitions */

/* Function Definitions */

void 
FSDDelayCalculator::emxEnsureCapacity(emxArray__common *emxArray, int oldNumel, int elementSize)
{
  int newNumel;
  int i;
  void *newData;
  newNumel = 1;
  for (i = 0; i < emxArray->numDimensions; i++) {
    newNumel *= emxArray->size[i];
  }

  if (newNumel > emxArray->allocatedSize) {
    i = emxArray->allocatedSize;
    if (i < 16) {
      i = 16;
    }

    while (i < newNumel) {
      i <<= 1;
    }

    newData = calloc((unsigned int)i, (unsigned int)elementSize);
    if (emxArray->data != NULL) {
      memcpy(newData, emxArray->data, (unsigned int)(elementSize * oldNumel));
      if (emxArray->canFreeData) {
        free(emxArray->data);
      }
    }

    emxArray->data = newData;
    emxArray->allocatedSize = i;
    emxArray->canFreeData = TRUE;
  }
}

void
FSDDelayCalculator::b_eml_sort(const emxArray_real_T *x, emxArray_real_T *y)
{
  int dim;
  emxArray_real_T *vwork;
  unsigned int unnamed_idx_0;
  int vstride;
  int k;
  int vspread;
  int npages;
  int i2;
  int i;
  emxArray_int32_T *iidx;
  int i1;
  int j;
  int ix;
  dim = 1;
  if (x->size[0] != 1) {
    dim = 0;
  }

  emxInit_real_T(&vwork, 1);
  unnamed_idx_0 = (unsigned int)x->size[dim];
  vstride = vwork->size[0];
  vwork->size[0] = (int)unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)vwork, vstride, (int)sizeof(double));
  for (vstride = 0; vstride < 2; vstride++) {
    k = y->size[0] * y->size[1];
    y->size[vstride] = x->size[vstride];
    emxEnsureCapacity((emxArray__common *)y, k, (int)sizeof(double));
  }

  vstride = 1;
  k = 1;
  while (k <= dim) {
    vstride *= x->size[0];
    k = 2;
  }

  vspread = (x->size[dim] - 1) * vstride;
  npages = 1;
  k = dim + 2;
  while (k < 3) {
    npages *= x->size[1];
    k = 3;
  }

  i2 = 0;
  i = 1;
  emxInit_int32_T(&iidx, 1);
  while (i <= npages) {
    i1 = i2 - 1;
    i2 += vspread;
    for (j = 1; j <= vstride; j++) {
      i1++;
      i2++;
      ix = i1;
      for (k = 0; k < x->size[dim]; k++) {
        vwork->data[k] = x->data[ix];
        ix += vstride;
      }

      eml_sort_idx(vwork, iidx);
      ix = i1;
      for (k = 0; k < x->size[dim]; k++) {
        y->data[ix] = vwork->data[iidx->data[k] - 1];
        ix += vstride;
      }
    }

    i++;
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
}

void
FSDDelayCalculator::b_emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions)
{
  emxArray_real_T *emxArray;
  int i;
  *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
  emxArray = *pEmxArray;
  emxArray->data = (double *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = TRUE;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

void 
FSDDelayCalculator::c_emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions)
{
  emxArray_real_T *emxArray;
  int i;
  *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
  emxArray = *pEmxArray;
  emxArray->data = (double *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = TRUE;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}


double 
FSDDelayCalculator::b_sum(const emxArray_real_T *x)
{
  double y;
  int k;
  if (x->size[0] == 0) {
    y = 0.0;
  } else {
    y = x->data[0];
    for (k = 2; k <= x->size[0]; k++) {
      y += x->data[k - 1];
    }
  }

  return y;
}

void
FSDDelayCalculator::eml_sort(const emxArray_real_T *x, emxArray_real_T *y)
{
  emxArray_real_T *vwork;
  unsigned int unnamed_idx_0;
  int i2;
  int i;
  emxArray_int32_T *iidx;
  int i1;
  int ix;
  int k;
  emxInit_real_T(&vwork, 1);
  unnamed_idx_0 = (unsigned int)x->size[0];
  i2 = vwork->size[0];
  vwork->size[0] = (int)unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)vwork, i2, (int)sizeof(double));
  for (i2 = 0; i2 < 2; i2++) {
    i = y->size[0] * y->size[1];
    y->size[i2] = x->size[i2];
    emxEnsureCapacity((emxArray__common *)y, i, (int)sizeof(double));
  }

  i2 = 1;
  i = 1;
  emxInit_int32_T(&iidx, 1);
  while (i <= x->size[1]) {
    i1 = i2 - 1;
    i2 += x->size[0];
    ix = i1;
    for (k = 0; k < x->size[0]; k++) {
      vwork->data[k] = x->data[ix];
      ix++;
    }

    eml_sort_idx(vwork, iidx);
    ix = i1;
    for (k = 0; k < x->size[0]; k++) {
      y->data[ix] = vwork->data[iidx->data[k] - 1];
      ix++;
    }

    i++;
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
}

void
FSDDelayCalculator::eml_sort_idx(const emxArray_real_T *x, emxArray_int32_T *idx)
{
  unsigned int unnamed_idx_0;
  int k;
  boolean_T p;
  emxArray_int32_T *idx0;
  int i;
  int i2;
  int j;
  int pEnd;
  int b_p;
  int q;
  int qEnd;
  int kEnd;
  unnamed_idx_0 = (unsigned int)x->size[0];
  k = idx->size[0];
  idx->size[0] = (int)unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)idx, k, (int)sizeof(int));
  if (x->size[0] == 0) {
  } else {
    for (k = 1; k <= x->size[0]; k++) {
      idx->data[k - 1] = k;
    }

    for (k = 1; k <= x->size[0] - 1; k += 2) {
      if ((x->data[k - 1] <= x->data[k]) || rtIsNaN(x->data[k])) {
        p = TRUE;
      } else {
        p = FALSE;
      }

      if (p) {
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    emxInit_int32_T(&idx0, 1);
    k = idx0->size[0];
    idx0->size[0] = x->size[0];
    emxEnsureCapacity((emxArray__common *)idx0, k, (int)sizeof(int));
    i = x->size[0];
    for (k = 0; k < i; k++) {
      idx0->data[k] = 1;
    }

    i = 2;
    while (i < x->size[0]) {
      i2 = i << 1;
      j = 1;
      for (pEnd = 1 + i; pEnd < x->size[0] + 1; pEnd = qEnd + i) {
        b_p = j;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > x->size[0] + 1) {
          qEnd = x->size[0] + 1;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          if ((x->data[idx->data[b_p - 1] - 1] <= x->data[idx->data[q] - 1]) ||
              rtIsNaN(x->data[idx->data[q] - 1])) {
            p = TRUE;
          } else {
            p = FALSE;
          }

          if (p) {
            idx0->data[k] = idx->data[b_p - 1];
            b_p++;
            if (b_p == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                idx0->data[k] = idx->data[q];
                q++;
              }
            }
          } else {
            idx0->data[k] = idx->data[q];
            q++;
            if (q + 1 == qEnd) {
              while (b_p < pEnd) {
                k++;
                idx0->data[k] = idx->data[b_p - 1];
                b_p++;
              }
            }
          }

          k++;
        }

        for (k = 0; k + 1 <= kEnd; k++) {
          idx->data[(j + k) - 1] = idx0->data[k];
        }

        j = qEnd;
      }

      i = i2;
    }

    emxFree_int32_T(&idx0);
  }
}

void
FSDDelayCalculator::emxFree_int32_T(emxArray_int32_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_int32_T *)NULL) {
    if ((*pEmxArray)->canFreeData) {
      free((void *)(*pEmxArray)->data);
    }

    free((void *)(*pEmxArray)->size);
    free((void *)*pEmxArray);
    *pEmxArray = (emxArray_int32_T *)NULL;
  }
}

void
FSDDelayCalculator::emxFree_real_T(emxArray_real_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_real_T *)NULL) {
    if ((*pEmxArray)->canFreeData) {
      free((void *)(*pEmxArray)->data);
    }

    free((void *)(*pEmxArray)->size);
    free((void *)*pEmxArray);
    *pEmxArray = (emxArray_real_T *)NULL;
  }
}

void
FSDDelayCalculator::emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions)
{
  emxArray_int32_T *emxArray;
  int i;
  *pEmxArray = (emxArray_int32_T *)malloc(sizeof(emxArray_int32_T));
  emxArray = *pEmxArray;
  emxArray->data = (int *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = TRUE;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

void
FSDDelayCalculator::emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions)
{
  emxArray_real_T *emxArray;
  int i;
  *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
  emxArray = *pEmxArray;
  emxArray->data = (double *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = TRUE;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

void 
FSDDelayCalculator::power(const emxArray_real_T *a, emxArray_real_T *y)
{
  unsigned int uv1[2];
  int i2;
  int k;
  for (i2 = 0; i2 < 2; i2++) {
    uv1[i2] = (unsigned int)a->size[i2];
  }

  i2 = y->size[0] * y->size[1];
  y->size[0] = 2;
  y->size[1] = (int)uv1[1];
  emxEnsureCapacity((emxArray__common *)y, i2, (int)sizeof(double));
  i2 = (int)uv1[1] << 1;
  for (k = 0; k < i2; k++) {
    y->data[k] = a->data[k] * a->data[k];
  }
}

double
FSDDelayCalculator::rt_powd_snf(double u0, double u1)
{
  double y;
  double d0;
  double d1;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    d0 = fabs(u0);
    d1 = fabs(u1);
    if (rtIsInf(u1)) {
      if (d0 == 1.0) {
        y = rtNaN;
      } else if (d0 > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void 
FSDDelayCalculator::sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int vstride;
  int k;
  int iy;
  int ixstart;
  int j;
  int ix;
  double s;
  for (vstride = 0; vstride < 3; vstride++) {
    sz[vstride] = (unsigned int)x->size[vstride];
  }

  vstride = y->size[0] * y->size[1];
  y->size[0] = 2;
  y->size[1] = (int)sz[1];
  emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
  if (x->size[1] == 0) {
    vstride = y->size[0] * y->size[1];
    y->size[0] = 2;
    emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
    vstride = y->size[0] * y->size[1];
    y->size[1] = (int)sz[1];
    emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
    k = (int)sz[1] << 1;
    for (vstride = 0; vstride < k; vstride++) {
      y->data[vstride] = 0.0;
    }
  } else {
    vstride = 1;
    for (k = 0; k < 2; k++) {
      vstride *= x->size[k];
    }

    iy = -1;
    ixstart = -1;
    for (j = 1; j <= vstride; j++) {
      ixstart++;
      ix = ixstart;
      s = x->data[ixstart];
      for (k = 0; k < 2; k++) {
        ix += vstride;
        s += x->data[ix];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}
void 
FSDDelayCalculator::var(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int vstride;
  int k;
  int ix;
  int iy;
  int j;
  int b_ix;
  double xbar;
  double r;
  double b_y;
  for (vstride = 0; vstride < 3; vstride++) {
    sz[vstride] = (unsigned int)x->size[vstride];
  }

  vstride = y->size[0] * y->size[1];
  y->size[0] = 2;
  y->size[1] = (int)sz[1];
  emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
  if (x->size[1] == 0) {
    vstride = y->size[0] * y->size[1];
    y->size[0] = 2;
    emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
    vstride = y->size[0] * y->size[1];
    y->size[1] = (int)sz[1];
    emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
    k = (int)sz[1] << 1;
    for (vstride = 0; vstride < k; vstride++) {
      y->data[vstride] = 0.0;
    }
  } else {
    vstride = 1;
    for (k = 0; k < 2; k++) {
      vstride *= x->size[k];
    }

    ix = -1;
    iy = -1;
    for (j = 1; j <= vstride; j++) {
      ix++;
      iy++;
      b_ix = ix;
      xbar = x->data[ix];
      for (k = 0; k < 2; k++) {
        b_ix += vstride;
        xbar += x->data[b_ix];
      }

      xbar /= 3.0;
      b_ix = ix;
      r = x->data[ix] - xbar;
      b_y = r * r;
      for (k = 0; k < 2; k++) {
        b_ix += vstride;
        r = x->data[b_ix] - xbar;
        b_y += r * r;
      }

      b_y /= 2.0;
      y->data[iy] = b_y;
    }
  }
}

double 
FSDDelayCalculator::FSDFunctionWTFFork(const emxArray_real_T *data1, const emxArray_real_T
  *data2, double data1_len, double data2_len, double delta_x, double alpha_F,
  double N, const emxArray_real_T *Q)
{
  double Y;
  emxArray_real_T *data1_trunk;
  unsigned int data1_index;
  unsigned int data2_index;
  double d;
  double alpha_K;
  int Done;
  double r;
  int i0;
  int loop_ub;
  emxArray_real_T *data2_trunk;
  emxArray_real_T *data1_trunk_new;
  emxArray_real_T *data2_trunk_new;
  int i;
  emxArray_real_T *x;
  emxArray_real_T *weight;
  int k;
  emxArray_real_T *x_q;
  emxArray_real_T *GDomF_q;
  emxArray_real_T *Decision_q;
  int GDomF;
  int GNotDomF;
  emxArray_real_T *b_x;
  emxArray_real_T *y;
  emxArray_real_T *c_x;
  emxArray_real_T *d_x;
  int q;
  double a_beta;
  int b_weight;
  emxArray_real_T *x_q_avg;
  emxArray_real_T *x_q_var;
  unsigned int uv0[3];
  emxArray_real_T *b_y;
  emxArray_real_T *x_q_sum_sqr;
  emxArray_real_T *Sum_x_q;
  emxArray_real_T *b_x_q;
  emxArray_real_T *b_x_q_sum_sqr;
  emxArray_real_T *b_r;
  emxArray_real_T *e_x;
  double a_alpha;
  emxInit_real_T(&data1_trunk, 1);

  /* #! /usr/bin/octave -qf */
  /* % */
  /* First Part of FSDFunction: Structure */
  /* function Y=FSDFunction(data1, data2, quantile_option, n_0) */
  /* function [Y,total_sample_size,r]=FSDFunctionWTFFork(data1,data2, data1_len, data2_len, delta_x, alpha_F, N) */
  /* data1=zeros(100,1); */
  /* data2=zeros(100,2); */
  /* i=0; */
  /* for i=1:1:100 */
  /*     data1(i,1)=10e8; */
  /* end */
  /* for i=1:1:100 */
  /*     data2(i,1)=8e8; */
  /* end */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2'); */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_1_1'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2_2_2'); */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_1_1'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2_2_2'); */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_1_1_500'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2_2_2_500'); */
  /* 10M40M50M case - 500 Samples */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_1_1_1_1_500'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2_2_2_2_2_500'); */
  /* 10M30M50M case - 500 Samples */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_2_2_2_500'); */
  /* data2New=csvread('/Users/zerdocross/Downloads/case2_1_1_1_500'); */
  /* 10M24M50M case - 500 Samples */
  /* data1New=csvread('/Users/zerdocross/Downloads/case1_2_2_1_500'); */
  /* data2New=csvread('Users/zerdocross/Downloads/case2_1_1_2_500'); */
  /* data1New=data1New(1:length(data1New)/6*5); */
  /* data2New=data2New(1:length(data2New)/6*5); */
  /* data1=csvread('/Users/zerdocross/Downloads/loggg_FSDTurn0_case1(2)'); */
  /* data2=csvread('/Users/zerdocross/Downloads/loggg_FSDTurn0_case2(2)'); */
  /* data1_len=400; */
  /* data2_len=400; */
  /* data1=sort(data1New,'ascend'); */
  /* data2=sort(data2New,'descend'); */
  /* data1=data1New; */
  /* data2=data2New; */
  /* data1=csvread('/Users/zerdocross/Downloads/experiment data/timestamp files/6Node_Throughput.txt'); */
  /* data2=csvread('/Users/zerdocross/Downloads/experiment data/timestamp files/073014Case6_Throughput.txt'); */
  data1_index = 1U;

  /*  Record Offset for data1_trunk */
  data2_index = 1U;

  /*  Record Offset for data2_trunk */
  /*  number of systems */
  /* Q = [0.7 0.8 0.9];  */
  /* Q = [0.1 0.2 0.3];  */
  /* Q = [0.6 0.7 0.8];  */
  /* Q = [0.4 0.5 0.6]; */
  /* Q = [0.7 0.8 0.9]; */
  /* Q = [0.8 0.85 0.9]; */
  /* Q = [0.75 0.85 0.95]; */
  /* Q = [0.7 0.8 0.9]; */
  /* N = 10;       % have to be the same length as one with data1 and data2                 */
  /* delta_x = 100000000; */
  /* delta_x = 1.5*10e5; */
  /* delta_x = 3000000000; */
  /* delta_x = 18070026; */
  /* delta_x = 4294967295; %1610612735.625; */
  /* delta_x = 4294967295; */
  d = 3.0 * delta_x / 8.0;

  /* Selection_count_H0 = 0;          % H0: G NOT FSD F  */
  /* Selection_count_H1 = 0;          % H1: G FSD F */
  /* alpha_F = 0.05;                                */
  alpha_K = alpha_F / (double)Q->size[1];

  /*  number of average quantile estimates in the initial sample */
  /* replication = 1; */
  /* fprintf(fid,'Q set is: %1.4f ... %1.4f \n', Q(1), Q(length(Q))); */
  /* fprintf(fid,'delta_x is: %1.3f \n', delta_x); */
  /* fprintf(fid,'N is: %d \n', N); */
  /* fprintf(fid,'Number of replications is: %d \n', replication); */
  /* 1 means HD estimator, 2 means WA estimator */
  Done = 0;
  r = 3.0 * N;
  i0 = data1_trunk->size[0];
  data1_trunk->size[0] = (int)r;
  emxEnsureCapacity((emxArray__common *)data1_trunk, i0, (int)sizeof(double));
  loop_ub = (int)r;
  for (i0 = 0; i0 < loop_ub; i0++) {
    data1_trunk->data[i0] = 0.0;
  }

  emxInit_real_T(&data2_trunk, 1);
  r = 3.0 * N;
  i0 = data2_trunk->size[0];
  data2_trunk->size[0] = (int)r;
  emxEnsureCapacity((emxArray__common *)data2_trunk, i0, (int)sizeof(double));
  loop_ub = (int)r;
  for (i0 = 0; i0 < loop_ub; i0++) {
    data2_trunk->data[i0] = 0.0;
  }

  emxInit_real_T(&data1_trunk_new, 1);
  i0 = data1_trunk_new->size[0];
  data1_trunk_new->size[0] = (int)N;
  emxEnsureCapacity((emxArray__common *)data1_trunk_new, i0, (int)sizeof(double));
  loop_ub = (int)N;
  for (i0 = 0; i0 < loop_ub; i0++) {
    data1_trunk_new->data[i0] = 0.0;
  }

  emxInit_real_T(&data2_trunk_new, 1);
  i0 = data2_trunk_new->size[0];
  data2_trunk_new->size[0] = (int)N;
  emxEnsureCapacity((emxArray__common *)data2_trunk_new, i0, (int)sizeof(double));
  loop_ub = (int)N;
  for (i0 = 0; i0 < loop_ub; i0++) {
    data2_trunk_new->data[i0] = 0.0;
  }

  r = N * 3.0;
  for (i = 0; i < (int)r; i++) {
    data1_trunk->data[i] = data1->data[(int)data1_index - 1];
    data2_trunk->data[i] = data2->data[(int)data2_index - 1];
    data1_index++;
    data2_index++;
  }

  b_emxInit_real_T(&x, 2);

  /*  Weight calculation for the HD estimator. */
  /*  Weight calculation for the WA estimator */
  i0 = x->size[0] * x->size[1];
  x->size[0] = 1;
  x->size[1] = Q->size[1];
  emxEnsureCapacity((emxArray__common *)x, i0, (int)sizeof(double));
  loop_ub = Q->size[0] * Q->size[1];

  std::clog << "loop_ub: " << loop_ub << ", Q->size[0]: " << Q->size[0] << "Q->size[1]:" << Q->size[1] << "\n"; 

  for (i0 = 0; i0 < loop_ub; i0++) {
    x->data[i0] = N * Q->data[i0] + 0.5;
  }

  b_emxInit_real_T(&weight, 2);
  i0 = weight->size[0] * weight->size[1];
  weight->size[0] = 1;
  weight->size[1] = x->size[1];
  emxEnsureCapacity((emxArray__common *)weight, i0, (int)sizeof(double));
  loop_ub = x->size[0] * x->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    weight->data[i0] = x->data[i0];
  }

  for (k = 0; k < x->size[1]; k++) {
    weight->data[k] = floor(weight->data[k]);
  }

  emxFree_real_T(&x);
  c_emxInit_real_T(&x_q, 3);

  /* (length(Q),1) vector */
  /* for rep = 1:replication */
  /* fprintf('Replication number: %d \n', rep);         */
  i0 = x_q->size[0] * x_q->size[1] * x_q->size[2];
  x_q->size[0] = 2;
  emxEnsureCapacity((emxArray__common *)x_q, i0, (int)sizeof(double));
  k = Q->size[1];
  i0 = x_q->size[0] * x_q->size[1] * x_q->size[2];
  x_q->size[1] = k;
  x_q->size[2] = 3;
  emxEnsureCapacity((emxArray__common *)x_q, i0, (int)sizeof(double));
  loop_ub = (Q->size[1] << 1) * 3;
  for (i0 = 0; i0 < loop_ub; i0++) {
    x_q->data[i0] = 0.0;
  }

  emxInit_real_T(&GDomF_q, 1);
  i0 = GDomF_q->size[0];
  GDomF_q->size[0] = Q->size[1];
  emxEnsureCapacity((emxArray__common *)GDomF_q, i0, (int)sizeof(double));
  loop_ub = Q->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    GDomF_q->data[i0] = 0.0;
  }

  emxInit_real_T(&Decision_q, 1);

  /*  H1: G dominates F at q-quantile */
  /*  H0: G does NOT dominate F at q-quantile */
  i0 = Decision_q->size[0];
  Decision_q->size[0] = Q->size[1];
  emxEnsureCapacity((emxArray__common *)Decision_q, i0, (int)sizeof(double));
  loop_ub = Q->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    Decision_q->data[i0] = 0.0;
  }

  GDomF = 0;

  /*  H1: G dominates F at EVERY q-quantile */
  GNotDomF = 0;

  /*  H0: G does NOT dominate F at EVERY q-quantile */
  b_emxInit_real_T(&b_x, 2);
  b_emxInit_real_T(&y, 2);
  b_emxInit_real_T(&c_x, 2);
  b_emxInit_real_T(&d_x, 2);
  for (i = 0; i < 2; i++) {
    if (1 + i == 1) {
      i0 = y->size[0] * y->size[1];
      y->size[0] = (int)N;
      y->size[1] = 3;
      emxEnsureCapacity((emxArray__common *)y, i0, (int)sizeof(double));
      for (k = 0; k + 1 <= data1_trunk->size[0]; k++) {
        y->data[k] = data1_trunk->data[k];
      }

      i0 = b_x->size[0] * b_x->size[1];
      b_x->size[0] = y->size[0];
      b_x->size[1] = y->size[1];
      emxEnsureCapacity((emxArray__common *)b_x, i0, (int)sizeof(double));
      loop_ub = y->size[0] * y->size[1];
      for (i0 = 0; i0 < loop_ub; i0++) {
        b_x->data[i0] = y->data[i0];
      }
    } else {
      i0 = y->size[0] * y->size[1];
      y->size[0] = (int)N;
      y->size[1] = 3;
      emxEnsureCapacity((emxArray__common *)y, i0, (int)sizeof(double));
      for (k = 0; k + 1 <= data2_trunk->size[0]; k++) {
        y->data[k] = data2_trunk->data[k];
      }

      i0 = b_x->size[0] * b_x->size[1];
      b_x->size[0] = y->size[0];
      b_x->size[1] = y->size[1];
      emxEnsureCapacity((emxArray__common *)b_x, i0, (int)sizeof(double));
      loop_ub = y->size[0] * y->size[1];
      for (i0 = 0; i0 < loop_ub; i0++) {
        b_x->data[i0] = y->data[i0];
      }
    }

    i0 = c_x->size[0] * c_x->size[1];
    c_x->size[0] = b_x->size[0];
    c_x->size[1] = b_x->size[1];
    emxEnsureCapacity((emxArray__common *)c_x, i0, (int)sizeof(double));
    loop_ub = b_x->size[0] * b_x->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      c_x->data[i0] = b_x->data[i0];
    }

    eml_sort(c_x, b_x);
    for (q = 0; q < Q->size[1]; q++) {
      r = (0.5 + weight->data[q]) - N * Q->data[q];
      a_beta = (0.5 - weight->data[q]) + N * Q->data[q];
      loop_ub = b_x->size[1];
      k = (int)weight->data[q];
      b_weight = (int)(weight->data[q] + 1.0);
      i0 = d_x->size[0] * d_x->size[1];
      d_x->size[0] = 1;
      d_x->size[1] = loop_ub;
      emxEnsureCapacity((emxArray__common *)d_x, i0, (int)sizeof(double));
      for (i0 = 0; i0 < loop_ub; i0++) {
        d_x->data[d_x->size[0] * i0] = b_x->data[(k + b_x->size[0] * i0) - 1] *
          r + b_x->data[(b_weight + b_x->size[0] * i0) - 1] * a_beta;
      }

      for (i0 = 0; i0 < 3; i0++) {
        x_q->data[(i + x_q->size[0] * q) + x_q->size[0] * x_q->size[1] * i0] =
          d_x->data[i0];
      }
    }
  }

  emxFree_real_T(&d_x);
  emxFree_real_T(&c_x);
  emxFree_real_T(&data2_trunk);
  emxFree_real_T(&data1_trunk);
  b_emxInit_real_T(&x_q_avg, 2);
  b_emxInit_real_T(&x_q_var, 2);
  sum(x_q, x_q_var);
  rdivide(x_q_var, 3.0, x_q_avg);

  /*  K by length(Q) */
  for (i0 = 0; i0 < 3; i0++) {
    uv0[i0] = (unsigned int)x_q->size[i0];
  }

  c_emxInit_real_T(&b_y, 3);
  i0 = b_y->size[0] * b_y->size[1] * b_y->size[2];
  b_y->size[0] = 2;
  b_y->size[1] = (int)uv0[1];
  b_y->size[2] = 3;
  emxEnsureCapacity((emxArray__common *)b_y, i0, (int)sizeof(double));
  i0 = ((int)uv0[1] << 1) * 3;
  for (k = 0; k < i0; k++) {
    b_y->data[k] = x_q->data[k] * x_q->data[k];
  }

  b_emxInit_real_T(&x_q_sum_sqr, 2);
  b_emxInit_real_T(&Sum_x_q, 2);
  sum(b_y, x_q_sum_sqr);

  /*  K by length(Q) */
  var(x_q, x_q_var);

  /*  K by length(Q) */
  sum(x_q, Sum_x_q);

  /*  K by length(Q) */
  r = 1.0 / alpha_K;
  a_beta = 1.0 / alpha_K;
  emxFree_real_T(&b_y);
  emxFree_real_T(&x_q);
  for (i = 0; i < 2; i++) {
    for (loop_ub = 0; loop_ub < 2; loop_ub++) {
      if (1 + i < 1 + loop_ub) {
        for (q = 0; q < Q->size[1]; q++) {
          /* fprintf('Initial: i: %d, j: %d, IB for Quantile %d: %.4f\n', i, j, q, IB); */
          /* fprintf('Initial: i: %d, j: %d, OB for Quantile %d: %.4f\n', i, j, q, OB); */
          /* fprintf('Initial: i: %d, j: %d, Sum_x_q(i,q) and Sum_x_q(j,q) for Quantile %d: %.4f, %.4f\n', i, j, q,Sum_x_q(i,q), Sum_x_q(j,q)); */
          if (Sum_x_q->data[Sum_x_q->size[0] * q] - Sum_x_q->data[1 +
              Sum_x_q->size[0] * q] < 3.0 * (delta_x - d) - (x_q_var->
               data[x_q_var->size[0] * q] + x_q_var->data[1 + x_q_var->size[0] *
               q]) * (r - 1.0) / (2.0 * d)) {
            Decision_q->data[q] = 1.0;

            /* fprintf('Initial: GNotDomF_q(%d)=1\n',q); */
          } else {
            if (Sum_x_q->data[Sum_x_q->size[0] * q] - Sum_x_q->data[1 +
                Sum_x_q->size[0] * q] > 3.0 * d + (x_q_var->data[x_q_var->size[0]
                 * q] + x_q_var->data[1 + x_q_var->size[0] * q]) * (a_beta - 1.0)
                / (2.0 * d)) {
              GDomF_q->data[q] = 1.0;
              Decision_q->data[q] = 1.0;

              /* fprintf('Initial: GDomF_q(%d)=1\n',q); */
            }
          }
        }

        if (b_sum(Decision_q) == Q->size[1]) {
          Done = 1;
          if (b_sum(GDomF_q) == Q->size[1]) {
            GDomF = 1;
          } else {
            GNotDomF = 1;
          }
        } else {
          Done = 0;
        }
      }
    }
  }

  r = 3.0;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     */
  b_emxInit_real_T(&b_x_q, 2);
  b_emxInit_real_T(&b_x_q_sum_sqr, 2);
  b_emxInit_real_T(&b_r, 2);
  b_emxInit_real_T(&e_x, 2);
  while ((Done == 0) && ((double)data1_index + N <= data1_len) && ((double)
          data2_index + N <= data2_len)) {
    r++;
    a_beta = (rt_powd_snf(1.0 / alpha_K, 2.0 / (r - 1.0)) - 1.0) * ((r - 1.0) /
      2.0);
    a_alpha = (rt_powd_snf(1.0 / alpha_K, 2.0 / (r - 1.0)) - 1.0) * ((r - 1.0) /
      2.0);
    i0 = b_x_q->size[0] * b_x_q->size[1];
    b_x_q->size[0] = 2;
    emxEnsureCapacity((emxArray__common *)b_x_q, i0, (int)sizeof(double));
    k = Q->size[1];
    i0 = b_x_q->size[0] * b_x_q->size[1];
    b_x_q->size[1] = k;
    emxEnsureCapacity((emxArray__common *)b_x_q, i0, (int)sizeof(double));
    loop_ub = Q->size[1] << 1;
    for (i0 = 0; i0 < loop_ub; i0++) {
      b_x_q->data[i0] = 0.0;
    }

    for (i = 0; i < 2; i++) {
      /*              if i==1  */
      /*                %x = normrnd(mu1, sigma1, [N,1]); */
      /*                x = exprnd(mu1, [N,1]); */
      /*              elseif i==2 */
      /*                %x = normrnd(mu2, sigma2, [N,1]); */
      /*                x = exprnd(mu2, [N,1]); */
      /*              end */
      for (loop_ub = 0; loop_ub < (int)N; loop_ub++) {
        data1_trunk_new->data[loop_ub] = data1->data[(int)data1_index - 1];
        data2_trunk_new->data[loop_ub] = data2->data[(int)data2_index - 1];
        data1_index++;
        data2_index++;
      }

      if (1 + i == 1) {
        i0 = y->size[0] * y->size[1];
        y->size[0] = (int)N;
        y->size[1] = 1;
        emxEnsureCapacity((emxArray__common *)y, i0, (int)sizeof(double));
        for (k = 0; k + 1 <= data1_trunk_new->size[0]; k++) {
          y->data[k] = data1_trunk_new->data[k];
        }

        i0 = b_x->size[0] * b_x->size[1];
        b_x->size[0] = y->size[0];
        b_x->size[1] = y->size[1];
        emxEnsureCapacity((emxArray__common *)b_x, i0, (int)sizeof(double));
        loop_ub = y->size[0] * y->size[1];
        for (i0 = 0; i0 < loop_ub; i0++) {
          b_x->data[i0] = y->data[i0];
        }
      } else {
        i0 = y->size[0] * y->size[1];
        y->size[0] = (int)N;
        y->size[1] = 1;
        emxEnsureCapacity((emxArray__common *)y, i0, (int)sizeof(double));
        for (k = 0; k + 1 <= data2_trunk_new->size[0]; k++) {
          y->data[k] = data2_trunk_new->data[k];
        }

        i0 = b_x->size[0] * b_x->size[1];
        b_x->size[0] = y->size[0];
        b_x->size[1] = y->size[1];
        emxEnsureCapacity((emxArray__common *)b_x, i0, (int)sizeof(double));
        loop_ub = y->size[0] * y->size[1];
        for (i0 = 0; i0 < loop_ub; i0++) {
          b_x->data[i0] = y->data[i0];
        }
      }

      i0 = e_x->size[0] * e_x->size[1];
      e_x->size[0] = b_x->size[0];
      e_x->size[1] = b_x->size[1];
      emxEnsureCapacity((emxArray__common *)e_x, i0, (int)sizeof(double));
      loop_ub = b_x->size[0] * b_x->size[1];
      for (i0 = 0; i0 < loop_ub; i0++) {
        e_x->data[i0] = b_x->data[i0];
      }

      b_eml_sort(e_x, b_x);
      for (q = 0; q < Q->size[1]; q++) {
        b_x_q->data[i + b_x_q->size[0] * q] = b_x->data[(int)weight->data[q] - 1]
          * ((0.5 + weight->data[q]) - N * Q->data[q]) + b_x->data[(int)
          (weight->data[q] + 1.0) - 1] * ((0.5 - weight->data[q]) + N * Q->
          data[q]);
      }
    }

    i0 = b_r->size[0] * b_r->size[1];
    b_r->size[0] = 2;
    b_r->size[1] = x_q_avg->size[1];
    emxEnsureCapacity((emxArray__common *)b_r, i0, (int)sizeof(double));
    loop_ub = x_q_avg->size[0] * x_q_avg->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      b_r->data[i0] = (r - 1.0) * x_q_avg->data[i0] + b_x_q->data[i0];
    }

    rdivide(b_r, r, x_q_avg);

    /*  K by length(Q) */
    power(b_x_q, x_q_var);
    i0 = x_q_sum_sqr->size[0] * x_q_sum_sqr->size[1];
    x_q_sum_sqr->size[0] = 2;
    emxEnsureCapacity((emxArray__common *)x_q_sum_sqr, i0, (int)sizeof(double));
    k = x_q_sum_sqr->size[0];
    loop_ub = x_q_sum_sqr->size[1];
    loop_ub *= k;
    for (i0 = 0; i0 < loop_ub; i0++) {
      x_q_sum_sqr->data[i0] += x_q_var->data[i0];
    }

    /*  K by length(Q) */
    power(x_q_avg, x_q_var);
    i0 = b_x_q_sum_sqr->size[0] * b_x_q_sum_sqr->size[1];
    b_x_q_sum_sqr->size[0] = 2;
    b_x_q_sum_sqr->size[1] = x_q_sum_sqr->size[1];
    emxEnsureCapacity((emxArray__common *)b_x_q_sum_sqr, i0, (int)sizeof(double));
    loop_ub = x_q_sum_sqr->size[0] * x_q_sum_sqr->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      b_x_q_sum_sqr->data[i0] = x_q_sum_sqr->data[i0] - r * x_q_var->data[i0];
    }

    rdivide(b_x_q_sum_sqr, r - 1.0, x_q_var);

    /*  K by length(Q)                 */
    i0 = Sum_x_q->size[0] * Sum_x_q->size[1];
    Sum_x_q->size[0] = 2;
    emxEnsureCapacity((emxArray__common *)Sum_x_q, i0, (int)sizeof(double));
    k = Sum_x_q->size[0];
    loop_ub = Sum_x_q->size[1];
    loop_ub *= k;
    for (i0 = 0; i0 < loop_ub; i0++) {
      Sum_x_q->data[i0] += b_x_q->data[i0];
    }

    /*  K by length(Q) */
    /* This part will be commented out if you want to stop comparing for */
    /* a particular q as soon as the decision is made for that q. */
    i0 = GDomF_q->size[0];
    GDomF_q->size[0] = Q->size[1];
    emxEnsureCapacity((emxArray__common *)GDomF_q, i0, (int)sizeof(double));
    loop_ub = Q->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      GDomF_q->data[i0] = 0.0;
    }

    /*  H1: G dominates F at q-quantile */
    /*  H0: G does NOT dominate F at q-quantile */
    i0 = Decision_q->size[0];
    Decision_q->size[0] = Q->size[1];
    emxEnsureCapacity((emxArray__common *)Decision_q, i0, (int)sizeof(double));
    loop_ub = Q->size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
      Decision_q->data[i0] = 0.0;
    }

    for (i = 0; i < 2; i++) {
      for (loop_ub = 0; loop_ub < 2; loop_ub++) {
        if (1 + i < 1 + loop_ub) {
          for (q = 0; q < Q->size[1]; q++) {
            /* if Decision_q(q) == 0 */
            /* fprintf('While Done==0: i: %d, j: %d, IB for Quantile %d: %.4f\n', i, j, q, IB); */
            /* fprintf('while Done==0: i: %d, j: %d, OB for Quantile %d: %.4f\n', i, j, q, OB); */
            /* fprintf('while Done==0: i: %d, j: %d, Sum_x_q(i,q) and Sum_x_q(j,q) for Quantile %d: %.4f, %.4f\n', i, j, q,Sum_x_q(i,q), Sum_x_q(j,q));                     */
            if (Sum_x_q->data[Sum_x_q->size[0] * q] - Sum_x_q->data[1 +
                Sum_x_q->size[0] * q] < r * (delta_x - d) - (x_q_var->
                 data[x_q_var->size[0] * q] + x_q_var->data[1 + x_q_var->size[0]
                 * q]) * a_beta / (2.0 * d)) {
              Decision_q->data[q] = 1.0;

              /* fprintf('while Done==0: GNotDomF_q(%d)=1\n',q); */
            } else {
              if (Sum_x_q->data[Sum_x_q->size[0] * q] - Sum_x_q->data[1 +
                  Sum_x_q->size[0] * q] > r * d + (x_q_var->data[x_q_var->size[0]
                   * q] + x_q_var->data[1 + x_q_var->size[0] * q]) * a_alpha /
                  (2.0 * d)) {
                GDomF_q->data[q] = 1.0;
                Decision_q->data[q] = 1.0;

                /* fprintf('while Done==0: GDomF_q(%d)=1\n',q); */
              }
            }

            /* end */
          }

          if (b_sum(Decision_q) == Q->size[1]) {
            Done = 1;
            if (b_sum(GDomF_q) == Q->size[1]) {
              GDomF = 1;
            } else {
              GNotDomF = 1;
            }
          } else {
            Done = 0;
          }
        }
      }
    }
  }

  emxFree_real_T(&e_x);
  emxFree_real_T(&b_r);
  emxFree_real_T(&b_x_q_sum_sqr);
  emxFree_real_T(&y);
  emxFree_real_T(&b_x_q);
  emxFree_real_T(&Sum_x_q);
  emxFree_real_T(&x_q_var);
  emxFree_real_T(&x_q_sum_sqr);
  emxFree_real_T(&x_q_avg);
  emxFree_real_T(&b_x);
  emxFree_real_T(&Decision_q);
  emxFree_real_T(&GDomF_q);
  emxFree_real_T(&weight);
  emxFree_real_T(&data2_trunk_new);
  emxFree_real_T(&data1_trunk_new);
  if (GNotDomF == 1) {
    /* sSelection_count_H0 = Selection_count_H0 + 1; */
    /* fprintf('H0 Selected: %d %d\n', data1_index, data2_index); */
    Y = 0.0;
  } else if (GDomF == 1) {
    /* Selection_count_H1 = Selection_count_H1 + 1; */
    /* fprintf('H1 Selected: %d %d\n', data1_index, data2_index); */
    Y = 1.0;
  } else {
    /* fprintf('Done == 0: Running Out of Samples: %d %d\n', data1_index, data2_index); */
    Y = 2.0;
  }

  /* end */
  /* average_sample_size = total_sample_size/replication; */
  /* fprintf('the percentage of H0 selection is: %.4f \n',Selection_count_H0/replication); */
  /* fprintf('the percentage of H1 selection is: %.4f \n',Selection_count_H1/replication); */
  /* fprintf('the average sample size is: %.1f \n',average_sample_size); */
  /* fprintf(fid, 'the percentage of H0 selection is: %.4f \n',Selection_count_H0/replication); */
  /* fprintf(fid, 'the percentage of H1 selection is: %.4f \n',Selection_count_H1/replication); */
  /* fprintf(fid, 'the average sample size is: %.1f \n',average_sample_size); */
  /* fclose(fid); */
  /* Y=Selection_count_H1/replication; */
  /* end */
  return Y;
}



void
FSDDelayCalculator::FSDFunctionWTFFork_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

void
FSDDelayCalculator::FSDFunctionWTFFork_terminate(void)
{
  /* (no terminate code required) */
}

emxArray_real_T*
FSDDelayCalculator::emxCreateND_real_T(int numDimensions, int *size)
{
  emxArray_real_T *emx;
  int numEl;
  int i;
  emxInit_real_T(&emx, numDimensions);
  numEl = 1;
  for (i = 0; i < numDimensions; i++) {
    numEl *= size[i];
    emx->size[i] = size[i];
  }

  emx->data = (double *)calloc((unsigned int)numEl, sizeof(double));
  emx->numDimensions = numDimensions;
  emx->allocatedSize = numEl;
  return emx;
}

emxArray_real_T*
FSDDelayCalculator::emxCreateWrapperND_real_T(double *data, int numDimensions, int *
  size)
{
  emxArray_real_T *emx;
  int numEl;
  int i;
  emxInit_real_T(&emx, numDimensions);
  numEl = 1;
  for (i = 0; i < numDimensions; i++) {
    numEl *= size[i];
    emx->size[i] = size[i];
  }

  emx->data = data;
  emx->numDimensions = numDimensions;
  emx->allocatedSize = numEl;
  emx->canFreeData = FALSE;
  return emx;
}

emxArray_real_T*
FSDDelayCalculator::emxCreateWrapper_real_T(double *data, int rows, int cols)
{
  emxArray_real_T *emx;
  int size[2];
  int numEl;
  int i;
  size[0] = rows;
  size[1] = cols;
  emxInit_real_T(&emx, 2);
  numEl = 1;
  for (i = 0; i < 2; i++) {
    numEl *= size[i];
    emx->size[i] = size[i];
  }

  emx->data = data;
  emx->numDimensions = 2;
  emx->allocatedSize = numEl;
  emx->canFreeData = FALSE;
  return emx;
}

emxArray_real_T *
FSDDelayCalculator::emxCreate_real_T(int rows, int cols)
{
  emxArray_real_T *emx;
  int size[2];
  int numEl;
  int i;
  size[0] = rows;
  size[1] = cols;
  emxInit_real_T(&emx, 2);
  numEl = 1;
  for (i = 0; i < 2; i++) {
    numEl *= size[i];
    emx->size[i] = size[i];
  }

  emx->data = (double *)calloc((unsigned int)numEl, sizeof(double));
  emx->numDimensions = 2;
  emx->allocatedSize = numEl;
  return emx;
}

void
FSDDelayCalculator::emxDestroyArray_real_T(emxArray_real_T *emxArray)
{
  emxFree_real_T(&emxArray);
}

real_T
FSDDelayCalculator::rtGetInf(void)
{
  size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
  real_T inf = 0.0;
  if (bitsPerReal == 32U) {
    inf = rtGetInfF();
  } else {
    uint16_T one = 1U;
    enum {
      LittleEndian,
      BigEndian
    } machByteOrder = (*((uint8_T *) &one) == 1U) ? LittleEndian : BigEndian;
    switch (machByteOrder) {
     case LittleEndian:
      {
        union {
          LittleEndianIEEEDouble bitVal;
          real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0x7FF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        inf = tmpVal.fltVal;
        break;
      }

     case BigEndian:
      {
        union {
          BigEndianIEEEDouble bitVal;
          real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0x7FF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        inf = tmpVal.fltVal;
        break;
      }
    }
  }

  return inf;
}

/* Function: rtGetInfF ==================================================
 * Abstract:
 * Initialize rtInfF needed by the generated code.
 * Inf is initialized as non-signaling. Assumes IEEE.
 */
real32_T
FSDDelayCalculator::rtGetInfF(void)
{
  IEEESingle infF;
  infF.wordL.wordLuint = 0x7F800000U;
  return infF.wordL.wordLreal;
}

/* Function: rtGetMinusInf ==================================================
 * Abstract:
 * Initialize rtMinusInf needed by the generated code.
 * Inf is initialized as non-signaling. Assumes IEEE.
 */
real_T
FSDDelayCalculator::rtGetMinusInf(void)
{
  size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
  real_T minf = 0.0;
  if (bitsPerReal == 32U) {
    minf = rtGetMinusInfF();
  } else {
    uint16_T one = 1U;
    enum {
      LittleEndian,
      BigEndian
    } machByteOrder = (*((uint8_T *) &one) == 1U) ? LittleEndian : BigEndian;
    switch (machByteOrder) {
     case LittleEndian:
      {
        union {
          LittleEndianIEEEDouble bitVal;
          real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0xFFF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        minf = tmpVal.fltVal;
        break;
      }

     case BigEndian:
      {
        union {
          BigEndianIEEEDouble bitVal;
          real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0xFFF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        minf = tmpVal.fltVal;
        break;
      }
    }
  }

  return minf;
}

/* Function: rtGetMinusInfF ==================================================
 * Abstract:
 * Initialize rtMinusInfF needed by the generated code.
 * Inf is initialized as non-signaling. Assumes IEEE.
 */
real32_T
FSDDelayCalculator::rtGetMinusInfF(void)
{
  IEEESingle minfF;
  minfF.wordL.wordLuint = 0xFF800000U;
  return minfF.wordL.wordLreal;
}

real_T
FSDDelayCalculator::rtGetNaN(void)
{
  size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
  real_T nan = 0.0;
  if (bitsPerReal == 32U) {
    nan = rtGetNaNF();
  } else {
  uint16_T one = 1U;
  enum {
    LittleEndian,
    BigEndian
  } machByteOrder = (*((uint8_T *) &one) == 1U) ? LittleEndian : BigEndian;
  switch (machByteOrder) {
   case LittleEndian:
    {
      union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
      } tmpVal;

      tmpVal.bitVal.words.wordH = 0xFFF80000U;
      tmpVal.bitVal.words.wordL = 0x00000000U;
      nan = tmpVal.fltVal;
      break;
    }

   case BigEndian:
    {
        union {
          BigEndianIEEEDouble bitVal;
          real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0x7FFFFFFFU;
        tmpVal.bitVal.words.wordL = 0xFFFFFFFFU;
        nan = tmpVal.fltVal;
        break;
      }
    }
  }

  return nan;
}

/* Function: rtGetNaNF ==================================================
 * Abstract:
 * Initialize rtNaNF needed by the generated code.
 * NaN is initialized as non-signaling. Assumes IEEE.
 */
real32_T
FSDDelayCalculator::rtGetNaNF(void)
{
  IEEESingle nanF = { { 0 } };
  uint16_T one = 1U;
  enum {
    LittleEndian,
    BigEndian
  } machByteOrder = (*((uint8_T *) &one) == 1U) ? LittleEndian : BigEndian;
  switch (machByteOrder) {
   case LittleEndian:
    {
      nanF.wordL.wordLuint = 0xFFC00000U;
      break;
    }

   case BigEndian:
    {
      nanF.wordL.wordLuint = 0x7FFFFFFFU;
      break;
    }
  }

  return nanF.wordL.wordLreal;
}

/* End of code generation (rtGetNaN.c) */

/* Function: rt_InitInfAndNaN ==================================================
 * Abstract:
 * Initialize the rtInf, rtMinusInf, and rtNaN needed by the
 * generated code. NaN is initialized as non-signaling. Assumes IEEE.
 */
void
FSDDelayCalculator::rt_InitInfAndNaN(size_t realSize)
{
  (void) (realSize);
  rtNaN = rtGetNaN();
  rtNaNF = rtGetNaNF();
  rtInf = rtGetInf();
  rtInfF = rtGetInfF();
  rtMinusInf = rtGetMinusInf();
  rtMinusInfF = rtGetMinusInfF();
}

/* Function: rtIsInf ==================================================
 * Abstract:
 * Test if value is infinite
 */
boolean_T
FSDDelayCalculator::rtIsInf(real_T value)
{
  return ((value==rtInf || value==rtMinusInf) ? 1U : 0U);
}

/* Function: rtIsInfF =================================================
 * Abstract:
 * Test if single-precision value is infinite
 */
boolean_T
FSDDelayCalculator::rtIsInfF(real32_T value)
{
  return(((value)==rtInfF || (value)==rtMinusInfF) ? 1U : 0U);
}

/* Function: rtIsNaN ==================================================
 * Abstract:
 * Test if value is not a number
 */
boolean_T
FSDDelayCalculator::rtIsNaN(real_T value)
{
#if defined(_MSC_VER) && (_MSC_VER <= 1200)
  return _isnan(value)? TRUE:FALSE;
#else
  return (value!=value)? 1U:0U;
#endif
}

/* Function: rtIsNaNF =================================================
 * Abstract:
 * Test if single-precision value is not a number
 */
boolean_T
FSDDelayCalculator::rtIsNaNF(real32_T value)
{
#if defined(_MSC_VER) && (_MSC_VER <= 1200)
  return _isnan((real_T)value)? true:false;
#else
  return (value!=value)? 1U:0U;
#endif
}

void 
FSDDelayCalculator::rdivide(const emxArray_real_T *x, double y, emxArray_real_T *z)
{
  int i1;
  int loop_ub;
  i1 = z->size[0] * z->size[1];
  z->size[0] = 2;
  z->size[1] = x->size[1];
  emxEnsureCapacity((emxArray__common *)z, i1, (int)sizeof(double));
  loop_ub = x->size[0] * x->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    z->data[i1] = x->data[i1] / y;
  }
}

FSDDelayCalculator::FSDDelayCalculator()
{
	defaultSize[0] = MAX_PROBE_TRAIN_LENGTH; //default size
	//m_delays_p1 = emxCreateND_uint64_T(1, defaultSize);
	//m_delays_p2 = emxCreateND_uint64_T(1, defaultSize);

	debuggingRetVal=0;
}
/* End of code generation (FSDFunction.c) */

//Newly Added
int
FSDDelayCalculator::AvgFunction(double* p1, double* p2)
{
	int kk = MAX_PROBE_TRAIN_LENGTH;
	double avgP1=0;
	double avgP2=0;

	for(int kkk=0;kkk<kk; kkk++)
	{
		avgP1+=p1[kkk];
		avgP2+=p2[kkk];
	}

	avgP1=avgP1/MAX_PROBE_TRAIN_LENGTH;
	avgP2=avgP2/MAX_PROBE_TRAIN_LENGTH;

	NS_LOG_UNCOND ("" << Simulator::Now() << "avgP1: " << avgP1 << " avgP2: " << avgP2);
	if(avgP1 > avgP2)
	{
		return 1;
	}

	else return 0;
}

double
FSDDelayCalculator::CalculateFSDRelationship(double* p1, double* p2, int* _size, double _delta_x, double _alpha_F, double N, double* Q, int* _Qsize)
{
	//int i=0;
	//for( i=0; i<MAX_PROBE_TRAIN_LENGTH; i++)
	//{
	//	m_delays_p1[i]=p1[i];
	//	m_delays_p2[i]=p2[i];
	//}

	m_delays_p1 = emxCreateWrapperND_real_T(p1, 1, _size);
	m_delays_p2 = emxCreateWrapperND_real_T(p2, 1, _size);
	quantiles = emxCreateWrapperND_real_T(Q,2,_Qsize);			

	//FSDFunction(const emxArray_uint64_T *data1, const emxArray_uint64_T *data2,
    //unsigned int data1_len, int data2_len, unsigned int delta_x,
    //unsigned int alpha_F, int replication)

	NS_LOG_UNCOND ("" << Simulator::Now() << "START FSDFUNCTION RUNNING.");

	//return FSDFunction(m_delays_p1, m_delays_p2,_size[0], _size[0], _delta_x, _alpha_F, replication);

	if(CHECK_MODE == 0)
	{
		return FSDFunctionWTFFork(m_delays_p1, m_delays_p2,_size[0], _size[0], _delta_x, _alpha_F, N, quantiles);
		/*if(debuggingRetVal == 0)
		{
			debuggingRetVal = 1;
			return 0;
		}
		else
		{
			return 1;
		}*/
		//return debuggingRetVal;
	}
	else
	{
		return AvgFunction(p1, p2);
	}

}

}
