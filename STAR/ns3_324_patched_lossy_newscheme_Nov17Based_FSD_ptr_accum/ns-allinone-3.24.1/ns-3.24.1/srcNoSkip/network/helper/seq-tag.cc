/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "seq-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Seqform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class SeqTag : public Tag
{
public:
	SeqTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetSeq (void) const;
  void SetSeq (double Seq) ;

private:
  double m_Seq; //!< The time stored in the tag
};

SeqTag::SeqTag ()
  : m_Seq (0.0)
{
}

TypeId
SeqTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::SeqTagTag")
    .SetParent<Tag> ()
    .SetGroupName("SeqTag")
    .AddConstructor<SeqTag> ()
    .AddAttribute ("LastSeq",
                   "Last Seq Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&SeqTag::GetSeq),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
SeqTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
SeqTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
SeqTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Seq);
}
void
SeqTag::Deserialize (TagBuffer i)
{
  m_Seq = i.ReadDouble ();
}
void
SeqTag::Print (std::ostream &os) const
{
  os << "m_Seq=" << m_Seq;
}
double
SeqTag::GetSeq (void) const
{
  return m_Seq;
}

void
SeqTag::SetSeq (double Seq)
{
	m_Seq=Seq;
}

SeqTagger::SeqTagger ()
{
	this->lastSeq=0.0;
}

void
SeqTagger::WriteSeqValue (Ptr<const Packet> packet)
{
  SeqTag tag;
  tag.SetSeq(this->lastSeq);
  packet->AddByteTag (tag);
  this->lastSeq++;
}
void
SeqTagger::RecordSeqValue (Ptr<const Packet> packet)
{
  SeqTag tag;
  lastSeq=0;
  bool found;
  //std::clog << "SeqTagger: start to call packet->FindFirstMatchingByteTag.\n";
  found = packet->FindFirstMatchingByteTag (tag);
  //std::clog << "SeqTagger: packet->FindFirstMatchingByteTag Called.";
  if (!found)
    {
	  //std::clog << "SeqTagger Not Found.\n";
      return;
    }
  lastSeq=tag.GetSeq ();

}

double
SeqTagger::GetLastSeq (void) const
{
  return lastSeq;
}

} // namespace ns3
