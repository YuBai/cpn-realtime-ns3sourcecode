/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 */
#ifndef STA_WIFI_MAC_H
#define STA_WIFI_MAC_H

#define AVERAGE_SNR_SCHEME 0 //1: avg snr; 2: latest snr
#define MAX_BUFFER_LENGTH 10000000
//#define VIDEO_MODE 0 //0: treat as usual; 1: video mode
#define ROLL_MODE 2 //0: nothing happens; 1: fixed probabilistic - probability determined by another value will be input. 2: CSMA/CA mode
//#define MAX_ATTEMPT_TIME 6

#include "regular-wifi-mac.h"
#include "ns3/event-id.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"
#include "ns3/probe-tag.h"
#include "ns3/probe-seq-tag.h"
#include "ns3/fake-pkt-tag.h"
#include "ns3/FSD-ServerSide.h"
#include "ns3/mac-tag.h"
#include "ns3/delay-samples-tag.h"
#include "ns3/FSD-ClientSide.h"
#include "ns3/fake-mac-tag.h"
#include "ns3/op-tag.h"
#include "ns3/alter-path-tag.h"
#include "ns3/statistics-tag.h"
#include "ns3/chosen-tag.h"
#include "ns3/finish-probe-tag.h"
#include "ns3/my-snr-tag.h"
#include "ns3/seq-tag.h"
#include "ns3/probe-train-length-tag.h"
#include "ns3/alter-path-tag.h"
#include "ns3/probe-time-tag.h"
#include "ns3/end-of-train-tag.h"
#include "ns3/hop-tag.h"
#include "ns3/second-hop-tag.h"
#include "ns3/port-tag.h"
#include "ns3/enqueue-time-tag.h"
#include "ns3/enqueue-time-compensate-tag.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/stop-traffic-tag.h"

#include "supported-rates.h"
#include "amsdu-subframe-header.h"

#include "ns3/queueing-delay-estimation.h"
#include "ns3/followdown-delay-estimation.h"
#include "ns3/wifi-mac-queue.h"

#include "ns3/global-ap-table.h"
#include "ns3/app-copy-tag.h"


namespace ns3  {

class MgtAddBaRequestHeader;

/**
 * \ingroup wifi
 *
 * The Wifi MAC high model for a non-AP STA in a BSS.
 */
class StaWifiMac : public RegularWifiMac
{
public:
  static TypeId GetTypeId (void);

  StaWifiMac ();
  virtual ~StaWifiMac ();

  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   *
   * The packet should be enqueued in a tx queue, and should be
   * dequeued as soon as the channel access function determines that
   * access is granted to this MAC.
   */
  virtual void Enqueue (Ptr<const Packet> packet, Mac48Address to);

  /**
   * \param missed the number of beacons which must be missed
   * before a new association sequence is started.
   */
  void SetMaxMissedBeacons (uint32_t missed);
  /**
   * \param timeout
   *
   * If no probe response is received within the specified
   * timeout, the station sends a new probe request.
   */
  void SetProbeRequestTimeout (Time timeout);
  /**
   * \param timeout
   *
   * If no association response is received within the specified
   * timeout, the station sends a new association request.
   */
  void SetAssocRequestTimeout (Time timeout);

  /**
   * Start an active association sequence immediately.
   */
  void StartActiveAssociation (void);

  globalAPTable& GetGAT(void);

private:
  /**
   * The current MAC state of the STA.
   */
  enum MacState
  {
    ASSOCIATED,
    WAIT_PROBE_RESP,
    WAIT_ASSOC_RESP,
    BEACON_MISSED,
    REFUSED
  };

  /**
   * Enable or disable active probing.
   *
   * \param enable enable or disable active probing
   */
  void SetActiveProbing (bool enable);
  /**
   * Return whether active probing is enabled.
   *
   * \return true if active probing is enabled, false otherwise
   */
  bool GetActiveProbing (void) const;

  virtual void Receive (Ptr<Packet> packet, const WifiMacHeader *hdr);

  /**
   * Forward a probe request packet to the DCF. The standard is not clear on the correct
   * queue for management frames if QoS is supported. We always use the DCF.
   */
  void SendProbeRequest (void);
  /**
   * Forward an association request packet to the DCF. The standard is not clear on the correct
   * queue for management frames if QoS is supported. We always use the DCF.
   */
  void SendAssociationRequest (void);
  /**
   * Try to ensure that we are associated with an AP by taking an appropriate action
   * depending on the current association status.
   */
  void TryToEnsureAssociated (void);
  /**
   * This method is called after the association timeout occurred. We switch the state to
   * WAIT_ASSOC_RESP and re-send an association request.
   */
  void AssocRequestTimeout (void);
  /**
   * This method is called after the probe request timeout occurred. We switch the state to
   * WAIT_PROBE_RESP and re-send a probe request.
   */
  void ProbeRequestTimeout (void);
  /**
   * Return whether we are associated with an AP.
   *
   * \return true if we are associated with an AP, false otherwise
   */
  bool IsAssociated (void) const;
  /**
   * Return whether we are waiting for an association response from an AP.
   *
   * \return true if we are waiting for an association response from an AP, false otherwise
   */
  bool IsWaitAssocResp (void) const;
  /**
   * This method is called after we have not received a beacon from the AP
   */
  void MissedBeacons (void);
  /**
   * Restarts the beacon timer.
   *
   * \param delay the delay before the watchdog fires
   */
  void RestartBeaconWatchdog (Time delay);
  /**
   * Return an instance of SupportedRates that contains all rates that we support
   * including HT rates.
   *
   * \return SupportedRates all rates that we support
   */
  SupportedRates GetSupportedRates (void) const;
  /**
   * Set the current MAC state.
   *
   * \param value the new state
   */
  void SetState (enum MacState value);
  /**
   * Return the HT capability of the current AP.
   *
   * \return the HT capability that we support
   */
  HtCapabilities GetHtCapabilities (void) const;
  void sendEOF(void);
  void setProbeTrainCounter(void);
  /**
   * Return the VHT capability of the current AP.
   *
   * \return the VHT capability that we support
   */
  VhtCapabilities GetVhtCapabilities (void) const;


  void ReportECParameters(void);
  void SetCurrentContext(void);
  int GetCurrentContext(void);
  void TriggerFSD(void);
  void EndThisCircle(void);
  void testFunction(void);
  void resortSamples(double* samples, int sampleLen);

  enum MacState m_state;
  Time m_probeRequestTimeout;
  Time m_assocRequestTimeout;
  EventId m_probeRequestEvent;
  EventId m_assocRequestEvent;
  EventId m_beaconWatchdog;

  EventId m_reportECParameters;
  EventId m_setCurrentContext;
  EventId m_periodicTriggering;

  Time m_beaconWatchdogEnd;
  uint32_t m_maxMissedBeacons;
  bool m_activeProbing;
  //FollowdownDelayEstimation m_fdje;
  //Added by Y.B.
  ProbeTagger m_pt;
  ProbeTrainLengthTagger m_ptlt;
  ProbeTrainLengthTagger m_ptlt_checker;
  ProbeTrainLengthTagger m_ptlt_checker_tmp;
  ProbeSeqTagger m_pst;
  AlterPathTagger m_apt;
  AlterPathTagger m_apt_tmp;
  ProbeTimeTagger m_ptt;
  ProbeTimeTagger m_ptt_checker;
  ProbeTimeTagger m_ptt_checker_tmp;
  FakePktTagger m_fpt;
  FakePktTagger m_fpt_tmp;
  EndOfTrainTagger m_ett;
  FSDDelayFeedbacker m_fdfb; //very dirty hack - need to be improved if necessary
  //MacTagger m_mt;
  DelaySamplesTagger m_dst;
  FSDDelayCalculator m_fdc;
  double m_samples_one[MAX_PROBE_TRAIN_LENGTH];
  double m_samples_two[MAX_PROBE_TRAIN_LENGTH];
  //double m_totalSamples_one[(INITIAL_PROBE_TRAIN_LENGTH+MAX_PROBE_TRAIN_LENGTH)*((MAX_PROBE_TRAIN_LENGTH-INITIAL_PROBE_TRAIN_LENGTH)/INCREASE_STEP)/2];
  //double m_totalSamples_two[(INITIAL_PROBE_TRAIN_LENGTH+MAX_PROBE_TRAIN_LENGTH)*((MAX_PROBE_TRAIN_LENGTH-INITIAL_PROBE_TRAIN_LENGTH)/INCREASE_STEP)/2];
  double m_totalSamples_one[MAX_BUFFER_LENGTH];
  double m_totalSamples_two[MAX_BUFFER_LENGTH];
  int m_oneFilled;
  int m_twoFilled;
  int m_pathDecision; //0: not switch; 1: switch; 2: insufficient samples
  int m_probeOrNot; //Variable Controlling Value of Probing Tag
  int m_alterOrNot; //Variable Comtrolling Value of Alter Path Tag
  int m_endTrainOrNot; //Variable Controlling Whether Train is Sent
  int m_endTrainOrNot_ACK; //Variable Controlling Whether ACK for End Train Is Received
  int m_feedback_ACK; //Variable Controlling Whether we should send feedback ACK
  int m_feedback_ACK_ACK; //Variable Controlling Whether we should send feedback ACK
  //Mac48Address m_macPathOne;
  //Mac48Address m_macPathTwo;
  //int m_macPathOne_channel;
  //int m_macPathTwo_channel;
  //int m_macPathOne_IPRouting;
  //int m_macPathTwo_IPRouting;
  Mac48Address m_macPathResult;
  int probeTrainCounter;
  FakeMacTagger m_fmt;
  FakeMacTagger m_fmt_tmp;
  ChosenTagger m_ct;
  OpTagger m_ot;
  ProbingDelayEstimation m_pde;
  StatisticsTagger m_stat;
  uint64_t pktAmountSample; //for trigger calculating
  uint64_t throughputThreshold;	//for probing trigger - in the unit of packets
  int m_triggerOrNot; //To guarantee that algorithm only triggers once - make things simpler
  Mac48Address m_theChosenOne;
  Mac48Address m_eofTo;
  int startTime; //in the unit of seconds - check out whether threshold is hit
  int startTimeP; //start periodic triggering time
  //int periodP; //how long triggering interval is
  FinishProbeTagger m_finipt;
  Time lastTriggerTime; //make sure that after a period of observation we make decision on whether we should trigger FSD
  Time eofSendTime;
  //Time FSDTriggeringDuration;
  uint64_t lastPktAmountSample;
  int probeTime; //distinguish between different times of probings
  long triggeringTime; //distinguish between different FSD runs
  int appTriggeringState; //determine which state system is in now from aspect of letting application run.
  int ACTUAL_PROBE_TRAIN_LENGTH;
  int feedbackPktCurrentRun_one;
  int feedbackPktCurrentRun_two;
  Ptr<Packet> endOfTrainCopy;
  Ptr<YansWifiPhy> wifiPhyTmp;

  Ptr<Packet> app1Copy;
  Mac48Address app1From;
  Mac48Address app1To;
  Ptr<Packet> app1CopyTmp;

  Ptr<Packet> app2Copy;
  Mac48Address app2From;
  Mac48Address app2To;
  Ptr<Packet> app2CopyTmp;

  //Ptr<Packet> app3Copy;
  Mac48Address app3From;
  Mac48Address app3To;
  //Ptr<Packet> app3CopyTmp;

  HopTagger m_ht;
  PortTagger m_ptg;

  SecondHopTagger m_sht;
  int kk_triggeringTime_one;
  int kk_probeTime_one;
  int kk_triggeringTime_two;
  int kk_probeTime_two;
  int currentAP1TrainLen;
  int currentAP2TrainLen;
  uint32_t deltaX;

  //Mac48Address m_currentAssoc;
  Ssid ssidOne;
  Ssid ssidTwo;
  //SocketAddressTag m_sat;

  Ptr<WifiMacQueue> m_dca_queue;
  Ptr<WifiMacQueue> tmpQueueAPP1;
  Ptr<WifiMacQueue> tmpQueueAPP2;
  Ptr<WifiMacQueue> tmpQueueAPP3;

  //Added for SNR Comparison
  uint64_t path1SNRTimes;
  uint64_t path2SNRTimes;
  double path1SNRSum;
  double path2SNRSum;
  Time windowLength;
  Mac48Address SNRAP1;
  Mac48Address SNRAP2;
  Time lastCompTime;
  double path1AvgSNR;
  double path2AvgSNR;
  Mac48Address SNRSchemeChosenOne;

  Ptr<Packet> stopTrafficPkt;
  WifiMacHeader stopTrafficPktHdr;
  StopTrafficTagger m_stt;
  Mac48Address stopTrafficFrom;
  Mac48Address stopTrafficTo;

  TracedCallback<Mac48Address> m_assocLogger;
  TracedCallback<Mac48Address> m_deAssocLogger;

  int initialAssocAP;
  globalAPTable m_gat;
  int currentAssocAP;
  int currentContext;
  int setFlag;

  int app1CopyState;//1: copying; 2: copied
  int app2CopyState;//3: copying; 4: copied
  AppCopyTagger m_act;
  //for different triggering methods
  Ptr <UniformRandomVariable>m_decisionDice;//=CreateObject<UniformRandomVariable>()
  uint32_t m_RollingModeOneProb;
  uint32_t m_maxFSDNodeID;
  uint32_t m_minFSDNodeID;
  Time randomDelayOffset;
  uint32_t m_maxWindowSize;
  uint32_t m_minWindowSize;
  int VIDEO_MODE;
  int attemptCounter;

  int MAX_ATTEMPT_TIME;
  int FSD_TIME_OUT;
  int DELTA_T;
  int NODE_AMOUNT_IN_NETWORK;
};

} //namespace ns3

#endif /* STA_WIFI_MAC_H */
