#!/bin/bash

for f in MissCTSDrop*
	do
		oldFileName=$(echo $f)
		newFileName=$(echo $f | sed -e 's/\\//g' -e 's/\./_/g' -e 's/{/_/g' -e 's/}/_/g' -e 's/__/_/g' -e 's/PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_RandomTopo_err//g' -e 's/__/_/g' -e 's/_tmp//g')
		mv $oldFileName $newFileName	

	done
