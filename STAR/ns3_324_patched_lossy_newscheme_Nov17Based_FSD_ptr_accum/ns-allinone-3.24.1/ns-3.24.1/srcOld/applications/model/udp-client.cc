/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "udp-client.h"
#include "seq-ts-header.h"
#include <cstdlib>
#include <cstdio>
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/delay-samples-tag.h"
#include "ns3/stop-traffic-tag.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("UdpClient");

NS_OBJECT_ENSURE_REGISTERED (UdpClient);

TypeId
UdpClient::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::UdpClient")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<UdpClient> ()
    .AddAttribute ("MaxPackets",
                   "The maximum number of packets the application will send",
                   UintegerValue (100),
                   MakeUintegerAccessor (&UdpClient::m_count),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("Interval",
                   "The time to wait between packets", TimeValue (Seconds (1.0)),
                   MakeTimeAccessor (&UdpClient::m_interval),
                   MakeTimeChecker ())
    .AddAttribute ("RemoteAddress",
                   "The destination Address of the outbound packets",
                   AddressValue (),
                   MakeAddressAccessor (&UdpClient::m_peerAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort", "The destination port of the outbound packets",
                   UintegerValue (100),
                   MakeUintegerAccessor (&UdpClient::m_peerPort),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("PacketSize",
                   "Size of packets generated. The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.",
                   UintegerValue (1024),
                   MakeUintegerAccessor (&UdpClient::m_size),
                   MakeUintegerChecker<uint32_t> (12,1500))
	.AddAttribute ("QoSLevel",
				   "Level of QoS which follows 802.11D standard (priority: lowest->highest): 1 - background, 0 - best effort (default), 3 - excellent effort, 4 -  controlled load, 5 - videos, 6 - voices, and 7 - network control",
				   UintegerValue (3),
				   MakeUintegerAccessor (&UdpClient::m_tid),
				   MakeUintegerChecker<uint8_t> (0,7))
  ;
  return tid;
}

UdpClient::UdpClient ()
{
  NS_LOG_FUNCTION (this);
  m_sent = 0;
  m_socket = 0;
  m_sendEvent = EventId ();
  m_rebootInterval = Seconds (1);
  cancelEvent = 0;
  m_flag = 0;
}

UdpClient::~UdpClient ()
{
  NS_LOG_FUNCTION (this);
}

void
UdpClient::SetRemote (Ipv4Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = Address(ip);
  m_peerPort = port;
}

void
UdpClient::SetRemote (Ipv6Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = Address(ip);
  m_peerPort = port;
}

void
UdpClient::SetRemote (Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = ip;
  m_peerPort = port;
}

Address
UdpClient::GetRemotePeerAddr(void)
{
	return m_peerAddress;//InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort);
}

uint16_t
UdpClient::GetPort()
{
	return m_peerPort;
}

void
UdpClient::SetPort(uint16_t port)
{
	m_peerPort = port;
}

int
UdpClient::GetFlag()
{
	return m_flag;
}

void
UdpClient::SetFlag(int flag)
{
	m_flag=flag;
	NS_LOG_UNCOND_YB("" << Simulator::Now() << ": m_flag now is " << m_flag);
	//m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
	//Simulator::Cancel (m_sendEvent);
	/*if(m_flag != 0)
	{
		//m_sendEvent = Simulator::ScheduleNow (&UdpClient::Send, this);
		//m_flag = 0;
	}*/

}

void
UdpClient::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

void
UdpClient::StartApplication (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_UNCOND ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": START APPLICATION WITH PORT NUMBER " << m_peerPort);

  cancelEvent = 0;
  int retVal;

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
        {
    	  retVal = m_socket->Bind ();
    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_socket->Bind() returns " << retVal);

    	  retVal = m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_socket->Connect() returns " << retVal);

        }
      else if (Ipv6Address::IsMatchingType(m_peerAddress) == true)
        {
          m_socket->Bind6 ();
          m_socket->Connect (Inet6SocketAddress (Ipv6Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
    }

  //Modified by Y.B.
  //m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ()); //Original Version


  m_socket->SetRecvCallback (MakeCallback (&UdpClient::HandleRead, this)); //Modified Version
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": START APPLICATION FINISHED");
  m_qt.SetTid(m_tid);

  m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);

}

void
UdpClient::StopApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0)
  {
	  m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
  }

  Simulator::Cancel (m_sendEvent);
}

void
UdpClient::Send (void)
{
  NS_LOG_FUNCTION (this);

  //NS_LOG_UNCOND ("Send ENTERED.");
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": SEND ENTERED");

  NS_ASSERT (m_sendEvent.IsExpired ());
  SeqTsHeader seqTs;
  seqTs.SetSeq (m_sent);
  Ptr<Packet> p = Create<Packet> (m_size-(8+4)); // 8+4 : the size of the seqTs header
  p->AddHeader (seqTs);

  m_udje.PrepareTx(p);
  m_fdje.PrepareEqTime(p);
  m_pde.PrepareEqTime(p);
  m_st.WriteSeqValue(p);
  m_ptg.WritePortValue(p,m_peerPort);

  IdTagger m_id (this->m_node->GetId());
  m_id.WriteIdValue(p);
  p->AddPacketTag(m_qt);

  PeerIdTagger m_pit;
  if(GetNode()->GetId() == 0)
  {
//	  if(Ipv4Address::ConvertFrom(m_peerAddress) == "10.1.3.1" || Ipv4Address::ConvertFrom(m_peerAddress) == "10.1.4.3")
//	  {
//		  m_pit.WritePeerIdValue(p,3);//stupid hacking
//	  }

	  uint32_t tmpIP=Ipv4Address::ConvertFrom(m_peerAddress).Get();
	  uint32_t lastDigit=tmpIP & 0x000000ff;
	  m_pit.WritePeerIdValue(p,lastDigit+1);
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": This is node " << GetNode()->GetId() << ", my peer address is " << Ipv4Address::ConvertFrom(m_peerAddress) << " and peer node ID is " << lastDigit+1);
  }

  std::stringstream peerAddressStringStream;
  if (Ipv4Address::IsMatchingType (m_peerAddress))
    {
      peerAddressStringStream << Ipv4Address::ConvertFrom (m_peerAddress);
    }
  else if (Ipv6Address::IsMatchingType (m_peerAddress))
    {
      peerAddressStringStream << Ipv6Address::ConvertFrom (m_peerAddress);
    }
  if(m_flag == 0)
  {
	  if ((m_socket->Send (p)) >= 0)
	    {
		  // Added by Y.B.
		  m_udje.PrepareTx(p);
		  m_fdje.PrepareEqTime(p);

		  ++m_sent;

	      NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
	                                    << peerAddressStringStream.str () << " Uid: "
	                                    << p->GetUid () << " Time: "
	                                    << (Simulator::Now ()).GetSeconds () << " From: "
	                                    << GetNode()->GetId());
	      NS_LOG_UNCOND ("TraceDelay TX " << m_size << " bytes to "
	                                         << peerAddressStringStream.str () << " Uid: "
	                                         << p->GetUid () << " Time: "
	                                         << (Simulator::Now ()).GetSeconds () << " From: "
											 << GetNode()->GetId() << " With IP:"
											 << this->GetNode()->GetObject<Ipv4>()->GetAddress(1,0) << " Port: "
											 << m_peerPort);


	    }
	  else
	    {
	      NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
	                                          << peerAddressStringStream.str () << " from "
											  << GetNode()->GetId());
	      NS_LOG_UNCOND ("" << Simulator::Now() << ": Error while sending " << m_size << " bytes to "
	                                          << peerAddressStringStream.str () << " from "
											  << GetNode()->GetId());
	      //NS_LOG_UNCOND ("Error while sending " << m_size << " bytes to "
	       //                                         << peerAddressStringStream.str ());
	    }

	  //NS_LOG_UNCOND ("cancelEvent: " << cancelEvent);
	  if (m_sent < m_count)// && cancelEvent == 0)
	    {
	      m_sendEvent = Simulator::Schedule (m_interval, &UdpClient::Send, this);
	    }
  }
  else
  {
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": Flag is set so I am going to use new address.");
	  Address m_SendToAddress=InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort);
	  if ((m_socket->SendTo (p,0,m_SendToAddress)) >= 0)
	    {
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": Flag is set and it seems that new address" << Ipv4Address::ConvertFrom(m_peerAddress) << " works.");
		  // Added by Y.B.
		  m_udje.PrepareTx(p);
		  m_fdje.PrepareEqTime(p);

		  ++m_sent;

	      NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
	                                    << peerAddressStringStream.str () << " Uid: "
	                                    << p->GetUid () << " Time: "
	                                    << (Simulator::Now ()).GetSeconds () << " From: "
	                                    << GetNode()->GetId());
	      NS_LOG_UNCOND ("TraceDelay TX " << m_size << " bytes to "
	                                         << peerAddressStringStream.str () << " Uid: "
	                                         << p->GetUid () << " Time: "
	                                         << (Simulator::Now ()).GetSeconds () << " From: "
											 << GetNode()->GetId() << " With IP:"
											 << this->GetNode()->GetObject<Ipv4>()->GetAddress(1,0) << " Port: "
											 << m_peerPort);


	    }
	  else
	    {
	      NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
	                                          << peerAddressStringStream.str () << " from "
											  << GetNode()->GetId());
	      NS_LOG_UNCOND ("" << Simulator::Now() << ": Error while sending " << m_size << " bytes to "
	                                          << peerAddressStringStream.str () << " from "
											  << GetNode()->GetId());
	      //NS_LOG_UNCOND ("Error while sending " << m_size << " bytes to "
	       //                                         << peerAddressStringStream.str ());
	    }

	  //NS_LOG_UNCOND ("cancelEvent: " << cancelEvent);
	  if (m_sent < m_count)// && cancelEvent == 0)
	    {
	      m_sendEvent = Simulator::Schedule (m_interval, &UdpClient::Send, this);
	    }
  }




}

void
UdpClient::HandleRead (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);

  //NS_LOG_UNCOND ("HANDLE READ CALLED");

  Ptr<Packet> packet;
  Address from;
  FakePktTagger m_fpt;
  StopTrafficTagger m_stt;

  while ((packet = socket->RecvFrom (from)))
    {
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ENTERED INTO RECVFROM");

	  m_stt.RecordStopTrafficValue(packet);
	  m_ot.RecordOpValue(packet);

	  /*if(m_stt.GetLastStopTraffic() != 0)
	  {
		  NS_LOG_UNCOND("" << Simulator::Now() << ": I Got A Packettt with stop traffic tag " << m_stt.GetLastStopTraffic() << " and it comes from " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " and I am Node " << GetNode()->GetId());
		  //int stopTrafficValue=m_stt.GetLastStopTraffic();
		  if(m_stt.GetLastStopTraffic() == 1) //DjMax: stop!
		  {
			  Simulator::Cancel (m_sendEvent);
			  //reset m_flag so we could change IP address correspondingly
			  //this->SetFlag(0);

		  }
		  else if(m_stt.GetLastStopTraffic() == 2)
		  {
		      m_sendEvent = Simulator::Schedule (Seconds(0), &UdpClient::Send, this);
		  }


		  NS_LOG_UNCOND("Done.");
	      packet->RemoveAllPacketTags ();
		  NS_LOG_UNCOND("Done.");
	      packet->RemoveAllByteTags ();
		  NS_LOG_UNCOND("Done.");

	      m_stt.WriteStopTrafficValue(packet,stopTrafficValue);
		  NS_LOG_UNCOND("Done.");

	      //SeqTsHeader seqTsTmp;
	      seqTsTmp.SetSeq (m_sent);
	      packet->AddHeader (seqTsTmp);
	      if(m_socket->Send (packet) >= 0)
	      {
	    	  ++m_sent;
	    	  NS_LOG_UNCOND("" << Simulator::Now() << ": I sent a stop traffic packet with tag value " << stopTrafficValue);
	      }//

	      int tmp;
		  NS_LOG_UNCOND("Done.");
	      tmp=socket->SendTo (packet, 0, from);
		  NS_LOG_UNCOND("Done.");
	      NS_LOG_UNCOND("" << Simulator::Now() << ": I called SendTo() with Return Value " << tmp);  //Send Packet Back
	  }*/


	  if(m_ot.GetLastOp() == INITIAL_PROBE_TRAIN_LENGTH) //use m_ot to capture how many packets to send - from now on train length of APP2 should be same as length of train
	  {
		  NS_LOG_UNCOND ("" << Simulator::Now() << ": m_ot=" << m_ot.GetLastOp() << " - a new train started.");
		  m_sent=0;
		  m_count=m_ot.GetLastOp();
		  Simulator::Cancel (m_sendEvent);
	      m_sendEvent = Simulator::Schedule (Seconds(0), &UdpClient::Send, this);

	  }

	  if(m_ot.GetLastOp() == 3 && cancelEvent == 0) //switch to another path
	  {
		  NS_LOG_UNCOND ("" << Simulator::Now() << ": m_ot==3 - Freeze on App with Port Number=" << m_peerPort);
		  Simulator::Cancel (m_sendEvent);

		  //Correction
		  cancelEvent = 1;
		  //m_rebootEvent = Simulator::ScheduleNow (&UdpClient::StopApplication, this);
		  //m_rebootEvent = Simulator::Schedule (m_rebootInterval, &UdpClient::StartApplication, this);
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " : SEND EVENT CANCELLED With cancelEvent Value " << cancelEvent);
		  return;
	  }
	  else if(m_ot.GetLastOp() == 4 && cancelEvent == 1)
	  {
		  NS_LOG_UNCOND ("" << Simulator::Now() << ": m_ot==4 - DeFreeze on App with Port Number=" << m_peerPort);
		  //m_rebootEvent = Simulator::ScheduleNow (&UdpClient::StopApplication, this);
		  //m_rebootEvent = Simulator::Schedule (m_rebootInterval, &UdpClient::StartApplication, this);
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " : SEND EVENT RESTORED With cancelEvent Value " << cancelEvent);
		  m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
		  cancelEvent = 0;

		  return;
	  }
	  // Added by Y.B.
	  else
	  {
		  m_udje.RecordRx(packet);
		 	  //NS_LOG_UNCOND (InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << packet->GetSize() << "," << ",UDP Delay," << m_udje.GetLastDelay());
		 	  //NS_LOG_UNCOND (InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << packet->GetSize() << "," << ",UDP Jitter," << m_udje.GetLastJitter());

		 	  m_fpt.RecordFakePktValue(packet);
		 	  packet->PeekPacketTag(m_qtRead);
		 	  int tmp = m_qtRead.GetTid();

		       if (InetSocketAddress::IsMatchingType (from))
		         {
		           NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
		                        InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
		                        InetSocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
		 					   m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter());
		           NS_LOG_UNCOND ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
		                                  InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
		                                  InetSocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
		           					   m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter() << " and qtTag " << tmp);
		         }
		       else if (Inet6SocketAddress::IsMatchingType (from))
		         {
		           NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
		                        Inet6SocketAddress::ConvertFrom (from).GetIpv6 () << " port " <<
		                        Inet6SocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
		 					   m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter());
		         }
	  }

    }
}



} // Namespace ns3
