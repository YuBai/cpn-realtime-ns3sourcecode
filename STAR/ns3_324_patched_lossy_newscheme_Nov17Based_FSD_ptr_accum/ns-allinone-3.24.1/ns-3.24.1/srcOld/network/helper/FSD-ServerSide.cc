#include "FSD-ServerSide.h"
#include "ns3/log.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE ("FSDServerSide");

FSDDelayFeedbacker::FSDDelayFeedbacker ():
m_pst (0),
fullOrNot (false),
lastCompletedTime (0),
timoutThreshold (Seconds(0.0)),
currentLength(INITIAL_PROBE_TRAIN_LENGTH)
{

}

void
FSDDelayFeedbacker::WriteDelaySamples(Ptr<const Packet> packet)
{
	m_dst.WriteDelaySamplesValue(packet, m_delays);

}

void
FSDDelayFeedbacker::RecordDelaySamples(Ptr<const Packet> packet)
{
	m_dst.RecordDelaySamplesValue(packet,m_delays);
}


void
FSDDelayFeedbacker::FinalizeSampleSet()
{
	int j=0;

	if(lastCompletedTime + timoutThreshold < Simulator::Now()) //which means it is a new probing now
	{
		lastCompletedTime = Simulator::Now();
		NS_LOG_UNCOND ("" << Simulator::Now() << ": LAST COMPLETED TIME REFRESHED: " << lastCompletedTime);
	}
	else													   //redundant packets - ignored.
	{
		NS_LOG_UNCOND (""<< Simulator::Now() << ": I GOT REDUNDANT PACKETS. RETURNED");
		return;
	}

	for(j=0; j<MAX_PROBE_TRAIN_LENGTH;j++)
	{
		if(m_delays[j] == 0) //which means packet is not received during probing
		{
			m_delays[j] = std::numeric_limits<double>::max();
			//m_delays[j] = 0;
		}

	}

	NS_LOG_UNCOND("" << Simulator::Now() << "FINILIZED: First Element" << m_delays[0]);
	fullOrNot=true;

}


void
FSDDelayFeedbacker::StoreFSDDelayInfo (Ptr<const Packet> packet)
{
	ProbingDelayEstimation m_pde;
    EnqueueTimeTagger m_ettg;
	EnqueueTimeCompensateTagger m_etct;

	m_pde.RecordDqTime(packet);
	m_pst.RecordProbeSeqValue(packet);
	m_fpt.RecordFinishProbeValue(packet);
	m_ptlt.RecordProbeTrainLengthValue(packet);
	m_mt.RecordMacValue(packet);

	m_ettg.RecordEqTime(packet);
	m_etct.RecordEqTime(packet);

	if(m_ptlt.GetLastProbeTrainLength() != currentLength)
	{
		currentLength = m_ptlt.GetLastProbeTrainLength();
		NS_LOG_UNCOND_YB ("" << Simulator::Now() << "CURRENT LENGTH REFRESHED INTO " << currentLength);
	}


	if(m_pst.GetLastProbeSeq() >= currentLength) //&& m_pst.GetLastProbeSeq() < MAX_PROBE_TRAIN_LENGTH+10) //no && in AP Case
	{
			//NS_LOG_UNCOND ("CURRENTLENGTH REACHED-START FINILIZING WITH m_pst:" << m_pst.GetLastProbeSeq() << " AND m_fpt:" << m_fpt.GetLastFinishProbe());
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << "CURRENTLENGTH REACHED WITH m_pst:" << m_pst.GetLastProbeSeq() << " AND m_fpt:" << m_fpt.GetLastFinishProbe());
			//this->FinalizeSampleSet();
			return;
	}
	else
	{
		this->m_delays[(int)m_pst.GetLastProbeSeq()] = m_pde.GetLastQDelay().GetNanoSeconds();
		//this->m_delays[m_pst.GetLastProbeSeq()]=m_pst.GetLastProbeSeq(); //debugging

		NS_LOG_UNCOND ("" << Simulator::Now() << ": THE LAST SEQ VALUE OBTAINED IS " << m_pst.GetLastProbeSeq() << " WITH FAKE MAC: " << m_mt.GetLastMac());
		NS_LOG_UNCOND ("" << Simulator::Now() << ": LAST SEQ VALUE: " << m_pst.GetLastProbeSeq() << ", Delay: " << this->m_delays[(int)m_pst.GetLastProbeSeq()] << " UID: " << packet->GetUid() << " EnTmpQueue Time: " << m_ettg.GetLastRecordedEnqueueTime() << " Compensate Time: " << m_etct.GetLastRecordedCompensateTime());
		//NS_LOG_UNCOND ("M_PST READING ON SERVER SIDE IS " << m_pst.GetLastProbeSeq());
	}

}


void
FSDDelayFeedbacker::CopyTo (double* delayCopy) //future work: length check. Currently: assume that delayCopy is a double array with size 100
{
	int j=0;
	for(j=0;j<MAX_PROBE_TRAIN_LENGTH;j++)
	{
		delayCopy[j]=m_delays[j];
	}

}

void
FSDDelayFeedbacker::Reset()
{
	memset(m_delays,0, sizeof(m_delays));
	fullOrNot = false;
	//m_pst.ResetLastProbeSeq();
}

bool
FSDDelayFeedbacker::isFull()
{
	return fullOrNot;
}

}
