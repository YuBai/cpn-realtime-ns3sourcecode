/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-third-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeThirdMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeThirdMacTag : public Tag
{
public:
	FakeThirdMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeThirdMac (void) const;
  void SetFakeThirdMac (uint8_t FakeThirdMac) ;

private:
  uint8_t m_FakeThirdMac; //!< The time stored in the tag
};

FakeThirdMacTag::FakeThirdMacTag ()
  : m_FakeThirdMac (0)
{
}

TypeId
FakeThirdMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeThirdMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeThirdMacTag")
    .AddConstructor<FakeThirdMacTag> ()
    .AddAttribute ("LastFakeThirdMac",
                   "Last FakeThirdMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeThirdMacTag::GetFakeThirdMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeThirdMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeThirdMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeThirdMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeThirdMac);
}
void
FakeThirdMacTag::Deserialize (TagBuffer i)
{
  m_FakeThirdMac = i.ReadU8 ();
}
void
FakeThirdMacTag::Print (std::ostream &os) const
{
  os << "m_FakeThirdMac=" << m_FakeThirdMac;
}
uint8_t
FakeThirdMacTag::GetFakeThirdMac (void) const
{
  return m_FakeThirdMac;
}

void
FakeThirdMacTag::SetFakeThirdMac (uint8_t FakeThirdMac)
{
	m_FakeThirdMac=FakeThirdMac;
}

FakeThirdMacTagger::FakeThirdMacTagger ()
{
	this->lastFakeThirdMac=0.0;
}

void
FakeThirdMacTagger::WriteFakeThirdMacValue (Ptr<const Packet> packet)
{
  FakeThirdMacTag tag;
  tag.SetFakeThirdMac(this->lastFakeThirdMac);
  packet->AddByteTag (tag);
}
void
FakeThirdMacTagger::RecordFakeThirdMacValue (Ptr<const Packet> packet)
{
  FakeThirdMacTag tag;
  lastFakeThirdMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeThirdMac=tag.GetFakeThirdMac ();

}

uint8_t
FakeThirdMacTagger::GetLastFakeThirdMac (void) const
{
  return lastFakeThirdMac;
}

void
FakeThirdMacTagger::SetLastFakeThirdMac (uint8_t FakeThirdMac)
{
	lastFakeThirdMac=FakeThirdMac;
}


} // namespace ns3
