/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "op-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Opform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class OpTag : public Tag
{
public:
	OpTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetOp (void) const;
  void SetOp (double Op) ;

private:
  double m_Op; //!< The time stored in the tag
};

OpTag::OpTag ()
  : m_Op (0.0)
{
}

TypeId
OpTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::OpTag")
    .SetParent<Tag> ()
    .SetGroupName("OpTag")
    .AddConstructor<OpTag> ()
    .AddAttribute ("LastOp",
                   "Last Op Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&OpTag::GetOp),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
OpTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
OpTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
OpTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Op);
}
void
OpTag::Deserialize (TagBuffer i)
{
  m_Op = i.ReadDouble ();
}
void
OpTag::Print (std::ostream &os) const
{
  os << "m_Op=" << m_Op;
}
double
OpTag::GetOp (void) const
{
  return m_Op;
}

void
OpTag::SetOp (double Op)
{
	m_Op=Op;
}

OpTagger::OpTagger ()
{
	this->lastOp=0.0;
}

void
OpTagger::WriteOpValue (Ptr<const Packet> packet, double tagValue)
{
  OpTag tag;
  tag.SetOp(tagValue);
  packet->AddByteTag (tag);
}
int
OpTagger::RecordOpValue (Ptr<const Packet> packet)
{
  OpTag tag;
  lastOp=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastOp=tag.GetOp ();
  return (int)lastOp;
}

double
OpTagger::GetLastOp (void) const
{
  return lastOp;
}

} // namespace ns3
