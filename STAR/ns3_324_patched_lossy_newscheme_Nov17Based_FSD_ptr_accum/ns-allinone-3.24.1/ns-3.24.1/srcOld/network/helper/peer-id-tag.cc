/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "peer-id-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to PeerIdform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class PeerIdTag : public Tag
{
public:
	PeerIdTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetPeerId (void) const;
  void SetPeerId (double PeerId) ;

private:
  double m_PeerId; //!< The time stored in the tag
};

PeerIdTag::PeerIdTag ()
  : m_PeerId (0.0)
{
}

TypeId
PeerIdTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::PeerIdTag")
    .SetParent<Tag> ()
    .SetGroupName("PeerIdTag")
    .AddConstructor<PeerIdTag> ()
    .AddAttribute ("LastPeerId",
                   "Last PeerId Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&PeerIdTag::GetPeerId),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
PeerIdTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
PeerIdTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
PeerIdTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_PeerId);
}
void
PeerIdTag::Deserialize (TagBuffer i)
{
  m_PeerId = i.ReadDouble ();
}
void
PeerIdTag::Print (std::ostream &os) const
{
  os << "m_PeerId=" << m_PeerId;
}
double
PeerIdTag::GetPeerId (void) const
{
  return m_PeerId;
}

void
PeerIdTag::SetPeerId (double PeerId)
{
	m_PeerId=PeerId;
}

PeerIdTagger::PeerIdTagger ()
{
	this->lastPeerId=0.0;
}

void
PeerIdTagger::WritePeerIdValue (Ptr<const Packet> packet, double tagValue)
{
  PeerIdTag tag;
  tag.SetPeerId(tagValue);
  packet->AddByteTag (tag);
}
int
PeerIdTagger::RecordPeerIdValue (Ptr<const Packet> packet)
{
  PeerIdTag tag;
  lastPeerId=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastPeerId=tag.GetPeerId ();
  return (int)lastPeerId;
}

double
PeerIdTagger::GetLastPeerId (void) const
{
  return lastPeerId;
}

} // namespace ns3
