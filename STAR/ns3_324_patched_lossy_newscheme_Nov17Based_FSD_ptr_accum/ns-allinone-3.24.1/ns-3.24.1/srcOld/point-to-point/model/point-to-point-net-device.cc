/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007, 2008 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/log.h"
#include "ns3/queue.h"
#include "ns3/simulator.h"
#include "ns3/mac48-address.h"
#include "ns3/llc-snap-header.h"
#include "ns3/error-model.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/uinteger.h"
#include "ns3/pointer.h"
#include "point-to-point-net-device.h"
#include "point-to-point-channel.h"
#include "ppp-header.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("PointToPointNetDevice");

NS_OBJECT_ENSURE_REGISTERED (PointToPointNetDevice);

TypeId 
PointToPointNetDevice::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::PointToPointNetDevice")
    .SetParent<NetDevice> ()
    .SetGroupName ("PointToPoint")
    .AddConstructor<PointToPointNetDevice> ()
    .AddAttribute ("Mtu", "The MAC-level Maximum Transmission Unit",
                   UintegerValue (DEFAULT_MTU),
                   MakeUintegerAccessor (&PointToPointNetDevice::SetMtu,
                                         &PointToPointNetDevice::GetMtu),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("Address", 
                   "The MAC address of this device.",
                   Mac48AddressValue (Mac48Address ("ff:ff:ff:ff:ff:ff")),
                   MakeMac48AddressAccessor (&PointToPointNetDevice::m_address),
                   MakeMac48AddressChecker ())
    .AddAttribute ("DataRate", 
                   "The default data rate for point to point links",
                   DataRateValue (DataRate ("32768b/s")),
                   MakeDataRateAccessor (&PointToPointNetDevice::m_bps),
                   MakeDataRateChecker ())
    .AddAttribute ("ReceiveErrorModel", 
                   "The receiver error model used to simulate packet loss",
                   PointerValue (),
                   MakePointerAccessor (&PointToPointNetDevice::m_receiveErrorModel),
                   MakePointerChecker<ErrorModel> ())
    .AddAttribute ("InterframeGap", 
                   "The time to wait between packet (frame) transmissions",
                   TimeValue (Seconds (0.0)),
                   MakeTimeAccessor (&PointToPointNetDevice::m_tInterframeGap),
                   MakeTimeChecker ())

    //
    // Transmit queueing discipline for the device which includes its own set
    // of trace hooks.
    //
    .AddAttribute ("TxQueue", 
                   "A queue to use as the transmit queue in the device.",
                   PointerValue (),
                   MakePointerAccessor (&PointToPointNetDevice::m_queue),
                   MakePointerChecker<Queue> ())

    //
    // Trace sources at the "top" of the net device, where packets transition
    // to/from higher layers.
    //
    .AddTraceSource ("MacTx", 
                     "Trace source indicating a packet has arrived "
                     "for transmission by this device",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_macTxTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("MacTxDrop", 
                     "Trace source indicating a packet has been dropped "
                     "by the device before transmission",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_macTxDropTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("MacPromiscRx", 
                     "A packet has been received by this device, "
                     "has been passed up from the physical layer "
                     "and is being forwarded up the local protocol stack.  "
                     "This is a promiscuous trace,",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_macPromiscRxTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("MacRx", 
                     "A packet has been received by this device, "
                     "has been passed up from the physical layer "
                     "and is being forwarded up the local protocol stack.  "
                     "This is a non-promiscuous trace,",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_macRxTrace),
                     "ns3::Packet::TracedCallback")
#if 0
    // Not currently implemented for this device
    .AddTraceSource ("MacRxDrop", 
                     "Trace source indicating a packet was dropped "
                     "before being forwarded up the stack",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_macRxDropTrace),
                     "ns3::Packet::TracedCallback")
#endif
    //
    // Trace souces at the "bottom" of the net device, where packets transition
    // to/from the channel.
    //
    .AddTraceSource ("PhyTxBegin", 
                     "Trace source indicating a packet has begun "
                     "transmitting over the channel",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyTxBeginTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("PhyTxEnd", 
                     "Trace source indicating a packet has been "
                     "completely transmitted over the channel",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyTxEndTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("PhyTxDrop", 
                     "Trace source indicating a packet has been "
                     "dropped by the device during transmission",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyTxDropTrace),
                     "ns3::Packet::TracedCallback")
#if 0
    // Not currently implemented for this device
    .AddTraceSource ("PhyRxBegin", 
                     "Trace source indicating a packet has begun "
                     "being received by the device",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyRxBeginTrace),
                     "ns3::Packet::TracedCallback")
#endif
    .AddTraceSource ("PhyRxEnd", 
                     "Trace source indicating a packet has been "
                     "completely received by the device",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyRxEndTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("PhyRxDrop", 
                     "Trace source indicating a packet has been "
                     "dropped by the device during reception",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_phyRxDropTrace),
                     "ns3::Packet::TracedCallback")

    //
    // Trace sources designed to simulate a packet sniffer facility (tcpdump).
    // Note that there is really no difference between promiscuous and 
    // non-promiscuous traces in a point-to-point link.
    //
    .AddTraceSource ("Sniffer", 
                    "Trace source simulating a non-promiscuous packet sniffer "
                     "attached to the device",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_snifferTrace),
                     "ns3::Packet::TracedCallback")
    .AddTraceSource ("PromiscSniffer", 
                     "Trace source simulating a promiscuous packet sniffer "
                     "attached to the device",
                     MakeTraceSourceAccessor (&PointToPointNetDevice::m_promiscSnifferTrace),
                     "ns3::Packet::TracedCallback")
  ;
  return tid;
}


PointToPointNetDevice::PointToPointNetDevice () 
  :
    m_txMachineState (READY),
    m_channel (0),
    m_linkUp (false),
    m_currentPkt (0),
	m_pst (0.0),
	m_fdfb_one(MAX_FSD_CLIENT_ALLOWED_P),
	m_fdfb_two(MAX_FSD_CLIENT_ALLOWED_P),
//	lastTrainLengthOne(-1),
//	lastTrainLengthTwo(-1),
//	lastTriggeringTimeOne (-1),
//	lastTriggeringTimeTwo (-1),
	lastTrainLengthOne(MAX_FSD_CLIENT_ALLOWED_P),
	lastTrainLengthTwo(MAX_FSD_CLIENT_ALLOWED_P),
	lastTriggeringTimeOne(MAX_FSD_CLIENT_ALLOWED_P),
	lastTriggeringTimeTwo(MAX_FSD_CLIENT_ALLOWED_P),
	probingTimeOutPath1 (MAX_FSD_CLIENT_ALLOWED_P),
	probingTimeOutPath2 (MAX_FSD_CLIENT_ALLOWED_P),
	lastTimeStampPath1 (MAX_FSD_CLIENT_ALLOWED_P),
	lastTimeStampPath2 (MAX_FSD_CLIENT_ALLOWED_P)
{
  NS_LOG_FUNCTION (this);

  ObjectFactory m_queueFactoryT;
  m_queueFactoryT.SetTypeId("ns3::DropTailQueue");
  m_cachedQueue.resize(MAX_FSD_CLIENT_ALLOWED_P);
  for(int i=0;i<MAX_FSD_CLIENT_ALLOWED_P;i++)
  {
	  m_cachedQueue[i]=m_queueFactoryT.Create<Queue>();
	  m_cachedQueue[i]->SetAttribute("MaxPackets",UintegerValue (10000000));
  }

  m_probingOrNot.resize(MAX_FSD_CLIENT_ALLOWED_P);
  std::fill(m_probingOrNot.begin(),m_probingOrNot.end(),0);

  lastProbeTime.resize(MAX_FSD_CLIENT_ALLOWED_P);
  std::fill(lastProbeTime.begin(),lastProbeTime.end(),-1);

  lastTriggeringTime.resize(MAX_FSD_CLIENT_ALLOWED_P);
  std::fill(lastTriggeringTime.begin(),lastTriggeringTime.end(),Seconds(0));

  std::fill(lastTrainLengthOne.begin(),lastTrainLengthOne.end(),-1);
  std::fill(lastTrainLengthTwo.begin(),lastTrainLengthTwo.end(),-1);
  std::fill(lastTriggeringTimeOne.begin(),lastTriggeringTimeOne.end(),-1);
  std::fill(lastTriggeringTimeTwo.begin(),lastTriggeringTimeTwo.end(),-1);
  std::fill(probingTimeOutPath1.begin(),probingTimeOutPath1.end(),Seconds(10));
  std::fill(probingTimeOutPath2.begin(),probingTimeOutPath2.end(),Seconds(10));
  std::fill(lastTimeStampPath1.begin(),lastTimeStampPath1.end(),Seconds(0));
  std::fill(lastTimeStampPath2.begin(),lastTimeStampPath2.end(),Seconds(0));

  m_fpt.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_ett.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_macPathOne.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_macPathTwo.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_fmt_one.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_fmt_two.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_apt.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_finipt.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_ptlt_checker.resize(MAX_FSD_CLIENT_ALLOWED_P);
  m_ptt_checker.resize(MAX_FSD_CLIENT_ALLOWED_P);
  pkt_PathOne.resize(MAX_FSD_CLIENT_ALLOWED_P);
  pkt_PathTwo.resize(MAX_FSD_CLIENT_ALLOWED_P);
  Packet_PathOneCopy.resize(MAX_FSD_CLIENT_ALLOWED_P);
  Packet_PathTwoCopy.resize(MAX_FSD_CLIENT_ALLOWED_P);
  //m_queue->SetAttribute("MaxPackets",UintegerValue (10000000));
}

PointToPointNetDevice::~PointToPointNetDevice ()
{
  NS_LOG_FUNCTION (this);
}

void
PointToPointNetDevice::AddHeader (Ptr<Packet> p, uint16_t protocolNumber)
{
  NS_LOG_FUNCTION (this << p << protocolNumber);
  PppHeader ppp;
  ppp.SetProtocol (EtherToPpp (protocolNumber));
  p->AddHeader (ppp);
}

bool
PointToPointNetDevice::ProcessHeader (Ptr<Packet> p, uint16_t& param)
{
  NS_LOG_FUNCTION (this << p << param);
  PppHeader ppp;
  p->RemoveHeader (ppp);
  param = PppToEther (ppp.GetProtocol ());
  return true;
}

void
PointToPointNetDevice::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_node = 0;
  m_channel = 0;
  m_receiveErrorModel = 0;
  m_currentPkt = 0;
  NetDevice::DoDispose ();
}

void
PointToPointNetDevice::SetDataRate (DataRate bps)
{
  NS_LOG_FUNCTION (this);
  m_bps = bps;
}

void
PointToPointNetDevice::SetInterframeGap (Time t)
{
  NS_LOG_FUNCTION (this << t.GetSeconds ());
  m_tInterframeGap = t;
}

bool
PointToPointNetDevice::TransmitStart (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this << p);
  NS_LOG_LOGIC ("UID is " << p->GetUid () << ")");

  //Debugging
  FakePktTagger m_fpt_tmp;
  FakeMacTagger m_fmt_tmp;
  AlterPathTagger m_apt_tmp;
  ProbeTimeTagger m_ptt_tmp;
  ProbeTrainLengthTagger m_ptlt_tmp;
  m_fpt_tmp.RecordFakePktValue(p);
  m_fmt_tmp.RecordFakeMacValue(p);
  m_apt_tmp.RecordAlterPathValue(p);
  m_ptt_tmp.RecordProbeTimeValue(p);
  m_ptlt_tmp.RecordProbeTrainLengthValue(p);
  if(m_fpt_tmp.GetLastFakePkt() != 0)
  {
    NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": P2P Device " << GetAddress() << " Detect Fake Packet in PointToPointNetDevice::TransmitStart() with m_fpt=" << m_fpt_tmp.GetLastFakePkt() << " m_fmt=" << m_fmt_tmp.GetLastFakeMac() << " m_apt=" << m_apt_tmp.GetLastAlterPath() << " m_ptt=" << m_ptt_tmp.GetLastProbeTime() << " m_ptlt=" << m_ptlt_tmp.GetLastProbeTrainLength());
  }

  //
  // This function is called to start the process of transmitting a packet.
  // We need to tell the channel that we've started wiggling the wire and
  // schedule an event that will be executed when the transmission is complete.
  //
  NS_ASSERT_MSG (m_txMachineState == READY, "Must be READY to transmit");
  m_txMachineState = BUSY;
  m_currentPkt = p;
  m_phyTxBeginTrace (m_currentPkt);

  Time txTime = m_bps.CalculateBytesTxTime (p->GetSize ());
  Time txCompleteTime = txTime + m_tInterframeGap;

  NS_LOG_LOGIC ("Schedule TransmitCompleteEvent in " << txCompleteTime.GetSeconds () << "sec");
  Simulator::Schedule (txCompleteTime, &PointToPointNetDevice::TransmitComplete, this);

  bool result = m_channel->TransmitStart (p, this, txTime);
  if (result == false)
    {
      m_phyTxDropTrace (p);
    }
  return result;
}

void
PointToPointNetDevice::TransmitComplete (void)
{
  NS_LOG_FUNCTION (this);

  //
  // This function is called to when we're all done transmitting a packet.
  // We try and pull another packet off of the transmit queue.  If the queue
  // is empty, we are done, otherwise we need to start transmitting the
  // next packet.
  //
  NS_ASSERT_MSG (m_txMachineState == BUSY, "Must be BUSY if transmitting");
  m_txMachineState = READY;

  NS_ASSERT_MSG (m_currentPkt != 0, "PointToPointNetDevice::TransmitComplete(): m_currentPkt zero");

  m_phyTxEndTrace (m_currentPkt);
  m_currentPkt = 0;

  Ptr<Packet> p = m_queue->Dequeue ();
  if (p == 0)
    {
      //
      // No packet was on the queue, so we just exit.
      //
      return;
    }

  //
  // Got another packet off of the queue, so start the transmit process agin.
  //
  m_snifferTrace (p);
  m_promiscSnifferTrace (p);
  TransmitStart (p);
}

bool
PointToPointNetDevice::Attach (Ptr<PointToPointChannel> ch)
{
  NS_LOG_FUNCTION (this << &ch);

  m_channel = ch;

  m_channel->Attach (this);

  //
  // This device is up whenever it is attached to a channel.  A better plan
  // would be to have the link come up when both devices are attached, but this
  // is not done for now.
  //
  NotifyLinkUp ();
  return true;
}

void
PointToPointNetDevice::SetQueue (Ptr<Queue> q)
{
  NS_LOG_FUNCTION (this << q);
  m_queue = q;
}

void
PointToPointNetDevice::SetReceiveErrorModel (Ptr<ErrorModel> em)
{
  NS_LOG_FUNCTION (this << em);
  m_receiveErrorModel = em;
}

void
PointToPointNetDevice::SynchronizeProbingOrNot(int _nodeID, int _m_probingOrNot) //in all-node version this should be set with parameter indicating which node is probing
{
	for(unsigned int i=0;i<2;i++)
	{
		Ptr<NetDevice> tmpD=GetNode()->GetDevice(i);
		Ptr<PointToPointNetDevice> tmpP=DynamicCast<PointToPointNetDevice>(tmpD);
		//NS_LOG_UNCOND("" << Simulator::Now() << ": i=" << i << " and mac address is " << tmpP->GetAddress());
		tmpP->SetProbingOrNot(_nodeID, _m_probingOrNot);
	}

}

void
PointToPointNetDevice::PourPktsToSelectedDevice(int _nodeID)
//PointToPointNetDevice::PourPktsToSelectedDevice(uint32_t localID)
{
	for(uint32_t i=0;i<2;i++)
	{
		Ptr<NetDevice> tmpD=GetNode()->GetDevice(i);
		Ptr<PointToPointNetDevice> tmpP=DynamicCast<PointToPointNetDevice>(tmpD);

		if(tmpP->GetAddress() == GetAddress())
		{
			NS_LOG_UNCOND_YB("ENTER INTO SAME: Cached Queue Length is " << tmpP->GetCachedQueue(_nodeID)->GetNPackets());
			continue;
		}
		else
		{
			NS_LOG_UNCOND_YB("ENTER INTO POUR ELSE: Cached Queue Length is " << tmpP->GetCachedQueue(_nodeID)->GetNPackets());
			while(!tmpP->GetCachedQueue(_nodeID)->IsEmpty())
			{
				//tmpP->GetCachedQueue()->Enqueue(m_cachedQueue->Dequeue());
//				Ptr<Packet> tmpPkt=tmpP->GetCachedQueue()->Dequeue();
//				NS_LOG_UNCOND("" << Simulator::Now() << ": PourPktsToselectedDevice: move cached pkt with UID: " << tmpPkt->GetUid() << " from this device to device " << i);
//				m_cachedQueue->Enqueue(tmpPkt);
				//tmpP->GetCachedQueue()->Enqueue(m_cachedQueue->Dequeue());
				Ptr<Packet> tmpPkt=tmpP->GetCachedQueue(_nodeID)->Dequeue();
				NS_LOG_UNCOND("" << Simulator::Now() << ": PourPktsToselectedDevice: move cached pkt with UID: " << tmpPkt->GetUid() << " from this device to device " << i);
				m_cachedQueue[_nodeID]->Enqueue(tmpPkt);
			}
		}

	}
//	Ptr<NetDevice> tmpD=GetNode()->GetDevice(localID);
//	Ptr<PointToPointNetDevice> tmpP=DynamicCast<PointToPointNetDevice>(tmpD);
//	NS_LOG_UNCOND("" << Simulator::Now() << "PourPktsToSelectedDevice: There are " << this->GetQueue()->GetNPackets() << " in total cached in queue!");
//	while(!m_cachedQueue->IsEmpty())
//	{
//		tmpP->GetCachedQueue()->Enqueue(m_cachedQueue->Dequeue());
//	}

}

void
PointToPointNetDevice::Receive (Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << packet);
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Receive Entered.");

  FakePktTagger m_fpt_tmp;
  FakeMacTagger m_fmt_tmp;
  AlterPathTagger m_apt_tmp;
  ProbeTimeTagger m_ptt_tmp;
  ProbeTrainLengthTagger m_ptlt_tmp;
  PortTagger m_pttag;
  //IdTagger m_id_tmp (0.0);
  IdTagger m_it_tmp (0.0);

  m_fpt_tmp.RecordFakePktValue(packet);
  m_fmt_tmp.RecordFakeMacValue(packet);
  m_apt_tmp.RecordAlterPathValue(packet);
  m_ptt_tmp.RecordProbeTimeValue(packet);
  m_ptlt_tmp.RecordProbeTrainLengthValue(packet);
  m_pttag.RecordPortValue(packet);
  //m_id_tmp.RecordIdValue(packet);
  m_it_tmp.RecordIdValue(packet);

  if(m_fpt_tmp.GetLastFakePkt() != 0)
  {
    NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": P2P Device " << GetAddress() << " Detect Fake Packet in PointToPointNetDevice::Receive() with m_fpt=" << m_fpt_tmp.GetLastFakePkt() << " m_fmt=" << m_fmt_tmp.GetLastFakeMac() << " m_apt=" << m_apt_tmp.GetLastAlterPath() << " m_ptt=" << m_ptt_tmp.GetLastProbeTime() << " m_ptlt=" << m_ptlt_tmp.GetLastProbeTrainLength());
  }

  //Database Log
  NS_LOG_UNCOND ("" << Simulator::Now() << ": P2P Device " << GetAddress() << "Receive Packet with UID " << packet->GetUid());


  uint16_t protocol = 0;
  ProbeTagger m_pt;
  MacTagger m_mt;

  m_pt.RecordProbeValue(packet);
  m_mt.RecordMacValue(packet);
  m_pst.RecordProbeSeqValue(packet);
  m_apt[m_it_tmp.GetLastId()].RecordAlterPathValue(packet);
  m_ett[m_it_tmp.GetLastId()].RecordEndOfTrainValue(packet);
  //For Debug
  //if(packet->GetSize() > 1000)
  //{
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":" << GetAddress() << "m_pt=" << m_pt.GetLastProbe() << " m_mt=" << m_mt.GetLastMac() << " m_pst=" << m_pst.GetLastProbeSeq());
  //}


  //very dirty hacking - need more flexibility in the future
  //Mac48Address targetAddr ("00:00:00:00:00:02");
  //if(GetAddress() == targetAddr)
  Mac48Address targetAddr1 ("00:00:00:00:00:01");
  Mac48Address targetAddr2 ("00:00:00:00:00:03");
  if(GetAddress() == targetAddr1 || GetAddress() == targetAddr2)
  {
	  //For New Scheme - Very Dirty Hack - Start
	  if(GetAddress() == targetAddr1)
	  {
		  if(m_mt.GetLastMac() == "00:00:00:00:00:06")
		  {
			  NS_LOG_UNCOND ("" << Simulator::Now() << ": There is something wrong on " << GetAddress() << " - I received a packet with mac tag: " << m_mt.GetLastMac() << ": Dropped.");
			  return;
		  }
	  }
	  if(GetAddress() == targetAddr2)
	  {
		  if(m_mt.GetLastMac() == "00:00:00:00:00:05")
		  {
			  NS_LOG_UNCOND ("" << Simulator::Now() << ": There is something wrong on " << GetAddress() << " - I received a packet with mac tag: " << m_mt.GetLastMac() << ": Dropped.");
			  return;
		  }
	  }
	  //For New Scheme - Very Dirty Hack - End

	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "PointToPointNetDevice::Receive Triggered on " << GetAddress() << "with m_pt=" << m_pt.GetLastProbe()
			  << ", ID: " << GetNode()->GetId() << ", m_pttag.GetLastPort: " << m_pttag.GetLastPort() << ", m_pt.GetLastProbe(): " << m_pt.GetLastProbe());

	  if(GetNode()->GetId() == 0 && m_pttag.GetLastPort() == 9 && m_it_tmp.GetLastId() >=3 && m_pt.GetLastProbe() == 0 && Simulator::Now() > Seconds(7)) //stupid modification
	  {
		  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and I have received a packet from port 11 without probe tag set!");
		  m_probingOrNot[m_it_tmp.GetLastId()]=0;
		  SynchronizeProbingOrNot(m_it_tmp.GetLastId(),m_probingOrNot[m_it_tmp.GetLastId()]);

		  //put cached packets into corresponding queue
		  //get my local device ID (since this phenomenon indicates that this device is selected after FSD) so others could know which cache queue they should go
//		  uint32_t localID;
//		  for(unsigned int i=0;i<2;i++)
//		  {
//			  Ptr<NetDevice> tmpD=GetNode()->GetDevice(i);
//			  Ptr<PointToPointNetDevice> tmpP=DynamicCast<PointToPointNetDevice>(tmpD);
//			  if(tmpP->GetAddress() == GetAddress())
//			  {
//				  localID=tmpP->GetNode()->GetId();
//				  break;
//			  }
//		  }
		  //PourPktsToSelectedDevice(localID);
		  PourPktsToSelectedDevice(m_it_tmp.GetLastId());
	  }

	  if(m_pt.GetLastProbe() != 0)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_pt:1 " << GetAddress() << "m_pst:" << m_pst.GetLastProbeSeq());
		  m_ptlt_checker[m_it_tmp.GetLastId()].RecordProbeTrainLengthValue(packet);
		  m_ptt_checker[m_it_tmp.GetLastId()].RecordProbeTimeValue(packet);

		  if(m_probingOrNot[m_it_tmp.GetLastId()] == 0 && lastProbeTime[m_it_tmp.GetLastId()] != m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime() && GetNode()->GetId() == 0)
		  {
			  NS_LOG_UNCOND("" << Simulator::Now() << ": a new probing period start with probe time " << m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime() << " from node with ID " << m_it_tmp.GetLastId() << " and UID: " << packet->GetUid() << " and I am " << GetAddress());
			  lastProbeTime[m_it_tmp.GetLastId()] = m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime();
			  lastTriggeringTime[m_it_tmp.GetLastId()]=Simulator::Now();
			  m_probingOrNot[m_it_tmp.GetLastId()] = 1;
			  SynchronizeProbingOrNot(m_it_tmp.GetLastId(),m_probingOrNot[m_it_tmp.GetLastId()]);

			  //newly added for timeout setting - guarantee that samples gathered last time will not affect this time's sampling result
			  m_fdfb_one[m_it_tmp.GetLastId()].Reset();
			  m_fdfb_two[m_it_tmp.GetLastId()].Reset();

			  NS_LOG_UNCOND("SYNC DONE.");
		  }

		  //if(m_mt.GetLastMac() != 0)
		  //{
			//  NS_LOG_UNCOND ("m_pt:" << m_pt.GetLastProbe() << "m_mt:" << m_mt.GetLastMac());
		  //}
		  //Mac48Address tmp ("00:00:00:00:00:00");
		  if(m_apt[m_it_tmp.GetLastId()].GetLastAlterPath() != 0)
		  {
			  lastTimeStampPath2[m_it_tmp.GetLastId()]=Simulator::Now();
			  //then schedule an event after (lastTimeStampPath1+probingTimeOutPath1) seconds - if at that time lastTimeStampPath1+probingTimeOutPath1=Simulator::Now() then it implies we did not receive anything on probing thus we need to finalize the first probing train
			  Packet_PathTwoCopy[m_it_tmp.GetLastId()]=packet->Copy();

			  if(m_ett[m_it_tmp.GetLastId()].GetLastEndOfTrain() != 0 && !m_fdfb_two[m_it_tmp.GetLastId()].isFull())
			  {
				  NS_LOG_UNCOND ("" << Simulator::Now() << "END OF TRAIN RECEIVED ON SERVER SIDE FOR CASE II WITH LOCAL ADDRESS " << GetAddress());
				  //return;
				  if(lastTrainLengthTwo[m_it_tmp.GetLastId()] == -1 && lastTriggeringTimeTwo[m_it_tmp.GetLastId()] == -1)
				  {
					  lastTrainLengthTwo[m_it_tmp.GetLastId()] = m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength();
					  lastTriggeringTimeTwo[m_it_tmp.GetLastId()] = m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime();
					  m_fdfb_two[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
					  m_fdfb_two[m_it_tmp.GetLastId()].FinalizeSampleSet();

					  pkt_PathTwo[m_it_tmp.GetLastId()] = packet->Copy();

					  NS_LOG_UNCOND ("" << Simulator::Now() << "FINILIZED m_fdfb_two DUE TO " << lastTrainLengthTwo[m_it_tmp.GetLastId()] << "-" << lastTriggeringTimeTwo[m_it_tmp.GetLastId()]);
				  }
				  else if(lastTrainLengthTwo[m_it_tmp.GetLastId()] != m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength()
						  || lastTriggeringTimeTwo[m_it_tmp.GetLastId()] != m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime())
				  {
					  //meaning this is a new one.
					  lastTrainLengthTwo[m_it_tmp.GetLastId()] = m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength();
					  lastTriggeringTimeTwo[m_it_tmp.GetLastId()] = m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime();
					  m_fdfb_two[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
					  m_fdfb_two[m_it_tmp.GetLastId()].FinalizeSampleSet();

					  pkt_PathTwo[m_it_tmp.GetLastId()] = packet->Copy();

					  NS_LOG_UNCOND ("" << Simulator::Now() << "FINILIZED m_fdfb_two DUE TO " << lastTrainLengthTwo[m_it_tmp.GetLastId()] << "-" << lastTriggeringTimeTwo[m_it_tmp.GetLastId()]);
				  }

			  }
			  else if(m_ett[m_it_tmp.GetLastId()].GetLastEndOfTrain() != 0)
			  {
				  NS_LOG_UNCOND ("" << Simulator::Now() << "UNNECESSARY EOF TRAIN PKT RECEIVED.");
			  }
			  else
			  {
				  m_fdfb_two[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
				  pkt_PathTwo[m_it_tmp.GetLastId()] = packet->Copy();
				  m_macPathTwo[m_it_tmp.GetLastId()] = m_mt.GetLastMac();
				  m_fmt_two[m_it_tmp.GetLastId()].SetLastFakeMac(m_macPathTwo[m_it_tmp.GetLastId()]);
				  NS_LOG_UNCOND ("" << Simulator::Now() << "RECEIVE CASE II: " << this << " m_pt:" << m_pt.GetLastProbe() << " m_mt:" << m_mt.GetLastMac() << " m_fmt_two:" << m_fmt_two[m_it_tmp.GetLastId()].GetLastFakeMac());
				  Simulator::Schedule (probingTimeOutPath2[m_it_tmp.GetLastId()], &PointToPointNetDevice::FinalizeFdfb2AndSend, this,m_it_tmp.GetLastId());

			  }

		  }
		  else
		  {
			  NS_LOG_UNCOND_YB("IN 1");

			  lastTimeStampPath1[m_it_tmp.GetLastId()]=Simulator::Now();
			  //then schedule an event after (lastTimeStampPath1+probingTimeOutPath2) seconds - if at that time lastTimeStampPath2+probingTimeOutPath2=Simulator::Now() then it implies we did not receive anything on probing thus we need to finalize the first probing train
			  Packet_PathOneCopy[m_it_tmp.GetLastId()]=packet->Copy();

			  if(m_ett[m_it_tmp.GetLastId()].GetLastEndOfTrain() != 0 && !m_fdfb_one[m_it_tmp.GetLastId()].isFull())
			  {
			  	  NS_LOG_UNCOND ("" << Simulator::Now() << "END OF TRAIN RECEIVED ON SERVER SIDE FOR CASE I WITH LOCAL ADDRESS: " << GetAddress());
			  	  //return;
			  	  //m_fdfb_one.FinalizeSampleSet();
				  if(lastTrainLengthOne[m_it_tmp.GetLastId()] == -1 && lastTriggeringTimeOne[m_it_tmp.GetLastId()] == -1)
				  {
					  lastTrainLengthOne[m_it_tmp.GetLastId()] = m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength();
					  lastTriggeringTimeOne[m_it_tmp.GetLastId()] = m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime();
					  m_fdfb_one[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
					  m_fdfb_one[m_it_tmp.GetLastId()].FinalizeSampleSet();

					  pkt_PathOne[m_it_tmp.GetLastId()] = packet->Copy();

					  NS_LOG_UNCOND ("" << Simulator::Now() << "FINILIZED m_fdfb_one DUE TO " << lastTrainLengthOne[m_it_tmp.GetLastId()] << "-" << lastTriggeringTimeOne[m_it_tmp.GetLastId()]);
				  }
				  else if(lastTrainLengthOne[m_it_tmp.GetLastId()] != m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength()
						  || lastTriggeringTimeOne[m_it_tmp.GetLastId()] != m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime())
				  {
					  //meaning this is a new one.
					  lastTrainLengthOne[m_it_tmp.GetLastId()] = m_ptlt_checker[m_it_tmp.GetLastId()].GetLastProbeTrainLength();
					  lastTriggeringTimeOne[m_it_tmp.GetLastId()] = m_ptt_checker[m_it_tmp.GetLastId()].GetLastProbeTime();
					  m_fdfb_one[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
					  m_fdfb_one[m_it_tmp.GetLastId()].FinalizeSampleSet();

					  pkt_PathOne[m_it_tmp.GetLastId()] = packet->Copy();

					  NS_LOG_UNCOND ("" << Simulator::Now() << "FINILIZED m_fdfb_one DUE TO " << lastTrainLengthOne[m_it_tmp.GetLastId()] << "-" << lastTriggeringTimeOne[m_it_tmp.GetLastId()]);
				  }

			  }
			  else if(m_ett[m_it_tmp.GetLastId()].GetLastEndOfTrain() != 0)
			  {
				  NS_LOG_UNCOND ("" << Simulator::Now() << "UNNECESSARY EOF TRAIN PKT RECEIVED.");
			  }
			  else
			  {
				  NS_LOG_UNCOND_YB("IN 2");

				  m_fdfb_one[m_it_tmp.GetLastId()].StoreFSDDelayInfo(packet);
				  m_macPathOne[m_it_tmp.GetLastId()] = m_mt.GetLastMac();
				  pkt_PathOne[m_it_tmp.GetLastId()] = packet->Copy();
				  m_fmt_one[m_it_tmp.GetLastId()].SetLastFakeMac(m_macPathOne[m_it_tmp.GetLastId()]);
				  NS_LOG_UNCOND ("" << Simulator::Now() << "RECEIVE CASE I: " << this << " m_pt:" << m_pt.GetLastProbe() << " m_mt:" << m_mt.GetLastMac() << " m_fmt_one:" << m_fmt_one[m_it_tmp.GetLastId()].GetLastFakeMac());
				  Simulator::Schedule (probingTimeOutPath1[m_it_tmp.GetLastId()], &PointToPointNetDevice::FinalizeFdfb1AndSend, this, m_it_tmp.GetLastId());

			  }

		  }

		  //Time to Feedback Obtained Delay Information
		  if(m_fdfb_one[m_it_tmp.GetLastId()].isFull())
		  {
			  //Ptr<Packet>fakePacket = Create<Packet>(1400);
			  //Ptr<Node>node = this->GetNode();
			  //Ptr<Application>udpServer = node->GetApplication(0);
			  //Ptr<Socket>socket = udpServer->GetSocket();
			  //socket->Send(packet);

			  NS_LOG_UNCOND ("" << Simulator::Now() << "ENTER INTO M_FDFB_ONE FULL CASE.");
			  //Dirty Hacking - Take Some Advantage of CBR Flow
			  //Since One Tag Can Be Added for One Packet - We Generate another Copy for Current Packet
			  //Ptr<Packet>tmp1 = packet->Copy();
			  //Ptr<Packet>tmp2 = packet->Copy();

		      ProcessHeader (pkt_PathOne[m_it_tmp.GetLastId()], protocol);
		      NS_LOG_UNCOND_YB ("" << Simulator::Now() << "HEAD PROCESSED.");

			  m_fdfb_one[m_it_tmp.GetLastId()].WriteDelaySamples(pkt_PathOne[m_it_tmp.GetLastId()]);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "DelaySamples Written.");

			  //m_fdfb_one.RecordDelaySamples(pkt_PathOne);
			  //NS_LOG_UNCOND ("DelaySamples Recorded.");

			  m_fpt[m_it_tmp.GetLastId()].WriteFakePktValue(pkt_PathOne[m_it_tmp.GetLastId()],1);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Fake Pkt Value Written.");

			  m_fpt[m_it_tmp.GetLastId()].RecordFakePktValue(pkt_PathOne[m_it_tmp.GetLastId()]);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Fake Pkt Value Recorded with " << m_fpt[m_it_tmp.GetLastId()].GetLastFakePkt());

			  m_fmt_one[m_it_tmp.GetLastId()].WriteFakeMacValue(pkt_PathOne[m_it_tmp.GetLastId()]);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Fake Mac Value Written.");

			  m_fmt_one[m_it_tmp.GetLastId()].RecordFakeMacValue(pkt_PathOne[m_it_tmp.GetLastId()]);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Fake Mac Value Recorded.");

			  //m_mt.SetLastMac(m_macPathOne);
			  //m_mt.WriteMacValue(pkt_PathOne);

			  //m_apt.WriteAlterPathValue(pkt_PathOne,0);
			  //NS_LOG_UNCOND ("Alter Path Value Written.");

			  //m_apt.RecordAlterPathValue(pkt_PathOne);
			  //NS_LOG_UNCOND ("m_apt for tmp1: " << m_apt.GetLastAlterPath());

			  NS_LOG_UNCOND ("" << Simulator::Now() << "DELAY SAMPLES ADDED WITH ORIGINAL PATH " << m_fmt_one[m_it_tmp.GetLastId()].GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two[m_it_tmp.GetLastId()].GetLastFakeMac());

			  m_fdfb_one[m_it_tmp.GetLastId()].Reset();
			  m_fmt_one[m_it_tmp.GetLastId()].SetLastFakeMac("00:00:00:00:00:00");

			  NS_LOG_UNCOND ("" << Simulator::Now() << "DELAY SAMPLES RESET AND FAKE MAC TAG FOR FIRST PATH BECOMES " << m_fmt_one[m_it_tmp.GetLastId()].GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two[m_it_tmp.GetLastId()].GetLastFakeMac());

			  //Only Need to Submit tmp Since packet will be handled as usual
		      m_rxCallback (this, pkt_PathOne[m_it_tmp.GetLastId()], protocol, GetRemote ());
		      //return;
		  }

		  if(m_fdfb_two[m_it_tmp.GetLastId()].isFull())
		  {
			  NS_LOG_UNCOND ("" << Simulator::Now() << "ENTER INTO M_FDFB_TWO FULL CASE.");
			  //Dirty Hacking - Take Some Advantage of CBR Flow
			  //Since One Tag Can Be Added for One Packet - We Generate another Copy for Current Packet
			  //Ptr<Packet>tmp1 = packet->Copy();
			  //Ptr<Packet>tmp2 = packet->Copy();

		      ProcessHeader (pkt_PathTwo[m_it_tmp.GetLastId()], protocol);
		      NS_LOG_UNCOND_YB ("" << Simulator::Now() << "ProcessHeader Done.");


			  m_fdfb_two[m_it_tmp.GetLastId()].WriteDelaySamples(pkt_PathTwo[m_it_tmp.GetLastId()]);
			  m_fpt[m_it_tmp.GetLastId()].WriteFakePktValue(pkt_PathTwo[m_it_tmp.GetLastId()],1);
			  m_fmt_two[m_it_tmp.GetLastId()].WriteFakeMacValue(pkt_PathTwo[m_it_tmp.GetLastId()]);
			  //m_fmt_two.RecordFakeMacValue(pkt_PathTwo);

			  //m_mt.SetLastMac(m_macPathTwo);
			  //m_mt.WriteMacValue(pkt_PathTwo);

			  //m_apt.WriteAlterPathValue(pkt_PathTwo,1);
			  //m_apt.RecordAlterPathValue(pkt_PathTwo);

			  NS_LOG_UNCOND ("" << Simulator::Now() << "DELAY SAMPLES ADDED WITH ORIGINAL PATH " << m_fmt_one[m_it_tmp.GetLastId()].GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two[m_it_tmp.GetLastId()].GetLastFakeMac());

			  m_fdfb_two[m_it_tmp.GetLastId()].Reset();
			  m_fmt_two[m_it_tmp.GetLastId()].SetLastFakeMac("00:00:00:00:00:00");

			  NS_LOG_UNCOND ("" << Simulator::Now() << "DELAY SAMPLES RESET AND FAKE MAC TAG FOR FIRST PATH BECOMES " << m_fmt_one[m_it_tmp.GetLastId()].GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two[m_it_tmp.GetLastId()].GetLastFakeMac());

			  //Only Need to Submit tmp Since packet will be handled as usual
		      m_rxCallback (this, pkt_PathTwo[m_it_tmp.GetLastId()], protocol, GetRemote ());
		      //return;

		  }

/*		  if(m_fdfb_one.isFull() && m_fdfb_two.isFull())
		  {
			  //Ptr<Packet>fakePacket = Create<Packet>(1400);
			  //Ptr<Node>node = this->GetNode();
			  //Ptr<Application>udpServer = node->GetApplication(0);
			  //Ptr<Socket>socket = udpServer->GetSocket();
			  //socket->Send(packet);

			  NS_LOG_UNCOND ("ENTER INTO ALL FULL CASE.");
			  //Dirty Hacking - Take Some Advantage of CBR Flow
			  //Since One Tag Can Be Added for One Packet - We Generate another Copy for Current Packet
			  //Ptr<Packet>tmp1 = packet->Copy();
			  //Ptr<Packet>tmp2 = packet->Copy();

		      ProcessHeader (pkt_PathOne, protocol);
		      ProcessHeader (pkt_PathTwo, protocol);

			  m_fdfb_one.WriteDelaySamples(pkt_PathOne);
			  m_fpt.WriteFakePktValue(pkt_PathOne,1);
			  m_fmt_one.WriteFakeMacValue(pkt_PathOne);
			  m_fmt_one.RecordFakeMacValue(pkt_PathOne);

			  m_fdfb_two.WriteDelaySamples(pkt_PathTwo);
			  m_fpt.WriteFakePktValue(pkt_PathTwo,1);
			  m_fmt_two.WriteFakeMacValue(pkt_PathTwo);
			  m_fmt_two.RecordFakeMacValue(pkt_PathTwo);

			  m_mt.SetLastMac(m_macPathOne);
			  m_mt.WriteMacValue(pkt_PathOne);
			  m_mt.SetLastMac(m_macPathTwo);
			  m_mt.WriteMacValue(pkt_PathTwo);

			  m_apt.WriteAlterPathValue(pkt_PathOne,0);
			  m_apt.RecordAlterPathValue(pkt_PathOne);
			  NS_LOG_UNCOND ("m_apt for tmp1: " << m_apt.GetLastAlterPath());

			  m_apt.WriteAlterPathValue(pkt_PathTwo,1);
			  m_apt.RecordAlterPathValue(pkt_PathTwo);
			  NS_LOG_UNCOND ("m_apt for tmp2: " << m_apt.GetLastAlterPath());

			  NS_LOG_UNCOND ("DELAY SAMPLES ADDED WITH ORIGINAL PATH " << m_fmt_one.GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two.GetLastFakeMac());

			  m_fdfb_one.Reset();
			  m_fdfb_two.Reset();
			  m_fmt_one.SetLastFakeMac("00:00:00:00:00:00");
			  m_fmt_two.SetLastFakeMac("00:00:00:00:00:00");

			  NS_LOG_UNCOND ("DELAY SAMPLES RESET AND FAKE MAC TAG FOR FIRST PATH BECOMES " << m_fmt_one.GetLastFakeMac() << " AND ALTERNATIVE PATH " << m_fmt_two.GetLastFakeMac());

			  //Only Need to Submit tmp Since packet will be handled as usual
		      m_rxCallback (this, pkt_PathOne, protocol, GetRemote ());
		      m_rxCallback (this, pkt_PathTwo, protocol, GetRemote ());


			  //NS_LOG_UNCOND ("Feedback Packet Sent.");
			  //m_rxCallback (this, fakePacket, protocol, GetRemote ());

		  }*/

	  }

  }

  if (m_receiveErrorModel && m_receiveErrorModel->IsCorrupt (packet) )
    {
      // 
      // If we have an error model and it indicates that it is time to lose a
      // corrupted packet, don't forward this packet up, let it go.
      //
      m_phyRxDropTrace (packet);
    }
  else 
    {
      // 
      // Hit the trace hooks.  All of these hooks are in the same place in this 
      // device because it is so simple, but this is not usually the case in
      // more complicated devices.
      //
      m_snifferTrace (packet);
      m_promiscSnifferTrace (packet);
      m_phyRxEndTrace (packet);

      //
      // Trace sinks will expect complete packets, not packets without some of the
      // headers.
      //
      Ptr<Packet> originalPacket = packet->Copy ();

      //
      // Strip off the point-to-point protocol header and forward this packet
      // up the protocol stack.  Since this is a simple point-to-point link,
      // there is no difference in what the promisc callback sees and what the
      // normal receive callback sees.
      //
      ProcessHeader (packet, protocol);

      if (!m_promiscCallback.IsNull ())
        {
          m_macPromiscRxTrace (originalPacket);
          m_promiscCallback (this, packet, protocol, GetRemote (), GetAddress (), NetDevice::PACKET_HOST);
        }

      m_macRxTrace (originalPacket);
      m_rxCallback (this, packet, protocol, GetRemote ());
    }
}

Ptr<Queue>
PointToPointNetDevice::GetQueue (void) const
{ 
  NS_LOG_FUNCTION (this);
  return m_queue;
}

Ptr<Queue>
PointToPointNetDevice::GetCachedQueue(int _nodeID)
{
	return m_cachedQueue[_nodeID];
}

int
PointToPointNetDevice::GetProbingOrNot(int _nodeID)
{
	return m_probingOrNot[_nodeID];
}

void
PointToPointNetDevice::SetProbingOrNot(int _nodeID, int _m_probingOrNot)
{
	NS_LOG_UNCOND_YB("" << Simulator::Now() << ": SetProbingOrNot: _nodeID " << _nodeID << " _m_probingOrNot " << _m_probingOrNot);

	m_probingOrNot[_nodeID]=_m_probingOrNot;

	//newly added - to guarantee that samples gathered last time will not affect result of this time
	//m_fdfb_one[_nodeID].Reset();
	//m_fdfb_two[_nodeID].Reset();

}

void
PointToPointNetDevice::NotifyLinkUp (void)
{
  NS_LOG_FUNCTION (this);
  m_linkUp = true;
  m_linkChangeCallbacks ();
}

void
PointToPointNetDevice::SetIfIndex (const uint32_t index)
{
  NS_LOG_FUNCTION (this);
  m_ifIndex = index;
}

uint32_t
PointToPointNetDevice::GetIfIndex (void) const
{
  return m_ifIndex;
}

Ptr<Channel>
PointToPointNetDevice::GetChannel (void) const
{
  return m_channel;
}

//
// This is a point-to-point device, so we really don't need any kind of address
// information.  However, the base class NetDevice wants us to define the
// methods to get and set the address.  Rather than be rude and assert, we let
// clients get and set the address, but simply ignore them.

void
PointToPointNetDevice::SetAddress (Address address)
{
  NS_LOG_FUNCTION (this << address);
  m_address = Mac48Address::ConvertFrom (address);
}

Address
PointToPointNetDevice::GetAddress (void) const
{
  return m_address;
}

bool
PointToPointNetDevice::IsLinkUp (void) const
{
  NS_LOG_FUNCTION (this);
  return m_linkUp;
}

void
PointToPointNetDevice::AddLinkChangeCallback (Callback<void> callback)
{
  NS_LOG_FUNCTION (this);
  m_linkChangeCallbacks.ConnectWithoutContext (callback);
}

//
// This is a point-to-point device, so every transmission is a broadcast to
// all of the devices on the network.
//
bool
PointToPointNetDevice::IsBroadcast (void) const
{
  NS_LOG_FUNCTION (this);
  return true;
}

//
// We don't really need any addressing information since this is a 
// point-to-point device.  The base class NetDevice wants us to return a
// broadcast address, so we make up something reasonable.
//
Address
PointToPointNetDevice::GetBroadcast (void) const
{
  NS_LOG_FUNCTION (this);
  return Mac48Address ("ff:ff:ff:ff:ff:ff");
}

bool
PointToPointNetDevice::IsMulticast (void) const
{
  NS_LOG_FUNCTION (this);
  return true;
}

Address
PointToPointNetDevice::GetMulticast (Ipv4Address multicastGroup) const
{
  NS_LOG_FUNCTION (this);
  return Mac48Address ("01:00:5e:00:00:00");
}

Address
PointToPointNetDevice::GetMulticast (Ipv6Address addr) const
{
  NS_LOG_FUNCTION (this << addr);
  return Mac48Address ("33:33:00:00:00:00");
}

bool
PointToPointNetDevice::IsPointToPoint (void) const
{
  NS_LOG_FUNCTION (this);
  return true;
}

bool
PointToPointNetDevice::IsBridge (void) const
{
  NS_LOG_FUNCTION (this);
  return false;
}

bool
PointToPointNetDevice::Send (
  Ptr<Packet> packet, 
  const Address &dest, 
  uint16_t protocolNumber)
{
  NS_LOG_FUNCTION (this << packet << dest << protocolNumber);
  NS_LOG_LOGIC ("p=" << packet << ", dest=" << &dest);
  NS_LOG_LOGIC ("UID is " << packet->GetUid ());
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Send Entered.");

  PortTagger m_pt_tmp;
  m_pt_tmp.RecordPortValue(packet);

  PeerIdTagger m_pit;
  m_pit.RecordPeerIdValue(packet);

  //
  // If IsLinkUp() is false it means there is no channel to send any packet 
  // over so we just hit the drop trace on the packet and return an error.
  //
  if (IsLinkUp () == false)
    {
	  //NS_LOG_UNCOND ("PACKET DROPPED DUE TO ISLINKUP()");
//	  MacTagger m_mt;
//	  ProbeTagger m_pt;
//	  FakePktTagger m_fpt_tmp;
//	  m_mt.RecordMacValue(packet);
//	  m_pt.RecordProbeValue(packet);
//	  m_pst.RecordProbeSeqValue(packet);
//	  m_fpt_tmp.RecordFakePktValue(packet);
//
//	  NS_LOG_UNCOND("" << Simulator::Now() << "LOCAL=" << GetAddress() << " m_mt=" << m_mt.GetLastMac() << " m_pt=" << m_pt.GetLastProbe() << " m_pst=" << m_pst.GetLastProbeSeq() << " m_fpt_tmp=" << m_fpt_tmp.GetLastFakePkt() << ": PACKET DROPPED DUE TO ISLINKUP()");
	  m_macTxDropTrace (packet);
      return false;
    }

  //
  // Stick a point to point protocol header on the packet in preparation for
  // shoving it out the door.
  //
  AddHeader (packet, protocolNumber);

  m_macTxTrace (packet);

  if(GetNode()->GetId() == 0 && m_probingOrNot[m_pit.GetLastPeerId()] ==1 && m_pt_tmp.GetLastPort() == 10 && m_pit.GetLastPeerId() >= 3) //stupid hacking - way to detect which user it is focusing on is needed
  {
	  NS_LOG_UNCOND("" << Simulator::Now() << ": I am in the state of m_probingOrNot and port ID is 10 for packet with UID: " << packet->GetUid() << " aiming at node " << m_pit.GetLastPeerId());
	  //timestamp inqueue time
	  EnqueueTimeTagger m_ettg;
	  m_ettg.PrepareEqTime(packet,Simulator::Now());
	  m_cachedQueue[m_pit.GetLastPeerId()]->Enqueue(packet);
	  return true;
  }
  else if(GetNode()->GetId() == 0 && m_probingOrNot[m_pit.GetLastPeerId()] == 0 && m_pt_tmp.GetLastPort() == 10 && m_pit.GetLastPeerId() >= 3)
  {
	  if(m_cachedQueue[m_pit.GetLastPeerId()]->GetNPackets() != 0)
	  {
		  NS_LOG_UNCOND("" << Simulator::Now() << ": Send: There are " << m_cachedQueue[m_pit.GetLastPeerId()]->GetNPackets() << " in total cached in queue!");
	  }
	  while(!m_cachedQueue[m_pit.GetLastPeerId()]->IsEmpty())
	  {
		  //timestamp dequeue time
		  Ptr<Packet> tmpP=m_cachedQueue[m_pit.GetLastPeerId()]->Dequeue();
		  EnqueueTimeTagger m_ettg;
		  EnqueueTimeCompensateTagger m_etct;
		  m_ettg.RecordEqTime(tmpP);
		  m_etct.PrepareEqTime(tmpP, Simulator::Now()-m_ettg.GetLastRecordedEnqueueTime());
		  PeerIdTagger m_pit_tmp;
		  m_pit_tmp.RecordPeerIdValue(packet);
		  NS_LOG_UNCOND("" << Simulator::Now() << ": packet with UID: " << tmpP->GetUid() << " has moved from m_cachedQueue to m_queue with peer ID: " << m_pit_tmp.GetLastPeerId() << ". Enqueue Time: " << m_ettg.GetLastRecordedEnqueueTime());
		  m_queue->Enqueue(tmpP);
	  }
  }

  //
  // We should enqueue and dequeue the packet to hit the tracing hooks.
  //
  if (m_queue->Enqueue (packet))
    {
      //
      // If the channel is ready for transition we send the packet right now
      // 
      if (m_txMachineState == READY)
        {
          packet = m_queue->Dequeue ();
          m_snifferTrace (packet);
          m_promiscSnifferTrace (packet);

          //Added by Y.B. for Detecting Delay Information Attaching Point
//          m_fpt.RecordFakePktValue(packet);
//          if(m_fpt.GetLastFakePkt() != 0)
//          {
//        	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Fake Packet Detected from Sending Function on " << GetAddress());
//          }
          return TransmitStart (packet);
        }
      return true;
    }

//  // Enqueue may fail (overflow)
//  MacTagger m_mt;
//  ProbeTagger m_pt;
//  FakePktTagger m_fpt_tmp;
//  m_mt.RecordMacValue(packet);
//  m_pt.RecordProbeValue(packet);
//  m_pst.RecordProbeSeqValue(packet);
//  m_fpt_tmp.RecordFakePktValue(packet);
//
//  NS_LOG_UNCOND("" << Simulator::Now() << "LOCAL=" << GetAddress() << " m_mt=" << m_mt.GetLastMac() << " m_pt=" << m_pt.GetLastProbe() << " m_pst=" << m_pst.GetLastProbeSeq() << " m_fpt_tmp=" << m_fpt_tmp.GetLastFakePkt() << ": PACKET DROPPED DUE TO QUEUE OVERFLOW()");
  m_macTxDropTrace (packet);
  return false;
}

bool
PointToPointNetDevice::SendFrom (Ptr<Packet> packet, 
                                 const Address &source, 
                                 const Address &dest, 
                                 uint16_t protocolNumber)
{
  NS_LOG_FUNCTION (this << packet << source << dest << protocolNumber);
  return false;
}

Ptr<Node>
PointToPointNetDevice::GetNode (void) const
{
  return m_node;
}

void
PointToPointNetDevice::SetNode (Ptr<Node> node)
{
  NS_LOG_FUNCTION (this);
  m_node = node;
}

bool
PointToPointNetDevice::NeedsArp (void) const
{
  NS_LOG_FUNCTION (this);
  return false;
}

void
PointToPointNetDevice::SetReceiveCallback (NetDevice::ReceiveCallback cb)
{
  m_rxCallback = cb;
}

void
PointToPointNetDevice::SetPromiscReceiveCallback (NetDevice::PromiscReceiveCallback cb)
{
  m_promiscCallback = cb;
}

bool
PointToPointNetDevice::SupportsSendFrom (void) const
{
  NS_LOG_FUNCTION (this);
  return false;
}

void
PointToPointNetDevice::DoMpiReceive (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this << p);
  Receive (p);
}

Address 
PointToPointNetDevice::GetRemote (void) const
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT (m_channel->GetNDevices () == 2);
  for (uint32_t i = 0; i < m_channel->GetNDevices (); ++i)
    {
      Ptr<NetDevice> tmp = m_channel->GetDevice (i);
      if (tmp != this)
        {
          return tmp->GetAddress ();
        }
    }
  NS_ASSERT (false);
  // quiet compiler.
  return Address ();
}

bool
PointToPointNetDevice::SetMtu (uint16_t mtu)
{
  NS_LOG_FUNCTION (this << mtu);
  m_mtu = mtu;
  return true;
}

uint16_t
PointToPointNetDevice::GetMtu (void) const
{
  NS_LOG_FUNCTION (this);
  return m_mtu;
}

uint16_t
PointToPointNetDevice::PppToEther (uint16_t proto)
{
  NS_LOG_FUNCTION_NOARGS();
  switch(proto)
    {
    case 0x0021: return 0x0800;   //IPv4
    case 0x0057: return 0x86DD;   //IPv6
    default: NS_ASSERT_MSG (false, "PPP Protocol number not defined!");
    }
  return 0;
}

uint16_t
PointToPointNetDevice::EtherToPpp (uint16_t proto)
{
  NS_LOG_FUNCTION_NOARGS();
  switch(proto)
    {
    case 0x0800: return 0x0021;   //IPv4
    case 0x86DD: return 0x0057;   //IPv6
    default: NS_ASSERT_MSG (false, "PPP Protocol number not defined!");
    }
  return 0;
}

void
PointToPointNetDevice::FinalizeFdfb1AndSend(int _nodeID)
{
	if(Simulator::Now() >= probingTimeOutPath1[_nodeID]+lastTimeStampPath1[_nodeID])
	{
		NS_LOG_UNCOND ("" << Simulator::Now() << ": Timeout Happened on FinalizedFdfb1AndSend.");
		m_fdfb_one[_nodeID].FinalizeSampleSet();
		Receive(Packet_PathOneCopy[_nodeID]);
	}

}

void
PointToPointNetDevice::FinalizeFdfb2AndSend(int _nodeID)
{
	if(Simulator::Now() >= probingTimeOutPath2[_nodeID]+lastTimeStampPath2[_nodeID])
	{
		NS_LOG_UNCOND ("" << Simulator::Now() << ": Timeout Happened on FinalizedFdfb2AndSend.");
		m_fdfb_two[_nodeID].FinalizeSampleSet();
		Receive(Packet_PathTwoCopy[_nodeID]);
	}

}

} // namespace ns3
