#!/bin/bash

step=30
for((i=30;i<1530;i=i+30))
	do
		tmp=$(($i-$step))
		echo $tmp
		sed -i "s/#define INITIAL_PROBE_TRAIN_LENGTH $tmp/#define INITIAL_PROBE_TRAIN_LENGTH $i/g" src/network/helper/delay-samples-tag.h
		./waf build
		./waf --run PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_0055_005_bac > loggg_{$i}_Len30Incre0_129750000_multChannel_stopDownlinkTrafficWhenProbing.err 2>&1
	done


