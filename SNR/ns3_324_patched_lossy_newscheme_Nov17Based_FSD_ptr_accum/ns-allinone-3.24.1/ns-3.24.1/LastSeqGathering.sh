#!/bin/bash

while read line; do


	cat $line | grep "LAST SEQ" | grep "Delay" | sed -e 's/ns:/,/g' -e 's/LAST SEQ VALUE: /,/g' -e 's/Delay:/,/g' -e 's/UID: /,/g' -e 's/EnTmpQueue Time: /,/g' -e 's/Compensate Time:/,/g' -e 's/ns//g' > LASTSEQ_$line
	echo $line done
done < $1
