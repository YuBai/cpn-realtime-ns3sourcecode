#!/bin/bash

interval=(0.055804 0.002029 0.001033 0.000693 0.000522 0.000418 0.000349 0.000299 0.000262 0.000233 0.000210)
for((i=0;i<11;i=i+1))
        do
		c=${interval[$i]}
                echo $c
                sed -i "s/Seconds (0.0002)/Seconds ("$c")/g" scratch/PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_0055_005.cc
                ./waf build	
                ./waf --run PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_0055_005 > logggTest_channelLimVSchannelLim_speedScan_"$c" 2>&1
		mkdir speed_"$c"
		mv *.pcap speed_"$c"
		sed -i "s/Seconds ("$c")/Seconds (0.0002)/g" scratch/PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_0055_005.cc

        done

