#!/bin/bash

mkdir FSDChunk_2826506

#cat jobRat.2826506.err | grep "FSD" > logggFSD_2826506

for ((i=0;i<48;i++))
	do

		cat logggFSD_2826506 | grep "FSDTurn:"$i"-" > FSDChunk_2826506/logggFSD_2826506_$i
		cat FSDChunk_2826506/logggFSD_2826506_$i | grep "case1" >  FSDChunk_2826506/logggFSD_2826506_{$i}_case1
		cat FSDChunk_2826506/logggFSD_2826506_$i | grep "case2" >  FSDChunk_2826506/logggFSD_2826506_{$i}_case2
		cat FSDChunk_2826506/logggFSD_2826506_{$i}_case1 | cut -d: -f5 > FSDChunk_2826506/logggFSD_2826506_{$i}_case1_fin		
		cat FSDChunk_2826506/logggFSD_2826506_{$i}_case2 | cut -d: -f6 > FSDChunk_2826506/logggFSD_2826506_{$i}_case2_fin		
	
	done
