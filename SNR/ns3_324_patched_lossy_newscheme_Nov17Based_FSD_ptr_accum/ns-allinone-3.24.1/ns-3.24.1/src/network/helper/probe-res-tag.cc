/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the ProbeRese that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "probe-res-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to ProbeResform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ProbeResTag : public Tag
{
public:
	ProbeResTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetProbeRes (void) const;
  void SetProbeRes (double ProbeRes) ;

private:
  double m_ProbeRes; //!< The time stored in the tag
};

ProbeResTag::ProbeResTag ()
{
}

TypeId
ProbeResTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ProbeResTag")
    .SetParent<Tag> ()
    .SetGroupName("ProbeResTag")
    .AddConstructor<ProbeResTag> ()
  ;
  return tid;
}
TypeId
ProbeResTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ProbeResTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ProbeResTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_ProbeRes);
}
void
ProbeResTag::Deserialize (TagBuffer i)
{
  m_ProbeRes = i.ReadDouble ();
}
void
ProbeResTag::Print (std::ostream &os) const
{
  os << "m_ProbeRes=" << m_ProbeRes;
}
double
ProbeResTag::GetProbeRes (void) const
{
  return m_ProbeRes;
}

void
ProbeResTag::SetProbeRes (double ProbeRes)
{
	m_ProbeRes=ProbeRes;
	//std::clog << "m_ProbeRes now is " << m_ProbeRes << "\n";
}

ProbeResTagger::ProbeResTagger ()
{
	this->lastProbeRes=0;
}

void
ProbeResTagger::WriteProbeResValue (Ptr<const Packet> packet, double tagValue)
{
  ProbeResTag tag;
  tag.SetProbeRes(tagValue);
  packet->AddPacketTag (tag);
}
double
ProbeResTagger::RecordProbeResValue (Ptr<const Packet> packet)
{
  ProbeResTag tag;
  lastProbeRes = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastProbeRes=tag.GetProbeRes ();
  return (double)lastProbeRes;
}

double
ProbeResTagger::GetLastProbeRes (void) const
{
  return lastProbeRes;
}


} // namespace ns3
