/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the Rolee that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "role-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to Roleform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class RoleTag : public Tag
{
public:
	RoleTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetRole (void) const;
  void SetRole (double Role) ;

private:
  double m_Role; //!< The time stored in the tag
};

RoleTag::RoleTag ()
{
}

TypeId
RoleTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::RoleTag")
    .SetParent<Tag> ()
    .SetGroupName("RoleTag")
    .AddConstructor<RoleTag> ()
  ;
  return tid;
}
TypeId
RoleTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
RoleTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
RoleTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Role);
}
void
RoleTag::Deserialize (TagBuffer i)
{
  m_Role = i.ReadDouble ();
}
void
RoleTag::Print (std::ostream &os) const
{
  os << "m_Role=" << m_Role;
}
double
RoleTag::GetRole (void) const
{
  return m_Role;
}

void
RoleTag::SetRole (double Role)
{
	m_Role=Role;
	//std::clog << "m_Role now is " << m_Role << "\n";
}

RoleTagger::RoleTagger ()
{
	this->lastRole=0;
}

void
RoleTagger::WriteRoleValue (Ptr<const Packet> packet, double tagValue)
{
  RoleTag tag;
  tag.SetRole(tagValue);
  packet->AddPacketTag (tag);
}
double
RoleTagger::RecordRoleValue (Ptr<const Packet> packet)
{
  RoleTag tag;
  lastRole = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastRole=tag.GetRole ();
  return (double)lastRole;
}

double
RoleTagger::GetLastRole (void) const
{
  return lastRole;
}


} // namespace ns3
