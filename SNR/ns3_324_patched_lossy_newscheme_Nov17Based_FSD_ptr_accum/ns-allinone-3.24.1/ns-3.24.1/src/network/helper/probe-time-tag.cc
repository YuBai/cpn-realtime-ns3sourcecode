/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "probe-time-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to ProbeTimeform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ProbeTimeTag : public Tag
{
public:
	ProbeTimeTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetProbeTime (void) const;
  void SetProbeTime (double ProbeTime) ;

private:
  double m_ProbeTime; //!< The time stored in the tag
};

ProbeTimeTag::ProbeTimeTag ()
  : m_ProbeTime (0.0)
{
}

TypeId
ProbeTimeTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ProbeTimeTag")
    .SetParent<Tag> ()
    .SetGroupName("ProbeTimeTag")
    .AddConstructor<ProbeTimeTag> ()
    .AddAttribute ("LastProbeTime",
                   "Last ProbeTime Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ProbeTimeTag::GetProbeTime),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ProbeTimeTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ProbeTimeTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ProbeTimeTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_ProbeTime);
}
void
ProbeTimeTag::Deserialize (TagBuffer i)
{
  m_ProbeTime = i.ReadDouble ();
}
void
ProbeTimeTag::Print (std::ostream &os) const
{
  os << "m_ProbeTime=" << m_ProbeTime;
}
double
ProbeTimeTag::GetProbeTime (void) const
{
  return m_ProbeTime;
}

void
ProbeTimeTag::SetProbeTime (double ProbeTime)
{
	m_ProbeTime=ProbeTime;
}

ProbeTimeTagger::ProbeTimeTagger ()
{
	this->lastProbeTime=0.0;
}

void
ProbeTimeTagger::WriteProbeTimeValue (Ptr<const Packet> packet, double tagValue)
{
  ProbeTimeTag tag;
  tag.SetProbeTime(tagValue);
  packet->AddByteTag (tag);
}
int
ProbeTimeTagger::RecordProbeTimeValue (Ptr<const Packet> packet)
{
  ProbeTimeTag tag;
  lastProbeTime=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastProbeTime=tag.GetProbeTime ();
  return (int)lastProbeTime;
}

double
ProbeTimeTagger::GetLastProbeTime (void) const
{
  return lastProbeTime;
}

} // namespace ns3
