/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "probe-train-length-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to ProbeTrainLengthform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ProbeTrainLengthTag : public Tag
{
public:
	ProbeTrainLengthTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetProbeTrainLength (void) const;
  void SetProbeTrainLength (double ProbeTrainLength) ;

private:
  double m_ProbeTrainLength; //!< The time stored in the tag
};

ProbeTrainLengthTag::ProbeTrainLengthTag ()
  : m_ProbeTrainLength (0.0)
{
}

TypeId
ProbeTrainLengthTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ProbeTrainLengthTag")
    .SetParent<Tag> ()
    .SetGroupName("ProbeTrainLengthTag")
    .AddConstructor<ProbeTrainLengthTag> ()
    .AddAttribute ("LastProbeTrainLength",
                   "Last ProbeTrainLength Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ProbeTrainLengthTag::GetProbeTrainLength),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ProbeTrainLengthTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ProbeTrainLengthTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ProbeTrainLengthTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_ProbeTrainLength);
}
void
ProbeTrainLengthTag::Deserialize (TagBuffer i)
{
  m_ProbeTrainLength = i.ReadDouble ();
}
void
ProbeTrainLengthTag::Print (std::ostream &os) const
{
  os << "m_ProbeTrainLength=" << m_ProbeTrainLength;
}
double
ProbeTrainLengthTag::GetProbeTrainLength (void) const
{
  return m_ProbeTrainLength;
}

void
ProbeTrainLengthTag::SetProbeTrainLength (double ProbeTrainLength)
{
	m_ProbeTrainLength=ProbeTrainLength;
}

ProbeTrainLengthTagger::ProbeTrainLengthTagger ()
{
	this->lastProbeTrainLength=0.0;
}

void
ProbeTrainLengthTagger::WriteProbeTrainLengthValue (Ptr<const Packet> packet, double tagValue)
{
  ProbeTrainLengthTag tag;
  tag.SetProbeTrainLength(tagValue);
  packet->AddByteTag (tag);
}
int
ProbeTrainLengthTagger::RecordProbeTrainLengthValue (Ptr<const Packet> packet)
{
  ProbeTrainLengthTag tag;
  lastProbeTrainLength=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastProbeTrainLength=tag.GetProbeTrainLength ();
  return (int)lastProbeTrainLength;
}

double
ProbeTrainLengthTagger::GetLastProbeTrainLength (void) const
{
  return lastProbeTrainLength;
}

} // namespace ns3
