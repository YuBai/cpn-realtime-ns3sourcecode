/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 */

#include "ap-wifi-mac.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include "ns3/boolean.h"
#include "qos-tag.h"
#include "wifi-phy.h"
#include "dcf-manager.h"
#include "mac-rx-middle.h"
#include "mac-tx-middle.h"
#include "mgt-headers.h"
#include "mac-low.h"
#include "amsdu-subframe-header.h"
#include "msdu-aggregator.h"
#include "ns3/stop-traffic-tag.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ApWifiMac");

NS_OBJECT_ENSURE_REGISTERED (ApWifiMac);

TypeId
ApWifiMac::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::ApWifiMac")
    .SetParent<RegularWifiMac> ()
    .SetGroupName ("Wifi")
    .AddConstructor<ApWifiMac> ()
    .AddAttribute ("BeaconInterval", "Delay between two beacons",
                   TimeValue (MicroSeconds (102400)),
                   MakeTimeAccessor (&ApWifiMac::GetBeaconInterval,
                                     &ApWifiMac::SetBeaconInterval),
                   MakeTimeChecker ())
    .AddAttribute ("BeaconJitter", "A uniform random variable to cause the initial beacon starting time (after simulation time 0) "
                   "to be distributed between 0 and the BeaconInterval.",
                   StringValue ("ns3::UniformRandomVariable"),
                   MakePointerAccessor (&ApWifiMac::m_beaconJitter),
                   MakePointerChecker<UniformRandomVariable> ())
    .AddAttribute ("EnableBeaconJitter", "If beacons are enabled, whether to jitter the initial send event.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&ApWifiMac::m_enableBeaconJitter),
                   MakeBooleanChecker ())
    .AddAttribute ("BeaconGeneration", "Whether or not beacons are generated.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&ApWifiMac::SetBeaconGeneration,
                                        &ApWifiMac::GetBeaconGeneration),
                   MakeBooleanChecker ())
  ;
  return tid;
}

ApWifiMac::ApWifiMac ()
{
  NS_LOG_FUNCTION (this);
  m_beaconDca = CreateObject<DcaTxop> ();
  m_beaconDca->SetAifsn (1);
  m_beaconDca->SetMinCw (0);
  m_beaconDca->SetMaxCw (0);
  m_beaconDca->SetLow (m_low);
  m_beaconDca->SetManager (m_dcfManager);
  m_beaconDca->SetTxMiddle (m_txMiddle);

  dropOrNot = 0;
  m_currentProRUid.resize(5000); //stupid way
  m_currentProRRole.resize(5000); //stupid way
  // Let the lower layers know that we are acting as an AP.
  SetTypeOfStation (AP);

  m_enableBeaconGeneration = false;
}

ApWifiMac::~ApWifiMac ()
{
  NS_LOG_FUNCTION (this);
}

void
ApWifiMac::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_beaconDca = 0;
  m_enableBeaconGeneration = false;
  m_beaconEvent.Cancel ();
  RegularWifiMac::DoDispose ();
}

void
ApWifiMac::SetAddress (Mac48Address address)
{
  NS_LOG_FUNCTION (this << address);
  //As an AP, our MAC address is also the BSSID. Hence we are
  //overriding this function and setting both in our parent class.
  RegularWifiMac::SetAddress (address);
  RegularWifiMac::SetBssid (address);
}

void
ApWifiMac::SetBeaconGeneration (bool enable)
{
  NS_LOG_FUNCTION (this << enable);
  if (!enable)
    {
      m_beaconEvent.Cancel ();
    }
  else if (enable && !m_enableBeaconGeneration)
    {
      m_beaconEvent = Simulator::ScheduleNow (&ApWifiMac::SendOneBeacon, this);
    }
  m_enableBeaconGeneration = enable;
}

bool
ApWifiMac::GetBeaconGeneration (void) const
{
  NS_LOG_FUNCTION (this);
  return m_enableBeaconGeneration;
}

Time
ApWifiMac::GetBeaconInterval (void) const
{
  NS_LOG_FUNCTION (this);
  return m_beaconInterval;
}

void
ApWifiMac::SetWifiRemoteStationManager (Ptr<WifiRemoteStationManager> stationManager)
{
  NS_LOG_FUNCTION (this << stationManager);
  m_beaconDca->SetWifiRemoteStationManager (stationManager);
  RegularWifiMac::SetWifiRemoteStationManager (stationManager);
}

void
ApWifiMac::SetLinkUpCallback (Callback<void> linkUp)
{
  NS_LOG_FUNCTION (this << &linkUp);
  RegularWifiMac::SetLinkUpCallback (linkUp);

  //The approach taken here is that, from the point of view of an AP,
  //the link is always up, so we immediately invoke the callback if
  //one is set
  linkUp ();
}

void
ApWifiMac::SetBeaconInterval (Time interval)
{
  NS_LOG_FUNCTION (this << interval);
  if ((interval.GetMicroSeconds () % 1024) != 0)
    {
      NS_LOG_WARN ("beacon interval should be multiple of 1024us (802.11 time unit), see IEEE Std. 802.11-2012");
    }
  m_beaconInterval = interval;
}

void
ApWifiMac::StartBeaconing (void)
{
  NS_LOG_FUNCTION (this);
  SendOneBeacon ();
}

int64_t
ApWifiMac::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_beaconJitter->SetStream (stream);
  return 1;
}

void
ApWifiMac::ForwardDown (Ptr<const Packet> packet, Mac48Address from,
                        Mac48Address to)
{
  NS_LOG_FUNCTION (this << packet << from << to);
  //If we are not a QoS AP then we definitely want to use AC_BE to
  //transmit the packet. A TID of zero will map to AC_BE (through \c
  //QosUtilsMapTidToAc()), so we use that as our default here.
  uint8_t tid = 0;

  //If we are a QoS AP then we attempt to get a TID for this packet
  if (m_qosSupported)
    {
      tid = QosUtilsGetTidForPacket (packet);
      //Any value greater than 7 is invalid and likely indicates that
      //the packet had no QoS tag, so we revert to zero, which'll
      //mean that AC_BE is used.
      if (tid > 7)
        {
          tid = 0;
        }
    }

  ForwardDown (packet, from, to, tid);
}

void
ApWifiMac::ForwardDown (Ptr<const Packet> packet, Mac48Address from,
                        Mac48Address to, uint8_t tid)
{
  NS_LOG_FUNCTION (this << packet << from << to << static_cast<uint32_t> (tid));
  WifiMacHeader hdr;
  //debugging
  m_fpt.RecordFakePktValue(packet);
  m_fmt.RecordFakeMacValue(packet);
  m_apt.RecordAlterPathValue(packet);
  m_ptt.RecordProbeTimeValue(packet);
  m_ptlt.RecordProbeTrainLengthValue(packet);
  if(m_fpt.GetLastFakePkt() != 0)
  {
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": AP " << GetAddress() << " Detect Fake Packet in APWifiMac::ForwardDown() with m_fpt=" << m_fpt.GetLastFakePkt() << " m_fmt=" << m_fmt.GetLastFakeMac() << " m_apt=" << m_apt.GetLastAlterPath() << " m_ptt=" << m_ptt.GetLastProbeTime() << " m_ptlt=" << m_ptlt.GetLastProbeTrainLength());
  }

  //For now, an AP that supports QoS does not support non-QoS
  //associations, and vice versa. In future the AP model should
  //support simultaneously associated QoS and non-QoS STAs, at which
  //point there will need to be per-association QoS state maintained
  //by the association state machine, and consulted here.
  if (m_qosSupported)
    {
      hdr.SetType (WIFI_MAC_QOSDATA);
      hdr.SetQosAckPolicy (WifiMacHeader::NORMAL_ACK);
      hdr.SetQosNoEosp ();
      hdr.SetQosNoAmsdu ();
      //Transmission of multiple frames in the same TXOP is not
      //supported for now
      hdr.SetQosTxopLimit (0);
      //Fill in the QoS control field in the MAC header
      hdr.SetQosTid (tid);
    }
  else
    {
      hdr.SetTypeData ();
    }

  if (m_htSupported || m_vhtSupported)
    {
      hdr.SetNoOrder ();
    }
  hdr.SetAddr1 (to);
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (from);
  hdr.SetDsFrom ();
  hdr.SetDsNotTo ();

  //IdTagger m_it (9999);
  //m_fdje.PrepareEqTime(packet);
  //m_st.WriteSeqValue(packet);
  //m_it.WriteIdValue(packet);

//  if(to == "00:00:00:00:00:07" && dropOrNot == 1)
//  {
//	  NS_LOG_UNCOND ("" << Simulator::Now() << ": this is " << this->GetSsid() << ": dropOrNot set. Packet is dropped");
//	  return;
//  }

  if (m_qosSupported)
    {
      //Sanity check that the TID is valid
      NS_ASSERT (tid < 8);
      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
    }
  else
    {
      m_dca->Queue (packet, hdr);
    }
}

void
ApWifiMac::Enqueue (Ptr<const Packet> packet, Mac48Address to, Mac48Address from)
{
  NS_LOG_FUNCTION (this << packet << to << from);
  if (to.IsBroadcast () || m_stationManager->IsAssociated (to))
    {
      ForwardDown (packet, from, to);
    }
}

void
ApWifiMac::Enqueue (Ptr<const Packet> packet, Mac48Address to)
{
  NS_LOG_FUNCTION (this << packet << to);
  //We're sending this packet with a from address that is our own. We
  //get that address from the lower MAC and make use of the
  //from-spoofing Enqueue() method to avoid duplicated code.
  Enqueue (packet, to, m_low->GetAddress ());
}

bool
ApWifiMac::SupportsSendFrom (void) const
{
  NS_LOG_FUNCTION (this);
  return true;
}

SupportedRates
ApWifiMac::GetSupportedRates (void) const
{
  NS_LOG_FUNCTION (this);
  SupportedRates rates;
  //If it is an HT-AP then add the BSSMembershipSelectorSet
  //which only includes 127 for HT now. The standard says that the BSSMembershipSelectorSet
  //must have its MSB set to 1 (must be treated as a Basic Rate)
  //Also the standard mentioned that at leat 1 element should be included in the SupportedRates the rest can be in the ExtendedSupportedRates
  if (m_htSupported || m_vhtSupported)
    {
      for (uint32_t i = 0; i < m_phy->GetNBssMembershipSelectors (); i++)
        {
          rates.SetBasicRate (m_phy->GetBssMembershipSelector (i));
        }
    }
  //Send the set of supported rates and make sure that we indicate
  //the Basic Rate set in this set of supported rates.
  for (uint32_t i = 0; i < m_phy->GetNModes (); i++)
    {
      WifiMode mode = m_phy->GetMode (i);
      rates.AddSupportedRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1));
      //Add rates that are part of the BSSBasicRateSet (manufacturer dependent!)
      //here we choose to add the mandatory rates to the BSSBasicRateSet,
      //exept for 802.11b where we assume that only the non HR-DSSS rates are part of the BSSBasicRateSet
      if (mode.IsMandatory () && (mode.GetModulationClass () != WIFI_MOD_CLASS_HR_DSSS))
        {
          m_stationManager->AddBasicMode (mode);
        }
    }
  //set the basic rates
  for (uint32_t j = 0; j < m_stationManager->GetNBasicModes (); j++)
    {
      WifiMode mode = m_stationManager->GetBasicMode (j);
      rates.SetBasicRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1));
    }

  return rates;
}

HtCapabilities
ApWifiMac::GetHtCapabilities (void) const
{
  HtCapabilities capabilities;
  capabilities.SetHtSupported (1);
  if (m_htSupported)
    {
      capabilities.SetLdpc (m_phy->GetLdpc ());
      capabilities.SetSupportedChannelWidth (m_phy->GetChannelWidth () == 40);
      capabilities.SetShortGuardInterval20 (m_phy->GetGuardInterval ());
      capabilities.SetShortGuardInterval40 (m_phy->GetChannelWidth () == 40 && m_phy->GetGuardInterval ());
      capabilities.SetGreenfield (m_phy->GetGreenfield ());
      capabilities.SetMaxAmsduLength (1); //hardcoded for now (TBD)
      capabilities.SetLSigProtectionSupport (!m_phy->GetGreenfield ());
      capabilities.SetMaxAmpduLength (3); //hardcoded for now (TBD)
      uint64_t maxSupportedRate = 0; //in bit/s
      for (uint8_t i = 0; i < m_phy->GetNMcs (); i++)
        {
          WifiMode mcs = m_phy->GetMcs (i);
          capabilities.SetRxMcsBitmask (mcs.GetMcsValue ());
          if (mcs.GetDataRate (m_phy->GetGuardInterval (), m_phy->GetGuardInterval (), 1) > maxSupportedRate)
            {
              maxSupportedRate = mcs.GetDataRate (m_phy->GetGuardInterval (), m_phy->GetGuardInterval (), 1);
            }
        }
      capabilities.SetRxHighestSupportedDataRate (maxSupportedRate / 1e6); //in Mbit/s
      capabilities.SetTxMcsSetDefined (m_phy->GetNMcs () > 0);
      capabilities.SetTxMaxNSpatialStreams (m_phy->GetNumberOfTransmitAntennas ());
    }
  return capabilities;
}

VhtCapabilities
ApWifiMac::GetVhtCapabilities (void) const
{
  VhtCapabilities capabilities;
  capabilities.SetVhtSupported (1);
  if (m_vhtSupported)
    {
      if (m_phy->GetChannelWidth () == 160)
        {
          capabilities.SetSupportedChannelWidthSet (1);
        }
      else
        {
          capabilities.SetSupportedChannelWidthSet (0);
        }
      capabilities.SetMaxMpduLength (2); //hardcoded for now (TBD)
      capabilities.SetRxLdpc (m_phy->GetLdpc ());
      capabilities.SetShortGuardIntervalFor80Mhz ((m_phy->GetChannelWidth () == 80) && m_phy->GetGuardInterval ());
      capabilities.SetShortGuardIntervalFor160Mhz ((m_phy->GetChannelWidth () == 160) && m_phy->GetGuardInterval ());
      capabilities.SetMaxAmpduLengthExponent (7); //hardcoded for now (TBD)
      uint8_t maxMcs = 0;
      for (uint8_t i = 0; i < m_phy->GetNMcs (); i++)
        {
          WifiMode mcs = m_phy->GetMcs (i);
          if (mcs.GetMcsValue () > maxMcs)
            {
              maxMcs = mcs.GetMcsValue ();
            }
        }
      capabilities.SetRxMcsMap (maxMcs, 1); //Only 1 SS is currently supported
      capabilities.SetTxMcsMap (maxMcs, 1); //Only 1 SS is currently supported
    }
  return capabilities;
}

void
ApWifiMac::SendProbeResp (Mac48Address to, int _nodeID)
{
  NS_LOG_FUNCTION (this << to);
  WifiMacHeader hdr;
  hdr.SetProbeResp ();
  hdr.SetAddr1 (to);
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (GetAddress ());
  hdr.SetDsNotFrom ();
  hdr.SetDsNotTo ();
  Ptr<Packet> packet = Create<Packet> ();
  MgtProbeResponseHeader probe;
  probe.SetSsid (GetSsid ());
  probe.SetSupportedRates (GetSupportedRates ());
  probe.SetBeaconIntervalUs (m_beaconInterval.GetMicroSeconds ());
  if (m_htSupported || m_vhtSupported)
    {
      probe.SetHtCapabilities (GetHtCapabilities ());
      hdr.SetNoOrder ();
    }
  if (m_vhtSupported)
    {
      probe.SetVhtCapabilities (GetVhtCapabilities ());
    }
  packet->AddHeader (probe);

  //Added by Y.B.
  if(m_currentProRUid[_nodeID] != 0 && m_currentProRRole[_nodeID] != 0)
  {
	  if(m_currentProRRole[_nodeID] == 1)
	  {
		  UidTagger m_ut;
		  RoleTagger m_rt;
		  ProbeResTagger m_prt;
		  m_ut.WriteUidValue(packet,m_currentProRUid[_nodeID]);
		  m_rt.WriteRoleValue(packet,m_currentProRRole[_nodeID]+1);
		  m_prt.WriteProbeResValue(packet,1);
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": m_ut:" << m_currentProRUid[_nodeID] << " m_rt: " << m_currentProRRole[_nodeID] << " set at ap-wifi-mac side..");

		  //not quite good since we need something to distinguish between different nodes - but this should work for one node
		  m_currentProRRole[_nodeID]=0;
		  m_currentProRUid[_nodeID]=0;
	  }
	  else
	  {
		  NS_LOG_UNCOND("" << Simulator::Now() << ": m_ut:" << m_currentProRUid[_nodeID] << " m_rt: " << m_currentProRRole[_nodeID] << " something goes wrong on ap-wifi-mac.");
	  }
  }

  //The standard is not clear on the correct queue for management
  //frames if we are a QoS AP. The approach taken here is to always
  //use the DCF for these regardless of whether we have a QoS
  //association or not.
  //m_dca->Queue (packet, hdr);
  QosTag m_qt;
  m_qt.SetTid(7);
  packet->AddPacketTag(m_qt);
  uint8_t tidQ = 0;
  tidQ = QosUtilsGetTidForPacket (packet);
  m_edca[QosUtilsMapTidToAc(tidQ)]->Queue(packet,hdr);

}

void
ApWifiMac::SendAssocResp (Mac48Address to, bool success)
{
  NS_LOG_FUNCTION (this << to << success);
  WifiMacHeader hdr;
  hdr.SetAssocResp ();
  hdr.SetAddr1 (to);
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (GetAddress ());
  hdr.SetDsNotFrom ();
  hdr.SetDsNotTo ();
  Ptr<Packet> packet = Create<Packet> ();
  MgtAssocResponseHeader assoc;
  StatusCode code;
  if (success)
    {
      code.SetSuccess ();
    }
  else
    {
      code.SetFailure ();
    }
  assoc.SetSupportedRates (GetSupportedRates ());
  assoc.SetStatusCode (code);

  if (m_htSupported || m_vhtSupported)
    {
      assoc.SetHtCapabilities (GetHtCapabilities ());
      hdr.SetNoOrder ();
    }
  if (m_vhtSupported)
    {
      assoc.SetVhtCapabilities (GetVhtCapabilities ());
    }
  packet->AddHeader (assoc);

  //IdTagger m_it (9999);
  //m_fdje.PrepareEqTime(packet);
  //m_st.WriteSeqValue(packet);
  //m_it.WriteIdValue(packet);
  // The standard is not clear on the correct queue for management
  // frames if we are a QoS AP. The approach taken here is to always
  // use the DCF for these regardless of whether we have a QoS
  // association or not.
  
  //m_dca->Queue (packet, hdr);
  //m_edca[AC_VO]->Queue(packet,hdr);
  QosTag m_qt;
  m_qt.SetTid(7);
  packet->AddPacketTag(m_qt); 
  uint8_t tidQ = 0;
  tidQ = QosUtilsGetTidForPacket (packet);
  m_edca[QosUtilsMapTidToAc(tidQ)]->Queue(packet,hdr);
}

void
ApWifiMac::SendOneBeacon (void)
{
  NS_LOG_FUNCTION (this);
  WifiMacHeader hdr;
  hdr.SetBeacon ();
  hdr.SetAddr1 (Mac48Address::GetBroadcast ());
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (GetAddress ());
  hdr.SetDsNotFrom ();
  hdr.SetDsNotTo ();
  Ptr<Packet> packet = Create<Packet> ();
  MgtBeaconHeader beacon;
  beacon.SetSsid (GetSsid ());
  beacon.SetSupportedRates (GetSupportedRates ());
  beacon.SetBeaconIntervalUs (m_beaconInterval.GetMicroSeconds ());
  if (m_htSupported || m_vhtSupported)
    {
      beacon.SetHtCapabilities (GetHtCapabilities ());
      hdr.SetNoOrder ();
    }
  if (m_vhtSupported)
    {
      beacon.SetVhtCapabilities (GetVhtCapabilities ());
    }
  packet->AddHeader (beacon);

  //The beacon has it's own special queue, so we load it in there
  m_beaconDca->Queue (packet, hdr);
  m_beaconEvent = Simulator::Schedule (m_beaconInterval, &ApWifiMac::SendOneBeacon, this);
}

void
ApWifiMac::TxOk (const WifiMacHeader &hdr)
{
  NS_LOG_FUNCTION (this);
  RegularWifiMac::TxOk (hdr);

  if (hdr.IsAssocResp ()
      && m_stationManager->IsWaitAssocTxOk (hdr.GetAddr1 ()))
    {
      NS_LOG_DEBUG ("associated with sta=" << hdr.GetAddr1 ());
      m_stationManager->RecordGotAssocTxOk (hdr.GetAddr1 ());
    }
}

void
ApWifiMac::TxFailed (const WifiMacHeader &hdr)
{
  NS_LOG_FUNCTION (this);
  RegularWifiMac::TxFailed (hdr);

  if (hdr.IsAssocResp ()
      && m_stationManager->IsWaitAssocTxOk (hdr.GetAddr1 ()))
    {
      NS_LOG_DEBUG ("assoc failed with sta=" << hdr.GetAddr1 ());
      m_stationManager->RecordGotAssocTxFailed (hdr.GetAddr1 ());
    }
}

void
ApWifiMac::Receive (Ptr<Packet> packet, const WifiMacHeader *hdr)
{
  NS_LOG_FUNCTION (this << packet << hdr);
  //NS_LOG_UNCOND ("Receive Entered.");
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ApWifiMac::Receive: one packet with UID: " << packet->GetUid() << " arrives and I am " << GetAddress());

  Mac48Address from = hdr->GetAddr2 ();
  MacTagger m_mt;

  IdTagger m_it_tmp (0.0);
  m_it_tmp.RecordIdValue(packet);

  //m_st.RecordSeqValue(packet);
  //m_dje.RecordRx(packet);

  //IdTagger m_it (0);
  //m_it.RecordIdValue(packet);
  m_pt.RecordProbeValue(packet);

  //newly added for potential bug
  StopTrafficTagger m_stt_tmp;
  m_stt_tmp.RecordStopTrafficValue(packet);
  if(m_stt_tmp.GetLastStopTraffic() != 0)
  {
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": Oops! I find a stop-traffic packet at ApWifiMac::Receive with local address " << GetAddress());
  }

  /*if(m_pt.GetLastProbe() == 0)
  {
	  int i=0;
	  if(hdr->IsData())
		  {
		  	  i=1;
		  }
	  NS_LOG_UNCOND ("" << GetAddress() << "isData=" << i << "PktLen=" << packet->GetSize() << "---No PT INFO FOUND.");
  }
  else
  {
	  int i=0;
	  if(hdr->IsData())
		  {
		  	  i=1;
		  }
	  NS_LOG_UNCOND ("" << GetAddress() << "isData=" << i << "PktLen=" << packet->GetSize() << "PT INFO FOUND.");

  }*/

  //Added by Y.B.
  //m_qde.RecordDqTime(packet);
  //NS_LOG_UNCOND (hdr->GetAddr1() << "," << hdr->GetAddr2() << "," << hdr->GetSequenceNumber() << "," << packet->GetSize() << "QueueingDelayOnReceiverSide" << m_qde.GetLastQDelay());

  if (hdr->IsData ())
    {
      Mac48Address bssid = hdr->GetAddr1 ();
      //NS_LOG_UNCOND("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Uid: " << packet->GetUid() << " Enter into hdr->IsData ()" << ": !hdr->IsFromDs():" << !hdr->IsFromDs() << ", hdr->IsToDs(): " << hdr->IsToDs() << ", bssid: " << bssid << ", GetAddress():" << GetAddress() << "m_stationManager->IsAssociated(from):" << m_stationManager->IsAssociated(from) << ", m_stationManager->IsWaitAssocTxOk(from): " << m_stationManager->IsWaitAssocTxOk(from));
      if (!hdr->IsFromDs ()
          && hdr->IsToDs ()
          && bssid == GetAddress ()
          && m_stationManager->IsAssociated (from))
        {
    	  //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into !hdr->IsFromDs ()");
          Mac48Address to = hdr->GetAddr3 ();
          if (to == GetAddress ())
            {
              NS_LOG_DEBUG ("frame for me from=" << from);
              //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into to == GetAddress ()");
              //NS_LOG_UNCOND ("" << Simulator::Now() << " Local=" << GetAddress() << ": frame for me from=" << from);
              if (hdr->IsQosData ())
                {
                  if (hdr->IsQosAmsdu ())
                    {
                      NS_LOG_DEBUG ("Received A-MSDU from=" << from << ", size=" << packet->GetSize ());
                      DeaggregateAmsduAndForward (packet, hdr);
                      packet = 0;
                    }
                  else
                    {
                	  if(m_pt.GetLastProbe() == 1)
                	  {
                		  m_mt.SetLastMac(GetAddress());
                		  m_mt.WriteMacValue(packet);
                		  //NS_LOG_UNCOND ("" << GetAddress() << "tag pkt with m_mt");
                		  m_mt.RecordMacValue(packet);
                		  //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into to == GetAddress () else with address " << GetAddress() << "and tagged with " << m_mt.GetLastMac());
                		  //NS_LOG_UNCOND (hdr->GetAddr1() << "," << hdr->GetAddr2() << "," << hdr->GetSequenceNumber() << "," << packet->GetSize() << ",Node" << m_it.GetLastId() << ",PacketTagger" << m_st.GetLastSeq() << ",MacLayerDelay" << m_dje.GetLastDelay());
                		  m_mt.RecordMacValue(packet);
                		  //NS_LOG_UNCOND ("NOW M_MT BECOMES " << m_mt.GetLastMac());

                	  }
		      NS_LOG_UNCOND_YB ("1: " << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << "Forwarded up");
                      ForwardUp (packet, from, bssid);
                    }
                }
              else
                {
		  NS_LOG_UNCOND_YB ("2: " << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << "Forwarded up");
            	  ForwardUp (packet, from, bssid);
                }
            }
          else if (to.IsGroup ()
                   || m_stationManager->IsAssociated (to))
            {
              NS_LOG_DEBUG ("forwarding frame from=" << from << ", to=" << to);
              Ptr<Packet> copy = packet->Copy ();

              //If the frame we are forwarding is of type QoS Data,
              //then we need to preserve the UP in the QoS control
              //header...
              if (hdr->IsQosData ())
                {
                  ForwardDown (packet, from, to, hdr->GetQosTid ());
                }
              else
                {
                  ForwardDown (packet, from, to);
                }

              //NS_LOG_UNCOND (hdr->GetAddr1() << "," << hdr->GetAddr2() << "," << hdr->GetSequenceNumber() << "," << packet->GetSize() << ",Node" << m_it.GetLastId() << ",PacketTagger" << m_st.GetLastSeq() << ",MacLayerDelay" << m_dje.GetLastDelay());
	      NS_LOG_UNCOND_YB ("3: " << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << "Forwarded up");
              ForwardUp (copy, from, to);
            }
          else
            {
        	  //NS_LOG_UNCOND (hdr->GetAddr1() << "," << hdr->GetAddr2() << "," << hdr->GetSequenceNumber() << "," << packet->GetSize() << ",Node" << m_it.GetLastId() << ",PacketTagger" << m_st.GetLastSeq() << ",MacLayerDelay" << m_dje.GetLastDelay());
        	  NS_LOG_UNCOND_YB ("4: " << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << "Forwarded up");
        	  ForwardUp (packet, from, to);
            }
        }
      else if (hdr->IsFromDs ()
               && hdr->IsToDs ())
        {
          //this is an AP-to-AP frame
          //we ignore for now.
          NS_LOG_UNCOND ("" << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << " Dropped due to AP-to-AP frame.");
          NotifyRxDrop (packet);
        }
      else
        {
          // we can ignore these frames since
          // they are not targeted at the AP
    	  /*NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into not targeted at the AP");
    	  if(m_pt.GetLastProbe() == 1)
    	  {
    		  NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "However this is a probing packet!");
    		  Mac48Address probeTo = hdr->GetAddr3 ();
           	  //m_mt.SetLastMac(GetAddress());
           	  //m_mt.WriteMacValue(packet);
           	  //m_mt.RecordMacValue(packet);
    		  m_mt.SetLastMac(GetAddress());
    		  m_mt.WriteMacValue(packet);
    		  m_mt.RecordMacValue(packet);
    		  m_apt.WriteAlterPathValue(packet,1);
    		  ForwardUp (packet, from, probeTo);
    		  NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Packet Forwarded!");
    	  }
    	  else
    	  {
    		  NotifyRxDrop (packet);
    	  }*/
	  NS_LOG_UNCOND ("" << Simulator::Now() << ": packet with Uid: " << packet->GetUid() << "Dropped.");
    	  NotifyRxDrop (packet);
        }
      return;
    }
  else if (hdr->IsMgt ())
    {
	  //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into hdr->IsMgt");
      if (hdr->IsProbeReq ())
        {
    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << GetAddress() << " received a ProbeReq. Start processing.");
    	  //Added by Y.B.
    	  UidTagger m_ut;
    	  RoleTagger m_rt;
    	  m_ut.RecordUidValue(packet);
    	  m_rt.RecordRoleValue(packet);
    	  if(m_ut.GetLastUid() != 0 && m_rt.GetLastRole() != 0)
    	  {
    		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": packet with src addr: " << hdr->GetAddr2() << " is detected at " << GetAddress() << " with UID tag: " << m_ut.GetLastUid() << " and Role tag: " << m_rt.GetLastRole());
    		  m_currentProRUid[m_it_tmp.GetLastId()]=m_ut.GetLastUid();
    		  m_currentProRRole[m_it_tmp.GetLastId()]=m_rt.GetLastRole();
    	  }

    	  MgtProbeRequestHeader probe;
    	  packet->RemoveHeader(probe);
    	  //if(probe.GetSsid())
    	  //{
    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << GetAddress() << " received a ProbeReq with SSID " << probe.GetSsid() << " from " << hdr->GetAddr2());
    	  if(hdr->GetAddr2() == "00:00:00:00:00:07" && !probe.GetSsid().IsEqual(this->GetSsid()))
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "this is " << this->GetSsid() << ": 00:00:00:00:00:07 switches to " << probe.GetSsid());
    		  dropOrNot = 1;
    	  }
    	  else if(hdr->GetAddr2() == "00:00:00:00:00:07" && probe.GetSsid().IsEqual(this->GetSsid()))
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "this is " << this->GetSsid() << ": 00:00:00:00:00:07 switches back to " << probe.GetSsid());
    		  dropOrNot = 0;
    	  }
    	  //}

          NS_ASSERT (hdr->GetAddr1 ().IsBroadcast ());
          //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() <<  "Enter into hdr->IsProbeReq");
          if(probe.GetSsid().IsEqual(this->GetSsid()))
          {
              SendProbeResp (from,m_it_tmp.GetLastId());
          }
          return;
        }
      else if (hdr->GetAddr1 () == GetAddress ())
        {
    	  //NS_LOG_UNCOND ("" << GetAddress() << "PktLen=" << packet->GetSize() << "PT INFO=" << m_pt.GetLastProbe() << "Enter into hdr->GetAddr1 () == GetAddress ()");
          if (hdr->IsAssocReq ())
            {
              //first, verify that the the station's supported
              //rate set is compatible with our Basic Rate set
              MgtAssocRequestHeader assocReq;
              packet->RemoveHeader (assocReq);
              SupportedRates rates = assocReq.GetSupportedRates ();
              bool problem = false;
              for (uint32_t i = 0; i < m_stationManager->GetNBasicModes (); i++)
                {
                  WifiMode mode = m_stationManager->GetBasicMode (i);
                  if (!rates.IsSupportedRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1)))
                    {
                      problem = true;
                      break;
                    }
                }
              if (m_htSupported)
                {
                  //check that the STA supports all MCSs in Basic MCS Set
                  HtCapabilities htcapabilities = assocReq.GetHtCapabilities ();
                  for (uint32_t i = 0; i < m_stationManager->GetNBasicMcs (); i++)
                    {
                      WifiMode mcs = m_stationManager->GetBasicMcs (i);
                      if (!htcapabilities.IsSupportedMcs (mcs.GetMcsValue ()))
                        {
                          problem = true;
                          break;
                        }
                    }
                }
              if (m_vhtSupported)
                {
                  //check that the STA supports all MCSs in Basic MCS Set
                  VhtCapabilities vhtcapabilities = assocReq.GetVhtCapabilities ();
                  for (uint32_t i = 0; i < m_stationManager->GetNBasicMcs (); i++)
                    {
                      WifiMode mcs = m_stationManager->GetBasicMcs (i);
                      if (!vhtcapabilities.IsSupportedTxMcs (mcs.GetMcsValue ()))
                        {
                          problem = true;
                          break;
                        }
                    }
                }
              if (problem)
                {
                  //One of the Basic Rate set mode is not
                  //supported by the station. So, we return an assoc
                  //response with an error status.
                  SendAssocResp (hdr->GetAddr2 (), false);
                }
              else
                {
                  //station supports all rates in Basic Rate Set.
                  //record all its supported modes in its associated WifiRemoteStation
                  for (uint32_t j = 0; j < m_phy->GetNModes (); j++)
                    {
                      WifiMode mode = m_phy->GetMode (j);
                      if (rates.IsSupportedRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1)))
                        {
                          m_stationManager->AddSupportedMode (from, mode);
                        }
                    }
                  if (m_htSupported)
                    {
                      HtCapabilities htcapabilities = assocReq.GetHtCapabilities ();
                      m_stationManager->AddStationHtCapabilities (from,htcapabilities);
                      for (uint32_t j = 0; j < m_phy->GetNMcs (); j++)
                        {
                          WifiMode mcs = m_phy->GetMcs (j);
                          if (mcs.GetModulationClass () == WIFI_MOD_CLASS_HT && htcapabilities.IsSupportedMcs (mcs.GetMcsValue ()))
                            {
                              m_stationManager->AddSupportedMcs (from, mcs);
                            }
                        }
                    }
                  if (m_vhtSupported)
                    {
                      VhtCapabilities vhtCapabilities = assocReq.GetVhtCapabilities ();
                      m_stationManager->AddStationVhtCapabilities (from, vhtCapabilities);
                      for (uint32_t i = 0; i < m_phy->GetNMcs (); i++)
                        {
                          WifiMode mcs = m_phy->GetMcs (i);
                          if (mcs.GetModulationClass () == WIFI_MOD_CLASS_VHT && vhtCapabilities.IsSupportedTxMcs (mcs.GetMcsValue ()))
                            {
                              m_stationManager->AddSupportedMcs (hdr->GetAddr2 (), mcs);
                              //here should add a control to add basic MCS when it is implemented
                            }
                        }
                    }
                  m_stationManager->RecordWaitAssocTxOk (from);

                  // send assoc response with success status.
                  SendAssocResp (hdr->GetAddr2 (), true);
                }
              return;
            }
          else if (hdr->IsDisassociation ())
            {
              m_stationManager->RecordDisassociated (from);
              NS_LOG_UNCOND ("" << Simulator::Now() << ": " << hdr->GetAddr4() << " disassociated from " << this->GetAddress());
              return;
            }
        }
    }

  //Invoke the receive handler of our parent class to deal with any
  //other frames. Specifically, this will handle Block Ack-related
  //Management Action frames.
  RegularWifiMac::Receive (packet, hdr);
}

void
ApWifiMac::DeaggregateAmsduAndForward (Ptr<Packet> aggregatedPacket,
                                       const WifiMacHeader *hdr)
{
  NS_LOG_FUNCTION (this << aggregatedPacket << hdr);
  MsduAggregator::DeaggregatedMsdus packets =
    MsduAggregator::Deaggregate (aggregatedPacket);

  for (MsduAggregator::DeaggregatedMsdusCI i = packets.begin ();
       i != packets.end (); ++i)
    {
      if ((*i).second.GetDestinationAddr () == GetAddress ())
        {
          ForwardUp ((*i).first, (*i).second.GetSourceAddr (),
                     (*i).second.GetDestinationAddr ());
        }
      else
        {
          Mac48Address from = (*i).second.GetSourceAddr ();
          Mac48Address to = (*i).second.GetDestinationAddr ();
          NS_LOG_DEBUG ("forwarding QoS frame from=" << from << ", to=" << to);
          ForwardDown ((*i).first, from, to, hdr->GetQosTid ());
        }
    }
}

void
ApWifiMac::DoInitialize (void)
{
  NS_LOG_FUNCTION (this);
  m_beaconDca->Initialize ();
  m_beaconEvent.Cancel ();
  if (m_enableBeaconGeneration)
    {
      if (m_enableBeaconJitter)
        {
          int64_t jitter = m_beaconJitter->GetValue (0, m_beaconInterval.GetMicroSeconds ());
          NS_LOG_DEBUG ("Scheduling initial beacon for access point " << GetAddress () << " at time " << jitter << " microseconds");
          m_beaconEvent = Simulator::Schedule (MicroSeconds (jitter), &ApWifiMac::SendOneBeacon, this);
        }
      else
        {
          NS_LOG_DEBUG ("Scheduling initial beacon for access point " << GetAddress () << " at time 0");
          m_beaconEvent = Simulator::ScheduleNow (&ApWifiMac::SendOneBeacon, this);
        }
    }
  RegularWifiMac::DoInitialize ();
}

} //namespace ns3
