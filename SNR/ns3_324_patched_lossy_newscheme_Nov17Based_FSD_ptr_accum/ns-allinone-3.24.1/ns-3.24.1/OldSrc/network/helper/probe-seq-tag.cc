/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "probe-seq-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to ProbeSeqform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ProbeSeqTag : public Tag
{
public:
	ProbeSeqTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeProbeSeq
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint16_t GetProbeSeq (void) const;
  void SetProbeSeq (uint16_t ProbeSeq) ;

private:
  uint16_t m_ProbeSeq; //!< The time stored in the tag
};

ProbeSeqTag::ProbeSeqTag ()
  : m_ProbeSeq (0)
{
}

TypeId
ProbeSeqTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ProbeSeqTag")
    .SetParent<Tag> ()
    .SetGroupName("ProbeSeqTag")
    .AddConstructor<ProbeSeqTag> ()
 //   .AddAttribute ("LastSeqValue",
 //                  "Last Sequence Value Obtained",
 //                  DoubleValue (0.0),
 //                  MakeIntegerAccessor (&ProbeSeqTag::GetProbeSeq),
 //                  MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ProbeSeqTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ProbeSeqTag::GetSerializedSize (void) const
{
  return sizeof(uint16_t);
}
void
ProbeSeqTag::Serialize (TagBuffer i) const
{
  i.WriteU16(m_ProbeSeq);
}
void
ProbeSeqTag::Deserialize (TagBuffer i)
{
  m_ProbeSeq = i.ReadU16();
}
void
ProbeSeqTag::Print (std::ostream &os) const
{
  os << "m_ProbeSeq=" << m_ProbeSeq;
}
uint16_t
ProbeSeqTag::GetProbeSeq (void) const
{
  return m_ProbeSeq;
}

void
ProbeSeqTag::SetProbeSeq (uint16_t ProbeSeq)
{
	m_ProbeSeq=ProbeSeq;
}

ProbeSeqTagger::ProbeSeqTagger (uint16_t id)
{
	this->lastProbeSeq=id;
}

void
ProbeSeqTagger::WriteProbeSeqValue (Ptr<const Packet> packet)
{
  ProbeSeqTag tag;
  tag.SetProbeSeq(this->lastProbeSeq);
  packet->AddByteTag (tag);
  this->lastProbeSeq = this->lastProbeSeq+1; //% (MAX_PROBE_TRAIN_LENGTH+1);
}
void
ProbeSeqTagger::RecordProbeSeqValue (Ptr<const Packet> packet)
{
  ProbeSeqTag tag;
  lastProbeSeq = 0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastProbeSeq=tag.GetProbeSeq ();

}

uint16_t
ProbeSeqTagger::GetLastProbeSeq (void) const
{
  return lastProbeSeq;
}

void
ProbeSeqTagger::ResetLastProbeSeq()
{
	lastProbeSeq=0;
}

void
ProbeSeqTagger::SetLastProbeSeq(uint16_t probeSeq)
{
	lastProbeSeq = probeSeq;
}


} // namespace ns3
