/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "probe-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Probeform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ProbeTag : public Tag
{
public:
	ProbeTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetProbe (void) const;
  void SetProbe (double Probe) ;

private:
  double m_Probe; //!< The time stored in the tag
};

ProbeTag::ProbeTag ()
  : m_Probe (0.0)
{
}

TypeId
ProbeTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ProbeTag")
    .SetParent<Tag> ()
    .SetGroupName("ProbeTag")
    .AddConstructor<ProbeTag> ()
    .AddAttribute ("LastProbe",
                   "Last Probe Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ProbeTag::GetProbe),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ProbeTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ProbeTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ProbeTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Probe);
}
void
ProbeTag::Deserialize (TagBuffer i)
{
  m_Probe = i.ReadDouble ();
}
void
ProbeTag::Print (std::ostream &os) const
{
  os << "m_Probe=" << m_Probe;
}
double
ProbeTag::GetProbe (void) const
{
  return m_Probe;
}

void
ProbeTag::SetProbe (double Probe)
{
	m_Probe=Probe;
}

ProbeTagger::ProbeTagger ()
{
	this->lastProbe=0.0;
}

void
ProbeTagger::WriteProbeValue (Ptr<const Packet> packet, double tagValue)
{
  ProbeTag tag;
  tag.SetProbe(tagValue);
  packet->AddByteTag (tag);
}
int
ProbeTagger::RecordProbeValue (Ptr<const Packet> packet)
{
  ProbeTag tag;
  lastProbe=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastProbe=tag.GetProbe ();
  return (int)lastProbe;
}

double
ProbeTagger::GetLastProbe (void) const
{
  return lastProbe;
}

} // namespace ns3
