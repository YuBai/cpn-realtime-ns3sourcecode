/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "owner-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Idform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class OwnerTag : public Tag
{
public:
	OwnerTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetId (void) const;
  void SetId (double Id) ;

private:
  double m_Id; //!< The time stored in the tag
};

OwnerTag::OwnerTag ()
  : m_Id (0.0)
{
}

TypeId
OwnerTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::OwnerTag")
    .SetParent<Tag> ()
    .SetGroupName("OwnerTag")
    .AddConstructor<OwnerTag> ()
    .AddAttribute ("LastId",
                   "Last Id Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&OwnerTag::GetId),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
OwnerTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
OwnerTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
OwnerTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Id);
}
void
OwnerTag::Deserialize (TagBuffer i)
{
  m_Id = i.ReadDouble();
}
void
OwnerTag::Print (std::ostream &os) const
{
  os << "m_Id=" << m_Id;
}
double
OwnerTag::GetId (void) const
{
  return m_Id;
}

void
OwnerTag::SetId (double Id)
{
	m_Id=Id;
}

OwnerTagger::OwnerTagger (double id)
{
	this->lastId=id;
}

void
OwnerTagger::WriteIdValue (Ptr<const Packet> packet)
{
  OwnerTag tag;
  tag.SetId(this->lastId);
  packet->AddByteTag (tag);
  this->lastId++;
}
void
OwnerTagger::RecordIdValue (Ptr<const Packet> packet)
{
  OwnerTag tag;
  lastId = 0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastId=tag.GetId ();

}

double
OwnerTagger::GetLastId (void) const
{
  return lastId;
}

} // namespace ns3
