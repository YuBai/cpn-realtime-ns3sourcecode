/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "finish-probe-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to FinishProbeform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FinishProbeTag : public Tag
{
public:
	FinishProbeTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetFinishProbe (void) const;
  void SetFinishProbe (double FinishProbe) ;

private:
  double m_FinishProbe; //!< The time stored in the tag
};

FinishProbeTag::FinishProbeTag ()
  : m_FinishProbe (0.0)
{
}

TypeId
FinishProbeTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FinishProbeTag")
    .SetParent<Tag> ()
    .SetGroupName("finishProbeTag")
    .AddConstructor<FinishProbeTag> ()
    .AddAttribute ("LastFinishProbe",
                   "Last FinishProbe Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&FinishProbeTag::GetFinishProbe),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
FinishProbeTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FinishProbeTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
FinishProbeTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_FinishProbe);
}
void
FinishProbeTag::Deserialize (TagBuffer i)
{
  m_FinishProbe = i.ReadDouble ();
}
void
FinishProbeTag::Print (std::ostream &os) const
{
  os << "m_FinishProbe=" << m_FinishProbe;
}
double
FinishProbeTag::GetFinishProbe (void) const
{
  return m_FinishProbe;
}

void
FinishProbeTag::SetFinishProbe (double FinishProbe)
{
	m_FinishProbe=FinishProbe;
}

FinishProbeTagger::FinishProbeTagger ()
{
	this->lastFinishProbe=0.0;
}

void
FinishProbeTagger::WriteFinishProbeValue (Ptr<const Packet> packet, double tagValue)
{
  FinishProbeTag tag;
  tag.SetFinishProbe(tagValue);
  packet->AddByteTag (tag);
}
int
FinishProbeTagger::RecordFinishProbeValue (Ptr<const Packet> packet)
{
  FinishProbeTag tag;
  lastFinishProbe=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastFinishProbe=tag.GetFinishProbe ();
  return (int)lastFinishProbe;
}

double
FinishProbeTagger::GetLastFinishProbe (void) const
{
  return lastFinishProbe;
}

} // namespace ns3
