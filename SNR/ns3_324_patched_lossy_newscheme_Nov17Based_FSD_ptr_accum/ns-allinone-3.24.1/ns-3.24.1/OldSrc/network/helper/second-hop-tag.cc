/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "second-hop-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to SecondHopform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class SecondHopTag : public Tag
{
public:
	SecondHopTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetSecondHop (void) const;
  void SetSecondHop (double SecondHop) ;

private:
  double m_SecondHop; //!< The time stored in the tag
};

SecondHopTag::SecondHopTag ()
{
}

TypeId
SecondHopTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::SecondHopTag")
    .SetParent<Tag> ()
    .SetGroupName("SecondHopTag")
    .AddConstructor<SecondHopTag> ()
  ;
  return tid;
}
TypeId
SecondHopTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
SecondHopTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
SecondHopTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_SecondHop);
}
void
SecondHopTag::Deserialize (TagBuffer i)
{
  m_SecondHop = i.ReadDouble ();
}
void
SecondHopTag::Print (std::ostream &os) const
{
  os << "m_SecondHop=" << m_SecondHop;
}
double
SecondHopTag::GetSecondHop (void) const
{
  return m_SecondHop;
}

void
SecondHopTag::SetSecondHop (double SecondHop)
{
	m_SecondHop=SecondHop;
}

SecondHopTagger::SecondHopTagger ()
{
	this->lastSecondHop=0;
}

void
SecondHopTagger::WriteSecondHopValue (Ptr<const Packet> packet, double tagValue)
{
  SecondHopTag tag;
  tag.SetSecondHop(tagValue);
  packet->AddPacketTag (tag);
}
double
SecondHopTagger::RecordSecondHopValue (Ptr<const Packet> packet)
{
  SecondHopTag tag;
  bool found;
  lastSecondHop = 0;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastSecondHop=tag.GetSecondHop ();
  return (int)lastSecondHop;
}

double
SecondHopTagger::GetLastSecondHop (void) const
{
  return lastSecondHop;
}


} // namespace ns3
