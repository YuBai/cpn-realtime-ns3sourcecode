/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-sixth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeSixthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeSixthMacTag : public Tag
{
public:
	FakeSixthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeSixthMac (void) const;
  void SetFakeSixthMac (uint8_t FakeSixthMac) ;

private:
  uint8_t m_FakeSixthMac; //!< The time stored in the tag
};

FakeSixthMacTag::FakeSixthMacTag ()
  : m_FakeSixthMac (0)
{
}

TypeId
FakeSixthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeSixthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeSixthMacTag")
    .AddConstructor<FakeSixthMacTag> ()
    .AddAttribute ("LastFakeSixthMac",
                   "Last FakeSixthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeSixthMacTag::GetFakeSixthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeSixthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeSixthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeSixthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeSixthMac);
}
void
FakeSixthMacTag::Deserialize (TagBuffer i)
{
  m_FakeSixthMac = i.ReadU8 ();
}
void
FakeSixthMacTag::Print (std::ostream &os) const
{
  os << "m_FakeSixthMac=" << m_FakeSixthMac;
}
uint8_t
FakeSixthMacTag::GetFakeSixthMac (void) const
{
  return m_FakeSixthMac;
}

void
FakeSixthMacTag::SetFakeSixthMac (uint8_t FakeSixthMac)
{
	m_FakeSixthMac=FakeSixthMac;
}

FakeSixthMacTagger::FakeSixthMacTagger ()
{
	this->lastFakeSixthMac=0.0;
}

void
FakeSixthMacTagger::WriteFakeSixthMacValue (Ptr<const Packet> packet)
{
  FakeSixthMacTag tag;
  tag.SetFakeSixthMac(this->lastFakeSixthMac);
  packet->AddByteTag (tag);
}
void
FakeSixthMacTagger::RecordFakeSixthMacValue (Ptr<const Packet> packet)
{
  FakeSixthMacTag tag;
  lastFakeSixthMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeSixthMac=tag.GetFakeSixthMac ();

}

uint8_t
FakeSixthMacTagger::GetLastFakeSixthMac (void) const
{
  return lastFakeSixthMac;
}

void
FakeSixthMacTagger::SetLastFakeSixthMac (uint8_t FakeSixthMac)
{
	lastFakeSixthMac=FakeSixthMac;
}


} // namespace ns3
