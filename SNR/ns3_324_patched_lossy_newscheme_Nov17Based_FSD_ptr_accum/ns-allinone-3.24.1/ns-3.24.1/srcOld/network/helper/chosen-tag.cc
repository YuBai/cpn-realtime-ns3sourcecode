/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "chosen-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Chosenform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ChosenTag : public Tag
{
public:
	ChosenTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetChosen (void) const;
  void SetChosen (double Chosen) ;

private:
  double m_Chosen; //!< The time stored in the tag
};

ChosenTag::ChosenTag ()
  : m_Chosen (0.0)
{
}

TypeId
ChosenTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ChosenTag")
    .SetParent<Tag> ()
    .SetGroupName("ChosenTag")
    .AddConstructor<ChosenTag> ()
    .AddAttribute ("LastChosen",
                   "Last Chosen Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ChosenTag::GetChosen),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ChosenTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ChosenTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ChosenTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Chosen);
}
void
ChosenTag::Deserialize (TagBuffer i)
{
  m_Chosen = i.ReadDouble ();
}
void
ChosenTag::Print (std::ostream &os) const
{
  os << "m_Chosen=" << m_Chosen;
}
double
ChosenTag::GetChosen (void) const
{
  return m_Chosen;
}

void
ChosenTag::SetChosen (double Chosen)
{
	m_Chosen=Chosen;
}

ChosenTagger::ChosenTagger ()
{
	this->lastChosen=0.0;
}

void
ChosenTagger::WriteChosenValue (Ptr<const Packet> packet, double tagValue)
{
  ChosenTag tag;
  tag.SetChosen(tagValue);
  packet->AddByteTag (tag);
}
int
ChosenTagger::RecordChosenValue (Ptr<const Packet> packet)
{
  ChosenTag tag;
  lastChosen=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastChosen=tag.GetChosen ();
  return (int)lastChosen;
}

double
ChosenTagger::GetLastChosen (void) const
{
  return lastChosen;
}

} // namespace ns3
