/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the Ratee that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "rate-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to Rateform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class RateTag : public Tag
{
public:
	RateTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetRate (void) const;
  void SetRate (double Rate) ;

private:
  double m_Rate; //!< The time stored in the tag
};

RateTag::RateTag ()
{
}

TypeId
RateTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::RateTag")
    .SetParent<Tag> ()
    .SetGroupName("RateTag")
    .AddConstructor<RateTag> ()
  ;
  return tid;
}
TypeId
RateTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
RateTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
RateTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Rate);
}
void
RateTag::Deserialize (TagBuffer i)
{
  m_Rate = i.ReadDouble ();
}
void
RateTag::Print (std::ostream &os) const
{
  os << "m_Rate=" << m_Rate;
}
double
RateTag::GetRate (void) const
{
  return m_Rate;
}

void
RateTag::SetRate (double Rate)
{
	m_Rate=Rate;
	//std::clog << "m_Rate now is " << m_Rate << "\n";
}

RateTagger::RateTagger ()
{
	this->lastRate=0;
}

void
RateTagger::WriteRateValue (Ptr<const Packet> packet, double tagValue)
{
  RateTag tag;
  tag.SetRate(tagValue);
  packet->AddPacketTag (tag);
}
double
RateTagger::RecordRateValue (Ptr<const Packet> packet)
{
  RateTag tag;
  lastRate = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastRate=tag.GetRate ();
  return (double)lastRate;
}

double
RateTagger::GetLastRate (void) const
{
  return lastRate;
}


} // namespace ns3
