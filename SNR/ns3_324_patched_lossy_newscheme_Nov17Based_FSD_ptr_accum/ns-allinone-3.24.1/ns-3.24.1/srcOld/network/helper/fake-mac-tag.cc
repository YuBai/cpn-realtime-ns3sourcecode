/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"


namespace ns3 {

/**
 * Tag to Macform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */

FakeMacTagger::FakeMacTagger() :
m_firmt(),
m_secmt(),
m_thimt(),
m_foumt(),
m_fifmt(),
m_sixmt()
{
	m_firmt.SetLastFakeFirstMac((uint8_t)0);
	m_secmt.SetLastFakeSecondMac((uint8_t)0);
	m_thimt.SetLastFakeThirdMac((uint8_t)0);
	m_foumt.SetLastFakeFourthMac((uint8_t)0);
	m_fifmt.SetLastFakeFifthMac((uint8_t)0);
	m_sixmt.SetLastFakeSixthMac((uint8_t)0);

}

void
FakeMacTagger::WriteFakeMacValue (Ptr<const Packet> packet)
{
	m_firmt.WriteFakeFirstMacValue(packet);
	m_secmt.WriteFakeSecondMacValue(packet);
	m_thimt.WriteFakeThirdMacValue(packet);
	m_foumt.WriteFakeFourthMacValue(packet);
	m_fifmt.WriteFakeFifthMacValue(packet);
	m_sixmt.WriteFakeSixthMacValue(packet);

}
void
FakeMacTagger::RecordFakeMacValue (Ptr<const Packet> packet)
{

	lastFakeMacArray[0]=0;
	lastFakeMacArray[1]=0;
	lastFakeMacArray[2]=0;
	lastFakeMacArray[3]=0;
	lastFakeMacArray[4]=0;
	lastFakeMacArray[5]=0;

	m_firmt.RecordFakeFirstMacValue(packet);
	m_secmt.RecordFakeSecondMacValue(packet);
	m_thimt.RecordFakeThirdMacValue(packet);
	m_foumt.RecordFakeFourthMacValue(packet);
	m_fifmt.RecordFakeFifthMacValue(packet);
	m_sixmt.RecordFakeSixthMacValue(packet);

	lastFakeMacArray[0]=m_firmt.GetLastFakeFirstMac();
	lastFakeMacArray[1]=m_secmt.GetLastFakeSecondMac();
	lastFakeMacArray[2]=m_thimt.GetLastFakeThirdMac();
	lastFakeMacArray[3]=m_foumt.GetLastFakeFourthMac();
	lastFakeMacArray[4]=m_fifmt.GetLastFakeFifthMac();
	lastFakeMacArray[5]=m_sixmt.GetLastFakeSixthMac();

	lastFakeMac.CopyFrom(lastFakeMacArray);

}

Mac48Address
FakeMacTagger::GetLastFakeMac (void) const
{
  return lastFakeMac;
}

void
FakeMacTagger::SetLastFakeMac (Mac48Address Mac)
{
	Mac.CopyTo(lastFakeMacArray);
	m_firmt.SetLastFakeFirstMac(lastFakeMacArray[0]);
	m_secmt.SetLastFakeSecondMac(lastFakeMacArray[1]);
	m_thimt.SetLastFakeThirdMac(lastFakeMacArray[2]);
	m_foumt.SetLastFakeFourthMac(lastFakeMacArray[3]);
	m_fifmt.SetLastFakeFifthMac(lastFakeMacArray[4]);
	m_sixmt.SetLastFakeSixthMac(lastFakeMacArray[5]);
	lastFakeMac=Mac;

}

void
FakeMacTagger::ResetFakeMac()
{
	lastFakeMacArray[0]=0;
	lastFakeMacArray[1]=0;
	lastFakeMacArray[2]=0;
	lastFakeMacArray[3]=0;
	lastFakeMacArray[4]=0;
	lastFakeMacArray[5]=0;

	lastFakeMac.CopyFrom(lastFakeMacArray);
}


} // namespace ns3
