/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "hop-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to Hopform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class HopTag : public Tag
{
public:
	HopTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetHop (void) const;
  void SetHop (double Hop) ;

private:
  double m_Hop; //!< The time stored in the tag
};

HopTag::HopTag ()
{
}

TypeId
HopTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::HopTag")
    .SetParent<Tag> ()
    .SetGroupName("HopTag")
    .AddConstructor<HopTag> ()
  ;
  return tid;
}
TypeId
HopTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
HopTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
HopTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Hop);
}
void
HopTag::Deserialize (TagBuffer i)
{
  m_Hop = i.ReadDouble ();
}
void
HopTag::Print (std::ostream &os) const
{
  os << "m_Hop=" << m_Hop;
}
double
HopTag::GetHop (void) const
{
  return m_Hop;
}

void
HopTag::SetHop (double Hop)
{
	m_Hop=Hop;
	//std::clog << "m_Hop now is " << m_Hop << "\n";
}

HopTagger::HopTagger ()
{
	this->lastHop=0;
}

void
HopTagger::WriteHopValue (Ptr<const Packet> packet, double tagValue)
{
  HopTag tag;
  tag.SetHop(tagValue);
  packet->AddPacketTag (tag);
}
double
HopTagger::RecordHopValue (Ptr<const Packet> packet)
{
  HopTag tag;
  lastHop = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastHop=tag.GetHop ();
  return (double)lastHop;
}

double
HopTagger::GetLastHop (void) const
{
  return lastHop;
}


} // namespace ns3
