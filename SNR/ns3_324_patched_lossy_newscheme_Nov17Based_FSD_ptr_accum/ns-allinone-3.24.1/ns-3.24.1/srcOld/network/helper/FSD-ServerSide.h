/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef FSD_SERVERSIDE_H
#define FSD_SERVERSIDE_H

//#define MAX_PROBE_TRAIN_LENGTH_SERVER 100 //length-1


#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/probe-seq-tag.h"
#include "ns3/delay-samples-tag.h"
#include "ns3/probing-delay-estimation.h"
#include "ns3/simulator.h"
#include "ns3/finish-probe-tag.h"
#include "ns3/probe-train-length-tag.h"
#include "ns3/mac-tag.h"
#include "ns3/enqueue-time-tag.h"
#include "ns3/enqueue-time-compensate-tag.h"

namespace ns3 {

/**
 * \ingroup stats
 *
 * \brief quick and dirty delay and jitter estimation
 *
 */
class FSDDelayFeedbacker
{
public:
	FSDDelayFeedbacker ();

  /**
   * \param packet the packet to send over a wire
   *
   * This method should be invoked once on each packet to
   * record within the packet the tx time which is used upon
   * packet reception to calculate the delay and jitter. The
   * tx time is stored in the packet as an ns3::Tag which means
   * that it does not use any network resources and is not
   * taken into account in transmission delay calculations.
   */
  void StoreFSDDelayInfo (Ptr<const Packet> packet);
  void WriteDelaySamples(Ptr<const Packet> packet);
  void RecordDelaySamples(Ptr<const Packet> packet);
  /**
   * \param packet the packet received
   *
   * Invoke this method to update the delay and jitter calculations
   * After a call to this method, \ref GetLastDelay and \ref GetLastJitter
   * will return an updated delay and jitter.
   */
  void CopyTo (double* delayCopy);
  void Reset ();
  bool isFull();
  void FinalizeSampleSet(void);

private:


  double m_delays[MAX_PROBE_TRAIN_LENGTH];
  ProbeSeqTagger m_pst;
  bool fullOrNot;
  DelaySamplesTagger m_dst;
  FinishProbeTagger m_fpt;
  int done;
  Time lastCompletedTime;
  Time timoutThreshold;
  int currentLength;
  ProbeTrainLengthTagger m_ptlt;


  //For Debugging Purpose
  MacTagger m_mt;

};

} // namespace ns3

#endif /* DELAY_JITTER_ESTIMATION_H */
