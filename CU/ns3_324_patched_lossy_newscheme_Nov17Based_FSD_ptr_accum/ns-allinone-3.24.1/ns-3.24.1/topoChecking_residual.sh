#!/bin/bash

#iValue=( 16 16 17 16 16 16 16 16 16 16 16 16 )
#jValue=( 0 1 0 0 0 0 1 1 2 2 3 4 )
#kValue=( 0 0 0 1 0 0 0 0 0 0 0 0 )
#lValue=( 0 0 0 0 1 2 0 1 0 1 0 0 )
#mValue=( 4 3 3 3 3 2 3 2 2 1 1 0 )

iValue=( 16 16 16 )
jValue=( 0 0 1 )
kValue=( 0 1 1 )
lValue=( 0 3 1 )
mValue=( 1 0 1 )

#interval=( 0.001033 0.000693 0.000522 0.000418 0.000349 0.000299 0.000262 0.000233 0.000210 ) 
#interval=( 0.055804 0.002029 0.001033 0.000693 0.000522 0.000418 0.000349 0.000299 0.000262 0.000233 0.000210 )

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL.cc"
slurmName="testCVC.slurm"

for((xx=0;xx<3;xx++))
	do
		i=${iValue[xx]}
		j=${jValue[xx]}
		k=${kValue[xx]}
		l=${lValue[xx]}
		m=${mValue[xx]}

		echo $i $j $k $l $m

		cp scratch/$fileName scratch/"$i""$j""$k""$l""$m"_$fileName
		cp $slurmName "$i""$j""$k""$l""$m"_$slurmName	

		sed -i "s/17,1,1,1,0/$i,$j,$k,$l,$m/g" scratch/"$i""$j""$k""$l""$m"_$fileName
		
                echo "./waf --run "$i""$j""$k""$l""$m"_PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL > logggTest_contentionVScontention_sameTopoWithCVCL_iIs"$i"_jIs"$j"_kIs"$k"_lIs"$l"_mIs"$m" 2>&1" >> "$i""$j""$k""$l""$m"_"$slurmName"

		echo sbatch "$i""$j""$k""$l""$m"_"$slurmName" >> test_CVC.slurm

done

./waf build
sbatch test_CVC.slurm
