#!/bin/bash

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVSchannelLim.cc"
slurmName="testCVC.slurm"
potentialCaseList="potentialCaseList"

	while read l; do

		
		echo $l
		a="$( echo $l | cut -d, -f1 )"
		b="$( echo $l | cut -d, -f2 )"
		c="$( echo $l | cut -d, -f3 )"
		d="$( echo $l | cut -d, -f4 )"

		let e=20-a
		let f=a-120+130

		echo $a $b $c $d $e $f

		#a: distance between user and distance of AP2 (should be equal to a-120)
		#b: speed of probing
		#c: amount of user playing games, 20-c is the amount of user keeping silent
		#d: speed of probing, should be as same as $b

		cp scratch/$fileName scratch/"$a"_"$b"_"$c"_"$d"_$fileName
		cp $slurmName "$a"_"$b"_"$c"_"$d"_$slurmName

		sed -i "s/17,1,1,1,0/$c,$e,1,1,0/g" scratch/"$a"_"$b"_"$c"_"$d"_$fileName
		sed -i -e "s/posi.x = 200/posi.x = $f/g" -e "s/posi.y = 200/posi.y = $f/g" scratch/"$a"_"$b"_"$c"_"$d"_$fileName
		sed -i "s/Seconds (0.0002))/Seconds ($b))/g" scratch/"$a"_"$b"_"$c"_"$d"_$fileName

		echo "./waf --run "$a"_"$b"_"$c"_"$d"_PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVSchannelLim > logggTest_aIs"$a"_bIs"$b"_cIs"$c"_dIs"$d"_eIs"$e"_contentionVSchannelLim.err 2>&1" >> "$a"_"$b"_"$c"_"$d"_$slurmName 
											
		echo "sbatch "$a"_"$b"_"$c"_"$d"_"$slurmName"" >> test_CVC.slurm 


	done < $potentialCaseList


#./waf build
