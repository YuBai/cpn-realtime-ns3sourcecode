/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-fourth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeFourthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeFourthMacTag : public Tag
{
public:
	FakeFourthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeFourthMac (void) const;
  void SetFakeFourthMac (uint8_t FakeFourthMac) ;

private:
  uint8_t m_FakeFourthMac; //!< The time stored in the tag
};

FakeFourthMacTag::FakeFourthMacTag ()
  : m_FakeFourthMac (0)
{
}

TypeId
FakeFourthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeFourthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeFourMacTag")
    .AddConstructor<FakeFourthMacTag> ()
    .AddAttribute ("LastFakeFourthMac",
                   "Last FakeFourthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeFourthMacTag::GetFakeFourthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeFourthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeFourthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeFourthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeFourthMac);
}
void
FakeFourthMacTag::Deserialize (TagBuffer i)
{
  m_FakeFourthMac = i.ReadU8 ();
}
void
FakeFourthMacTag::Print (std::ostream &os) const
{
  os << "m_FakeFourthMac=" << m_FakeFourthMac;
}
uint8_t
FakeFourthMacTag::GetFakeFourthMac (void) const
{
  return m_FakeFourthMac;
}

void
FakeFourthMacTag::SetFakeFourthMac (uint8_t FakeFourthMac)
{
	m_FakeFourthMac=FakeFourthMac;
}

FakeFourthMacTagger::FakeFourthMacTagger ()
{
	this->lastFakeFourthMac=0.0;
}

void
FakeFourthMacTagger::WriteFakeFourthMacValue (Ptr<const Packet> packet)
{
  FakeFourthMacTag tag;
  tag.SetFakeFourthMac(this->lastFakeFourthMac);
  packet->AddByteTag (tag);
}
void
FakeFourthMacTagger::RecordFakeFourthMacValue (Ptr<const Packet> packet)
{
  FakeFourthMacTag tag;
  SetLastFakeFourthMac(0);
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeFourthMac=tag.GetFakeFourthMac ();

}

uint8_t
FakeFourthMacTagger::GetLastFakeFourthMac (void) const
{
  return lastFakeFourthMac;
}

void
FakeFourthMacTagger::SetLastFakeFourthMac (uint8_t FakeFourthMac)
{
	lastFakeFourthMac=FakeFourthMac;
}


} // namespace ns3
