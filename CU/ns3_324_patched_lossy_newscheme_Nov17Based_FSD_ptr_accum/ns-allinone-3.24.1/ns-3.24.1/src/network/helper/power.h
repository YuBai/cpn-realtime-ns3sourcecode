/*
 * power.h
 *
 * Code generation for function 'power'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

#ifndef __POWER_H__
#define __POWER_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "FSDFunctionReference_types.h"

/* Function Declarations */
extern void b_power(const double a[6], double y[6]);
extern void power(const double a[18], double y[18]);
#endif
/* End of code generation (power.h) */
