/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "third-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to ThirdMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ThirdMacTag : public Tag
{
public:
	ThirdMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetThirdMac (void) const;
  void SetThirdMac (uint8_t ThirdMac) ;

private:
  uint8_t m_ThirdMac; //!< The time stored in the tag
};

ThirdMacTag::ThirdMacTag ()
  : m_ThirdMac (0)
{
}

TypeId
ThirdMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ThirdMacTag")
    .SetParent<Tag> ()
    .SetGroupName("ThirdMacTag")
    .AddConstructor<ThirdMacTag> ()
    .AddAttribute ("LastThirdMac",
                   "Last ThirdMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&ThirdMacTag::GetThirdMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
ThirdMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ThirdMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
ThirdMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_ThirdMac);
}
void
ThirdMacTag::Deserialize (TagBuffer i)
{
  m_ThirdMac = i.ReadU8 ();
}
void
ThirdMacTag::Print (std::ostream &os) const
{
  os << "m_ThirdMac=" << m_ThirdMac;
}
uint8_t
ThirdMacTag::GetThirdMac (void) const
{
  return m_ThirdMac;
}

void
ThirdMacTag::SetThirdMac (uint8_t ThirdMac)
{
	m_ThirdMac=ThirdMac;
}

ThirdMacTagger::ThirdMacTagger ()
{
	this->lastThirdMac=0.0;
}

void
ThirdMacTagger::WriteThirdMacValue (Ptr<const Packet> packet)
{
  ThirdMacTag tag;
  tag.SetThirdMac(this->lastThirdMac);
  packet->AddByteTag (tag);
}
void
ThirdMacTagger::RecordThirdMacValue (Ptr<const Packet> packet)
{
  ThirdMacTag tag;
  lastThirdMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastThirdMac=tag.GetThirdMac ();

}

uint8_t
ThirdMacTagger::GetLastThirdMac (void) const
{
  return lastThirdMac;
}

void
ThirdMacTagger::SetLastThirdMac (uint8_t ThirdMac)
{
	lastThirdMac=ThirdMac;
}


} // namespace ns3
