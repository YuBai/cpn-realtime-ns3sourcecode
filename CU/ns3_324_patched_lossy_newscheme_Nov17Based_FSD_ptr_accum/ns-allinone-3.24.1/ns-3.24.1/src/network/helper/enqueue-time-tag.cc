/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "enqueue-time-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class EnqueueTimeTag : public Tag
{
public:
	EnqueueTimeTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  Time GetEnqueueTime (void) const;
  void SetEnqueueTime (Time enqueueTime);
private:
  uint64_t m_Time; //!< The time stored in the tag
};

EnqueueTimeTag::EnqueueTimeTag ()
{
	m_Time = 0;
}

TypeId
EnqueueTimeTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::EnqueueTimeTag")
    .SetParent<Tag> ()
    .SetGroupName("EnqueueTimeTagger")
    .AddConstructor<EnqueueTimeTag> ()
    .AddAttribute ("EnqueueTime",
                   "The time when packet is entered into tmp queue",
                   StringValue ("0.0s"),
                   MakeTimeAccessor (&EnqueueTimeTag::GetEnqueueTime),
                   MakeTimeChecker ())
  ;
  return tid;
}
TypeId
EnqueueTimeTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
EnqueueTimeTag::GetSerializedSize (void) const
{
  return 8;
}
void
EnqueueTimeTag::Serialize (TagBuffer i) const
{
  i.WriteU64 (m_Time);
}
void
EnqueueTimeTag::Deserialize (TagBuffer i)
{
	m_Time = i.ReadU64 ();
}
void
EnqueueTimeTag::Print (std::ostream &os) const
{
  os << "m_Time=" << m_Time;
}
Time
EnqueueTimeTag::GetEnqueueTime (void) const
{
  return TimeStep (m_Time);
}

void
EnqueueTimeTag::SetEnqueueTime (Time enqueueTime)
{
  m_Time = enqueueTime.GetTimeStep();
}

EnqueueTimeTagger::EnqueueTimeTagger ()
{
	this->m_Time = Seconds(0.0);
}
void
EnqueueTimeTagger::PrepareEqTime (Ptr<const Packet> packet, Time eqTime)
{
  EnqueueTimeTag tag;
  tag.SetEnqueueTime(eqTime);
  packet->AddByteTag (tag);
}
void
EnqueueTimeTagger::RecordEqTime (Ptr<const Packet> packet)
{
  EnqueueTimeTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
	  //std::clog << "EnqueueTimeTagger::RecordEqTime: Not Found.\n";
      return;
    }
  m_Time = tag.GetEnqueueTime();
}

Time
EnqueueTimeTagger::GetLastRecordedEnqueueTime ()
{
	return m_Time;
}

} // namespace ns3
