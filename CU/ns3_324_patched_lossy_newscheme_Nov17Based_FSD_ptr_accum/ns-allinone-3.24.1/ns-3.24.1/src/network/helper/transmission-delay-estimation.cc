/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "transmission-delay-estimation.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class TransmissionDelayEstimationTimestampTag : public Tag
{
public:
	TransmissionDelayEstimationTimestampTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  Time GetTxTime (void) const;
private:
  uint64_t m_creationTime; //!< The time stored in the tag
};

TransmissionDelayEstimationTimestampTag::TransmissionDelayEstimationTimestampTag ()
  : m_creationTime (Simulator::Now ().GetTimeStep ())
{
}

TypeId
TransmissionDelayEstimationTimestampTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::TransmissionDelayEstimationTimestampTag")
    .SetParent<Tag> ()
    .SetGroupName("TransmissionDelayEstimationTag")
    .AddConstructor<TransmissionDelayEstimationTimestampTag> ()
    .AddAttribute ("CreationTime",
                   "The time at which the timestamp was created",
                   StringValue ("0.0s"),
                   MakeTimeAccessor (&TransmissionDelayEstimationTimestampTag::GetTxTime),
                   MakeTimeChecker ())
  ;
  return tid;
}
TypeId
TransmissionDelayEstimationTimestampTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
TransmissionDelayEstimationTimestampTag::GetSerializedSize (void) const
{
  return 8;
}
void
TransmissionDelayEstimationTimestampTag::Serialize (TagBuffer i) const
{
  i.WriteU64 (m_creationTime);
}
void
TransmissionDelayEstimationTimestampTag::Deserialize (TagBuffer i)
{
  m_creationTime = i.ReadU64 ();
}
void
TransmissionDelayEstimationTimestampTag::Print (std::ostream &os) const
{
  os << "CreationTime=" << m_creationTime;
}
Time
TransmissionDelayEstimationTimestampTag::GetTxTime (void) const
{
  return TimeStep (m_creationTime);
}

TransmissionDelayEstimation::TransmissionDelayEstimation ()
  : m_previousRx (Simulator::Now ()),
    m_previousRxTx (Simulator::Now ()),
    m_delay (Seconds (0.0))
{
}
void
TransmissionDelayEstimation::PrepareTransTime (Ptr<const Packet> packet)
{
  TransmissionDelayEstimationTimestampTag tag;
  packet->AddByteTag (tag);
}
void
TransmissionDelayEstimation::RecordTransTime (Ptr<const Packet> packet)
{
  TransmissionDelayEstimationTimestampTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
	  //std::cerr << "TransmissionDelayEstimation::RecordRX: Not Found.\n";
      return;
    }
  tag.GetTxTime ();

  Time delta = (Simulator::Now () - m_previousRx) - (tag.GetTxTime () - m_previousRxTx);
  m_previousRx = Simulator::Now ();
  m_previousRxTx = tag.GetTxTime ();
  m_delay = Simulator::Now () - tag.GetTxTime ();
}

Time 
TransmissionDelayEstimation::GetLastTransDelay (void) const
{
  return m_delay;
}

} // namespace ns3
