#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/mac48-address.h"
#include "ns3/ipv4-address.h"
#include "ns3/simulator.h"
#include "ns3/log.h"

#ifndef GLOBAL_CLIENT_TABLE
#define GLOBAL_CLIENT_TABLE 1

#define MAX_IP_ADDR 10 //indicating there could be at most 10 APs

namespace ns3{

	class globalClientTable{

		public:

			typedef struct{

				Mac48Address macAddr;
				Ipv4Address ipAddrGroup[MAX_IP_ADDR];

			}clientTable;

			globalClientTable();
			globalClientTable(int size);

			void setMacAddr(int _nodeID, Mac48Address _macAddr);
			Mac48Address getMacAddrBynodeID(int _nodeID);

			void setIPAddr(int _nodeID, int _APID, Ipv4Address _targetIP);
			Ipv4Address getIpAddrBynodeID(int _nodeID, int _APID);

			void reSize(int size);


		private:

			void initClientTable(int size);

			std::vector<clientTable> m_gct;

	};

//	class globalAPTable{
//
//		public:
//
//			typedef struct{
//
//				Mac48Address apAddr;
//				int apID;
//
//			} apTable;
//
//			globalAPTable();
//			globalAPTable(int size);
//			void reSize(int size);
//
//			void setMacAddr(int _apID, Mac48Address _macAddr);
//			int getAPIDByMacAddr(Mac48Address _macAddr);
//
//		private:
//
//			void initAPTable(int size);
//
//			std::vector<apTable>m_gat;
//
//	};
//

}

#endif
