/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 *
 */

#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/address.h"
#include "packet-loss-counter.h"

#include "ns3/udp-delay-jitter-estimation.h"
#include "ns3/followup-delay-estimation.h"
#include "ns3/seq-tag.h"
#include "ns3/id-tag.h"
#include "ns3/fake-pkt-tag.h"
#include "ns3/FSD-ServerSide.h"
#include "ns3/fake-mac-tag.h"
#include "ns3/chosen-tag.h"
#include "ns3/statistics-tag.h"
#include "ns3/alter-path-tag.h"
#include "ns3/probe-time-tag.h"
#include "ns3/probe-train-length-tag.h"
#include "ns3/finish-probe-tag.h"
#include "ns3/hop-tag.h" //used for recording ACK_ACK information
#include "ns3/second-hop-tag.h" //used for recording endOfTrain_ACK information
#include "ns3/port-tag.h"
#include "ns3/qos-tag.h"
#include "ns3/global-server-table.h"

namespace ns3 {
/**
 * \ingroup applications
 * \defgroup udpclientserver UdpClientServer
 */

/**
 * \ingroup udpclientserver
 * \class UdpServer
 * \brief A UDP server, receives UDP packets from a remote host.
 *
 * UDP packets carry a 32bits sequence number followed by a 64bits time
 * stamp in their payloads. The application uses the sequence number
 * to determine if a packet is lost, and the time stamp to compute the delay.
 */
class UdpServer : public Application
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  UdpServer ();
  virtual ~UdpServer ();
  /**
   * \brief Returns the number of lost packets
   * \return the number of lost packets
   */
  uint32_t GetLost (void) const;

  /**
   * \brief Returns the number of received packets
   * \return the number of received packets
   */
  uint32_t GetReceived (void) const;

  /**
   * \brief Returns the size of the window used for checking loss.
   * \return the size of the window used for checking loss.
   */
  uint16_t GetPacketWindowSize () const;

  /**
   * \brief Set the size of the window used for checking loss. This value should
   *  be a multiple of 8
   * \param size the size of the window used for checking loss. This value should
   *  be a multiple of 8
   */
  void SetPacketWindowSize (uint16_t size);

  globalServerTable& GetGST(void);

protected:
  virtual void DoDispose (void);

private:

  virtual void StartApplication (void);
  virtual void StopApplication (void);
  virtual void ReportStatistics (void);
  /**
   * \brief Handle a packet reception.
   *
   * This function is called by lower layers.
   *
   * \param socket the socket the packet was received to.
   */
  void HandleRead (Ptr<Socket> socket);

  uint16_t m_port; //!< Port on which we listen for incoming packets.
  Ptr<Socket> m_socket; //!< IPv4 Socket
  Ptr<Socket> m_socket6; //!< IPv6 Socket
  uint32_t m_received; //!< Number of received packets
  PacketLossCounter m_lossCounter; //!< Lost packet counter

  // Added by Y.B.
  UdpDelayJitterEstimation m_udje;
  FollowupDelayEstimation m_fde;
  SeqTagger m_st;
  FakePktTagger m_fpt;
  FSDDelayFeedbacker m_fdfb; //very dirty hack - need to be improved if necessary
  FakeMacTagger m_fmt;
  ProbeTimeTagger m_ptt;
  ProbeTrainLengthTagger m_ptlt;
  FinishProbeTagger m_finipt;
  HopTagger m_ht;
  SecondHopTagger m_sht;
  PortTagger m_ptg;
  QosTag m_qtRead;

  uint32_t rcvPktLastSlot;
  uint32_t rcvPktThisSlot;
  int currentTriggeringTime;
  int currentProbeTrainLen;
  Time m_reportInterval;
  ChosenTagger m_ct;
  Ptr<Packet> feedbackClone;
  Ptr<Packet> fakePkt1Clone;
  Ptr<Packet> fakePkt2Clone;
  Ptr<Packet> fakePkt1Clone_ACK;
  Ptr<Packet> fakePkt2Clone_ACK;
  //Ptr<Packet> fakePkt2Clone_Train_ACK;
  StatisticsTagger m_stat;
  Address feedbackAddr;
  AlterPathTagger m_apt;
  int feedbackACKedOne;
  int feedbackACKedTwo;
  int feedbackACKedOne_Counter;
  int feedbackACKedTwo_Counter;
  int fakePktClone_Train_ACK;

  Time lastTriggeredTime;
  globalServerTable m_gst;
  int m_probingPort;

};

} // namespace ns3

#endif /* UDP_SERVER_H */
