/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "followdown-delay-estimation.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FollowdownDelayEstimationTimestampTag : public Tag
{
public:
	FollowdownDelayEstimationTimestampTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  Time GetTxTime (void) const;
private:
  uint64_t m_creationTime; //!< The time stored in the tag
};

FollowdownDelayEstimationTimestampTag::FollowdownDelayEstimationTimestampTag ()
  : m_creationTime (Simulator::Now ().GetTimeStep ())
{
}

TypeId
FollowdownDelayEstimationTimestampTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FollowdownDelayEstimationTimestampTag")
    .SetParent<Tag> ()
    .SetGroupName("followDownDelayEstimation")
    .AddConstructor<FollowdownDelayEstimationTimestampTag> ()
    .AddAttribute ("CreationTime",
                   "The time at which the timestamp was created",
                   StringValue ("0.0s"),
                   MakeTimeAccessor (&FollowdownDelayEstimationTimestampTag::GetTxTime),
                   MakeTimeChecker ())
  ;
  return tid;
}
TypeId
FollowdownDelayEstimationTimestampTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FollowdownDelayEstimationTimestampTag::GetSerializedSize (void) const
{
  return 8;
}
void
FollowdownDelayEstimationTimestampTag::Serialize (TagBuffer i) const
{
  i.WriteU64 (m_creationTime);
}
void
FollowdownDelayEstimationTimestampTag::Deserialize (TagBuffer i)
{
  m_creationTime = i.ReadU64 ();
}
void
FollowdownDelayEstimationTimestampTag::Print (std::ostream &os) const
{
  os << "CreationTime=" << m_creationTime;
}
Time
FollowdownDelayEstimationTimestampTag::GetTxTime (void) const
{
  return TimeStep (m_creationTime);
}

FollowdownDelayEstimation::FollowdownDelayEstimation ()
  : m_previousRx (Simulator::Now ()),
    m_previousRxTx (Simulator::Now ()),
    m_delay (Seconds (0.0))
{
}
void
FollowdownDelayEstimation::PrepareEqTime (Ptr<const Packet> packet)
{
  FollowdownDelayEstimationTimestampTag tag;
  packet->AddByteTag (tag);
}
void
FollowdownDelayEstimation::RecordDqTime (Ptr<const Packet> packet)
{
  FollowdownDelayEstimationTimestampTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
	  //std::clog << "FollowdownDelayEstimation::RecordRX: Not Found.\n";
      return;
    }
  tag.GetTxTime ();

  Time delta = (Simulator::Now () - m_previousRx) - (tag.GetTxTime () - m_previousRxTx);
  m_previousRx = Simulator::Now ();
  m_previousRxTx = tag.GetTxTime ();
  m_delay = Simulator::Now () - tag.GetTxTime ();
}

Time 
FollowdownDelayEstimation::GetLastQDelay (void)
{
  return m_delay;
}
Time
GetLastRecordedTime (Ptr<const Packet> Packet)
{
	FollowdownDelayEstimationTimestampTag tag;
	bool found;
	found = Packet->FindFirstMatchingByteTag(tag);
	if (!found)
	{
		//std::clog << "FollowdownDelayEstimation::GetRecordTime: Not Found.\n";
		return Seconds(0);
	}
	return tag.GetTxTime();
}

} // namespace ns3
