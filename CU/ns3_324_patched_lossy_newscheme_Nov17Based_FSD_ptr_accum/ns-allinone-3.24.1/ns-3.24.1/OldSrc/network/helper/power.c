/*
 * power.c
 *
 * Code generation for function 'power'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "FSDFunctionReference.h"
#include "power.h"

/* Function Definitions */
void b_power(const double a[6], double y[6])
{
  int k;
  for (k = 0; k < 6; k++) {
    y[k] = a[k] * a[k];
  }
}

void power(const double a[18], double y[18])
{
  int k;
  for (k = 0; k < 18; k++) {
    y[k] = a[k] * a[k];
  }
}

/* End of code generation (power.c) */
