/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "enqueue-time-compensate-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {

/**
 * Tag to perform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class EnqueueTimeCompensateTag : public Tag
{
public:
	EnqueueTimeCompensateTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  Time GetCompensateTime (void) const;
  void SetCompensateTime (Time compensateTime);
private:
  uint64_t m_compensateTime; //!< The time stored in the tag
};

EnqueueTimeCompensateTag::EnqueueTimeCompensateTag ()
{
	m_compensateTime = 0;
}

TypeId
EnqueueTimeCompensateTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::EnqueueTimeCompensateTag")
    .SetParent<Tag> ()
    .SetGroupName("EnqueueTimeCompensateTagger")
    .AddConstructor<EnqueueTimeCompensateTag> ()
    .AddAttribute ("CompensateTime",
                   "The time duration spent by tagged packet staying in tmp queue",
                   StringValue ("0.0s"),
                   MakeTimeAccessor (&EnqueueTimeCompensateTag::GetCompensateTime),
                   MakeTimeChecker ())
  ;
  return tid;
}
TypeId
EnqueueTimeCompensateTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
EnqueueTimeCompensateTag::GetSerializedSize (void) const
{
  return 8;
}
void
EnqueueTimeCompensateTag::Serialize (TagBuffer i) const
{
  i.WriteU64 (m_compensateTime);
}
void
EnqueueTimeCompensateTag::Deserialize (TagBuffer i)
{
	m_compensateTime = i.ReadU64 ();
}
void
EnqueueTimeCompensateTag::Print (std::ostream &os) const
{
  os << "m_compensateTime=" << m_compensateTime;
}
Time
EnqueueTimeCompensateTag::GetCompensateTime (void) const
{
  return TimeStep (m_compensateTime);
}

void
EnqueueTimeCompensateTag::SetCompensateTime (Time compensateTime)
{
	this->m_compensateTime = compensateTime.GetTimeStep();
}

EnqueueTimeCompensateTagger::EnqueueTimeCompensateTagger ()
{
	this->m_compensateTime = Seconds(0.0);
}
void
EnqueueTimeCompensateTagger::PrepareEqTime (Ptr<const Packet> packet, Time eqTime)
{
  EnqueueTimeCompensateTag tag;
  tag.SetCompensateTime(eqTime);
  packet->AddByteTag (tag);
}
void
EnqueueTimeCompensateTagger::RecordEqTime (Ptr<const Packet> packet)
{
  EnqueueTimeCompensateTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
	  //std::clog << "EnqueueTimeCompensateTagger::RecordEqTime: Not Found.\n";
      return;
    }
  m_compensateTime = tag.GetCompensateTime();
}

Time
EnqueueTimeCompensateTagger::GetLastRecordedCompensateTime ()
{
	return m_compensateTime;
}

} // namespace ns3
