/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "sixth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to SixthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class SixthMacTag : public Tag
{
public:
	SixthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetSixthMac (void) const;
  void SetSixthMac (uint8_t SixthMac) ;

private:
  uint8_t m_SixthMac; //!< The time stored in the tag
};

SixthMacTag::SixthMacTag ()
  : m_SixthMac (0)
{
}

TypeId
SixthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::SixthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("SixMacTag")
    .AddConstructor<SixthMacTag> ()
    .AddAttribute ("LastSixthMac",
                   "Last SixthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&SixthMacTag::GetSixthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
SixthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
SixthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
SixthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_SixthMac);
}
void
SixthMacTag::Deserialize (TagBuffer i)
{
  m_SixthMac = i.ReadU8 ();
}
void
SixthMacTag::Print (std::ostream &os) const
{
  os << "m_SixthMac=" << m_SixthMac;
}
uint8_t
SixthMacTag::GetSixthMac (void) const
{
  return m_SixthMac;
}

void
SixthMacTag::SetSixthMac (uint8_t SixthMac)
{
	m_SixthMac=SixthMac;
}

SixthMacTagger::SixthMacTagger ()
{
	this->lastSixthMac=0.0;
}

void
SixthMacTagger::WriteSixthMacValue (Ptr<const Packet> packet)
{
  SixthMacTag tag;
  tag.SetSixthMac(this->lastSixthMac);
  packet->AddByteTag (tag);
}
void
SixthMacTagger::RecordSixthMacValue (Ptr<const Packet> packet)
{
  SixthMacTag tag;
  lastSixthMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastSixthMac=tag.GetSixthMac ();

}

uint8_t
SixthMacTagger::GetLastSixthMac (void) const
{
  return lastSixthMac;
}

void
SixthMacTagger::SetLastSixthMac (uint8_t SixthMac)
{
	lastSixthMac=SixthMac;
}


} // namespace ns3
