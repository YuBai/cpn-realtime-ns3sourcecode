/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "port-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Opform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class PortTag : public Tag
{
public:
	PortTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetPort (void) const;
  void SetPort (double Port) ;

private:
  double m_Port; //!< The time stored in the tag
};

PortTag::PortTag ()
  : m_Port (0.0)
{
}

TypeId
PortTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::PortTag")
    .SetParent<Tag> ()
    .SetGroupName("PortTag")
    .AddConstructor<PortTag> ()
    .AddAttribute ("LastPort",
                   "Last Port Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&PortTag::GetPort),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
PortTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
PortTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
PortTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Port);
}
void
PortTag::Deserialize (TagBuffer i)
{
  m_Port = i.ReadDouble ();
}
void
PortTag::Print (std::ostream &os) const
{
  os << "m_Port=" << m_Port;
}
double
PortTag::GetPort (void) const
{
  return m_Port;
}

void
PortTag::SetPort (double Port)
{
	m_Port=Port;
}

PortTagger::PortTagger ()
{
	this->lastPort=0.0;
}

void
PortTagger::WritePortValue (Ptr<const Packet> packet, double tagValue)
{
  PortTag tag;
  tag.SetPort(tagValue);
  packet->AddByteTag (tag);
}
int
PortTagger::RecordPortValue (Ptr<const Packet> packet)
{
  PortTag tag;
  lastPort=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastPort=tag.GetPort ();
  return (int)lastPort;
}

double
PortTagger::GetLastPort (void) const
{
  return lastPort;
}

} // namespace ns3
