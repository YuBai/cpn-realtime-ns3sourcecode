/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the Rounde that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "round-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to Roundform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class RoundTag : public Tag
{
public:
	RoundTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetRound (void) const;
  void SetRound (double Round) ;

private:
  double m_Round; //!< The time stored in the tag
};

RoundTag::RoundTag ()
{
}

TypeId
RoundTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::RoundTag")
    .SetParent<Tag> ()
    .SetGroupName("RoundTag")
    .AddConstructor<RoundTag> ()
  ;
  return tid;
}
TypeId
RoundTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
RoundTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
RoundTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Round);
}
void
RoundTag::Deserialize (TagBuffer i)
{
  m_Round = i.ReadDouble ();
}
void
RoundTag::Print (std::ostream &os) const
{
  os << "m_Round=" << m_Round;
}
double
RoundTag::GetRound (void) const
{
  return m_Round;
}

void
RoundTag::SetRound (double Round)
{
	m_Round=Round;
	//std::clog << "m_Round now is " << m_Round << "\n";
}

RoundTagger::RoundTagger ()
{
	this->lastRound=0;
}

void
RoundTagger::WriteRoundValue (Ptr<const Packet> packet, double tagValue)
{
  RoundTag tag;
  tag.SetRound(tagValue);
  packet->AddPacketTag (tag);
}
double
RoundTagger::RecordRoundValue (Ptr<const Packet> packet)
{
  RoundTag tag;
  lastRound = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastRound=tag.GetRound ();
  return (double)lastRound;
}

double
RoundTagger::GetLastRound (void) const
{
  return lastRound;
}


} // namespace ns3
