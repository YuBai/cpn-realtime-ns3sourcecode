/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "my-rssi-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to MyRssiform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class MyRssiTag : public Tag
{
public:
	MyRssiTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetMyRssi (void) const;
  void SetMyRssi (double MyRssi) ;

private:
  double m_MyRssi; //!< The time stored in the tag
};

MyRssiTag::MyRssiTag ()
  : m_MyRssi (0.0)
{
}

TypeId
MyRssiTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::MyRssiTag")
    .SetParent<Tag> ()
    .SetGroupName("MyRssi")
    .AddConstructor<MyRssiTag> ()
    .AddAttribute ("LastMyRssi",
                   "Last MyRssi Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&MyRssiTag::GetMyRssi),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
MyRssiTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
MyRssiTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
MyRssiTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_MyRssi);
}
void
MyRssiTag::Deserialize (TagBuffer i)
{
  m_MyRssi = i.ReadDouble ();
}
void
MyRssiTag::Print (std::ostream &os) const
{
  os << "m_MyRssi=" << m_MyRssi;
}
double
MyRssiTag::GetMyRssi (void) const
{
  return m_MyRssi;
}

void
MyRssiTag::SetMyRssi (double MyRssi)
{
	m_MyRssi=MyRssi;
}

MyRssiTagger::MyRssiTagger (double MyRssi)
{
	this->lastMyRssi=MyRssi;
}

void
MyRssiTagger::WriteMyRssiValue (Ptr<const Packet> packet)
{
  MyRssiTag tag;
  tag.SetMyRssi(this->lastMyRssi);
  packet->AddByteTag (tag);
}
void
MyRssiTagger::RecordMyRssiValue (Ptr<const Packet> packet)
{
  MyRssiTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastMyRssi=tag.GetMyRssi ();

}

double
MyRssiTagger::GetLastMyRssi (void) const
{
  return lastMyRssi;
}

} // namespace ns3
