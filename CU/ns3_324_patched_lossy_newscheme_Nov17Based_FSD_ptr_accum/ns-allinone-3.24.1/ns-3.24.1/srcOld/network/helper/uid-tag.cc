/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the Uide that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "uid-tag.h"

#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"

namespace ns3 {


/**
 * Tag to Uidform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class UidTag : public Tag
{
public:
	UidTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetUid (void) const;
  void SetUid (double Uid) ;

private:
  double m_Uid; //!< The time stored in the tag
};

UidTag::UidTag ()
{
}

TypeId
UidTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::UidTag")
    .SetParent<Tag> ()
    .SetGroupName("UidTag")
    .AddConstructor<UidTag> ()
  ;
  return tid;
}
TypeId
UidTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
UidTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
UidTag::Serialize (TagBuffer i) const
{
  i.WriteDouble(m_Uid);
}
void
UidTag::Deserialize (TagBuffer i)
{
  m_Uid = i.ReadDouble ();
}
void
UidTag::Print (std::ostream &os) const
{
  os << "m_Uid=" << m_Uid;
}
double
UidTag::GetUid (void) const
{
  return m_Uid;
}

void
UidTag::SetUid (double Uid)
{
	m_Uid=Uid;
	//std::clog << "m_Uid now is " << m_Uid << "\n";
}

UidTagger::UidTagger ()
{
	this->lastUid=0;
}

void
UidTagger::WriteUidValue (Ptr<const Packet> packet, double tagValue)
{
  UidTag tag;
  tag.SetUid(tagValue);
  packet->AddPacketTag (tag);
}
double
UidTagger::RecordUidValue (Ptr<const Packet> packet)
{
  UidTag tag;
  lastUid = 0;
  bool found;
  found = packet->PeekPacketTag (tag);
  if (!found)
    {
      return 0;
    }
  lastUid=tag.GetUid ();
  return (double)lastUid;
}

double
UidTagger::GetLastUid (void) const
{
  return lastUid;
}


} // namespace ns3
