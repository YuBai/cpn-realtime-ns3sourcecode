/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "statistics-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Statisticsform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class StatisticsTag : public Tag
{
public:
	StatisticsTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetStatistics (void) const;
  void SetStatistics (double Statistics) ;

private:
  double m_Statistics; //!< The time stored in the tag
};

StatisticsTag::StatisticsTag ()
  : m_Statistics (0.0)
{
}

TypeId
StatisticsTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::StatisticsTag")
    .SetParent<Tag> ()
    .SetGroupName("StatisticsTag")
    .AddConstructor<StatisticsTag> ()
    .AddAttribute ("LastStatistics",
                   "Last Statistics Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&StatisticsTag::GetStatistics),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
StatisticsTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
StatisticsTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
StatisticsTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Statistics);
}
void
StatisticsTag::Deserialize (TagBuffer i)
{
  m_Statistics = i.ReadDouble ();
}
void
StatisticsTag::Print (std::ostream &os) const
{
  os << "m_Statistics=" << m_Statistics;
}
double
StatisticsTag::GetStatistics (void) const
{
  return m_Statistics;
}

void
StatisticsTag::SetStatistics (double Statistics)
{
	m_Statistics=Statistics;
}

StatisticsTagger::StatisticsTagger ()
{
	this->lastStatistics=0.0;
}

void
StatisticsTagger::WriteStatisticsValue (Ptr<const Packet> packet, double tagValue)
{
  StatisticsTag tag;
  tag.SetStatistics(tagValue);
  packet->AddByteTag (tag);
}
int
StatisticsTagger::RecordStatisticsValue (Ptr<const Packet> packet)
{
  StatisticsTag tag;
  lastStatistics=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastStatistics=tag.GetStatistics ();
  return (int)lastStatistics;
}

double
StatisticsTagger::GetLastStatistics (void) const
{
  return lastStatistics;
}

} // namespace ns3
