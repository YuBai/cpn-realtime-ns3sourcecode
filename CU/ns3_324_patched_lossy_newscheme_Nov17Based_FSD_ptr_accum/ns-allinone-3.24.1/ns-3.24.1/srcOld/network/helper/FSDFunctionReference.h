/*
 * FSDFunctionReference.h
 *
 * Code generation for function 'FSDFunctionReference'
 *
 * C source code generated on: Sat Aug  1 17:13:58 2015
 *
 */

#ifndef __FSDFUNCTIONREFERENCE_H__
#define __FSDFUNCTIONREFERENCE_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "FSDFunctionReference_types.h"

/* Function Declarations */
extern signed char FSDFunctionReference(const unsigned int data1[500], const unsigned int data2[500], unsigned int data1_len, unsigned int data2_len, double delta_x, double alpha_F, signed char replication);
#endif
/* End of code generation (FSDFunctionReference.h) */
