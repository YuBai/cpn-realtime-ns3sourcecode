/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 *
 */

#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"

#include "ns3/udp-delay-jitter-estimation.h"
#include "ns3/followdown-delay-estimation.h"
#include "ns3/probing-delay-estimation.h"
#include "ns3/seq-tag.h"
#include "ns3/id-tag.h"
#include "ns3/fake-pkt-tag.h"
#include "ns3/op-tag.h"
#include "ns3/port-tag.h"
#include "ns3/qos-tag.h"

namespace ns3 {

class Socket;
class Packet;

/**
 * \ingroup udpclientserver
 * \class UdpClient
 * \brief A Udp client. Sends UDP packet carrying sequence number and time stamp
 *  in their payloads
 *
 */
class UdpClient : public Application
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  UdpClient ();

  virtual ~UdpClient ();

  /**
   * \brief set the remote address and port
   * \param ip remote IPv4 address
   * \param port remote port
   */
  void SetRemote (Ipv4Address ip, uint16_t port);
  /**
   * \brief set the remote address and port
   * \param ip remote IPv6 address
   * \param port remote port
   */
  void SetRemote (Ipv6Address ip, uint16_t port);
  /**
   * \brief set the remote address and port
   * \param ip remote IP address
   * \param port remote port
   */
  void SetRemote (Address ip, uint16_t port);
  Address GetRemotePeerAddr(void);
  //void SetRemotePeerAddr(Address _m_peerAddress);

  uint16_t GetPort();
  void SetPort(uint16_t port);

  void SetFlag(int flagVal);
  int GetFlag(void);

protected:
  virtual void DoDispose (void);

private:

  virtual void StartApplication (void);
  virtual void StopApplication (void);

  /**
   * \brief Send a packet
   */
  void Send (void);

  //Added by Y.B.
  void HandleRead (Ptr<Socket> socket);

  uint32_t m_count; //!< Maximum number of packets the application will send
  Time m_interval; //!< Packet inter-send time
  uint32_t m_size; //!< Size of the sent packet (including the SeqTsHeader)
  uint8_t m_tid;

  uint32_t m_sent; //!< Counter for sent packets
  Ptr<Socket> m_socket; //!< Socket
  Address m_peerAddress; //!< Remote peer address
  uint16_t m_peerPort; //!< Remote peer port
  EventId m_sendEvent; //!< Event to send the next packet

  //Added by Y.B.
   UdpDelayJitterEstimation m_udje;
   FollowdownDelayEstimation m_fdje;
   SeqTagger m_st;
   QosTag m_qt;
   QosTag m_qtRead;
   Time m_rebootInterval; //Restart Application Since Following Association Does not Work Anymore
  //IdTagger m_it;
   EventId m_rebootEvent; //Event to Reboot
   OpTagger m_ot;
   PortTagger m_ptg;
   int cancelEvent;
   ProbingDelayEstimation m_pde;
   int m_flag;

};

} // namespace ns3

#endif /* UDP_CLIENT_H */
