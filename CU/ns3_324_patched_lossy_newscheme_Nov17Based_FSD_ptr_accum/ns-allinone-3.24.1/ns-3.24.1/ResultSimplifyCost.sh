#!/bin/bash

mkdir ThroughputSimplifiedCost

ls Throughput/RX* > fileListRX
ls Throughput/TX* > fileListTX

while read line; do


	cut -d "," -f1-6,14 $line > "$line"Simplified
	echo $line Done

done < fileListRX

while read line; do


        cut -d "," -f2-3,7 $line > "$line"Simplified
        echo $line Done

done < fileListTX

mv Throughput/*Simplified ThroughputSimplifiedCost/
