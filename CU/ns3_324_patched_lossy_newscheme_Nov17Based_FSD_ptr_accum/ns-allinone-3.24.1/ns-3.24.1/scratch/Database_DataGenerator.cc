/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 			  Lv1        Lv2       Lv3       Lv4
 * Topology: Node1 ----\
 * 			 Node2 -----Node5----\
 * 			 		     		  Node7-----Node8
 * 			 Node3 -----Node6----/
 * 			 Node4 ----/
 *
 * IP Address for Lv1-Lv2 Layer3 Interface: 10.1.1.*, 255.255.255.0
 * IP Address for Lv2-Lv3 Layer3 Interface: 10.1.2.*, 255.255.255.0
 * IP Address for Lv3-Lv4 Layer3 Interface: 10.1.3.*, 255.255.255.0
 *
 * Bandwidth for Lv1-Lv2 Links: 56kbps, 330ns (100m link)
 * Bandwidth for Lv2-Lv3 Links: 10mbps, 6us (2km link)
 * Bandwidth for Lv3-Lv4 Links: 1Gbps, 0.67ms (200km link)
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DatabaseDataGenerator");

void
PopulateArpCache ()
{
  Ptr<ArpCache> arp = CreateObject<ArpCache> ();
  arp->SetAliveTimeout (Seconds(3600 * 24 * 365));
  for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
  {
    Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
    NS_ASSERT(ip !=0);
    ObjectVectorValue interfaces;
    ip->GetAttribute("InterfaceList", interfaces);
    for(ObjectVectorValue::Iterator j = interfaces.Begin(); j != interfaces.End (); j++)
    {
      //j->second->GetObject()
      Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
      NS_ASSERT(ipIface != 0);
      Ptr<NetDevice> device = ipIface->GetDevice();
      NS_ASSERT(device != 0);
      Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress ());
      for(uint32_t k = 0; k < ipIface->GetNAddresses (); k ++)
      {
        Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal();
        if(ipAddr == Ipv4Address::GetLoopback())
          continue;
        ArpCache::Entry * entry = arp->Add(ipAddr);
        entry->MarkWaitReply(0);
        entry->MarkAlive(addr);
      }
    }
  }
  for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
  {
    Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
    NS_ASSERT(ip !=0);
    ObjectVectorValue interfaces;
    ip->GetAttribute("InterfaceList", interfaces);
    for(ObjectVectorValue::Iterator j = interfaces.Begin(); j !=
interfaces.End (); j ++)
    {
      Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
      ipIface->SetAttribute("ArpCache", PointerValue(arp));
    }
  }
}

int
main (int argc, char *argv[])
{
  Time::SetResolution (Time::NS);
  //LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  //LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  NodeContainer nodes;
  nodes.Create (8);

  //-----------------------------------------------------------Node Containers
  //First-Layer Nodes
  NodeContainer firstLayerNodes;
  firstLayerNodes.Add(nodes.Get(0));
  firstLayerNodes.Add(nodes.Get(1));
  firstLayerNodes.Add(nodes.Get(2));
  firstLayerNodes.Add(nodes.Get(3));

  //Second-Layer Nodes
  NodeContainer secondLayerNodes;
  secondLayerNodes.Add(nodes.Get(4));
  secondLayerNodes.Add(nodes.Get(5));

  //Third-Layer Nodes
  NodeContainer thirdLayerNodes;
  thirdLayerNodes.Add(nodes.Get(6));

  //Fourth-Layer Nodes
  NodeContainer fourthLayerNodes;
  fourthLayerNodes.Add(nodes.Get(7));

  //Node Container for Lv1-Lv2 Links
  NodeContainer Link11;
  Link11.Add(firstLayerNodes.Get(0));
  Link11.Add(secondLayerNodes.Get(0));
  NodeContainer Link12;
  Link12.Add(firstLayerNodes.Get(1));
  Link12.Add(secondLayerNodes.Get(0));
  NodeContainer Link13;
  Link13.Add(firstLayerNodes.Get(2));
  Link13.Add(secondLayerNodes.Get(1));
  NodeContainer Link14;
  Link14.Add(firstLayerNodes.Get(3));
  Link14.Add(secondLayerNodes.Get(1));

  //Node Container for Lv2-Lv3 Links
  NodeContainer Link21;
  Link21.Add(secondLayerNodes.Get(0));
  Link21.Add(thirdLayerNodes.Get(0));
  NodeContainer Link22;
  Link22.Add(secondLayerNodes.Get(1));
  Link22.Add(thirdLayerNodes.Get(0));

  //Node Container for Lv3-Lv4 Links
  NodeContainer Link31;
  Link31.Add(thirdLayerNodes.Get(0));
  Link31.Add(fourthLayerNodes.Get(0));

  //-----------------------------------------------------------Links
  //All Links for Lv1-Lv2 Links
  PointToPointHelper Lv1pointToPoint;
  Lv1pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
  Lv1pointToPoint.SetChannelAttribute ("Delay", StringValue ("330ns"));
  NetDeviceContainer Link11devices, Link12devices, Link13devices, Link14devices;
  Link11devices = Lv1pointToPoint.Install (Link11);
  Link12devices = Lv1pointToPoint.Install (Link12);
  Link13devices = Lv1pointToPoint.Install (Link13);
  Link14devices = Lv1pointToPoint.Install (Link14);

  //All Links for Lv2-Lv3 Links
  PointToPointHelper Lv2pointToPoint;
  Lv2pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));
  Lv2pointToPoint.SetChannelAttribute ("Delay", StringValue ("0.006ms"));
  NetDeviceContainer Link21devices, Link22devices;
  Link21devices = Lv1pointToPoint.Install (Link21);
  Link22devices = Lv1pointToPoint.Install (Link22);

  //All Links for Lv3-Lv4 Links
  PointToPointHelper Lv3pointToPoint;
  Lv3pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
  Lv3pointToPoint.SetChannelAttribute ("Delay", StringValue ("0.67ms"));
  NetDeviceContainer Link31devices;
  Link31devices = Lv1pointToPoint.Install (Link31);

  //-----------------------------------------------------------Internet Stack
  InternetStackHelper stack;
  stack.Install (nodes);
  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer Link11interfaces = address.Assign (Link11devices);
  Ipv4InterfaceContainer Link12interfaces = address.Assign (Link12devices);
  Ipv4InterfaceContainer Link13interfaces = address.Assign (Link13devices);
  Ipv4InterfaceContainer Link14interfaces = address.Assign (Link14devices);
  address.SetBase("10.1.2.0","255.255.255.0");
  Ipv4InterfaceContainer Link21interfaces = address.Assign (Link21devices);
  Ipv4InterfaceContainer Link22interfaces = address.Assign (Link22devices);
  address.SetBase("10.1.3.0","255.255.255.0");
  Ipv4InterfaceContainer Link31interfaces = address.Assign (Link31devices);

  //-----------------------------------------------------------Applications
  UdpEchoServerHelper echoServer (9);
  ApplicationContainer serverApps = echoServer.Install (fourthLayerNodes);
  serverApps.Start (Seconds (0.0));
  serverApps.Stop (Seconds (300.0));

  UdpEchoClientHelper echoClient (Link31interfaces.GetAddress (1), 9);
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

  ApplicationContainer clientApps = echoClient.Install (firstLayerNodes);
  clientApps.Start (Seconds (10.0));
  clientApps.Stop (Seconds (300.0));
  PopulateArpCache ();
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();


  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
