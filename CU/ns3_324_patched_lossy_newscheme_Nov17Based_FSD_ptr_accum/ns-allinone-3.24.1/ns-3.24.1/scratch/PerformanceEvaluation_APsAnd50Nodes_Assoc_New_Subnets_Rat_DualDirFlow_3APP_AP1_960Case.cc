 /* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//#define NODE_AMOUNT 30
//#define NODE_AMOUNT_GROUP1 21
//#define NODE_AMOUNT_GROUP2 10
//#define START_NODE_ID 3
//#define MAX_FSD_NODES 10

#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/global-server-table.h"
#include "ns3/global-ap-table.h"
#include "ns3/global-client-table.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TwoAPsOriginal");

void
PopulateArpCache (int NODE_AMOUNT_GROUP1, int NODE_AMOUNT_GROUP2)
{
  Ptr<ArpCache> arp = CreateObject<ArpCache> ();
  arp->SetAliveTimeout (Seconds(3600 * 24 * 365));
  for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
  {
    Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
    NS_ASSERT(ip !=0);
    ObjectVectorValue interfaces;
    ip->GetAttribute("InterfaceList", interfaces);
    for(ObjectVectorValue::Iterator j = interfaces.Begin(); j != interfaces.End (); j++)
    {
      //j->second->GetObject()
      Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
      NS_ASSERT(ipIface != 0);
      Ptr<NetDevice> device = ipIface->GetDevice();
      NS_ASSERT(device != 0);
      Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress ());
      for(uint32_t k = 0; k < ipIface->GetNAddresses (); k ++)
      {
        Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal();
        if(ipAddr == Ipv4Address::GetLoopback())
          continue;
        ArpCache::Entry * entry = arp->Add(ipAddr);
        entry->MarkWaitReply(0);
        entry->MarkAlive(addr);

        NS_LOG_UNCOND("item to be inserted from interface: " << ipAddr << " " << addr);
      }
    }
  }
//  Ipv4Address ipAddrTmp ("10.1.4.2");
//  Mac48Address addrTmp ("00:00:00:00:00:07");
//  ArpCache::Entry * entry = arp->Add(ipAddrTmp);
//  entry->MarkWaitReply(0);
//  entry->MarkAlive(addrTmp);

  unsigned char macB[6];
  Ipv4Address ipAddrTmpStart="10.1.4.2";
  Ipv4Address ipAddrTmp;
  Mac48Address addrTmp;
  ArpCache::Entry * entry;
  macB[0]=0x00;
  macB[1]=0x00;
  macB[2]=0x00;
  macB[3]=0x00;
  macB[4]=0x00;
  macB[5]=0x06; //since first user is 00:00:00:00:00:07 so this should start from 06

  //loop for adding AP1's associated nodes
  int i;
  for(i=0;i<NODE_AMOUNT_GROUP1;i++)
  {
	  ipAddrTmp.Set(ipAddrTmpStart.Get()+i);
	  macB[5]=macB[5]+0x01;
	  //addrTmp=Mac48Address(macB);
	  addrTmp.CopyFrom(macB);
	  NS_LOG_UNCOND("i: " << i << "item to be inserted is " << ipAddrTmp << " " << addrTmp);

	  entry = arp->Add(ipAddrTmp);
	  entry->MarkWaitReply(0);
	  entry->MarkAlive(addrTmp);
  }

  ipAddrTmpStart="10.1.3.2";
  for(;i<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;i++)
  {
	  ipAddrTmp.Set(ipAddrTmpStart.Get()+i);
	  macB[5]=macB[5]+0x01;
	  addrTmp.CopyFrom(macB);
	  NS_LOG_UNCOND("i: " << i << "item to be inserted is " << ipAddrTmp << " " << addrTmp);

	  entry = arp->Add(ipAddrTmp);
	  entry->MarkWaitReply(0);
	  entry->MarkAlive(addrTmp);
  }


  for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
  {
    Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
    NS_ASSERT(ip !=0);
    ObjectVectorValue interfaces;
    ip->GetAttribute("InterfaceList", interfaces);
    for(ObjectVectorValue::Iterator j = interfaces.Begin(); j !=
interfaces.End (); j ++)
    {
      Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
      ipIface->SetAttribute("ArpCache", PointerValue(arp));
    }
  }
}


void
PopulateGlobalServerTable (int NODE_AMOUNT_GROUP1, int NODE_AMOUNT_GROUP2, int START_NODE_ID)
{
	int count=0;
	//globalServerTable pst;

	NodeList::Iterator ii = NodeList::Begin();
	Ptr<Application>testt=(*ii)->GetApplication(0);
	Ptr<UdpServer>test=DynamicCast<UdpServer>(testt);
	test->GetGST().reSize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID);

	Ptr<Application>testt1=(*ii)->GetApplication(1);
	Ptr<UdpServer>test1=DynamicCast<UdpServer>(testt1);

	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		if(count < START_NODE_ID) //meaning it is about server and ap nodes
		{
			count=count+1;
			continue;
		}
		else
		{
			if(count<START_NODE_ID+NODE_AMOUNT_GROUP1)//which means it is associated with AP1 - assuming AP1 corresponds to 0, and AP2 corresponds to 1 so on
			{
				test->GetGST().fillEntries(count,0,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				test->GetGST().setCurrentIPIndex(count,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				NS_LOG_UNCOND("m_gst: I filled 0th entry on " << count << " with IP address " << (*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal() << " and current IP index is " << test->GetGST().getCurrentIPIndex(count));

				Ipv4Address tmp=(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal();
				for(int j=1;j<2;j++) //stupid way - need to fix this later
				{
					uint32_t tt=tmp.Get()+0x00000100;
					Ipv4Address tmp1(tt);
					test->GetGST().fillEntries(count,j,tmp1);
					NS_LOG_UNCOND("m_gst: I filled " << j << "th entry on " << count << " with IP address " << tmp1);
				}

			}
			else
			{
				test->GetGST().fillEntries(count,1,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				test->GetGST().setCurrentIPIndex(count,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				NS_LOG_UNCOND("m_gst: I filled 1st entry on " << count << " with IP address " << (*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal() << " and current IP index is " << test->GetGST().getCurrentIPIndex(count));

				Ipv4Address tmp=(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal();
				for(int j=0;j<1;j++)
				{
					uint32_t tt=tmp.Get()-0x00000100;
					Ipv4Address tmp1(tt);
					test->GetGST().fillEntries(count,j,tmp1);
					NS_LOG_UNCOND("m_gst: I filled " << j << "th entry on " << count << " with IP address " << tmp1);
				}
			}
			count=count+1;
		}

	}
	test1->GetGST()=test->GetGST();

}

void
PopulateGlobalClientTable (int NODE_AMOUNT_GROUP1, int NODE_AMOUNT_GROUP2, int START_NODE_ID)
{

	int count=0;
	globalClientTable gct;
	gct.reSize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID);
	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		if(count < START_NODE_ID) //meaning it is about server and ap nodes
		{
			count=count+1;
			continue;
		}
		else
		{
			if(count<START_NODE_ID+NODE_AMOUNT_GROUP1)//which means it is associated with AP1 - assuming AP1 corresponds to 0, and AP2 corresponds to 1 so on
			{
				gct.setIPAddr(count,0,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				NS_LOG_UNCOND("m_gct: I filled 0th entry on " << count << " with IP address " << (*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());

				Ipv4Address tmp=(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal();
				for(int j=1;j<2;j++) //stupid way - need to fix this later
				{
					uint32_t tt=tmp.Get()+0x00000100;
					Ipv4Address tmp1(tt);
					gct.setIPAddr(count,j,tmp1);
					NS_LOG_UNCOND("m_gct: I filled " << j << "th entry on " << count << " with IP address " << tmp1);
				}

			}
			else
			{
				gct.setIPAddr(count,1,(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());
				NS_LOG_UNCOND("m_gct: I filled 1st entry on " << count << " with IP address " << (*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal());

				Ipv4Address tmp=(*i)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal();
				for(int j=0;j<1;j++)
				{
					uint32_t tt=tmp.Get()-0x00000100;
					Ipv4Address tmp1(tt);
					gct.setIPAddr(count,j,tmp1);
					NS_LOG_UNCOND("m_gct: I filled " << j << "th entry on " << count << " with IP address " << tmp1);
				}
			}
			count=count+1;

		}

	}

	NS_LOG_UNCOND("DONE DONE");

	int counta=0;
	NodeList::Iterator ii;
	for(ii= NodeList::Begin();ii != NodeList::End(); ++ii)
	{
		if(counta < START_NODE_ID)
		{
			counta=counta+1;
			//continue;
		}
		else
		{

		    Ptr<Ipv4L3Protocol> ip = (*ii)->GetObject<Ipv4L3Protocol> ();
			ip->GetGCT().reSize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID);
			ip->GetGCT()=gct;


		}

	}

}

void
PopulateGlobalAPTable (int START_NODE_ID)
{
	//well since this is light-workload, I decide to enter this part of information manually - change it to automatic version once it becomes necessary
	Ssid ssidOne=Ssid ("ns-3-ssid");
	Ssid ssidTwo=Ssid ("ns-3-ssid1");

	globalAPTable gat;
	gat.setMacAddr(0,"00:00:00:00:00:05");
	gat.setChannelNumber(0,1);
	gat.setRoutingPlan(0,0);
	gat.setSsid(0,ssidOne);

	gat.setMacAddr(1,"00:00:00:00:00:06");
	gat.setChannelNumber(1,6);
	gat.setRoutingPlan(1,1);
	gat.setSsid(1,ssidTwo);

	int counta=0;
	NodeList::Iterator ii;
	for(ii= NodeList::Begin();ii != NodeList::End(); ++ii)
	{
		if(counta < START_NODE_ID)
		{
			counta=counta+1;
			continue;
		}
		else
		{

			    Ptr<Ipv4L3Protocol> ip = (*ii)->GetObject<Ipv4L3Protocol> ();
			    NS_ASSERT(ip !=0);
				ObjectVectorValue interfaces;
				ip->GetAttribute("InterfaceList", interfaces);
				ObjectVectorValue::Iterator j = ++interfaces.Begin(); //since we know that second interface is the one we want for sta-nodes - seems that first one is reserved
				Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
			    NS_ASSERT(ipIface != 0);
			    Ptr<NetDevice> device = ipIface->GetDevice();
			    NS_ASSERT(device != 0);
			    NS_LOG_UNCOND("" << counta << ": MacAddress: " << Mac48Address::ConvertFrom(device->GetAddress()));
			    //Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress ());
			    Ptr<WifiNetDevice>wifiDev=DynamicCast<WifiNetDevice>(device);
			    Ptr<WifiMac>wifiM=wifiDev->GetMac();
			    NS_LOG_UNCOND("DOING");
			    Ptr<StaWifiMac>wifiS=DynamicCast<StaWifiMac>(wifiM);
			    wifiS->GetGAT()=gat;

		}

	}

	NS_LOG_UNCOND("DONE.");

}


void printRoutingTable (Ptr<Node> node)
{
	Ipv4StaticRoutingHelper helper;
	Ptr<Ipv4> stack = node -> GetObject<Ipv4>();
	Ptr<Ipv4StaticRouting> staticrouting = helper.GetStaticRouting(stack);
	uint32_t numroutes=staticrouting->GetNRoutes();
	Ipv4RoutingTableEntry entry;
	std::cout << "Routing table for device:" << Names::FindName(node) << "\n";
	std::cout << "Destination\tMask\t\tGateway\t\tIface\t\tDestination\n";
	for (uint32_t i=0; i<numroutes; i++)
	{
		entry=staticrouting->GetRoute(i);
		std::cout << entry.GetDestNetwork() << "\t" << entry.GetDestNetworkMask() << "\t" << entry.GetGateway() << "\t\t" << entry.GetInterface() << entry.GetDest() << "\n";
	}
	return;

}

int
main (int argc, char *argv[])
{
  int b_caseMode; //1: CVCL; 2: CVC
  int b_congMode; //1: AP1; 2: AP2
  int b_pattMode; //1: Game; 2: Video
  int b_planMode; //1: Low Load; 2: High Load;
  int b_planID; //1-4: plan 1-4 under specific plan mode
  int NODE_AMOUNT_GROUP1;
  int NODE_AMOUNT_GROUP2;
  int START_NODE_ID=3;
  int MAX_FSD_NODES=1; //single-node case
  int MaxPacketsBac=10000000;
  //int VideoMode=0;

  CommandLine cmd;
  cmd.AddValue ("caseMode", "Choose between CVCL (1) and CVC (2)", b_caseMode);
  cmd.AddValue ("congestionMode", "Choose between AP1 (1) and AP2 (2)", b_congMode);
  cmd.AddValue ("patternMode", "Choose between Game (1) and Video (2) (FSD Node)", b_pattMode);
  cmd.AddValue ("planMode", "Choose between Low Load (1) and High Load (2) (Background Traffic)", b_planMode);
  cmd.AddValue ("planID", "Choose From Slightest Load (1) to Heaviest Load (4) (Background Traffic)", b_planID);
  cmd.Parse (argc, argv);

  std::vector<double> DownlinkIntervalPattern;
  std::vector<int> DownlinkPacketSizePattern;
  std::vector<double> UplinkIntervalPattern;
  std::vector<int> UplinkPacketSizePattern;
  std::vector<double> QoSLevel;
  std::vector<double>location_x;
  std::vector<double>location_y;

  std::vector<double>AP1Location;
  std::vector<double>AP2Location;
  std::vector<double>FSDNodeLocation;

  //start to configure critical parameters according to input
  if(b_caseMode == 1)
  {
	  NODE_AMOUNT_GROUP1=21;
	  NODE_AMOUNT_GROUP2=0;
	  //int NODE_AMOUNT_GROUP2 10
	  location_x.resize(NODE_AMOUNT_GROUP1);
	  location_y.resize(NODE_AMOUNT_GROUP1);

	  AP1Location.resize(2);
	  AP2Location.resize(2);
	  FSDNodeLocation.resize(2);

	  AP1Location[0]=120;
	  AP1Location[1]=120;
	  AP2Location[0]=200;
	  AP2Location[1]=200;
	  FSDNodeLocation[0]=130;
	  FSDNodeLocation[1]=130;

	  DownlinkIntervalPattern.resize(NODE_AMOUNT_GROUP1);
	  DownlinkPacketSizePattern.resize(NODE_AMOUNT_GROUP1);
	  UplinkIntervalPattern.resize(NODE_AMOUNT_GROUP1);
	  UplinkPacketSizePattern.resize(NODE_AMOUNT_GROUP1);
	  QoSLevel.resize(NODE_AMOUNT_GROUP1);

  }
  else if(b_caseMode == 2)
  {
	  NODE_AMOUNT_GROUP1=21;
	  NODE_AMOUNT_GROUP2=10;
	  location_x.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
	  location_y.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);

	  AP1Location.resize(2);
	  AP2Location.resize(2);
	  FSDNodeLocation.resize(2);

	  AP1Location[0]=120;
	  AP1Location[1]=120;
	  AP2Location[0]=140;
	  AP2Location[1]=140;
	  FSDNodeLocation[0]=130;
	  FSDNodeLocation[1]=130;

	  DownlinkIntervalPattern.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
	  DownlinkPacketSizePattern.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
	  UplinkIntervalPattern.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
	  UplinkPacketSizePattern.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
	  QoSLevel.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);

  }
  else
  {
	  NS_FATAL_ERROR("illegal b_caseMode value!");
	  //return;
  }
  if(b_congMode == 1)
  {
	  //configure topology so more nodes are around AP1
	  if(b_caseMode == 1)
	  {
		  double _location_x[21]={130,105.914079231113,124.475888606459,102.986952842742,110.386809762941,99.5171824593703,115.817422829654,106.249418684946,117.353094288987,122.466576800813,120.820176225935,106.291602081894,117.634525928386,120.751038132146,118.727287593101,92.9548201920902,106.359312001751,108.079366569079,123.711960421105,107.168914669549,117.820059584465};
		  double _location_y[21]={130,117.752951819831,125.435509575906,124.734015431077,121.419122208482,103.375987147818,124.707885276708,112.930311697256,122.822334663814,124.297490229874,100.842648219092,103.328084194849,127.367289186291,94.9430121712089,103.503903566078,117.483672872321,106.281121970307,104.437370908317,119.643165178753,127.746241963854,114.354984182648};

		  for(int xi=0;xi<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xi++)
		  {
			  location_x[xi]=_location_x[xi];
			  location_y[xi]=_location_y[xi];
		  }

	  }
	  else
	  {
		  double _location_x[31]={130,105.914079231113,124.475888606459,102.986952842742,110.386809762941,99.5171824593703,115.817422829654,106.249418684946,117.353094288987,122.466576800813,120.820176225935,106.291602081894,117.634525928386,120.751038132146,118.727287593101,92.9548201920902,106.359312001751,108.079366569079,123.711960421105,107.168914669549,117.820059584465,143.061544044170,158.876813961636,144.918697533286,131.126401241110,145.476684710808,160.086284747016,142.346385768323,134.360188233685,139.388558620296,148.926120969371};
		  double _location_y[31]={130,117.752951819831,125.435509575906,124.734015431077,121.419122208482,103.375987147818,124.707885276708,112.930311697256,122.822334663814,124.297490229874,100.842648219092,103.328084194849,127.367289186291,94.9430121712089,103.503903566078,117.483672872321,106.281121970307,104.437370908317,119.643165178753,127.746241963854,114.354984182648,149.362184857041,157.337032832816,148.886505730059,145.433148823880,155.780278787662,142.304494334247,158.338115998237,161.769129384417,150.633259991935,130.350202035253};

		  for(int xi=0;xi<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xi++)
		  {
			  location_x[xi]=_location_x[xi];
			  location_y[xi]=_location_y[xi];
		  }

	  }
  }
  else if(b_congMode == 2)
  {

	  //configure topology so more nodes are around AP2
	  if(b_caseMode == 1)
	  {
		  double _location_x[21]={130,105.914079231113,124.475888606459,102.986952842742,110.386809762941,99.5171824593703,115.817422829654,106.249418684946,117.353094288987,122.466576800813,120.820176225935,106.291602081894,117.634525928386,120.751038132146,118.727287593101,92.9548201920902,106.359312001751,108.079366569079,123.711960421105,107.168914669549,117.820059584465};
		  double _location_y[21]={130,117.752951819831,125.435509575906,124.734015431077,121.419122208482,103.375987147818,124.707885276708,112.930311697256,122.822334663814,124.297490229874,100.842648219092,103.328084194849,127.367289186291,94.9430121712089,103.503903566078,117.483672872321,106.281121970307,104.437370908317,119.643165178753,127.746241963854,114.354984182648};

		  location_x[0]=FSDNodeLocation[0];
		  location_y[0]=FSDNodeLocation[1];

		  for(int xi=1;xi<NODE_AMOUNT_GROUP1;xi++)
		  {
			  location_x[xi]=_location_x[xi]+AP2Location[0]-AP1Location[0];
			  location_y[xi]=_location_y[xi]+AP2Location[1]-AP1Location[1];
		  }
		  for(int xi=NODE_AMOUNT_GROUP1;xi<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xi++)
		  {
			  location_x[xi]=_location_x[xi]+AP1Location[0]-AP2Location[0];
			  location_y[xi]=_location_y[xi]+AP1Location[1]-AP2Location[1];
		  }

		  //int tmp;
		  //tmp=NODE_AMOUNT_GROUP1;
		  //NODE_AMOUNT_GROUP1=NODE_AMOUNT_GROUP2;
		  //NODE_AMOUNT_GROUP2=tmp;
		  NODE_AMOUNT_GROUP2=NODE_AMOUNT_GROUP1-1;
		  NODE_AMOUNT_GROUP1=1;



	  }
	  else
	  {
		  double _location_x[31]={130,105.914079231113,124.475888606459,102.986952842742,110.386809762941,99.5171824593703,115.817422829654,106.249418684946,117.353094288987,122.466576800813,120.820176225935,106.291602081894,117.634525928386,120.751038132146,118.727287593101,92.9548201920902,106.359312001751,108.079366569079,123.711960421105,107.168914669549,117.820059584465,143.061544044170,158.876813961636,144.918697533286,131.126401241110,145.476684710808,160.086284747016,142.346385768323,134.360188233685,139.388558620296,148.926120969371};
		  double _location_y[31]={130,117.752951819831,125.435509575906,124.734015431077,121.419122208482,103.375987147818,124.707885276708,112.930311697256,122.822334663814,124.297490229874,100.842648219092,103.328084194849,127.367289186291,94.9430121712089,103.503903566078,117.483672872321,106.281121970307,104.437370908317,119.643165178753,127.746241963854,114.354984182648,149.362184857041,157.337032832816,148.886505730059,145.433148823880,155.780278787662,142.304494334247,158.338115998237,161.769129384417,150.633259991935,130.350202035253};

		  location_x[0]=FSDNodeLocation[0];
		  location_y[0]=FSDNodeLocation[1];

		  for(int xi=1;xi<NODE_AMOUNT_GROUP1;xi++)
		  {
			  _location_x[xi]=_location_x[xi]+AP2Location[0]-AP1Location[0];
			  _location_y[xi]=_location_y[xi]+AP2Location[1]-AP1Location[1];
		  }
		  for(int xi=NODE_AMOUNT_GROUP1;xi<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xi++)
		  {
			  _location_x[xi]=_location_x[xi]+AP1Location[0]-AP2Location[0];
			  _location_y[xi]=_location_y[xi]+AP1Location[1]-AP2Location[1];
		  }

		  for(int xi=1;xi<NODE_AMOUNT_GROUP2;xi++)
		  {
			  location_x[xi]=_location_x[xi+NODE_AMOUNT_GROUP1];
			  location_y[xi]=_location_y[xi+NODE_AMOUNT_GROUP1];
		  }

		  for(int xi=NODE_AMOUNT_GROUP2;xi<NODE_AMOUNT_GROUP2+NODE_AMOUNT_GROUP1;xi++)
		  {
			  location_x[xi]=_location_x[xi-NODE_AMOUNT_GROUP2+1];
			  location_y[xi]=_location_y[xi-NODE_AMOUNT_GROUP2+1];
		  }

		  int tmp;
		  tmp=NODE_AMOUNT_GROUP1;
		  NODE_AMOUNT_GROUP1=NODE_AMOUNT_GROUP2;
		  NODE_AMOUNT_GROUP2=tmp;


//		  location_x.

//		  //some change to guarantee that we always have structure {AP1's Nodes}{AP2's Nodes}
//		  location_x.insert(location_x.begin()+1,_location_x+NODE_AMOUNT_GROUP2-1,_location_x+NODE_AMOUNT_GROUP2+NODE_AMOUNT_GROUP1-1);
//		  location_x.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);
//		  location_y.insert(location_y.begin()+1,_location_y+NODE_AMOUNT_GROUP2-1,_location_y+NODE_AMOUNT_GROUP2+NODE_AMOUNT_GROUP1-1);
//		  location_y.resize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);


	  }
  }
  else
  {
	  NS_FATAL_ERROR("illegal b_congMode value!");
  }
  if(b_pattMode == 1)
  {
	  DownlinkIntervalPattern[0]=0.055;
	  DownlinkPacketSizePattern[0]=55;
	  UplinkIntervalPattern[0]=0.5;
	  UplinkPacketSizePattern[0]=62;
	  QoSLevel[0]=0;
	  //VideoMode=0;

  }
  else if(b_pattMode == 2)
  {
	  DownlinkIntervalPattern[0]=0.0023;
	  DownlinkPacketSizePattern[0]=1380;
	  UplinkIntervalPattern[0]=99999999;
	  UplinkPacketSizePattern[0]=1000;
	  QoSLevel[0]=5;
	  //VideoMode=1;
  }
  else
  {
	  NS_FATAL_ERROR("illegal b_pattMode value!");
  }
  if(b_planMode != 1 && b_planMode != 2)
  {
	  NS_FATAL_ERROR("illegal b_planMode value!");
  }
  if(b_planID <= 0 || b_planID >=5)
  {
	  NS_FATAL_ERROR("illegal b_planID value!");
  }
  else
  {
	  if(b_planMode == 1) //low load: 1; high load: 2
	  {
		  switch(b_planID)
		  {
			  case 1:
				  for(int k=1; k<NODE_AMOUNT_GROUP1;k++)
				  {
					  DownlinkIntervalPattern[k]=0.025234;
					  UplinkIntervalPattern[k]=99999999;
					  UplinkPacketSizePattern[k]=1000;
					  DownlinkPacketSizePattern[k]=1300;
					  QoSLevel[0]=5;
				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 2:
				  for(int k=1; k<NODE_AMOUNT_GROUP1;k++)
				  {
					  DownlinkIntervalPattern[k]=0.012357;
					  UplinkIntervalPattern[k]=99999999;
					  UplinkPacketSizePattern[k]=1000;
					  DownlinkPacketSizePattern[k]=1300;
					  QoSLevel[0]=5;
				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 3:
				  for(int k=1; k<NODE_AMOUNT_GROUP1;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.5;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[0]=0;
				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 4:
				  for(int k=1; k<NODE_AMOUNT_GROUP1;k++)
				  {
					  DownlinkIntervalPattern[k]=0.045;
					  UplinkIntervalPattern[k]=0.03;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[0]=0;
				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  default:

				  NS_FATAL_ERROR("something is wrong here!");

			  }
	  }
	  else
	  {

		  switch(b_planID)
		  {

			  case 1:
				  for(int k=1;k<NODE_AMOUNT_GROUP1; k++)
				  {
					  DownlinkIntervalPattern[k]=0.001617;
					  UplinkIntervalPattern[k]=99999999;
					  UplinkPacketSizePattern[k]=1000;
					  DownlinkPacketSizePattern[k]=1380;
					  QoSLevel[k]=5;

				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 2:
				  for(int k=1;k<NODE_AMOUNT_GROUP1; k++)
				  {
					  DownlinkIntervalPattern[k]=0.0008617;
					  UplinkIntervalPattern[k]=99999999;
					  UplinkPacketSizePattern[k]=1000;
					  DownlinkPacketSizePattern[k]=1380;
					  QoSLevel[k]=5;

				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 3:
				  for(int k=1;k<NODE_AMOUNT_GROUP1; k++)
				  {
					  DownlinkIntervalPattern[k]=0.003;
					  UplinkIntervalPattern[k]=0.045;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;

				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  case 4:
				  for(int k=1;k<NODE_AMOUNT_GROUP1; k++)
				  {
					  DownlinkIntervalPattern[k]=0.003;
					  UplinkIntervalPattern[k]=0.002;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;

				  }
				  for(int k=NODE_AMOUNT_GROUP1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
				  {
					  DownlinkIntervalPattern[k]=0.055;
					  UplinkIntervalPattern[k]=0.05;
					  UplinkPacketSizePattern[k]=62;
					  DownlinkPacketSizePattern[k]=55;
					  QoSLevel[k]=0;
				  }
				  break;

			  default:

				  NS_FATAL_ERROR("something is wrong here!");
		  }
	  }
  }

  NS_LOG_UNCOND("NODE_AMOUNT_GROUP1: " << NODE_AMOUNT_GROUP1 << ", NODE_AMOUNT_GROUP2: " << NODE_AMOUNT_GROUP2);

  //create basic structure
  UintegerValue ctsThr = 0; //original value:500
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);

  NodeContainer p2pNodesPairOne; // AP1 to Server
  p2pNodesPairOne.Create (2);

  NodeContainer p2pNodesPairTwo; // AP2 to Server
  p2pNodesPairTwo.Add(p2pNodesPairOne.Get(0));
  p2pNodesPairTwo.Create(1);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("0ns"));

  NetDeviceContainer p2pDevicesPairOne;
  p2pDevicesPairOne = pointToPoint.Install(p2pNodesPairOne);

  NetDeviceContainer p2pDevicesPairTwo;
  p2pDevicesPairTwo = pointToPoint.Install(p2pNodesPairTwo);

  NodeContainer apNodes;
  //apNodes = p2pNodesPairTwo;
  apNodes.Add(p2pNodesPairOne.Get(1));
  apNodes.Add(p2pNodesPairTwo.Get(1));

  NodeContainer serverNodes;
  //serverNodes.Add(p2pNodesPairOne.Get(1));
  serverNodes.Add(p2pNodesPairOne.Get(0));

  NodeContainer staNodeGroup1;
  staNodeGroup1.Create(NODE_AMOUNT_GROUP1);
  NodeContainer staNodeGroup2;
  staNodeGroup2.Create(NODE_AMOUNT_GROUP2);

  NodeContainer staNodeT;
  staNodeT.Add(staNodeGroup1);
  staNodeT.Add(staNodeGroup2);

  NodeContainer allNodes;
  allNodes.Add(serverNodes);
  allNodes.Add(apNodes);
  allNodes.Add(staNodeGroup1);
  allNodes.Add(staNodeGroup2);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

  phy.SetChannel (channel.Create ());
  phy.Set("ChannelNumber",UintegerValue(1));

  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetRemoteStationManager("ns3::IdealWifiManager");

  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);

  QosWifiMacHelper mac = QosWifiMacHelper::Default ();
  Ssid ssid = Ssid ("ns-3-ssid");

  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevicesOne;
  apDevicesOne = wifi.Install (phy, mac, apNodes.Get(0));

  phy.Set("ChannelNumber",UintegerValue(6));
  Ssid ssidOne = Ssid ("ns-3-ssid1"); //ns-3-ssid1
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssidOne));
  NetDeviceContainer apDevicesTwo;
  apDevicesTwo = wifi.Install (phy, mac, apNodes.Get(1));

  phy.Set("ChannelNumber",UintegerValue(1));
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));
//			   "RollingModeOneProb",UintegerValue(10*((NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2)/MAX_FSD_NODES)),
//			   "MinFSDNodeID",UintegerValue(START_NODE_ID+MAX_FSD_NODES-1),
//			   "MaxFSDNodeID",UintegerValue(START_NODE_ID+MAX_FSD_NODES-1),//NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2),
//			   "MaxWindowSize",UintegerValue(300),
//			   "VideoMode",UintegerValue(VideoMode));
  NodeContainer staNode1;
  NetDeviceContainer staDevices1;
  staDevices1 = wifi.Install(phy,mac,staNodeGroup1);

  phy.Set("ChannelNumber",UintegerValue(6));
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssidOne),
               "ActiveProbing", BooleanValue (true));
//			   "RollingModeOneProb",UintegerValue(10*((NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2)/MAX_FSD_NODES)),
//			   "MinFSDNodeID",UintegerValue(START_NODE_ID+MAX_FSD_NODES-1),//),
//			   "MaxFSDNodeID",UintegerValue(START_NODE_ID+MAX_FSD_NODES-1),//NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2),
//			   "MaxWindowSize",UintegerValue(300),
//			   "VideoMode",UintegerValue(VideoMode));
  NodeContainer staNode2;
  NetDeviceContainer staDevices2;
  staDevices2 = wifi.Install (phy, mac, staNodeGroup2);

  //Config::SetDefault("NodeList/*/$ns3::Ipv4L3Protocol/InterfaceList/*/ArpCache/AliveTimeout",TimeValue(Seconds(24*60*60)));

  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (serverNodes);
  mobility.Install (apNodes);
  mobility.Install (staNodeGroup1);
  mobility.Install (staNodeGroup2);

  //AP1
  Ptr<Node> objectTmp = apNodes.Get(0);
  Ptr<MobilityModel> position = objectTmp->GetObject<MobilityModel>();
  Vector posi;
  posi.x = AP1Location[0];
  posi.y = AP1Location[1];
  position->SetPosition(posi);

  //AP2
  objectTmp = apNodes.Get(1);
  position = objectTmp->GetObject<MobilityModel>();
  posi.x = AP2Location[0];//100 - original
  posi.y = AP2Location[1];//100 - original
  position->SetPosition(posi);

  objectTmp = allNodes.Get(0);
  position = objectTmp->GetObject<MobilityModel>();
  posi.x = 120;
  posi.y = 140;
  position->SetPosition(posi);

  //set remaining nodes
  int k;
  for(k = 0; k < NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2; k++)
  {
	  objectTmp = staNodeT.Get(k);
	  position=objectTmp->GetObject<MobilityModel>();
	  posi.x = location_x[k]; //scaling
	  posi.y = location_y[k];	//scaling
	  position->SetPosition(posi);
  }

  //OlsrHelper olsr;
  Ipv4StaticRoutingHelper staticRouting;
  Ipv4ListRoutingHelper list;
  list.Add (staticRouting, 10);
  //list.Add (olsr, 8);

  InternetStackHelper stack;
  stack.SetRoutingHelper (list); // has effect on the next Install ()
  stack.Install(allNodes);

  Ipv4AddressHelper address;

  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfacesOne;
  p2pInterfacesOne = address.Assign (p2pDevicesPairOne);

  address.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfacesTwo;
  p2pInterfacesTwo = address.Assign (p2pDevicesPairTwo);

  address.SetBase ("10.1.3.0", "255.255.255.0");
  //address.Assign (staDevices.Get(0));
  address.Assign (apDevicesOne);
  Ipv4InterfaceContainer staInterface1;
  staInterface1=address.Assign (staDevices1);
  NS_LOG_UNCOND("Next IP on AP1 is " << address.NewAddress());

  address.SetBase ("10.1.4.0", "255.255.255.0");
  address.Assign (apDevicesTwo);
  Ipv4InterfaceContainer staInterface2;
  for(int xixix=0;xixix<NODE_AMOUNT_GROUP1;xixix++)
  {
	  Ipv4Address tmp=address.NewAddress();
	  NS_LOG_UNCOND("" << tmp << " drained from address pool.");
  }
  staInterface2=address.Assign(staDevices2);
  NS_LOG_UNCOND("Next IP on AP2 is " << address.NewAddress());

  Ipv4InterfaceContainer staInterfaces;
  staInterfaces.Add(staInterface1);
  staInterfaces.Add(staInterface2);

  Ptr<Ipv4> Ipv4Ap0=apNodes.Get(0)->GetObject<Ipv4>();
  Ptr<Ipv4> Ipv4Ap1=apNodes.Get(1)->GetObject<Ipv4>();
  Ptr<Ipv4> Ipv4Serv=serverNodes.Get(0)->GetObject<Ipv4>();

  Ptr<Ipv4StaticRouting> staticRoutingAp0=staticRouting.GetStaticRouting(Ipv4Ap0);
  Ptr<Ipv4StaticRouting> staticRoutingAp1=staticRouting.GetStaticRouting(Ipv4Ap1);
  Ptr<Ipv4StaticRouting> staticRoutingServ=staticRouting.GetStaticRouting(Ipv4Serv);

  staticRoutingAp0->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.1.1"),1);
  staticRoutingAp1->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.2.1"),1);

  uint32_t ap0Src=0x0a010300; //dst is the same
  uint32_t ap1Src=0x0a010400; //dst is the same

  uint32_t sr1Src=0x0a010300;
  uint32_t sr1Dst=0x0a010102;

  uint32_t sr2Src=0x0a010400;
  uint32_t sr2Dst=0x0a010202;

  for(uint8_t i=0;i<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID;i++)
  {
	  //set up remaining entries for ap0's routing table
	  staticRoutingAp0->AddHostRouteTo(Ipv4Address(ap0Src+i),Ipv4Address(ap0Src+i),2);

	  //set up remaining entries for ap1's routing table
	  staticRoutingAp1->AddHostRouteTo(Ipv4Address(ap1Src+i),Ipv4Address(ap1Src+i),2);

	  //set up remaining entries for server's routing table
	  staticRoutingServ->AddHostRouteTo(Ipv4Address(sr1Src+i),Ipv4Address(sr1Dst),1);
	  staticRoutingServ->AddHostRouteTo(Ipv4Address(sr2Src+i),Ipv4Address(sr2Dst),2);

  }

  //staticRouting for Other Nodes
  int xxxx;
  for(xxxx=0;xxxx<NODE_AMOUNT_GROUP1;xxxx++)
  {
	  Ptr<Ipv4> Ipv4Sta2=staNodeT.Get(xxxx)->GetObject<Ipv4>();
	  Ptr<Ipv4StaticRouting> staticRoutingSta2=staticRouting.GetStaticRouting(Ipv4Sta2);
	  staticRoutingSta2->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.3.1"),1);

  }
  for(xxxx=NODE_AMOUNT_GROUP1;xxxx<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xxxx++)
  {
	  Ptr<Ipv4> Ipv4Staxx=staNodeT.Get(xxxx)->GetObject<Ipv4>();
	  Ptr<Ipv4StaticRouting> staticRoutingStaxx=staticRouting.GetStaticRouting(Ipv4Staxx);
	  staticRoutingStaxx->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.4.1"),1);
  }

  //install uplink side client-server applications for probings - APP2
  UdpServerHelper serverAPP2Up (11);
  ApplicationContainer serverAPPS2=serverAPP2Up.Install(serverNodes.Get(0));
  serverAPPS2.Start(Seconds(0.0));
  serverAPPS2.Stop(Seconds(30000.0));
  UdpClientHelper clientAPP2Up (p2pInterfacesOne.GetAddress(0),11);

  //BEWARE TO CHANGE THIS LINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!SHOULD BE THE SAME AS INITIAL_PROBE_TRAIN_LENGTH
  clientAPP2Up.SetAttribute ("MaxPackets", UintegerValue (2));
  clientAPP2Up.SetAttribute ("Interval", TimeValue (Seconds(0.0002)));//(Seconds (0.00000777)));//(Seconds (0.0002)));//(Seconds (0.00002)));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
  clientAPP2Up.SetAttribute ("PacketSize", UintegerValue (55));
  clientAPP2Up.SetAttribute("QoSLevel",UintegerValue(7));
  ApplicationContainer clientAPPS2;
  for(int i=0; i<MAX_FSD_NODES;i++)
  {
	  clientAPPS2 = clientAPP2Up.Install (staNodeT.Get (i));
	  clientAPPS2.Start (Seconds (2.1));
	  clientAPPS2.Stop (Seconds (30000));

  }

  //install uplink side client-server applications
  UdpServerHelper server (9);

  ApplicationContainer serverApps = server.Install (serverNodes.Get (0));
  serverApps.Start (Seconds (0.0));
  serverApps.Stop (Seconds (30000.0));

  UdpClientHelper *clientBackground[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
  ApplicationContainer clientAppsBackground[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
  for(k=0;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
  {
	  NS_LOG_UNCOND("User " << k << ": MaxPackets " << MaxPacketsBac << " Uplink Interval " << UplinkIntervalPattern[k] << " Uplink Packet Size " << UplinkPacketSizePattern[k] << " QoS Level " << QoSLevel[k]);

	  clientBackground[k] = new UdpClientHelper (p2pInterfacesOne.GetAddress(0), 9);
	  clientBackground[k]->SetAttribute("RemoteAddress", AddressValue(Address(p2pInterfacesOne.GetAddress(0))));
	  clientBackground[k]->SetAttribute("RemotePort", UintegerValue(9));
	  clientBackground[k]->SetAttribute("MaxPackets", UintegerValue(MaxPacketsBac));
	  //clientBackground[k]->SetAttribute("Interval", TimeValue(Seconds (traffic_pattern[k]/100)));
	  clientBackground[k]->SetAttribute("Interval", TimeValue(Seconds (UplinkIntervalPattern[k])));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
	  clientBackground[k]->SetAttribute("PacketSize", UintegerValue(UplinkPacketSizePattern[k]));
	  //QoSLevel
	  clientBackground[k]->SetAttribute("QoSLevel", UintegerValue(QoSLevel[k]));

	  clientAppsBackground[k] = clientBackground[k]->Install(staNodeT.Get(k));
	  clientAppsBackground[k].Start(Seconds(k*0.1+2));
	  clientAppsBackground[k].Stop(Seconds(30000));
  }
  NS_LOG_UNCOND ("Uplink DONE.");

  //install downlink side client-server applications
  UdpServerHelper server2 (10);

  ApplicationContainer serverApps2 = server2.Install (staNodeT);
  serverApps2.Start (Seconds (0.0));
  serverApps2.Stop (Seconds (30000.0));

  UdpClientHelper *clientBackground2[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
  ApplicationContainer clientAppsBackground2[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
  for(k=0;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
  {
	  NS_LOG_UNCOND("User " << k << ": MaxPackets " << MaxPacketsBac << " Downlink Interval " << DownlinkIntervalPattern[k] << " Downlink Packet Size " << DownlinkPacketSizePattern[k] << " QoS Level " << QoSLevel[k]);

	  clientBackground2[k] = new UdpClientHelper (staInterfaces.GetAddress(k), 10);
	  clientBackground2[k]->SetAttribute("RemoteAddress", AddressValue(Address(staInterfaces.GetAddress(k))));
	  clientBackground2[k]->SetAttribute("RemotePort", UintegerValue(10));
	  clientBackground2[k]->SetAttribute("MaxPackets", UintegerValue(MaxPacketsBac));
	  //clientBackground[k]->SetAttribute("Interval", TimeValue(Seconds (traffic_pattern[k]/100)));
	  clientBackground2[k]->SetAttribute("Interval", TimeValue(Seconds (DownlinkIntervalPattern[k])));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
	  clientBackground2[k]->SetAttribute("PacketSize", UintegerValue(DownlinkPacketSizePattern[k]));
	  clientBackground2[k]->SetAttribute("QoSLevel", UintegerValue(QoSLevel[k]));

	  clientAppsBackground2[k] = clientBackground2[k]->Install(serverNodes.Get(0));
	  clientAppsBackground2[k].Start(Seconds(k*0.1+2));
	  clientAppsBackground2[k].Stop(Seconds(30000));
  }

  NS_LOG_UNCOND ("Downlink DONE.");

  OutputStreamWrapper a = OutputStreamWrapper("routingTable",std::ios_base::in | std::ios_base::app);

  int i=0;
  NodeContainer::Iterator j;
  for (j = allNodes.Begin(); j != allNodes.End(); ++j)
    {
  	  Ptr<Node> object = *j;
  	  std::clog << "For node " << i << " its routing table is:\n";
  	  allNodes.Get(i)->GetObject<Ipv4>()->GetRoutingProtocol()->PrintRoutingTable(&a);
  	  i++;
    }

  i = 0;
  for (j = allNodes.Begin(); j != allNodes.End(); ++j)
    {
  	  Ptr<Node> object = *j;
  	  Ptr<MobilityModel> position = object->GetObject<MobilityModel>();
  	  NS_ASSERT (position != 0);
  	  Vector pos = position->GetPosition();
  	  std::cerr << "For node " << i << ": x=" << pos.x << ": y=" << pos.y << "\n";
  	  //std::cerr << "Also I have " << wifiStaNodes.Get(i)->GetDevice(0)->GetChannel()->GetNDevices() << "neighbors" << "\n";
  	  i++;
    }

  Simulator::Stop (Seconds (900.0));
  PopulateArpCache (NODE_AMOUNT_GROUP1, NODE_AMOUNT_GROUP2);
  PopulateGlobalServerTable(NODE_AMOUNT_GROUP1, NODE_AMOUNT_GROUP2, START_NODE_ID);
  PopulateGlobalClientTable(NODE_AMOUNT_GROUP1, NODE_AMOUNT_GROUP2, START_NODE_ID);
  PopulateGlobalAPTable(START_NODE_ID);

  //pointToPoint.EnablePcapAll ("third");
  NS_LOG_UNCOND("HERE.");
  phy.EnablePcap ("dup-AP1-lossless-rat-ap0-", apDevicesOne.Get (0));
  phy.EnablePcap ("dup-AP1-lossless-rat-ap1-", apDevicesTwo.Get (0));
  if(NODE_AMOUNT_GROUP1 == 0)
  {
	 phy.EnablePcap ("dup-AP1-lossless-rat-sta2-", staDevices2.Get(0));
  }
  else
  {
	 phy.EnablePcap ("dup-AP1-lossless-rat-sta-", staDevices1.Get(0));
  }
  pointToPoint.EnablePcap("dup-AP1-lossless-rat-serverPort1-", p2pDevicesPairOne.Get(0),false);
  pointToPoint.EnablePcap("dup-AP1-lossless-rat-serverPort2-", p2pDevicesPairTwo.Get(0),false);
  NS_LOG_UNCOND("HERE E");

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
