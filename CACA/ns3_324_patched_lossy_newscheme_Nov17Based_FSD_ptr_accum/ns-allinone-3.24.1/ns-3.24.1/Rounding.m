function lembdaI=Rounding(lembda,D,GSize,JSize)

    %return lembdaI with element including which AP to associate: if value
    %is 0 then its association relationship will not be changed
    %following proof method in "Approximation Algorithms for Scheduling Unrelated Parallel Machines"
    
    
    %generate edge matrix, weight matrix, and flag arrays to figure out
    %which one is included in the match and which one is not
    
    %lembda:row: jobs; column: vm
    JAssign=zeros(size(lembda,1),1); %storing which machine it is assigned to 
    
    for xixi=1:1:size(lembda,1) %handle with links with weight as 1
        
        for xixix=1:1:size(lembda,2)
            
            if(lembda(xixi,xixix) ==1)

                JAssign(xixi,1)=xixix;
            
            end            
            
        end
        
        
    end
    
    for xixi=1:1:size(JAssign,1)
        
        if(JAssign(xixi,1) ~= 0) %it is already assigned to a machine
            
            continue;
            
        else                     %if it is not
            
            for xixix=1:1:size(lembda,1) %check out circle
                
                k1=find(lembda(xixi,:));
                k2=find(lembda(xixix,:));

                if(xixi ~= xixix && ...
                    size(find(k2-k1),2)<=size(find(k1),2)-2)
                    %meaning there is a circle containing xixi and xixix -
                    %specifically, isequal(lembda(xixi,:),lembda(xixix,:))) 
                    
                    k1=find(lembda(xixi,:));
                    k2=find(lembda(xixix,:));
                    common=k1(k1==k2);                    
                    
                    if(JAssign(xixix,1) == 0) %meaning xixix is not assigned to a machine - so we assign them in a random order
                        
                        JAssign(xixix,1)=common(1,1);
                        JAssign(xixi,1)=common(1,2);
                        
                    else %meaninig xixix is already assigned - although this is not quite possible
                        
                        if(~any(common-JAssign(xixix,1)))
                            JAssign(xixi,1)=common(1,1); %assigning it to an arbitrary child
                        else
                            
                            Remaining=common(common~=JAssign(xixix,1));
                            JAssign(xixi,1)=Remaining(1,1); %assign remaining child to it
                            
                        end
                        
                    end
                

                end
                
            end            
            
            if(JAssign(xixi,1) == 0) %meaning this job is not involved in a circle
                    
                k1=find(lembda(xixi,:)); %firstly lets find all jobs here.
                
                for xxd=1:1:size(k1,2)

                    flagg=0;
                    for xxdd=1:1:size(JAssign,1)
                        
                        if(JAssign(xxdd,1) == k1(1,xxd))
                            flagg=0;
                            break;
                        else
                            flagg=xxd;
                            continue;
                        end
                        
                    end
                    
                    if(flagg ~= 0)
                        JAssign(xixi,1)=k1(1,flagg);
                    else
                        continue;
                    end
                                        
                end
                                    
            end
            
            if(JAssign(xixi,1) == 0)
                
                error('something goes wrong - I cannot find a matching.');
                
            end
            
            
        end
        
        
    end
    
    %lembdaI=JAssign;
    
    %pro-processing
    kfk=find(D);
    lembdaI=zeros(1,GSize*JSize);
    lembdaI(1,kfk)=JAssign(:,1);
    
    lembdaI=reshape(lembdaI,JSize,GSize);
%     E=zeros(MSize,JSize);
%     W=zeros(MSize,JSize);
%     MFlag=zeros(MSize,1);
%     JFlag=zeros(JSize,1);
%     lembdaI=zeros(size(lembda,1),1);
    
%     for xixi=1:1:MSize
%         
%         for xixix=1:1:JSize
%             
%             if(lembda(xixix,xixi) ~= 0)
%                 
%                 E(xixi,xixix)=1;
%                 W(xixi,xixix)=lembda((xixi-1)*JSize+xixix,1);
%                
%                 if(lembda((xixi-1)*JSize+xixix,1) == 1) %indicating that this is a tree involved
%                     
%                     MFlag(xixi,1)=1;
%                     JFlag(xixix,1)=1;
%                     lembdaI((xixi-1)*JSize+xixix,1)=xixi;
%                     
%                 end
%                 
%             end
%             
%         end
%         
%     end
%     
%     for xixi=1:1:MSize
%         
%         if(MFlag(xixi,1) == 0) %indicating that there is room for one more pair in match
%             
%             for xixix=1:1:size(E,2) %search all links connecting to it
%                 
%                 if(E(xixi,xixix) == 1 && MFlag(xixi,1) == 0) %meaning this machine is linked to a job
%                     
%                     MFlag(xixi,1)=1; %directly assign this job to this machine since in paper "any of its children" could be matched
%                     JFlag(xixix,1)=1;
%                     lembdaI((xixi-1)*JSize+xixix,1)=1;
%                 end
%                 
%             end
%             
%         end
%         
%     end
     

end