function [retValue,lembda,lembdaOrigin]=FMRAP(D,G,S,g,mask)

    %D is (Iij)
    %G is (lembda_{ij})
    %X is (dij)
    %we call linprog to get result directly
    %but first we need to generate matrices for lingprog    
    GSize=size(G,1);
    
    S=reshape(S,size(D,1)*size(G,1),1)';
    mask=reshape(mask,size(D,1)*size(G,1),1)';
    D=reshape(D,size(D,1)*size(G,1),1)';

    
    kfk=find(D); %however this is only half of data required - we need to enlarge k to get other half
    k=zeros(1,2*size(kfk,2));
    k(1,1:1:size(kfk,2))=kfk(1,:);
    for xixi=size(kfk,2)+1:1:2*size(kfk,2)
        
        if(k(1,xixi-size(kfk,2))>=0 && k(1,xixi-size(kfk,2))<=size(D,2)/GSize) %meaning this belongs to AP1 originally
            
            k(1,xixi)=k(1,xixi-size(kfk,2))+size(D,2)/GSize; %put its counter part into formula
            
        else %meaning this belongs to AP2 originally
            
            k(1,xixi)=k(1,xixi-size(kfk,2))-size(D,2)/GSize;
            
        end
                
    end
    k=sort(k);
    
    kfkf=find(mask); %however this is only half of data required - we need to enlarge k to get other half
    if(~isempty(kfkf))
        kk=zeros(1,2*size(kfkf,2));
        kkk=zeros(1,2*size(kfkf,2));
        kk(1,1:1:size(kfkf,2))=kfkf(1,:);
        for xixi=size(kfkf,2)+1:1:2*size(kfkf,2)
            
            if(kk(1,xixi-size(kfkf,2))>=0 && kk(1,xixi-size(kfkf,2))<=size(D,2)/GSize) %meaning this belongs to AP1 originally
                
                kk(1,xixi)=kk(1,xixi-size(kfkf,2))+size(D,2)/GSize; %put its counter part into formula
                kkk(1,xixi)=-1;
                kkk(1,xixi-size(kfkf,2)+size(D,2)/GSize)=1;
                
            else %meaning this belongs to AP2 originally
                
                kk(1,xixi)=kk(1,xixi-size(kfkf,2))-size(D,2)/GSize;
                kkk(1,xixi)=1;
                kkk(1,xixi-size(kfkf,2))=-1;
                
            end
            
        end
        [kk,kkIndex]=sort(kk);
        kkkThen=kkk(kkIndex);
    end
    
    A=zeros(size(G,1)+size(k,2)/GSize,size(k,2)); 
    B=zeros(size(G,1)+size(k,2)/GSize,1);
    
    lb=zeros(1,size(k,2));
    ub=zeros(1,size(k,2));
    
    %lb(1,1)=-inf;
    %ub(1,1)=inf;
    
    lb(1,1:1:size(k,2))=0;
    ub(1,1:1:size(k,2))=1;

    %A:fill elements for constraint 9
    for xixi=1:1:size(G,1)
        
        for xixix=(xixi-1)*size(k,2)/GSize+1:1:xixi*size(k,2)/GSize
                
                A(xixi,xixix-(xixi-1)*size(k,2)/GSize+(xixi-1)*size(k,2)/GSize)=...
                S(1,k(1,xixix));%*D(1,k(1,xixix));                    
                %S(1,k(1,xixix-(xixi-1)*size(k,2)/GSize));%*D(1,k(1,xixix));
               
        end
                
    end

%     %A: make modifications based on line 5
%     if(~isempty(kfkf))
%         
%         
%         
%         
%         
%     end
    
    
    
    for xixi=size(G,1)+1:1:size(G,1)+size(k,2)/GSize
        
        A(xixi,xixi-size(G,1))=1;
        A(xixi,xixi-size(G,1)+size(k,2)/GSize)=1;
        
    end
    
%     %A:fill elements for constraint 11 - stupid method but it should work
%     for xixix=size(G,1)+size(kk,1)+1:1:size(G,1)+size(kk,1)+size(k,2)
%         
%         A(xixix,k(1,xixix-size(G,1)-size(kk,1)))=1;
%         A(xixix+size(k,2),k(1,xixix-size(G,1)-size(kk,1)))=-1;
%         
%     end
    
    %B:fill elements for constraint 9
    B(1:1:size(G,1),1)=g-G(1:1:size(G,1),1);
    
    %B:fill elements for constraint 10
    B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,1)=1;
    
%     %B:fill elements for constraint 11
%     B(size(G,1)+size(kk,1)+1:1:size(G,1)+size(kk,1)+size(k,2),1)=1;
%     B(size(G,1)+size(kk,1)+size(k,2)+1:1:size(G,1)+size(kk,1)+2*size(k,2),1)=0;        
     
    %move constraint 10 into Aeq and Beq
    Aeq=A(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:);
    A(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:)=[];
    Beq=B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,1);
    B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:)=[];
    
    %f=k(1,1:1:size(k,2)/GSize);
    %f=ones(1,size(k,2));
    f=[];
    
    %feasibility=lcon2vert(A,B,Aeq,Beq,1e-10,false);
    %options = optimset('LargeScale','off','Simplex','on');
    options = optimoptions('linprog','Algorithm','simplex');
    %options = optimoptions('linprog','Algorithm','dual-simplex');
    x0=[];
    %feasibility=linprog(f,A,B,Aeq,Beq,lb,ub,x0,options);
    
    [feasibility,~,exitflag,~]=linprog(f,A,B,Aeq,Beq,lb,ub,x0,options);
    
    if(isempty(feasibility) || exitflag ~= 1)
        
        retValue=0;
        lembda=[];
        lembdaOrigin=lembda;
        
    else
        
        retValue=1;
        lembda=linprog(f,A,B,Aeq,Beq,lb,ub);
        retValueTmp=zeros(1,size(D,2));
        kP=k; %since first element is H
        for xixixi=1:1:size(kP,2)
        
            retValueTmp(1,kP(1,xixixi))=lembda(xixixi,1);
        
        end
            
            lembdaOrigin=lembda;
            lembda=retValueTmp;
        
    end
    
%     solution=linprog(f,A,B,Aeq,Beq);
%     
%     if(size(solution,1) == 0 && size(solution,2) == 0)
%        
%             retValue=0;
%             lembda=solution;
%         
%     else
%             lembda=solution(1:1:size(G,1)*size(D,2));
%             if(solution(1,1) ~= 0) %need to be modified
%         
%                 retValue=1;
%         
%             end
%     end



end