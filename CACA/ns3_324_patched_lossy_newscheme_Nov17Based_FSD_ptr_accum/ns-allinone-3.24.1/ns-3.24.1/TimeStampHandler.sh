for f in *.err

	do

	#udp-client: sixth column is UID, seventh column is time
	cat "$f" | grep "TraceDelay TX" | grep "From: 3" > "$f"_udp_client_tmp
	sed -e "s/TraceDelay TX //g" -e "s/ bytes to /,/g" -e "s/ Uid: /,/g" -e "s/ Time: /,/g" -e "s/ From: /,/g" -e "s/ With IP:m_local=/,/g" -e "s/; m_mask=/,/g" -e "s/; m_broadcast=/,/g" -e "s/; m_scope=/,/g" -e "s/; m_secondary=/,/g" -e "s/ Port:/,/g" -e "s/10\.1\.1\.1//g" -e "s/10\.1\.3\.1//g" -e "s/10\.1\.3\.255//g" -e "s/255\.255\.255\.0//g" < "$f"_udp_client_tmp > "$f"_udp_client

	#Sta-Wifi-Mac: fourth column is UID, first column is time
	cat "$f" | grep "Sta-Wifi-Mac" > "$f"_sta_wifi_mac_tmp
	sed -e "s/ns://g" -e "s/Sta-Wifi-Mac.cc: From/,/g" -e "s/To/,/g" -e "s/UID/,/g" -e "s/ //g" -e "s/://g" -e "s/f/15/g" -e "s/e/14/g" -e "s/d/13/g" -e "s/c/12/g" -e "s/b/11/g" -e "s/a/10/g" < "$f"_sta_wifi_mac_tmp > "$f"_sta_wifi_mac

	#AP-Wifi-Mac: sixth column is UID, first column is time 
	cat "$f" | grep "AP-" | grep "From 00:00:00:00:00:07" > "$f"_ap_wifi_mac_tmp
	sed -e "s/AP-Wifi-Mac.cc://g" -e "s/ns: From /,/g" -e "s/://g" -e "s/To/,/g" -e "s/Or/,/g" -e "s/UID/,/g" < "$f"_ap_wifi_mac_tmp > "$f"_ap_wifi_mac

	#Mac-RX-Middle: fifth column is UID, first column is time
	cat "$f" | grep "Mac-RX-Middle" | grep -v "From 00:00:00:00:00:05 To 00:00:00:00:00:05" | grep -v "From 00:00:00:00:00:06 To 00:00:00:00:00:06" | grep "From 00:00:00:00:00:07" > "$f"_mac_rx_middle_tmp
	sed -e "s/ns/,/g" -e "s/Mac-RX-Middle.cc: From//g" -e "s/To/,/g" -e "s/OR/,/g" -e "s/UID/,/g" -e "s/://g" -e "s/ //g" < "$f"_mac_rx_middle_tmp > "$f"_mac_rx_middle

	#udp-server: 
	cat "$f" | grep "TraceDelay: RX" | grep "10\.1\.3\.1," > "$f"_udp_server_tmp
sed -e "s/TraceDelay: RX//g" -e "s/bytes from //g" -e "s/Sequence Number//g" -e "s/Uid//g" -e "s/TXtime//g" -e "s/ns//g" -e "s/RXtime//g" -e "s/Delay//g" -e "s/Node//g" -e "s/PacketTagger//g" -e "s/Last//g" -e "s/Jitter//g" -e "s/Followup //g" -e "s/Local Port//g" -e "s/\.//g" < "$f"_udp_server_tmp > "$f"_udp_server

	done



