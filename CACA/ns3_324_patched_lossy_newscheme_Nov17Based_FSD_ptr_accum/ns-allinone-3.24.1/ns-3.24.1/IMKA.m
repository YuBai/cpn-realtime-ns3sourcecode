%%
%   IMKA (Integer Minimum Knapsack Algorithm)
%   Item: what item (MU) that is available for this AP
%   a: {dij} - benefit
%   b: {cj} - cost
%   v: total cost constraint - target
%   Ii:vector with 1 representing ones should be removed
%   Bi:cost of removing ones in Ii 
function [Ii,Bi]=IMKA(item,a,b,V)

    %since problem is Interger Minimum, if changing all values here into
    %negative ones, then it becomes a Maximum Knapsack problem
    
    maxValue=0; %used to imply we cannot get feasible conclusion in this case
    benefitArray=zeros(size(a,1),size(a,1));
    costArray=zeros(size(a,1),size(a,1));
    selectedItemArray=zeros(size(a,1),size(a,1));
    
    benefitArray(1,:)=a;
    costArray(1,:)=b;
    %selectedItemArray(1,1:1:size(a,1))=1:1:size(a,1);
    for xx=1:1:size(a,1)
        
        selectedItemArray(1,xx)=xx*item(xx,1);
        
    end    
    
    %construct triangle for calculating maximum benefit and minimum cost
    %matrices
    for xixi=2:1:size(benefitArray,1) %line - representing how many items are put inside
        
        for xixix=1:1:size(benefitArray,1)-xixi+1 %column - representing that when combination including this item is selected what is benefit/cost
            
            %start to go through all elements here to see which one satisfy
            %relationship that benefit reaches to current maximum value, 
            %at the same cost is still under constraint
            exclusiveItems=selectedItemArray(1:1:xixi,xixix);
            for tmp=1:1:size(benefitArray,1)
                
                if(benefitArray(xixi-1,xixix)+a(tmp,1) > benefitArray(xixi,xixix)...
                        && ~any(exclusiveItems(:)==tmp)...
                        && a(tmp,1) ~= 0) %if we have better choice
                
                    %benefitArray(xixi,xixix)=max(benefitArray(xixi-1,xixix),benefitArray(xixi,xixix)+max());        
                    benefitArray(xixi,xixix)=benefitArray(xixi-1,xixix)+a(tmp,1);
                    costArray(xixi,xixix)=costArray(xixi-1,xixix)+b(tmp,1);
                    selectedItemArray(xixi,xixix)=tmp;
                end
                
            end
            if(benefitArray(xixi,xixix)==0) %if we have no choice
                
                benefitArray(xixi,xixix)=benefitArray(xixi-1,xixix);
                costArray(xixi,xixix)=costArray(xixi-1,xixix);
                selectedItemArray(xixi,xixix)=0;
                
            end
            
            
        end
        
    end

    %both matrices are constructed - now we need to find out sequence which
    %leads to minimum cost while satisfying that benefit is at least V
    %first let's find minimum cost one (i.e. its index in matrix)
    max=V;
    minCost=99999999;
    minX=0;
    minY=0;
    
%     backupX=1;
%     backupY=1;
    for xixi=1:1:size(costArray,1)
        
        for xixix=1:1:size(costArray,2)
            
            if(benefitArray(xixi,xixix) >= max ...
                    && costArray(xixi,xixix) < minCost...
                    && costArray(xixi,xixix) > 0)
                
                max=benefitArray(xixi,xixix);
                minCost=costArray(xixi,xixix);
                minX=xixi;
                minY=xixix;
                
            end
            
%             if(costArray(xixi,xixix) > V...
%                     && benefitArray(xixi,xixix) > max...
%                     && benefitArray(backupX,backupY) < benefitArray(xixi,xixix)) %we also maintain maximum benefit information so once minX=minY=0 we return this to trigger lowering lb
%                 
%                 backupX=xixi;
%                 backupY=xixix;
%                 
%             end
            
        end
        
    end
    
%     if(minX==0 || minY==0) %corner case: if there exists no feasible case satisfying constraint on V
%         
%         fprintf('no feasible solution.\n');
%         minX=backupX;
%         minY=backupY;       
%         
%     end
    
    %now we know which one is best, we need to retrive to figure out
    %solution here
    if(minX ~= 0 && minY ~= 0)
        Bi=costArray(minX,minY);
        selectedValueSequence=selectedItemArray(1:1:minX,minY);
        Ii=zeros(size(a,1),1);
        Ii(selectedValueSequence,1)=1;
    else
        fprintf('IMKA: no feasible solution.\n');
        Bi=maxValue;
        Ii=zeros(size(a,1),1);
    end
        
end