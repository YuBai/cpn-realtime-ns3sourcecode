function scheduleInfo=SimulationGroupsGenerator(AllFileAppendix,DataAppendix,location,uplinkTraffic,stepLen, firstGroupOffset, secondGroupOffset,maxTime)
%%
%This is used to generate arpInfo and scheduleInfo for a group of
%simulations. We assume that all files are within folder [AllFileAppendix
%Data Appendix], and file (parameters) are within $fileList. We read this
%file line by line, generate corresponding location file, uplinkTraffic
%file and downlinkTraffic file, then feed name of each 3 files into
%DataPreProcessing().

% AllFileAppendix='/Users/zerdocross/Downloads/testOnSuperComputer/';
% DateAppendix='May12/';
%fileList='fileList';
%caseAppendix='ScheduleCase/';

%data0=csvread(strcat(AllFileAppendix,DateAppendix,fileList));
%for xixi=1:1:size(data0,1)
    
%     location=strcat('locations_',num2str(data0(xixi,1)),'_',...
%         num2str(data0(xixi,2)),'_',num2str(data0(xixi,3)),'_',...
%         num2str(data0(xixi,4)),'_',num2str(data0(xixi,5)));
%     
%     uplinkTraffic=strcat('UplinkTraffic_',num2str(data0(xixi,1)),'_',...
%         num2str(data0(xixi,2)),'_',num2str(data0(xixi,3)),'_',...
%         num2str(data0(xixi,4)),'_',num2str(data0(xixi,5)));
%     
    
    scheduleInfo=DataPreProcessing(strcat(AllFileAppendix,DataAppendix),location,uplinkTraffic,...
        stepLen, firstGroupOffset, secondGroupOffset,maxTime);
    
%     fprintf('Case: %f,%f,%f,%f,%f Done.\n',data0(xixi,1),data0(xixi,2),data0(xixi,3),...
%         data0(xixi,4),data0(xixi,5));
%     
%end

end