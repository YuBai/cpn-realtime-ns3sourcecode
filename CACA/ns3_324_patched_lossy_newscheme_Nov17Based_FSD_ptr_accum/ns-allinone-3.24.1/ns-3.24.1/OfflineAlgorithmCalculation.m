%%
%Algorithm from paper called "An Approximation Algorithm for AP Association
%under User Migration Cost Constraint"
%Preliminary:  Read data from configuration files - Assuming there is only
%              one file C for nodes containing all data with:
%
%              column 1 and 2 as coordinates of node, 
%              column 3 as data rate of each corresponding node, 
%              column 4 as ID of AP it associates with initially, 
%              column 5 is length of packets it sends(in the unit of bits),
%              column 6 is migration cost of j (needed to be calculated) 
%
%              Another file A is for APs with similar stucture but not
%              containing column 3 (data rate), 4 (ID of association), 5 
%              (packets it sends), column 6 (migration cost). 
%              Instead:
%              column 3 should contain ID of AP, 
%              column 4 should contain AP load
%
%              Another file X is also for clients, which contains all data
%              on migration costs when associating with different AP in scope, 0
%              indicating it is unreachable
%               
%              Another file S is for clients too, which contains dij
%              information once j decides to associate with i

function [R,lembdaI]=OfflineAlgorithmCalculation(fileNameC,fileNameA,fileNameX,fileNameS,K)


%fileNameC='/Users/zerdocross/Downloads/testOnSuperComputer/April26/SimplifiedCase/testC';
%fileNameA='/Users/zerdocross/Downloads/testOnSuperComputer/April26/SimplifiedCase/testA';
%fileNameX='/Users/zerdocross/Downloads/testOnSuperComputer/April26/SimplifiedCase/testX';
%fileNameS='/Users/zerdocross/Downloads/testOnSuperComputer/April26/SimplifiedCase/testS';
dataCTmp=csvread(fileNameC);
dataATmp=csvread(fileNameA);
dataXTmp=csvread(fileNameX);
dataSTmpInitial=csvread(fileNameS);

dataCTmpInit=dataCTmp;
dataATmpInit=dataATmp;
dataXTmpInit=dataXTmp;

%some initialization
dataC=zeros(size(dataCTmp,1),size(dataCTmp,2)+1);
dataC(:,1:1:size(dataCTmp,2))=dataCTmp(:,:);
for xixi=1:1:size(dataC,1)
    
    dataC(xixi,size(dataC,2))=1/dataC(xixi,3);
    
end
dataA=dataATmp;
for xixi=1:1:size(dataA)

    for xixix=xixi:1:size(dataC,1)
        
        if(dataC(xixix,4) == dataA(xixi,3))
            dataA(xixi,4)=dataA(xixi,4)+dataC(xixix,size(dataC,2));
        end
    end
    
end

%initialize a matrix containing all initial association relationship
InitialI=zeros(size(dataC,1),size(dataA,1));
for xixi=1:1:size(dataA)
    
    for xixix=1:1:size(dataC,1)
        
        if(dataC(xixix,4)==xixi)
            
            InitialI(xixix,xixi)=1;
            
        end
        
    end
    
end

%Remove nodes which do not have choice
TF=dataSTmpInitial(:,2)==0;
TFP=dataSTmpInitial(:,2)~=0;
dataSTmp=dataSTmpInitial;
dataSTmp(TF,:)=[];
if(size(dataSTmp,1) == 0 || size(dataSTmp,2) == 0)
    
    fprintf('No Optimization Exists!');
    lembdaI=[];
    R=-1;
    return;
    
else
    
    dataC(TF,:)=[];
    dataXTmp(TF,:)=[];
    InitialI(TF,:)=[];
    
end
%%
%MRemove
lb=0;
ub=max(dataA(:,4));
epsilon=0.001;
%B=0;%containing all costs
%g=0;
%K=0.2;%maximum migration cost constraint
while ub>(1+epsilon)*lb

    I=zeros(size(dataC,1),size(dataA,1)); %ith element store flag indicating whether should it dis-associate with AP
    B=0;
    g=(lb+ub)/2;
    
    for xixi=1:1:size(dataA,1) %how many APs are there
        
        %tmpB=0;
        a=zeros(size(dataC,1),1);
        b=zeros(size(dataC,1),1);
        for xixix=1:1:size(dataC,1)
            
            a(xixix,1)=dataSTmp(xixix,xixi)*InitialI(xixix,xixi);
            b(xixix,1)=dataXTmp(xixix,xixi)*InitialI(xixix,xixi);
            
        end        
        item=InitialI(:,xixi);
        V=max([dataA(xixi,4)-g,0]);
        
        [Ii,Bi]=IMKA(item, a,b,V);
        I(:,xixi)=I(:,xixi)+Ii(:,1); %Does this work? - if it is, 1 here means MU is de-associated
        B=B+Bi;
        
    end
    if(B<=K && B~=0)
        
        D=I;
        ub=g;
        
    else
        
        lb=g;
        
    end
    
    
end
%finilize result after doing all these
G=zeros(size(dataA,1),1);
for xx=1:1:size(G,1)
    
    G(xx,1)=dataA(xx,4); %current load
    for xxi=1:1:size(dataC,1)
        
        if(dataC(xxi,4)==xx && I(xxi,xx)==1) %if it is associated with AP xx and above result shows it should be re-associated
            
            G(xx,1)=G(xx,1)-dataC(xxi,7);
            
        end
        
    end
    
end

if(size(find(G-dataA(:,4)),1) == 0 || size(find(G-dataA(:,4)),2) == 0) %meaning there is no node to be re-associated
    
    fprintf('There is No Optimization for Current Configuration.\n');
    lembdaI=[];
    R=-1;
    return;
    
end

%%
%MAssoc
%-----------------------July4: Need to be modified since implementation for
%5th line of algorithm 2 in paper is wrong (which could be the reason
%causing current optimization problem)------------------------------------%

D=I;
HfInit=LPMRAP(D,G,dataSTmp);
Hf=HfInit(1,1); %since we set fx in this way
ub=max(dataA(:,4));%since any solution is OK, we pick one we have originally for convenience
epsilon1=0.001;
lb=Hf;
g=0;

%for the purpose of debugging
%lb=0.03702;

mask=zeros(size(D,1),size(D,2));
while ub>(1+epsilon1)*lb
    
    D=I;
    mask=zeros(size(D,1),size(D,2));
    
    g=(lb+ub)/2;
%     for xixi=1:1:size(dataA,1)
%         
%         for xixixi=1:1:size(dataC,1)
%             
%             if(dataC(xixixi,size(dataC,2)) >= g-G(xixi,1))
%                 
%                 InitialI(xixixi,xixi)=-InitialI(xixixi,xixi); %set it to negative value implying it is de-associated
%                 
%             end
%             
%         end
%     end
    
    IPrime = InitialI - D; %figure out ones that should not be re-associated based on MRemove, and see whether we could larger benefits in MAssoc by di-associate them
    for xixi=1:1:size(IPrime,1) %per client
        
        for xixix=1:1:size(IPrime,2) %per AP
            
            if(IPrime(xixi,xixix) == 1)
                
                if(dataSTmp(xixi,xixix) >= g-G(xixix,1) && sum(D(xixi,:))==1) %a slight optimization on line 5: since we do not care re-association of node out of D
                    
                    fprintf('ALERT: I am here! I am here! I am here! (Important things should be repeated three times)\n');
                    %D(xixi,xixix)=1;
                    mask(xixi,xixix)=1;
                    
                end
                
            end
            
        end
        
    end
    
    
    [retVal,solution]=FMRAP(D,G,dataSTmp,g,mask);
    if(retVal == 1)
        
        ub=g;
        
    else
        
        lb=g;
        
    end
    
end
[retVal,lembda,lembdaOrigin]=FMRAP(D,G,dataSTmp,ub,mask);

%%
%Rounding
if(~isempty(lembda))
    
    %pre-processing optimization result before feeding it into rounding
    %algorithm according to replies from author

    %firstly we need to re-group to see how many virtual machine we need
    rawOptimizationResult=reshape(lembda,size(D,1),size(G,1))';
    vmAmount=zeros(size(G,1),1);
    count=1;
    for xixi=1:1:size(rawOptimizationResult,1)
        
        summ=sum(rawOptimizationResult(xixi,:));
        vmAmount(count,1)=ceil(summ);
        count=count+1;
        
    end

    %now we need to build a mapping between ID of virtual machine and that
    %of actual machine
    vmRMMapping=zeros(sum(vmAmount),1);
    for xixi=1:1:size(vmAmount,1)
        
        if(xixi==1)
            
            vmRMMapping(1:1:vmAmount(1,1),1)=dataA(xixi,3);
            
        else
            
            finalIndex=sum(vmAmount(1:1:xixi,1));
            vmRMMapping(vmAmount(xixi-1,1)+1:1:finalIndex,1)=dataA(xixi,3);            
            
        end
        
    end
    
    %we also need to make some modifications on feeded lembda to guarantee
    %that we feed same problem and assumption of pseudo-forest in paper
    %named "Approximation Algorithms for Scheduling Unrelated Parallel
    %Machines" work.
    lembdaPrime=zeros(int16(size(find(lembda),2)/2),size(vmRMMapping,1));%row: jobs; column: vm
    for xixi=1:1:size(lembdaPrime,1) %connect each job to vm randomly if lembda indicates that it has positive value       
        
        for xixix=1:1:size(G,1) %for each AP
            
             if(lembdaOrigin(xixi+(xixix-1)*int16(size(find(lembda),2)/size(G,1)),1) > 0)
                 
                 finalIndexx=sum(vmAmount(1:1:xixix-1,1));
                 lembdaPrime(xixi,mod(xixi-1,vmAmount(xixix,1))+1+finalIndexx)=lembdaOrigin((xixix-1)*size(lembdaPrime,1)+xixi,1);
                 
             end
            
        end       
        
    end
    
    
    %lembdaI=Rounding(lembda,size(G,1),size(D,1));
    lembdaI=Rounding(lembdaPrime,D,size(G,1),size(D,1));%size(vmRMMapping,1),int16(size(lembdaOrigin,1)/size(G,1)));
    
    %we need to post-process lembdaI to guarantee that we generate correct
    %code in following steps
    for xxf=1:1:size(lembdaI,1)
        
        for xxff=1:1:size(lembdaI,2)
            
            if(lembdaI(xxf,xxff) ~= 0)
                
                lembdaI(xxf,xxff)=vmRMMapping(lembdaI(xxf,xxff),1);
            
            end
            
        end
        
    end
    
    if(size(lembdaI,1) ~= size(dataCTmpInit,1)) %meaning we need to return another lembdaI correspondingly
        
        lembdaITmp=zeros(size(dataCTmpInit,1),size(lembdaI,2));
        lembdaITmp(TFP,:)=lembdaI;
        lembdaI=lembdaITmp;
        
    end
    
    R=1;
    
else
    
    fprintf('There is No Optimization for Current Configuration.\n');
    lembdaI=[];
    R=-1;
    
end

end