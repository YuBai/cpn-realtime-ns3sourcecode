/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//#define NODE_AMOUNT 30
#define NODE_AMOUNT_GROUP1 16
#define NODE_AMOUNT_GROUP2 0
#define START_NODE_ID 3
#define MAX_FSD_NODES 16

#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TwoAPsOriginal");

void
PopulateArpCache (int* APAssocRelationship)
{
	Ptr<ArpCache> arp = CreateObject<ArpCache> ();
	arp->SetAliveTimeout (Seconds(3600 * 24 * 365));
	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
		NS_ASSERT(ip !=0);
		ObjectVectorValue interfaces;
		ip->GetAttribute("InterfaceList", interfaces);
		for(ObjectVectorValue::Iterator j = interfaces.Begin(); j != interfaces.End (); j++)
		{
			//j->second->GetObject()
			Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
			NS_ASSERT(ipIface != 0);
			Ptr<NetDevice> device = ipIface->GetDevice();
			NS_ASSERT(device != 0);
			Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress ());
			for(uint32_t k = 0; k < ipIface->GetNAddresses (); k ++)
			{
				Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal();
				if(ipAddr == Ipv4Address::GetLoopback())
					continue;
				ArpCache::Entry * entry = arp->Add(ipAddr);
				entry->MarkWaitReply(0);
				entry->MarkAlive(addr);
			}
		}
	}


	unsigned char macB[6];
  	Ipv4Address ipAddrTmpStart1="10.1.4.2";
  	Ipv4Address ipAddrTmpStart2="10.1.3.2";
  	Ipv4Address ipAddrTmp;
  	Mac48Address addrTmp;
  	ArpCache::Entry * entry;
  	macB[0]=0x00;
  	macB[1]=0x00;
  	macB[2]=0x00;
  	macB[3]=0x00;
  	macB[4]=0x00;
  	macB[5]=0x06; //since first user is 00:00:00:00:00:07 so this should start from 06

  	int i;
  	for(i=0;i<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;i++)
  	{

          if(APAssocRelationship[i] == 1)
          {
                ipAddrTmp.Set(ipAddrTmpStart1.Get()+i);
                macB[5]=macB[5]+0x01;
                //addrTmp=Mac48Address(macB);
                addrTmp.CopyFrom(macB);
                NS_LOG_UNCOND("i: " << i << "item to be inserted is " << ipAddrTmp << " " << addrTmp);

                entry = arp->Add(ipAddrTmp);
                entry->MarkWaitReply(0);
                entry->MarkAlive(addrTmp);
          }
          else
          {

                ipAddrTmp.Set(ipAddrTmpStart2.Get()+i);
                macB[5]=macB[5]+0x01;
                addrTmp.CopyFrom(macB);
                NS_LOG_UNCOND("i: " << i << "item to be inserted is " << ipAddrTmp << " " << addrTmp);

                entry = arp->Add(ipAddrTmp);
                entry->MarkWaitReply(0);
                entry->MarkAlive(addrTmp);
          }
  	}



	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
		NS_ASSERT(ip !=0);
		ObjectVectorValue interfaces;
		ip->GetAttribute("InterfaceList", interfaces);
		for(ObjectVectorValue::Iterator j = interfaces.Begin(); j !=
				interfaces.End (); j ++)
		{
			Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
			ipIface->SetAttribute("ArpCache", PointerValue(arp));
		}
	}
}

void
PopulateGlobalServerTable ()
{

	NodeList::Iterator ii = NodeList::Begin();
	Ptr<Application>testt=(*ii)->GetApplication(2);
	Ptr<UdpClient>test=DynamicCast<UdpClient>(testt);
	test->GetGST().reSize(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID);
}

void
PopulateGlobalAPTable ()
{
	//well since this is light-workload, I decide to enter this part of information manually - change it to automatic version once it becomes necessary
	Ssid ssidOne=Ssid ("ns-3-ssid");
	Ssid ssidTwo=Ssid ("ns-3-ssid1");

	globalAPTable gat;
	gat.setMacAddr(0,"00:00:00:00:00:05");
	gat.setChannelNumber(0,1);
	gat.setLoad(0,0);
	gat.setSsid(0,ssidOne);

	gat.setMacAddr(1,"00:00:00:00:00:06");
	gat.setChannelNumber(1,6);
	gat.setLoad(1,0);
	gat.setSsid(1,ssidTwo);

	int counta=0;
	NodeList::Iterator ii;
	for(ii= NodeList::Begin();ii != NodeList::End(); ++ii)
	{
		if(counta < START_NODE_ID)
		{
			counta=counta+1;
			continue;
		}
		else
		{

			Ptr<Ipv4L3Protocol> ip = (*ii)->GetObject<Ipv4L3Protocol> ();
			NS_ASSERT(ip !=0);
			ObjectVectorValue interfaces;
			ip->GetAttribute("InterfaceList", interfaces);
			ObjectVectorValue::Iterator j = ++interfaces.Begin(); //since we know that second interface is the one we want for sta-nodes - seems that first one is reserved
			Ptr<Ipv4Interface> ipIface = j->second->GetObject<Ipv4Interface> ();
			NS_ASSERT(ipIface != 0);
			Ptr<NetDevice> device = ipIface->GetDevice();
			NS_ASSERT(device != 0);
			NS_LOG_UNCOND("MacAddress: " << Mac48Address::ConvertFrom(device->GetAddress()));
			//Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress ());
			Ptr<WifiNetDevice>wifiDev=DynamicCast<WifiNetDevice>(device);
			Ptr<WifiMac>wifiM=wifiDev->GetMac();
			NS_LOG_UNCOND("DOING");
			Ptr<StaWifiMac>wifiS=DynamicCast<StaWifiMac>(wifiM);
			wifiS->GetGAT()=gat;

		}

	}

	NS_LOG_UNCOND("DONE.");

}

void printRoutingTable (Ptr<Node> node)
{
	Ipv4StaticRoutingHelper helper;
	Ptr<Ipv4> stack = node -> GetObject<Ipv4>();
	Ptr<Ipv4StaticRouting> staticrouting = helper.GetStaticRouting(stack);
	uint32_t numroutes=staticrouting->GetNRoutes();
	Ipv4RoutingTableEntry entry;
	std::cout << "Routing table for device:" << Names::FindName(node) << "\n";
	std::cout << "Destination\tMask\t\tGateway\t\tIface\t\tDestination\n";
	for (uint32_t i=0; i<numroutes; i++)
	{
		entry=staticrouting->GetRoute(i);
		std::cout << entry.GetDestNetwork() << "\t" << entry.GetDestNetworkMask() << "\t" << entry.GetGateway() << "\t\t" << entry.GetInterface() << entry.GetDest() << "\n";
	}
	return;

}

int
main (int argc, char *argv[])
{

	// enable RTS/CTS
	/*const long double sysTime = time(0);
	  long double sysTimeAc = fmod(sysTime, 1000000);
	  NS_LOG_UNCOND ("sysTimeAc=" << sysTimeAc);
	  const long double sysTimeMS = sysTime;
	  NS_LOG_UNCOND ("Time is " << sysTimeMS);
	  RngSeedManager::SetSeed(sysTime);
	  RngSeedManager::SetRun(sysTimeAc);
	 */
	std::vector<std::string>fileNames;
	fileNames.resize(5);

	const char* _location="_location";
	const char* _uplinkTraffic="_uplinkTraffic";
	const char* _testS="_testS";
	const char* _solution="_solution";

	std::string tmmm (argv[0]);
	std::string tmmmm;
	fileNames[0]=std::string(tmmm);

	tmmmm=tmmm.append(_location);
	fileNames[1]=std::string(tmmmm);

	tmmmm=tmmm.append(_uplinkTraffic);
	fileNames[2]=std::string(tmmmm);

	tmmmm=tmmm.append(_testS);
	fileNames[3]=std::string(tmmmm);

	tmmmm=tmmm.append(_solution);
	fileNames[4]=std::string(tmmmm);

	NS_LOG_UNCOND("fileNames0: " << fileNames[0] << "\n"
			<< "fileNames1: " << fileNames[1] << "\n"
			<< "fileNames2: " << fileNames[2] << "\n"
			<< "fileNames3: " << fileNames[3] << "\n"
			<< "fileNames4: " << fileNames[4] << "\n");

	double MaxPacketsBac=10000000;
	int GroupAmt=10;
int TrafficPattern[10]={16,0,0,0,0,0,0,0,0,0};
	double DownlinkIntervalPattern[10]={99999999, 99999999, 99999999, 99999999, 99999999, 99999999, 99999999, 99999999, 99999999, 99999999}; //0.0023
double UplinkIntervalPattern[10]={0.012, 99999999,99999999,99999999,99999999,0.012, 99999999,99999999,99999999,0.002};
	double UplinkPacketSizePattern[10]={1024, 1024,1024,1024,1024, 1024, 1024,1024,1024,24};
	double DownlinkPacketSizePattern[10]={1000, 1000,1000,1000,1000, 1000, 1000,1000,1000,1000};
	double QoSLevel[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int APAssocRelationship[16]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int deltaT=60;

	UintegerValue ctsThr = 0; //original value:500
	Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);

double location_x[16]={13.7914,12.9108,11.7637,20.7343,11.3609,12.2919,18.6638,16.0702,21.4455,18.1636,14.3254,21.4142,29.0032,23.6404,25.7882,21.0428};
double location_y[16]={57.3241,54.9735,52.0979,58.9307,49.0527,49.9325,56.2223,49.3239,54.3059,48.4679,43.349,49.6553,54.1231,46.3845,47.5528,41.7912};

	NodeContainer p2pNodesPairOne; // AP1 to Server
	p2pNodesPairOne.Create (2);

	NodeContainer p2pNodesPairTwo; // AP2 to Server
	p2pNodesPairTwo.Add(p2pNodesPairOne.Get(0));
	p2pNodesPairTwo.Create(1);

	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("0ns"));

	NetDeviceContainer p2pDevicesPairOne;
	p2pDevicesPairOne = pointToPoint.Install(p2pNodesPairOne);

	NetDeviceContainer p2pDevicesPairTwo;
	p2pDevicesPairTwo = pointToPoint.Install(p2pNodesPairTwo);

	NodeContainer apNodes;
	//apNodes = p2pNodesPairTwo;
	apNodes.Add(p2pNodesPairOne.Get(1));
	apNodes.Add(p2pNodesPairTwo.Get(1));

	NodeContainer serverNodes;
	//serverNodes.Add(p2pNodesPairOne.Get(1));
	serverNodes.Add(p2pNodesPairOne.Get(0));

	NodeContainer staNodes;
	staNodes.Create(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2);

	NodeContainer allNodes;
	allNodes.Add(serverNodes);
	allNodes.Add(apNodes);
	allNodes.Add(staNodes);

	YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();channel.AddPropagationLoss("ns3::NakagamiPropagationLossModel");
	YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
	phy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

	phy.SetChannel (channel.Create ());
	phy.Set("ChannelNumber",UintegerValue(1));

	WifiHelper wifi = WifiHelper::Default ();
	wifi.SetRemoteStationManager("ns3::IdealWifiManager");

	wifi.SetStandard (WIFI_PHY_STANDARD_80211g);

	QosWifiMacHelper mac = QosWifiMacHelper::Default ();
	Ssid ssid = Ssid ("ns-3-ssid");

	mac.SetType ("ns3::ApWifiMac",
			"Ssid", SsidValue (ssid));

	NetDeviceContainer apDevicesOne;
	apDevicesOne = wifi.Install (phy, mac, apNodes.Get(0));

	phy.Set("ChannelNumber",UintegerValue(6));
	Ssid ssidOne = Ssid ("ns-3-ssid1"); //ns-3-ssid1
	mac.SetType ("ns3::ApWifiMac",
			"Ssid", SsidValue (ssidOne));
	NetDeviceContainer apDevicesTwo;
	apDevicesTwo = wifi.Install (phy, mac, apNodes.Get(1));

	/*  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssidOne),
               "ActiveProbing", BooleanValue (true));
	 */

	phy.Set("ChannelNumber",UintegerValue(1));
	mac.SetType ("ns3::StaWifiMac",
			"Ssid", SsidValue (ssid),
			"ActiveProbing", BooleanValue (false));
	NodeContainer staNode1;
	NetDeviceContainer staDevices1;
	staNode1.Add(staNodes.Get(0));
	staDevices1 = wifi.Install(phy, mac, staNode1);
	NodeContainer staNode1_1;
	NetDeviceContainer staDevices1_1;
  	for(int xx=1;xx<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xx++)
  	{
          	if(APAssocRelationship[xx] == 1)
          	{
                	staNode1_1.Add(staNodes.Get(xx));
          	}
  	}
	staDevices1_1 = wifi.Install(phy, mac, staNode1_1);

	phy.Set("ChannelNumber",UintegerValue(6));
	mac.SetType ("ns3::StaWifiMac",
			"Ssid", SsidValue (ssidOne),
			"ActiveProbing", BooleanValue (false));
	NodeContainer staNode2;
	NetDeviceContainer staDevices2;
  	for(int xx=1; xx<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2; xx++)
  	{
        	if(APAssocRelationship[xx] == 2)
        	{
          		staNode2.Add(staNodes.Get(xx));
        	}

  	}
	staDevices2 = wifi.Install (phy, mac, staNode2);

	//Config::SetDefault("NodeList/*/$ns3::Ipv4L3Protocol/InterfaceList/*/ArpCache/AliveTimeout",TimeValue(Seconds(24*60*60)));

	MobilityHelper mobility;

	mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (0.0),
			"MinY", DoubleValue (0.0),
			"DeltaX", DoubleValue (5.0),
			"DeltaY", DoubleValue (10.0),
			"GridWidth", UintegerValue (3),
			"LayoutType", StringValue ("RowFirst"));

	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (serverNodes);
	mobility.Install (apNodes);
	mobility.Install (staNodes);

	//Change Places of Two APs
	//AP1
	Ptr<Node> objectTmp = apNodes.Get(0);
	Ptr<MobilityModel> position = objectTmp->GetObject<MobilityModel>();
	Vector posi;
posi.x = 30;
posi.y = 40;
	position->SetPosition(posi);

	//AP2
	objectTmp = apNodes.Get(1);
	position = objectTmp->GetObject<MobilityModel>();
posi.x = 40;
posi.y = 30;
	position->SetPosition(posi);

	//Chosen Client
	//objectTmp = staNodes.Get(0);
	//position = objectTmp->GetObject<MobilityModel>();
	//posi.x = 130;//24 - once works(?)
	//posi.y = 130;//5 - once works(?)
	//position->SetPosition(posi);

	objectTmp = allNodes.Get(0);
	position = objectTmp->GetObject<MobilityModel>();
	posi.x = 120;
	posi.y = 140;
	position->SetPosition(posi);

	//set remaining nodes
	int k;
	for(k = 0; k < NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2; k++)
	{
		objectTmp = staNodes.Get(k);
		position=objectTmp->GetObject<MobilityModel>();
		posi.x = location_x[k] > 35 ? location_x[k]-10 : location_x[k]+10; //scaling
		posi.y = location_y[k] > 35 ? location_y[k]-10 : location_y[k]+10;	//scaling
		position->SetPosition(posi);
	}



	//OlsrHelper olsr;
	Ipv4StaticRoutingHelper staticRouting;
	Ipv4ListRoutingHelper list;
	list.Add (staticRouting, 10);
	//list.Add (olsr, 8);

	InternetStackHelper stack;
	stack.SetRoutingHelper (list); // has effect on the next Install ()
	stack.Install (serverNodes);
	stack.Install (apNodes);
	stack.Install (staNodes);

	Ipv4AddressHelper address,address1,address2;

	address.SetBase ("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer p2pInterfacesOne;
	p2pInterfacesOne = address.Assign (p2pDevicesPairOne);

	address.SetBase ("10.1.2.0", "255.255.255.0");
	Ipv4InterfaceContainer p2pInterfacesTwo;
	p2pInterfacesTwo = address.Assign (p2pDevicesPairTwo);

	address1.SetBase ("10.1.3.0", "255.255.255.0");
	//address.Assign (staDevices.Get(0));
	address1.Assign (apDevicesOne);
	address2.SetBase ("10.1.4.0", "255.255.255.0");
	address2.Assign (apDevicesTwo);

	Ipv4InterfaceContainer staInterface1, staInterface1_1,staInterface2;
	staInterface1=address1.Assign (staDevices1);	
	address2.NewAddress();	
	int count1,count2;
  	count1=0;
  	count2=0;
  	for(int xixix=1;xixix<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xixix++)
  	{

        	NS_LOG_UNCOND("" << xixix);
        	if(APAssocRelationship[xixix] == 1)
        	{
                	Ipv4InterfaceContainer tmpCon=address1.Assign(staDevices1_1.Get(count1));
                	staInterface1_1.Add(tmpCon);
                	count1++;
			address2.NewAddress();

        	}
        	else
        	{
                	Ipv4InterfaceContainer tmpCon=address2.Assign(staDevices2.Get(count2));
                	staInterface2.Add(tmpCon);
                	count2++;
			address1.NewAddress();
        	}

  	}

  	Ipv4InterfaceContainer staInterfaces;
  	staInterfaces.Add(staInterface1_1);
  	staInterfaces.Add(staInterface2);
	
	Ptr<Ipv4> Ipv4Sta1=staNodes.Get(0)->GetObject<Ipv4>();
	Ptr<Ipv4> Ipv4Ap0=apNodes.Get(0)->GetObject<Ipv4>();
	Ptr<Ipv4> Ipv4Ap1=apNodes.Get(1)->GetObject<Ipv4>();
	Ptr<Ipv4> Ipv4Serv=serverNodes.Get(0)->GetObject<Ipv4>();

	Ptr<Ipv4StaticRouting> staticRoutingSta1=staticRouting.GetStaticRouting(Ipv4Sta1);
	Ptr<Ipv4StaticRouting> staticRoutingAp0=staticRouting.GetStaticRouting(Ipv4Ap0);
	Ptr<Ipv4StaticRouting> staticRoutingAp1=staticRouting.GetStaticRouting(Ipv4Ap1);
	Ptr<Ipv4StaticRouting> staticRoutingServ=staticRouting.GetStaticRouting(Ipv4Serv);

	staticRoutingSta1->AddHostRouteTo(Ipv4Address("10.1.1.1"),Ipv4Address("10.1.3.1"),1);
	staticRoutingAp0->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.1.1"),1);
        staticRoutingAp1->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.2.1"),1);

	uint32_t ap0Src=0x0a010300; //dst is the same
	uint32_t ap1Src=0x0a010400; //dst is the same

  	uint32_t sr1Src=0x0a010300;
  	uint32_t sr1Dst=0x0a010102;

  	uint32_t sr2Src=0x0a010400;
  	uint32_t sr2Dst=0x0a010202;

  	for(uint8_t i=0;i<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2+START_NODE_ID;i++)
  	{
          	staticRoutingAp0->AddHostRouteTo(Ipv4Address(ap0Src+i),Ipv4Address(ap0Src+i),2);
          	staticRoutingAp0->AddHostRouteTo(Ipv4Address(ap1Src+i),Ipv4Address(ap1Src+i),2);

          	staticRoutingAp1->AddHostRouteTo(Ipv4Address(ap0Src+i),Ipv4Address(ap0Src+i),2);
          	staticRoutingAp1->AddHostRouteTo(Ipv4Address(ap1Src+i),Ipv4Address(ap1Src+i),2);

         	staticRoutingServ->AddHostRouteTo(Ipv4Address(sr1Src+i),Ipv4Address(sr1Dst),1);
          	staticRoutingServ->AddHostRouteTo(Ipv4Address(sr2Src+i),Ipv4Address(sr2Dst),2);
  	}

  	int xxxx;
  	Ptr<Ipv4> Ipv4Staxx;
  	Ptr<Ipv4StaticRouting> staticRoutingStaxx;
  	for(xxxx=0;xxxx<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;xxxx++)
  	{

          	if(APAssocRelationship[xxxx] == 1)
          	{
                	Ipv4Staxx=staNodes.Get(xxxx)->GetObject<Ipv4>();
                	staticRoutingStaxx=staticRouting.GetStaticRouting(Ipv4Staxx);
                	staticRoutingStaxx->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.3.1"),1);
          	}
          	else
          	{
                	Ipv4Staxx=staNodes.Get(xxxx)->GetObject<Ipv4>();
                	staticRoutingStaxx=staticRouting.GetStaticRouting(Ipv4Staxx);
                	staticRoutingStaxx->AddHostRouteTo(Ipv4Address("10.1.1.1"), Ipv4Address("10.1.4.1"),1);
          	}


  	}

	//prepare for location file. Uplink file is prepared during application assigning
	std::ofstream fileLocation;
	fileLocation.open(fileNames[1].c_str(),std::ios::out);
	for(unsigned int x=1; x<allNodes.GetN();x++)
	{

		position = allNodes.Get(x)->GetObject<MobilityModel>();
		fileLocation << x << "," << position->GetPosition().x << "," << position->GetPosition().y << "\n";

	}
	fileLocation.flush();
	fileLocation.close();

	std::ofstream fileUpLink;
	fileUpLink.open(fileNames[2].c_str(),std::ios::out);
	for(unsigned int x=0;x<serverNodes.GetN()+apNodes.GetN();x++)
	{
		fileUpLink << "0,1e+07,0.055,55,0\n";
	}

	//install uplink side client-server applications for probings - APP2
	UdpServerHelper serverAPP2Up (11);
	ApplicationContainer serverAPPS2=serverAPP2Up.Install(serverNodes.Get(0));
	serverAPPS2.Start(Seconds(0.0));
	serverAPPS2.Stop(Seconds(30000.0));
	UdpClientHelper clientAPP2Up (p2pInterfacesOne.GetAddress(0),11);

	//BEWARE TO CHANGE THIS LINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!SHOULD BE THE SAME AS INITIAL_PROBE_TRAIN_LENGTH
	clientAPP2Up.SetAttribute ("MaxPackets", UintegerValue (2));
	clientAPP2Up.SetAttribute ("Interval", TimeValue (Seconds(0.0019531)));//(Seconds (0.00000777)));//(Seconds (0.0002)));//(Seconds (0.00002)));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
	clientAPP2Up.SetAttribute ("PacketSize", UintegerValue (1024));
	clientAPP2Up.SetAttribute("QoSLevel",UintegerValue(0));
	ApplicationContainer clientAPPS2;
	for(int i=0; i<MAX_FSD_NODES;i++)
	{
 	 	clientAPPS2 = clientAPP2Up.Install (staNodes.Get (i));
		clientAPPS2.Start (Seconds (2.1));
		clientAPPS2.Stop (Seconds (30000));
	}

	//install uplink side client-server applications
	UdpServerHelper server (9);

	ApplicationContainer serverApps = server.Install (serverNodes.Get (0));
	serverApps.Start (Seconds (0.0));
	serverApps.Stop (Seconds (30000.0));

	UdpClientHelper client (p2pInterfacesOne.GetAddress (0), 9); //Modified A Little Bit Here - Change it into an Echo Client
	client.SetAttribute ("MaxPackets", UintegerValue (MaxPacketsBac));
	client.SetAttribute ("Interval", TimeValue (Seconds (UplinkIntervalPattern[0])));//0.055 - original //0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
	client.SetAttribute ("PacketSize", UintegerValue (UplinkPacketSizePattern[0]));
	client.SetAttribute ("QoSLevel",UintegerValue(QoSLevel[0]));

	ApplicationContainer clientApps = client.Install (staNodes.Get (0));
	clientApps.Start (Seconds ((NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2)*0.1+2+1.0));
	clientApps.Stop (Seconds (30000));

	UdpClientHelper *clientBackground[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
	ApplicationContainer clientAppsBackground[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
	for(k=1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
	{
		int j=0,l=0,sum=0;
		for(l=0;l<GroupAmt;l++)
		{
			if(TrafficPattern[l]+sum>=k && sum<k)
			{
				j=l;
				break;
			}
			else //which means we have already assigned that amount of users
			{
				sum=sum+TrafficPattern[l];
			}

		}

		NS_LOG_UNCOND("User " << k << ": MaxPackets " << MaxPacketsBac << " Uplink Interval " << UplinkIntervalPattern[j] << " Uplink Packet Size " << UplinkPacketSizePattern[j] << " QoS Level " << QoSLevel[j]);

		clientBackground[k] = new UdpClientHelper (p2pInterfacesOne.GetAddress(0), 9);
		clientBackground[k]->SetAttribute("RemoteAddress", AddressValue(Address(p2pInterfacesOne.GetAddress(0))));
		clientBackground[k]->SetAttribute("RemotePort", UintegerValue(9));
		clientBackground[k]->SetAttribute("MaxPackets", UintegerValue(MaxPacketsBac));
		//clientBackground[k]->SetAttribute("Interval", TimeValue(Seconds (traffic_pattern[k]/100)));
		clientBackground[k]->SetAttribute("Interval", TimeValue(Seconds (UplinkIntervalPattern[j])));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
		clientBackground[k]->SetAttribute("PacketSize", UintegerValue(UplinkPacketSizePattern[j]));
		//QoSLevel
		clientBackground[k]->SetAttribute("QoSLevel", UintegerValue(QoSLevel[j]));

		clientAppsBackground[k] = clientBackground[k]->Install(staNodes.Get(k));
		clientAppsBackground[k].Start(Seconds(k*0.1+2));
		clientAppsBackground[k].Stop(Seconds(30000));

		fileUpLink << "" << k << "," << MaxPacketsBac << "," << UplinkIntervalPattern[j] << "," << UplinkPacketSizePattern[j] << "," << QoSLevel[j] << "\n";

	}
	NS_LOG_UNCOND ("Uplink DONE.");
	fileUpLink.flush();
	fileUpLink.close();

	//install downlink side client-server applications
	UdpServerHelper server2 (10);

	ApplicationContainer serverApps2 = server2.Install (staNodes);
	serverApps2.Start (Seconds (0.0));
	serverApps2.Stop (Seconds (30000.0));

	UdpClientHelper client2 (staInterface1.GetAddress (0), 10); //Modified A Little Bit Here - Change it into an Echo Client
	client2.SetAttribute ("MaxPackets", UintegerValue (MaxPacketsBac));
	client2.SetAttribute ("Interval", TimeValue (Seconds (DownlinkIntervalPattern[0])));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
	client2.SetAttribute ("PacketSize", UintegerValue (DownlinkPacketSizePattern[0]));
	client2.SetAttribute ("QoSLevel", UintegerValue (QoSLevel[0]));
	client2.SetAttribute ("FirstGroupOffset",UintegerValue(NODE_AMOUNT_GROUP1));
	client2.SetAttribute ("SecondGroupOffset",UintegerValue(NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2));

	ApplicationContainer clientApps2 = client2.Install (serverNodes.Get (0));
	clientApps2.Start (Seconds ((NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2)*0.1+2+1.0));
	clientApps2.Stop (Seconds (30000));//(NanoSeconds(5504734412));//(Seconds (30000));

	UdpClientHelper *clientBackground2[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
	ApplicationContainer clientAppsBackground2[NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2];
	for(k=1;k<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;k++)
	{
		int j=0,l=0,sum=0;
		for(l=0;l<GroupAmt;l++)
		{
			if(TrafficPattern[l]+sum>=k && sum<k)
			{
				j=l;
				break;
			}
			else //which means we have already assigned that amount of users
			{
				sum=sum+TrafficPattern[l];
			}

		}

		NS_LOG_UNCOND("User " << k << ": MaxPackets " << MaxPacketsBac << " Downlink Interval " << DownlinkIntervalPattern[j] << " Downlink Packet Size " << DownlinkPacketSizePattern[j] << " QoS Level " << QoSLevel[j]);

		clientBackground2[k] = new UdpClientHelper (staInterfaces.GetAddress(k-1), 10);
		clientBackground2[k]->SetAttribute("RemoteAddress", AddressValue(Address(staInterfaces.GetAddress(k-1))));
		clientBackground2[k]->SetAttribute("RemotePort", UintegerValue(10));
		clientBackground2[k]->SetAttribute("MaxPackets", UintegerValue(MaxPacketsBac));
		clientBackground2[k]->SetAttribute("Interval", TimeValue(Seconds (DownlinkIntervalPattern[j])));//0.00004 - Heavy Load //0.007 - Medium Load //0.02 - Low Load
		clientBackground2[k]->SetAttribute("PacketSize", UintegerValue(DownlinkPacketSizePattern[j]));
		clientBackground2[k]->SetAttribute("QoSLevel", UintegerValue(QoSLevel[j]));
		clientBackground2[k]->SetAttribute("UserMatrix",UintegerValue(31));

		clientAppsBackground2[k] = clientBackground2[k]->Install(serverNodes.Get(0));
		clientAppsBackground2[k].Start(Seconds(k*0.1+2));
		clientAppsBackground2[k].Stop(Seconds(30000));

	}

	NS_LOG_UNCOND ("Downlink DONE.");

	//prepare for online CACA activation
	Ptr<UdpClient>test1=DynamicCast<UdpClient>(serverNodes.Get(0)->GetApplication(2));
	test1->SetFileNames(fileNames);
	for(int x=0;x<NODE_AMOUNT_GROUP1+NODE_AMOUNT_GROUP2;x++)
	{

		test1=DynamicCast<UdpClient>(serverNodes.Get(0)->GetApplication(2+x));

		for(int start=7;start<3607;start=start+deltaT)
		{
			test1->ScheduleScanInfoRequest(Seconds (start),0);
		}

	}

	OutputStreamWrapper a = OutputStreamWrapper("routingTable",std::ios_base::in | std::ios_base::app);

	int i=0;
	NodeContainer::Iterator j;
	for (j = allNodes.Begin(); j != allNodes.End(); ++j)
	{
		Ptr<Node> object = *j;
		std::clog << "For node " << i << " its routing table is:\n";
		allNodes.Get(i)->GetObject<Ipv4>()->GetRoutingProtocol()->PrintRoutingTable(&a);
		i++;
	}

Simulator::Stop (Seconds (2407));
	PopulateArpCache (APAssocRelationship);
	PopulateGlobalServerTable ();
	PopulateGlobalAPTable ();

	//pointToPoint.EnablePcapAll ("third");
	//phy.EnablePcap ("dup-AP1-lossless-rat-ap0-", apDevicesOne.Get (0));
	//phy.EnablePcap ("dup-AP1-lossless-rat-ap1-", apDevicesTwo.Get (0));
	//phy.EnablePcap ("dup-AP1-lossless-rat-sta-", staDevices1.Get(0));
	//phy.EnablePcap ("dup-AP1-lossless-rat-sta2-", staDevices2.Get(0));
	//pointToPoint.EnablePcap("dup-AP1-lossless-rat-serverPort1-", p2pDevicesPairOne.Get(0),false);
	//pointToPoint.EnablePcap("dup-AP1-lossless-rat-serverPort2-", p2pDevicesPairTwo.Get(0),false);

	Simulator::Run ();
	Simulator::Destroy ();
	return 0;
}