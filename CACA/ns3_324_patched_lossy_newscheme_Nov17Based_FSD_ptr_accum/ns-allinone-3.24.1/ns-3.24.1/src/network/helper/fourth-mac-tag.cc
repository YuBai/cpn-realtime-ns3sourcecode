/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fourth-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FourthMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FourthMacTag : public Tag
{
public:
	FourthMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFourthMac (void) const;
  void SetFourthMac (uint8_t FourthMac) ;

private:
  uint8_t m_FourthMac; //!< The time stored in the tag
};

FourthMacTag::FourthMacTag ()
  : m_FourthMac (0)
{
}

TypeId
FourthMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FourthMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fourthMacTag")
    .AddConstructor<FourthMacTag> ()
    .AddAttribute ("LastFourthMac",
                   "Last FourthMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FourthMacTag::GetFourthMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FourthMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FourthMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FourthMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FourthMac);
}
void
FourthMacTag::Deserialize (TagBuffer i)
{
  m_FourthMac = i.ReadU8 ();
}
void
FourthMacTag::Print (std::ostream &os) const
{
  os << "m_FourthMac=" << m_FourthMac;
}
uint8_t
FourthMacTag::GetFourthMac (void) const
{
  return m_FourthMac;
}

void
FourthMacTag::SetFourthMac (uint8_t FourthMac)
{
	m_FourthMac=FourthMac;
}

FourthMacTagger::FourthMacTagger ()
{
	this->lastFourthMac=0.0;
}

void
FourthMacTagger::WriteFourthMacValue (Ptr<const Packet> packet)
{
  FourthMacTag tag;
  tag.SetFourthMac(this->lastFourthMac);
  packet->AddByteTag (tag);
}
void
FourthMacTagger::RecordFourthMacValue (Ptr<const Packet> packet)
{
  FourthMacTag tag;
  SetLastFourthMac(0);
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFourthMac=tag.GetFourthMac ();

}

uint8_t
FourthMacTagger::GetLastFourthMac (void) const
{
  return lastFourthMac;
}

void
FourthMacTagger::SetLastFourthMac (uint8_t FourthMac)
{
	lastFourthMac=FourthMac;
}


} // namespace ns3
