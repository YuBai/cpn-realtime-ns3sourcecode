/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "node-id-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Opform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class NodeIDTag : public Tag
{
public:
	NodeIDTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetNodeID (void) const;
  void SetNodeID (double NodeID) ;

private:
  double m_NodeID; //!< The time stored in the tag
};

NodeIDTag::NodeIDTag ()
  : m_NodeID (0.0)
{
}

TypeId
NodeIDTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::NodeIDTag")
    .SetParent<Tag> ()
    .SetGroupName("NodeIDTag")
    .AddConstructor<NodeIDTag> ()
    .AddAttribute ("LastNodeID",
                   "Last NodeID Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&NodeIDTag::GetNodeID),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
NodeIDTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
NodeIDTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
NodeIDTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_NodeID);
}
void
NodeIDTag::Deserialize (TagBuffer i)
{
  m_NodeID = i.ReadDouble ();
}
void
NodeIDTag::Print (std::ostream &os) const
{
  os << "m_NodeID=" << m_NodeID;
}
double
NodeIDTag::GetNodeID (void) const
{
  return m_NodeID;
}

void
NodeIDTag::SetNodeID (double NodeID)
{
	m_NodeID=NodeID;
}

NodeIDTagger::NodeIDTagger ()
{
	this->lastNodeID=0.0;
}

void
NodeIDTagger::WriteNodeIDValue (Ptr<const Packet> packet, double tagValue)
{
  NodeIDTag tag;
  tag.SetNodeID(tagValue);
  packet->AddByteTag (tag);
}
int
NodeIDTagger::RecordNodeIDValue (Ptr<const Packet> packet)
{
  NodeIDTag tag;
  lastNodeID=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastNodeID=tag.GetNodeID ();
  return (int)lastNodeID;
}

double
NodeIDTagger::GetLastNodeID (void) const
{
  return lastNodeID;
}

} // namespace ns3
