/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "modulation-samples-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to ModulationSamplesform Modulation and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ModulationSamplesTag : public Tag
{
public:
	ModulationSamplesTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  void CopyTo (double* _buffer);
  void CopyFrom (double* _buffer) ;

private:
  double m_ModulationSamples[MAX_MODULATION_TRAIN_LENGTH]; //!< The time stored in the tag
};

ModulationSamplesTag::ModulationSamplesTag ()
{
	memset(m_ModulationSamples, 0, sizeof(m_ModulationSamples));
}

TypeId
ModulationSamplesTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ModulationSamplesTag")
    .SetParent<Tag> ()
    .SetGroupName("Network")
    .AddConstructor<ModulationSamplesTag> ()
;
  return tid;
}
TypeId
ModulationSamplesTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ModulationSamplesTag::GetSerializedSize (void) const
{
  //std::clog << "Modulation sample tag size is" << sizeof(double)*MAX_PROBE_TRAIN_LENGTH << "\n";
  return sizeof(double)*(MAX_MODULATION_TRAIN_LENGTH);
}
void
ModulationSamplesTag::Serialize (TagBuffer i) const
{
  int j=0;
  for(j=0;j<MAX_MODULATION_TRAIN_LENGTH;j++)
  {
	  i.WriteDouble(m_ModulationSamples[j]);
  }

}
void
ModulationSamplesTag::Deserialize (TagBuffer i)
{
	int j=0;
	for(j=0;j<MAX_MODULATION_TRAIN_LENGTH;j++)
	{
		m_ModulationSamples[j]=i.ReadDouble();
	}

}
void
ModulationSamplesTag::Print (std::ostream &os) const
{
}

void
ModulationSamplesTag::CopyTo (double* _buffer)
{
	int j=0;
	for(j=0;j<MAX_MODULATION_TRAIN_LENGTH;j++)
	{
		_buffer[j]=m_ModulationSamples[j];
	}

}

void
ModulationSamplesTag::CopyFrom (double* _buffer)
{
	int j=0;
	for(j=0;j<MAX_MODULATION_TRAIN_LENGTH;j++)
	{
		m_ModulationSamples[j]=_buffer[j];
	}

}

ModulationSamplesTagger::ModulationSamplesTagger ()
{
	memset(lastModulationSamples,0,sizeof(lastModulationSamples));
}

void
ModulationSamplesTagger::WriteModulationSamplesValue (Ptr<const Packet> packet, double* _buffer)
{
  ModulationSamplesTag tag;
  //std::clog << "ModulationSamplesTag Created.\n";
  tag.CopyFrom(_buffer);
  //std::clog << "ModulationSamplesTag CopyFrom Called.\n";
  packet->AddByteTag (tag);
  //std::clog << "ModulationSamplesTag Added.\n";
}
void
ModulationSamplesTagger::RecordModulationSamplesValue (Ptr<const Packet> packet, double* _buffer)
{
  ModulationSamplesTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  tag.CopyTo(_buffer);
}

} // namespace ns3
