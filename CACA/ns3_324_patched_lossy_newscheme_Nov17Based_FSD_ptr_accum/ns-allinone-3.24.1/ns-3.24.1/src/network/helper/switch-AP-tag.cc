/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "switch-AP-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Opform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class SwitchAPTag : public Tag
{
public:
	SwitchAPTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetSwitchAP (void) const;
  void SetSwitchAP (double SwitchAP) ;

private:
  double m_SwitchAP; //!< The time stored in the tag
};

SwitchAPTag::SwitchAPTag ()
  : m_SwitchAP (0.0)
{
}

TypeId
SwitchAPTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::SwitchAPTag")
    .SetParent<Tag> ()
    .SetGroupName("SwitchAPTag")
    .AddConstructor<SwitchAPTag> ()
    .AddAttribute ("LastSwitchAP",
                   "Last SwitchAP Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&SwitchAPTag::GetSwitchAP),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
SwitchAPTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
SwitchAPTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
SwitchAPTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_SwitchAP);
}
void
SwitchAPTag::Deserialize (TagBuffer i)
{
  m_SwitchAP = i.ReadDouble ();
}
void
SwitchAPTag::Print (std::ostream &os) const
{
  os << "m_SwitchAP=" << m_SwitchAP;
}
double
SwitchAPTag::GetSwitchAP (void) const
{
  return m_SwitchAP;
}

void
SwitchAPTag::SetSwitchAP (double SwitchAP)
{
	m_SwitchAP=SwitchAP;
}

SwitchAPTagger::SwitchAPTagger ()
{
	this->lastSwitchAP=0.0;
}

void
SwitchAPTagger::WriteSwitchAPValue (Ptr<const Packet> packet, double tagValue)
{
  SwitchAPTag tag;
  tag.SetSwitchAP(tagValue);
  packet->AddByteTag (tag);
}
int
SwitchAPTagger::RecordSwitchAPValue (Ptr<const Packet> packet)
{
  SwitchAPTag tag;
  lastSwitchAP=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastSwitchAP=tag.GetSwitchAP ();
  return (int)lastSwitchAP;
}

double
SwitchAPTagger::GetLastSwitchAP (void) const
{
  return lastSwitchAP;
}

} // namespace ns3
