/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "decision-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Chosenform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class DecisionTag : public Tag
{
public:
	DecisionTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetDecision (void) const;
  void SetDecision (double Decision) ;

private:
  double m_Decision; //!< The time stored in the tag
};

DecisionTag::DecisionTag ()
  : m_Decision (0.0)
{
}

TypeId
DecisionTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::DecisionTag")
    .SetParent<Tag> ()
    .SetGroupName("DecisionTag")
    .AddConstructor<DecisionTag> ()
    .AddAttribute ("LastDecision",
                   "Last Decision Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&DecisionTag::GetDecision),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
DecisionTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
DecisionTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
DecisionTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Decision);
}
void
DecisionTag::Deserialize (TagBuffer i)
{
  m_Decision = i.ReadDouble ();
}
void
DecisionTag::Print (std::ostream &os) const
{
  os << "m_Decision=" << m_Decision;
}
double
DecisionTag::GetDecision (void) const
{
  return m_Decision;
}

void
DecisionTag::SetDecision (double Decision)
{
	m_Decision=Decision;
}

DecisionTagger::DecisionTagger ()
{
	this->lastDecision=0.0;
}

void
DecisionTagger::WriteDecisionValue (Ptr<const Packet> packet, double tagValue)
{
  DecisionTag tag;
  tag.SetDecision(tagValue);
  packet->AddByteTag (tag);
}
int
DecisionTagger::RecordDecisionValue (Ptr<const Packet> packet)
{
  DecisionTag tag;
  lastDecision=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastDecision=tag.GetDecision ();
  return (int)lastDecision;
}

double
DecisionTagger::GetLastDecision (void) const
{
  return lastDecision;
}

} // namespace ns3
