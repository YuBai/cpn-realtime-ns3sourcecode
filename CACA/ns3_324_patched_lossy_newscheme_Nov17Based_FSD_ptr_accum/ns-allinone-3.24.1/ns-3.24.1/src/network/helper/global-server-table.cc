#include "global-server-table.h"

#define DEFAULT_GST_SIZE 1000

namespace ns3{

globalServerTable::globalServerTable()
{
	m_gst.resize(DEFAULT_GST_SIZE);
	initServerTable(DEFAULT_GST_SIZE);

}

globalServerTable::globalServerTable(int size)
{
	m_gst.resize(size);
	initServerTable(size);
}

void
globalServerTable::reSize(int size)
{
	m_gst.resize(size);
	initServerTable(size);
}


void
globalServerTable::setCurrentLoad(int _nodeID, double* _currentLoad)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::setCurrentCopyState(): _nodeID out of range!");
	}

	for(int x=0;x<MAX_MODULATION_TRAIN_LENGTH;x++)
	{
		m_gst[_nodeID].currentLoad[x]=_currentLoad[x];
	}
	NS_LOG_UNCOND("m_gst[" << _nodeID << "].currentLoad[0]:" << _currentLoad[0] << ", [1]: " << _currentLoad[1]);
	

}

double*
globalServerTable::getCurrentLoad(int _nodeID)
{
	if((uint32_t)_nodeID > m_gst.size())
	{
		NS_FATAL_ERROR("globalClientTable::getCurrentCopyState(): _nodeID out of range!");
	}
	return m_gst[_nodeID].currentLoad;

}

////need to be fixed
//int
//globalServerTable::getCurrentAP(int _nodeID)
//{
//	return 1;
//}

void
globalServerTable::initServerTable(int size)
{
	for(int i=0;i<size;i++)
	{
		for(int j=0; j<MAX_MODULATION_TRAIN_LENGTH;j++)
		{
			m_gst[i].currentLoad[j]=0;
		}
	}
}


}


