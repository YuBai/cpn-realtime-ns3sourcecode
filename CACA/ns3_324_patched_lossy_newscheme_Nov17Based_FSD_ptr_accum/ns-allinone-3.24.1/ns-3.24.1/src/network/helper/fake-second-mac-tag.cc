/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "fake-second-mac-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

namespace ns3 {

/**
 * Tag to FakeSecondMacform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class FakeSecondMacTag : public Tag
{
public:
	FakeSecondMacTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  uint8_t GetFakeSecondMac (void) const;
  void SetFakeSecondMac (uint8_t FakeSecondMac) ;

private:
  uint8_t m_FakeSecondMac; //!< The time stored in the tag
};

FakeSecondMacTag::FakeSecondMacTag ()
  : m_FakeSecondMac (0)
{
}

TypeId
FakeSecondMacTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::FakeSecondMacTag")
    .SetParent<Tag> ()
    .SetGroupName("fakeSecondMac")
    .AddConstructor<FakeSecondMacTag> ()
    .AddAttribute ("LastFakeSecondMac",
                   "Last FakeSecondMac Value Obtained",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FakeSecondMacTag::GetFakeSecondMac),
                   MakeUintegerChecker<uint8_t> ());
  return tid;
}
TypeId
FakeSecondMacTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
FakeSecondMacTag::GetSerializedSize (void) const
{
  return sizeof(uint8_t);
}
void
FakeSecondMacTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_FakeSecondMac);
}
void
FakeSecondMacTag::Deserialize (TagBuffer i)
{
  m_FakeSecondMac = i.ReadU8 ();
}
void
FakeSecondMacTag::Print (std::ostream &os) const
{
  os << "m_FakeSecondMac=" << m_FakeSecondMac;
}
uint8_t
FakeSecondMacTag::GetFakeSecondMac (void) const
{
  return m_FakeSecondMac;
}

void
FakeSecondMacTag::SetFakeSecondMac (uint8_t FakeSecondMac)
{
	m_FakeSecondMac=FakeSecondMac;
}

FakeSecondMacTagger::FakeSecondMacTagger ()
{
	this->lastFakeSecondMac=0.0;
}

void
FakeSecondMacTagger::WriteFakeSecondMacValue (Ptr<const Packet> packet)
{
  FakeSecondMacTag tag;
  tag.SetFakeSecondMac(this->lastFakeSecondMac);
  packet->AddByteTag (tag);
}
void
FakeSecondMacTagger::RecordFakeSecondMacValue (Ptr<const Packet> packet)
{
  FakeSecondMacTag tag;
  lastFakeSecondMac=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastFakeSecondMac=tag.GetFakeSecondMac ();

}

uint8_t
FakeSecondMacTagger::GetLastFakeSecondMac (void) const
{
  return lastFakeSecondMac;
}

void
FakeSecondMacTagger::SetLastFakeSecondMac (uint8_t FakeSecondMac)
{
	lastFakeSecondMac=FakeSecondMac;
}


} // namespace ns3
