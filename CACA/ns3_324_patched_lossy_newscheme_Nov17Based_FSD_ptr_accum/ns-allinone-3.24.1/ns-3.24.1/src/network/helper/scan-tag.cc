/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "scan-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Scanform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ScanTag : public Tag
{
public:
	ScanTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetScan (void) const;
  void SetScan (double Scan) ;

private:
  double m_Scan; //!< The time stored in the tag
};

ScanTag::ScanTag ()
  : m_Scan (0.0)
{
}

TypeId
ScanTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ScanTag")
    .SetParent<Tag> ()
    .SetGroupName("ScanTag")
    .AddConstructor<ScanTag> ()
    .AddAttribute ("LastScan",
                   "Last Scan Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ScanTag::GetScan),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ScanTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ScanTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ScanTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Scan);
}
void
ScanTag::Deserialize (TagBuffer i)
{
  m_Scan = i.ReadDouble ();
}
void
ScanTag::Print (std::ostream &os) const
{
  os << "m_Scan=" << m_Scan;
}
double
ScanTag::GetScan (void) const
{
  return m_Scan;
}

void
ScanTag::SetScan (double Scan)
{
	m_Scan=Scan;
}

ScanTagger::ScanTagger ()
{
	this->lastScan=0.0;
}

void
ScanTagger::WriteScanValue (Ptr<const Packet> packet, double tagValue)
{
  ScanTag tag;
  tag.SetScan(tagValue);
  packet->AddByteTag (tag);
}
int
ScanTagger::RecordScanValue (Ptr<const Packet> packet)
{
  ScanTag tag;
  lastScan=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastScan=tag.GetScan ();
  return (int)lastScan;
}

double
ScanTagger::GetLastScan (void) const
{
  return lastScan;
}

} // namespace ns3
