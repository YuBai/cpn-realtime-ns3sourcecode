/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "my-snr-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to MySnrform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class MySnrTag : public Tag
{
public:
	MySnrTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetMySnr (void) const;
  void SetMySnr (double MySnr) ;

private:
  double m_MySnr; //!< The time stored in the tag
};

MySnrTag::MySnrTag ()
  : m_MySnr (0.0)
{
}

TypeId
MySnrTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::MySnrTag")
    .SetParent<Tag> ()
    .SetGroupName("MySNR")
    .AddConstructor<MySnrTag> ()
    .AddAttribute ("LastMySnr",
                   "Last MySnr Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&MySnrTag::GetMySnr),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
MySnrTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
MySnrTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
MySnrTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_MySnr);
}
void
MySnrTag::Deserialize (TagBuffer i)
{
  m_MySnr = i.ReadDouble ();
}
void
MySnrTag::Print (std::ostream &os) const
{
  os << "m_MySnr=" << m_MySnr;
}
double
MySnrTag::GetMySnr (void) const
{
  return m_MySnr;
}

void
MySnrTag::SetMySnr (double MySnr)
{
	m_MySnr=MySnr;
}

MySnrTagger::MySnrTagger (double MySnr)
{
	this->lastMySnr=MySnr;
}

void
MySnrTagger::WriteMySnrValue (Ptr<const Packet> packet)
{
  MySnrTag tag;
  tag.SetMySnr(this->lastMySnr);
  packet->AddByteTag (tag);
}
void
MySnrTagger::RecordMySnrValue (Ptr<const Packet> packet)
{
  MySnrTag tag;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return;
    }
  lastMySnr=tag.GetMySnr ();

}

double
MySnrTagger::GetLastMySnr (void) const
{
  return lastMySnr;
}

} // namespace ns3
