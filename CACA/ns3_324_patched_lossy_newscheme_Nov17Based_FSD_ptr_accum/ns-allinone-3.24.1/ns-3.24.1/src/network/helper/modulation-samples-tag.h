/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef MODULATION_SAMPLES_TAG_H
#define MODULATION_SAMPLES_TAG_H

#define MAX_MODULATION_TRAIN_LENGTH 2 //Maximum APs Supported: length-1 - should be exactly same as one defined in FSD-ServerSide //16000+: error, 4095+: cannot get tag after it, 5119+: unknown

#include "ns3/nstime.h"
#include "ns3/packet.h"

namespace ns3 {

/**
 * \ingroup stats
 *
 * \brief quick and dirty delay and jitter estimation
 *
 */
class ModulationSamplesTagger
{
public:
	ModulationSamplesTagger ();

  /**
   * \param packet the packet to send over a wire
   *
   * This method should be invoked once on each packet to
   * record within the packet the tx time which is used upon
   * packet reception to calculate the Modulation and jitter. The
   * tx time is stored in the packet as an ns3::Tag which means
   * that it does not use any network resources and is not
   * taken into account in transmission Modulation calculations.
   */
  void WriteModulationSamplesValue (Ptr<const Packet> packet, double* _buffer);
  /**
   * \param packet the packet received
   *
   * Invoke this method to update the Modulation and jitter calculations
   * After a call to this method, \ref GetLastModulation and \ref GetLastJitter
   * will return an updated Modulation and jitter.
   */
  void RecordModulationSamplesValue (Ptr<const Packet> packet, double* _buffer);


//private:

  double lastModulationSamples[MAX_MODULATION_TRAIN_LENGTH]; //last ber

};

} // namespace ns3

#endif /* Modulation_JITTER_ESTIMATION_H */
