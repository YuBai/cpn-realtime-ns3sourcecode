/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "modulation-tag.h"
#include "ns3/tag.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/double.h"

namespace ns3 {

/**
 * Tag to Modulationform Delay and Jitter estimations
 *
 * The tag holds the packet's creation timestamp
 */
class ModulationTag : public Tag
{
public:
	ModulationTag ();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;

  /**
   * \brief Get the Transmission time stored in the tag
   * \return the transmission time
   */
  double GetModulation (void) const;
  void SetModulation (double Modulation) ;

private:
  double m_Modulation; //!< The time stored in the tag
};

ModulationTag::ModulationTag ()
  : m_Modulation (0.0)
{
}

TypeId
ModulationTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("anon::ModulationTag")
    .SetParent<Tag> ()
    .SetGroupName("ModulationTag")
    .AddConstructor<ModulationTag> ()
    .AddAttribute ("LastModulation",
                   "Last Modulation Value Obtained",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&ModulationTag::GetModulation),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}
TypeId
ModulationTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
ModulationTag::GetSerializedSize (void) const
{
  return sizeof(double);
}
void
ModulationTag::Serialize (TagBuffer i) const
{
  i.WriteDouble (m_Modulation);
}
void
ModulationTag::Deserialize (TagBuffer i)
{
  m_Modulation = i.ReadDouble ();
}
void
ModulationTag::Print (std::ostream &os) const
{
  os << "m_Modulation=" << m_Modulation;
}
double
ModulationTag::GetModulation (void) const
{
  return m_Modulation;
}

void
ModulationTag::SetModulation (double Modulation)
{
	m_Modulation=Modulation;
}

ModulationTagger::ModulationTagger ()
{
	this->lastModulation=0.0;
}

void
ModulationTagger::WriteModulationValue (Ptr<const Packet> packet, double tagValue)
{
  ModulationTag tag;
  tag.SetModulation(tagValue);
  packet->AddByteTag (tag);
}
int
ModulationTagger::RecordModulationValue (Ptr<const Packet> packet)
{
  ModulationTag tag;
  lastModulation=0;
  bool found;
  found = packet->FindFirstMatchingByteTag (tag);
  if (!found)
    {
      return 0;
    }
  lastModulation=tag.GetModulation ();
  return (int)lastModulation;
}

double
ModulationTagger::GetLastModulation (void) const
{
  return lastModulation;
}

} // namespace ns3
