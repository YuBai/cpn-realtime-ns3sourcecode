#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/mac48-address.h"
#include "ns3/ipv4-address.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/modulation-samples-tag.h"

#ifndef GLOBAL_SERVER_TABLE
#define GLOBAL_SERVER_TABLE 1

namespace ns3{

	class globalServerTable{

		public:

			typedef struct{

				double currentLoad[MAX_MODULATION_TRAIN_LENGTH];

			}ServerTable;

			globalServerTable();
			globalServerTable(int size);
			void fillEntries(int _nodeID, double *currentLoad);
			void reSize(int size);
			//void copyAnotherGST(globalServerTable _m_gst);

			void setCurrentLoad(int _nodeID, double *currentLoad);
			double* getCurrentLoad(int _nodeID);
			//int getCurrentAP(int _nodeID);

		private:

			void initServerTable(int size);
			std::vector<ServerTable> m_gst;

	};


}

#endif
