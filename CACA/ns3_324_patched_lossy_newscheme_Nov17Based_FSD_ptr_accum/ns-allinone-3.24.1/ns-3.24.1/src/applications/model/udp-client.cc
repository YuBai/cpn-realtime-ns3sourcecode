/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "udp-client.h"
#include "seq-ts-header.h"
#include <cstdlib>
#include <cstdio>
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/delay-samples-tag.h"
#include "ns3/stop-traffic-tag.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("UdpClient");

NS_OBJECT_ENSURE_REGISTERED (UdpClient);

TypeId
UdpClient::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::UdpClient")
    	.SetParent<Application> ()
	.SetGroupName("Applications")
	.AddConstructor<UdpClient> ()
	.AddAttribute ("MaxPackets",
		"The maximum number of packets the application will send",
		UintegerValue (100),
		MakeUintegerAccessor (&UdpClient::m_count),
		MakeUintegerChecker<uint32_t> ())
	.AddAttribute ("Interval",
		"The time to wait between packets", TimeValue (Seconds (1.0)),
		MakeTimeAccessor (&UdpClient::m_interval),
		MakeTimeChecker ())
	.AddAttribute ("RemoteAddress",
		"The destination Address of the outbound packets",
		AddressValue (),
		MakeAddressAccessor (&UdpClient::m_peerAddress),
		MakeAddressChecker ())
	.AddAttribute ("RemotePort", "The destination port of the outbound packets",
		UintegerValue (100),
		MakeUintegerAccessor (&UdpClient::m_peerPort),
		MakeUintegerChecker<uint16_t> ())
	.AddAttribute ("PacketSize",
		"Size of packets generated. The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.",
		UintegerValue (1024),
		MakeUintegerAccessor (&UdpClient::m_size),
		MakeUintegerChecker<uint32_t> (12,1500))
	.AddAttribute ("QoSLevel",
		"Level of QoS which follows 802.11D standard (priority: lowest->highest): 1 - background, 0 - best effort (default), 3 - excellent effort, 4 -  controlled load, 5 - videos, 6 - voices, and 7 - network control",
		UintegerValue (3),
		MakeUintegerAccessor (&UdpClient::m_tid),
		MakeUintegerChecker<uint8_t> (0,7))
	.AddAttribute ("Index",
		"Index to be passed down to MAC layer for switching to different AP",
		UintegerValue (0),
		MakeUintegerAccessor (&UdpClient::m_Index),
		MakeUintegerChecker<uint8_t> (0,100)) //assuming at most 101 APs are in the network
	.AddAttribute ("UserMatrix",
		"amount of users in the network for potential migration",
		UintegerValue (16),
		MakeUintegerAccessor (&UdpClient::m_flagMatrixLen),
		MakeUintegerChecker<uint8_t> (0,10000)) //assuming at most 100001 users are in the network
	.AddAttribute ("FirstGroupOffset",
		"First Group Offset",
		UintegerValue (10),
		MakeUintegerAccessor (&UdpClient::m_1st),
		MakeUintegerChecker<uint8_t> (0,100)) //assuming at most 100001 users are in the network
	.AddAttribute ("SecondGroupOffset",
		"Second Group Offset",
		UintegerValue (10),
		MakeUintegerAccessor (&UdpClient::m_2nd),
		MakeUintegerChecker<uint8_t> (0,100)) //assuming at most 100001 users are in the network
	.AddAttribute ("MigrationCost",
		"Migration Cost in the 10 fold",
		UintegerValue (10),
		MakeUintegerAccessor (&UdpClient::m_migrationCost),
		MakeUintegerChecker<uint8_t> (0,100)) //assuming at most 100001 users are in the network
	;
	return tid;
}

UdpClient::UdpClient ()
{
	NS_LOG_FUNCTION (this);
	m_sent = 0;
	m_socket = 0;
	m_sendEvent = EventId ();
	m_rebootInterval = Seconds (1);
	cancelEvent = 0;
	m_ScanTagValue = 0;
	m_scanRequestGlobalState=0;
	m_firstOptimizationOrNot=1;
	m_OptimizeOrNot=0;
	//flagM(m_flagMatrixLen);

}

UdpClient::~UdpClient ()
{
	NS_LOG_FUNCTION (this);
}

void
UdpClient::SetRemote (Ipv4Address ip, uint16_t port)
{
	NS_LOG_FUNCTION (this << ip << port);
	m_peerAddress = Address(ip);
	m_peerPort = port;
}

void
UdpClient::SetRemote (Ipv6Address ip, uint16_t port)
{
	NS_LOG_FUNCTION (this << ip << port);
	m_peerAddress = Address(ip);
	m_peerPort = port;
}

void
UdpClient::SetRemote (Address ip, uint16_t port)
{
	NS_LOG_FUNCTION (this << ip << port);
	m_peerAddress = ip;
	m_peerPort = port;
}

/*int
UdpClient::GetQoSLevel()
{
	return m_tid;
}*/

void
UdpClient::ScheduleSetMatrix(Time s)
{
	m_SetMatrixValueEvent=Simulator::Schedule(s,&UdpClient::DoScheduleSetMatrix,this);
}


void
UdpClient::DoScheduleSetMatrix(void)
{

	//flagM.
	for(uint32_t i=0;i<flagM.size();i++)
	{
		flagM[i]=0;
	}
	NS_LOG_UNCOND_YB("Here: size of flagM: " << flagM.size() << ", size of offset: " << offset.size());
	for(uint32_t i=0;i<offset.size();i++)
	{
		if(offset[i] != 0) //since we do not schedule re-assoc on server node
		{
			flagM[offset[i]]=-1; //-1: this needs to be scheduled; 1: scan request packet is sent; 2: re-assoc packet is sent
			NS_LOG_UNCOND("" << Simulator::Now() << ": node " << offset[i] << " needs to be scheduled AP change.");
		}
	}
	for(uint32_t i=0;i<offset.size();i++)
	{
		NS_LOG_UNCOND_YB("" << Simulator::Now() << ": flagM[" << i << "]: " << flagM[i]);
	}

}

globalServerTable&
UdpClient::GetGST(void)
{
	return m_gst;
}

std::vector<int>
UdpClient::GetMatrix(void)
{
	return flagM;
}

bool
UdpClient::CheckManagerOrNot(void)
{
	Ptr<Application>testt1;
	Ptr<UdpClient>test1;
	testt1=GetNode()->GetApplication(2); //since in current script first udp-client application is the 2+user-Amount one

	return testt1==this;

}

void
UdpClient::ScheduleSetOffsetMatrixValue(Time s, int index, int value)
{
	m_SetOffsetMatrixValueEvent=Simulator::Schedule(s, &UdpClient::DoSetOffsetMatrixValue,this,index,value);
}

void
UdpClient::DoSetOffsetMatrixValue(int index, int value)
{
	NS_LOG_UNCOND_YB("HERE..");
	offset[index]=value;
	NS_LOG_UNCOND_YB("HERE...");
}

void
UdpClient::SetMatrixValue(int index, int value)
{
	flagM[index]=value;
	NS_LOG_UNCOND_YB("" << Simulator::Now() << "Enter into Set Matrix Value: I am " << GetNode()->GetId() << " with index: " << index << " and value: " << value << "and flagM[index]=" << flagM[index] << " and flagM.size=" << flagM.size());
	CheckGlobalState();
}

void
UdpClient::SetLoadInfo(int index, double* value)
{
	//fagM[index]=value;
	m_gst.setCurrentLoad(index,value);
}

void
UdpClient::RunOptimizationAlgorithm()
{
	if(m_OptimizeOrNot == 1)
	{
		NS_LOG_UNCOND("" << Simulator::Now() << ": The Optimization Algorithm is Already Called this Time.");
		return;
	}
	NS_LOG_UNCOND("" << Simulator::Now() << ": I am Node " << GetNode()->GetId() << " and m_fileNames[0] is " << m_fileNames[0]);

	//assuming m_fileNames[3]: "./ApplicationName_testS", m_fileNames[1]: saved results
	std::ofstream fileLoad;
	fileLoad.open(m_fileNames[3].data(),std::ios::out);

	for(uint32_t x=3;x<flagM.size();x++)
	{
		double* tmp=m_gst.getCurrentLoad(x);

		/*if(tmp[0] == 0)
		{
			tmp[0]=1000;
		}
		if(tmp[1] == 0)
		{
			tmp[1]=1000;
		}*/
		
		//fileLoad << "" << x << "," << *(m_gst.getCurrentLoad(x)+((m_gst.getCurrentAP(x)+1)%2)) << "," << *(m_gst.getCurrentLoad(x)+(m_gst.getCurrentAP(x)%2)) << "\n";
		NS_LOG_UNCOND("" << Simulator::Now() << ": Writing Data: " << x << "," << tmp[0] << "," << tmp[1] << "\n");
		fileLoad << "" << tmp[0] << "," << tmp[1] << "\n";

	}
	fileLoad.flush();
	fileLoad.close();

	//assuming int* scheduleInfo=DataPreProcessing(appName,firstGroupOffset,secondGroupOffset,migration cost)
	//Matlab script should be able to calculate and return optimized results, update corresponding files and store results into some file
	std::stringstream command;
	//command << "matlab -r -nodisplay -nojvm DataPreProcessing(" << m_fileNames[0] << "," << m_1st << "," << m_2nd << "," << m_migrationCost/100  << "); catch; end; quit";

	//DataPreProcessing('/home/cpnlab/Downloads/testOnSuperComputer/March15_OnlineCACA/236m_cum_18980000_optimized_noBackgroundTraffic_periodicTriggering_QoSSupported_handlePktLoss_InfoComComp_AllNodeVersion/ns3_324_patched_lossy_newscheme_Nov17Based_FSD_ptr_accum/ns-allinone-3.24.1/ns-3.24.1/build/scratch/test', 21, 31, 0.1)
	command << "matlab -nodisplay -nojvm -r " << '"' << "DataPreProcessing(" << "'" << m_fileNames[0] << "'," << m_1st << "," << m_2nd << "," << 0.1 << "," << m_firstOptimizationOrNot << ");" << '"';
	//command <<  "matlab -nodisplay -nojvm -r " << '"' << "1+1;quit;" << '"' << '\n';
	//command << "matlab -nodisplay -nojvm -r " << '"' << "/home/cpnlab/Downloads/testOnSuperComputer/March15_OnlineCACA/236m_cum_18980000_optimized_noBackgroundTraffic_periodicTriggering_QoSSupported_handlePktLoss_InfoComComp_AllNodeVersion/ns3_324_patched_lossy_newscheme_Nov17Based_FSD_ptr_accum/ns-allinone-3.24.1/ns-3.24.1/build/scratch/DataPreProcessing " << m_fileNames[0] << " " << m_1st << " " << m_2nd << " " << 0.1;// << ";quit;";//<< '"' << "; catch; end; quit;";
	
	//command.flush();

	std::string tmp;
	getline(command,tmp);
	int xx=system(tmp.c_str());
	//int xx=system("matlab -nodisplay -nojvm -r helloworld");
	NS_LOG_UNCOND("" << Simulator::Now() << ": command is " << tmp << " and return value is " << xx);

	if(xx == -1)
	{
		std::perror("The error message is: ");
		
	}

	//solution is in the form of {NodeID,TargetAP}
	std::ifstream fileSolution;
	fileSolution.open(m_fileNames[4].c_str(),std::ios::in);
	NS_LOG_UNCOND("" << Simulator::Now() << ": m_fileNames[1] is " << m_fileNames[4].c_str());
	std::string value;

	while(getline(fileSolution,value))
	{

		if(value.find("No") != std::string::npos)
		{

			NS_LOG_UNCOND("" << Simulator::Now() << ": No optimization is found. Wait for time out.");
			for(uint32_t i=3;i<flagM.size();i++)
			{
				if(flagM[i] != 0 && decisionM[i] != 0)
				{
					flagM[i]=0;
					decisionM[i]=0;
				}
			}

		}

		else
		{
			int offset=0;
			for(unsigned int x=0;x<value.length();x++)
			{
				if(value[x] == ',')
				{
					offset=x;
					break;
				}

			}

			//put results into decisionM
			int nodeID=std::atoi(std::string(value,0,offset).c_str())-1;
			int decision=std::atoi(std::string(value,offset+1,value.length()).c_str());
			decisionM[nodeID+3]=decision;
			NS_LOG_UNCOND("" << Simulator::Now() << ": nodeID: " << nodeID << ", Decision: " << decision << ", offset: " << offset);
		}
	}

	m_firstOptimizationOrNot=m_firstOptimizationOrNot+1;
	SendReAssocRequest();
	m_OptimizeOrNot = 1;

}


//void
//UdpClient::RunOptimizationAlgorithm()
//{
//
//	const char* tmp="Tmp";
//
//	//read A, C, S, X files - beware about AP1 AP2 and AP0 AP1
//	std::ifstream fileA (m_fileNames[0]);
//	std::ifstream fileC (m_fileNames[1]);
//	std::ifstream fileS (m_fileNames[2]);
//	std::ifstream fileX (m_fileNames[3]);
//
//	char* m_fileNames0=m_fileNames[0];
//	char* m_fileNames1=m_fileNames[1];
//	char* m_fileNames2=m_fileNames[2];
//	char* m_fileNames3=m_fileNames[3];
//
//	std::ofstream fileAP (strcat(m_fileNames0,tmp));
//	std::ofstream fileCP (strcat(m_fileNames1,tmp));
//	std::ofstream fileSP (strcat(m_fileNames2,tmp));
//	std::ofstream fileXP (strcat(m_fileNames3,tmp));
//
//	//update load information - fileA
//	std::string value;
//	int lineNum=0;
//	while (fileA.good)
//	{
//		getline(fileA,value);
//		int counter=0;
//		int offset1=0,offset2=0,offset3=0;
//
//		//calculate total load first
//
//
//		for(int x=0;x<value.size();x++)
//		{
//
//		}
//	}
//
//
//	//update load information - fileC
//	lineNum=0;
//	while (fileC.good)
//	{
//
//		getline(fileC,value);
//
//		int counter=0;
//		int offset1=0,offset2=0,offset3=0;
//		for(int x=0;x<value.size();x++)
//		{
//			//get first offset for coordinates
//			if(value[x]==',' && counter == 2)
//			{
//				offset1=x-1;
//			}
//			else if(value[x]==',' && counter == 3)
//			{
//				offset2=x-1;
//			}
//			else if(value[x]==',' && counter == 4)
//			{
//				offset3=x-1;
//			}
//			else if(value[x] == ',')
//			{
//				counter++;
//			}
//
//		}
//		//stupid way
//		fileCP << std::string(value,0,offset1) << "," << *(m_gst.getCurrentLoad(lineNum)+((m_gst.getCurrentAP(lineNum)-1)%2)) << "," << std::string(value,offset2,offset3) << "," << *(m_gst.getCurrentLoad(lineNum)+(m_gst.getCurrentAP(lineNum)%2) << "," << m_migrationCost/10 << "\n");
//		lineNum++;
//	}
//
//
//	//write information back
//
//	//run matlab code
//
//	//read output results (in the form of decisionM) - beware about AP1 AP2 and AP0 AP1
//}


void
UdpClient::SendReAssocRequest()
{
	Ptr<Application>testt1;
	Ptr<UdpClient>test1;

	for(uint32_t i=3;i<flagM.size();i++)
	{
		if(flagM[i] != 0 && decisionM[i] != 0)
		{
			//Need to handle with later
			NS_LOG_UNCOND("" << Simulator::Now() << ": I am Node " << GetNode()->GetId() << " and application is " << i-1 << " and decision is " << decisionM[i]);
			testt1=GetNode()->GetApplication(i-1);
			test1=DynamicCast<UdpClient>(testt1);
			test1->SetScanTagValue(3);
			test1->SetReassocDecision(decisionM[i]);
			test1->ScheduleTargetIPSwitch(Seconds(1));
			//test1->DoScheduleTargetIPSwitching(decisionM[i]);
			flagM[i]=0;
			decisionM[i]=0;
		}
	}

}

void
UdpClient::SetFileNames(std::vector<std::string> fileNames)
{

	m_fileNames=fileNames;

}


void
UdpClient::SetScanTagValue(int _scanTagValue)
{
	m_ScanTagValue=_scanTagValue;//reset scan tag
	NS_LOG_UNCOND_YB("" << Simulator::Now() << ": this is node " << GetNode()->GetId() << " and my address is " << Ipv4Address::ConvertFrom(m_peerAddress) << " and peer port is " << m_peerPort << " and m_ScanTagValue now is " << m_ScanTagValue);

}

void
UdpClient::SetReassocDecision(int _reassocDecision)
{
	m_ReassocDecision=_reassocDecision;//set ReAssocDecision
	m_ScanIndex=_reassocDecision;
        Simulator::Cancel (m_sendEvent);
        m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
}

//beware of 3 and 4 here!!!!!!!!!!!!!!!
void
UdpClient::CheckGlobalState()
{
	//int i=0;
	for(uint32_t i=0;i<offset.size();i++)
	{
		NS_LOG_UNCOND("" << Simulator::Now() << ": CheckGlobalState[" << i << "]: " << flagM[i]);
	}


	NS_LOG_UNCOND_YB("Enter into Check Global State.");
	for(uint32_t i=3;i<flagM.size();i++)
	{
		if(flagM[i] == 0)
		{
			NS_LOG_UNCOND("entered here: " << flagM[i] << " and i is " << i << " - return");
			return;
		}
	}

//	NS_LOG_UNCOND_YB("m_scanRequestGlobalStateTmp has value " << m_scanRequestGlobalStateTmp);
	for(uint32_t i=4;i<flagM.size();i++)
	{
//		if(flagM[i] != 0 && m_scanRequestGlobalStateTmp != flagM[i]) //meaning this one is not finished yet
//		{
//			NS_LOG_UNCOND("" << Simulator::Now() << ": node " << i << " is not ready for Tmp state " << m_scanRequestGlobalStateTmp << " so we have to wait");
//			return;
//		}
		if(flagM[i] != flagM[3] && flagM[i] != 0 && flagM[3] != 0)
		{
			NS_FATAL_ERROR("" << Simulator::Now() << ": Inconsistent flag values detected! Something Must Goes Wrong!");
		}
	}
	m_scanRequestGlobalState=flagM[3];

	NS_LOG_UNCOND ("" << Simulator::Now() << ": m_scanRequestGlobalState is " << m_scanRequestGlobalState);

	switch(m_scanRequestGlobalState)
	{
	case 1:

		NS_LOG_UNCOND("" << Simulator::Now() << ": enter into state 1 but this should be impossible.");
		return;
	case 2:

		NS_LOG_UNCOND("" << Simulator::Now() << ": enter into state 2 so we need to send requests to corresponding nodes for re-assoc information.");
		//put "send re-assoc pkts from each application" function here
		//RunOptimizationAlgorithm();
		Simulator::Schedule(Seconds(3.0), &UdpClient::RunOptimizationAlgorithm, this);
		//Simulator::Schedule(Seconds(3.0), &UdpClient::SendReAssocRequest, this);
		return;

	case 3:

		NS_LOG_UNCOND("" << Simulator::Now() << ": enter into state 3 so we need to let those applications change their target IP.");
		//put "request corresponding applications to switch their IP" function here
		m_scanRequestGlobalState=0;
		return;

	default:

		NS_LOG_UNCOND("Default case: something goes wrong here.");
		return;

	}




}


void
UdpClient::DoDispose (void)
{
	NS_LOG_FUNCTION (this);
	Application::DoDispose ();
}

void
UdpClient::StartApplication (void)
{
	NS_LOG_FUNCTION (this);
	NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": START APPLICATION WITH PORT NUMBER " << m_peerPort);

	if(GetNode()->GetId() == 0 && CheckManagerOrNot())
	{
		NS_LOG_UNCOND("" << Simulator::Now() << " this application is detected as manager since it has m_peerAddress " << Ipv4Address::ConvertFrom(m_peerAddress) << " and peer port " << m_peerPort);
	}

	flagM.resize(m_flagMatrixLen+3); //+3: +3: first three nodes
	decisionM.resize(m_flagMatrixLen+3);

	cancelEvent = 0;
	int retVal;

	if (m_socket == 0)
	{
		TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
		m_socket = Socket::CreateSocket (GetNode (), tid);
		if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
		{
			retVal = m_socket->Bind ();
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_socket->Bind() returns " << retVal);

			retVal = m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_socket->Connect() returns " << retVal);

		}
		else if (Ipv6Address::IsMatchingType(m_peerAddress) == true)
		{
			m_socket->Bind6 ();
			m_socket->Connect (Inet6SocketAddress (Ipv6Address::ConvertFrom(m_peerAddress), m_peerPort));
		}
	}

	//Modified by Y.B.
	//m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ()); //Original Version


	m_socket->SetRecvCallback (MakeCallback (&UdpClient::HandleRead, this)); //Modified Version
	NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": START APPLICATION FINISHED");
	m_qt.SetTid(m_tid);

	m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);

}


void
//UdpClient::ScheduleAPSwitch(Time s, int index)
UdpClient::ScheduleAPSwitch(Time s)
{
	NS_LOG_UNCOND("" << Simulator::Now() << ": This is node " << GetNode()->GetId() << " and I have port number " << m_peerPort << " and I have scheduled scheduleAPswitch with scan index " << m_ScanIndex);
	m_SwitchAPEvent = Simulator::Schedule(s, &UdpClient::DoScheduleAPSwitching, this, m_ScanIndex);
}

void
UdpClient::DoScheduleAPSwitching(int index)
{
	m_Index=index;
	NS_LOG_UNCOND("" << Simulator::Now() << ": I am Node " << GetNode()->GetId() << " and now I have index value " << m_Index);
	m_ScanIndex=0;
	NS_LOG_UNCOND("" << Simulator::Now() << ": this node with ID " << GetNode()->GetId() << " and port number " << m_peerPort << " has reset its m_ScanIndex.");
	Simulator::Cancel (m_sendEvent);
	m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);

}

void
//UdpClient::ScheduleTargetIPSwitch(Time s, int index)
UdpClient::ScheduleTargetIPSwitch(Time s)
{
	//m_ScanTagValue=3;//reset scan tag
	//PortTagger m_ptg_tmp;
	//Ptr<Packet> m_scanReply_tmp=m_scanReply->Copy();
	//m_nit.RecordNodeIDValue(m_scanReply_tmp);
	//int nodeID=m_nit.GetLastNodeID();
	//assuming this global solution calculation does not cost time at all
	//packet->RemoveAllPacketTags ();
	//NS_LOG_UNCOND("Done.");
	//packet->RemoveAllByteTags ();
	//m_scanT.WriteScanValue(m_scanReply_tmp,3);//re-association information
	//int tmp;
	//NS_LOG_UNCOND("Done.");
	//tmp=m_socket->Send (m_scanReply_tmp);

	//m_sendEvent =
	//Simulator::Schedule (Seconds (1.0), &UdpClient::Send, this);

	//NS_LOG_UNCOND("Done.");
	//NS_LOG_UNCOND("" << Simulator::Now() << ": I called SendTo() for scanTagger value as 3 and packet sent has UID: " << m_scanReply_tmp->GetUid());  //Send Packet Back
	//Return Value " << tmp << "
	//NS_LOG_UNCOND("" << Simulator::Now() << ": node ID tag indicates that this packet comes from node " << nodeID);

	m_SwitchAPEvent = Simulator::Schedule(s, &UdpClient::DoScheduleTargetIPSwitching, this, m_ScanIndex);

}

void
UdpClient::DoScheduleTargetIPSwitching(int index)
{
	m_IPIndex=index;
	NS_LOG_UNCOND("" << Simulator::Now() << ": I am Node " << GetNode()->GetId() << " and now I have IP index value " << m_IPIndex);

	Ipv4Address localPeer=Ipv4Address::ConvertFrom(m_peerAddress);
	uint32_t localPeerInt=localPeer.Get();
	uint8_t subnet=localPeerInt; //first subnet
	uint8_t subnet2=localPeerInt >> 8; //second subnet

	if(m_IPIndex == 1) //meaning scheme 1 is chosen: for 10.1.3.* it should not change; for 10.1.4.* it should be changed into 10.1.3.${100+*}
	{
		if(subnet2 == 0x04)
		{
			localPeerInt=0x0A010300+subnet;
		}
		else //if subnet2 == 0x03
		{
			NS_LOG_UNCOND("" << Simulator::Now() << ": If(m_Index==1) case: if(subnet2==0x04) else case: something wrong on node " << GetNode()->GetId() << " with Remote IP " << localPeer);
		}
	}
	else ////meaning scheme 2 is chosen: for 10.1.4.* it should not change; for 10.1.3.* it should be changed into 10.1.4.${100+*}
	{
		if(subnet2 == 0x03)
		{
			localPeerInt=0x0A010400+subnet;
		}
		else
		{
			NS_LOG_UNCOND("" << Simulator::Now() << ": If(m_Index==2) case: if(subnet2==0x03) else case: something wrong on node " << GetNode()->GetId() << " with Remote IP " << localPeer);
		}
	}

	localPeer.Set(localPeerInt);
	m_peerAddress=Address(localPeer);
	m_SendToAddress=InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort);
	NS_LOG_UNCOND("" << Simulator::Now() << ": This is node " << GetNode ()->GetId() << " and remote IP address for this application now is " << Ipv4Address::ConvertFrom(m_peerAddress)
	<< "and return value of matchingType is " << Ipv4Address::IsMatchingType(m_peerAddress));
	m_ScanIndex=0;
	NS_LOG_UNCOND("" << Simulator::Now() << ": this node with ID " << GetNode()->GetId() << " and port number " << m_peerPort << " has reset its m_ScanIndex.");

        //Simulator::Cancel (m_sendEvent);
        //m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
}

void
UdpClient::ScheduleScanInfoRequest(Time s, int index)
{
	//NS_LOG_UNCOND("" << Simulator::Now() << ": I am Node " << GetNode()->GetId() << " and my peer port is " << m_peerPort << " and I entered into ScheduleScanInfoRequest.");
	if(offset.size() == 0)
	{
		offset.resize(m_flagMatrixLen+1);
	}

	m_ScanInfoEvent = Simulator::Schedule(s, &UdpClient::DoScheduleScanInforequest, this, index);

}

void
UdpClient::DoScheduleScanInforequest(int index)
{
	m_ScanIndex=index;
	NS_LOG_UNCOND("" << Simulator::Now() << ": This UdpClient with node ID " << GetNode()->GetId() << " and port number " << m_peerPort << " has scheduled scan info request with m_ScanIndex=" << m_ScanIndex);
	if(GetNode()->GetId() < 3) //meaning it is client node: 3 could be any other value if necessary
	{
		m_ScanTagValue=1;
		m_OptimizeOrNot = 0;
		Simulator::Cancel(m_sendEvent);
		m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
	}

}

void
UdpClient::StopApplication (void)
{
	NS_LOG_FUNCTION (this);

	if (m_socket != 0)
	{
		m_socket->Close ();
		m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
		m_socket = 0;
	}

	Simulator::Cancel (m_sendEvent);
}

void
UdpClient::Send (void)
{
	NS_LOG_FUNCTION (this);

	//NS_LOG_UNCOND ("Send ENTERED.");
	NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Node " << GetNode()->GetId() << ": SEND ENTERED");

	NS_ASSERT (m_sendEvent.IsExpired ());
	SeqTsHeader seqTs;
	seqTs.SetSeq (m_sent);
	Ptr<Packet> p = Create<Packet> (m_size-(8+4)); // 8+4 : the size of the seqTs header
	p->AddHeader (seqTs);

	m_udje.PrepareTx(p);
	m_fdje.PrepareEqTime(p);
	m_pde.PrepareEqTime(p);
	m_st.WriteSeqValue(p);
	m_ptg.WritePortValue(p,m_peerPort);

	IdTagger m_id (this->m_node->GetId());
	m_id.WriteIdValue(p);

	if(m_scanReply == 0)
	{
		m_scanReply=p->Copy();
	}

	if(m_Index != 0)
	{
		m_sat.WriteSwitchAPValue(p,m_Index);
	}

	m_nit.WriteNodeIDValue(p,GetNode()->GetId());

	if(m_ScanTagValue != 0)
	{
		QosTag m_qt_tmp;
		m_qt_tmp.SetTid(7);
		p->AddPacketTag(m_qt_tmp);
	}
	else
	{
		p->AddPacketTag(m_qt);
	}

	if(m_ScanTagValue != 0)
	{
		m_scanT.WriteScanValue(p,m_ScanTagValue);
		NS_LOG_UNCOND("" << Simulator::Now() << ": This is node " << GetNode()->GetId() << " and  packet containing scan tag value= " << m_ScanTagValue << " has UID " << p->GetUid() << " and this packet is to " << Ipv4Address::ConvertFrom (m_peerAddress));
		if(m_ScanTagValue == 3)
		{
			m_dt.WriteDecisionValue(p,m_ReassocDecision);
			NS_LOG_UNCOND("" << Simulator::Now() << ": packet containing scan tag value=3 has UID " << p->GetUid() << " and Reassoc Decision is " << m_ReassocDecision);
			m_ScanTagValue=0; //reset - so we only send one packet
			m_ReassocDecision=0;
		}
		m_ScanTagValue=0;

	}

	std::stringstream peerAddressStringStream;
	if (Ipv4Address::IsMatchingType (m_peerAddress))
	{
		peerAddressStringStream << Ipv4Address::ConvertFrom (m_peerAddress);
	}
	else if (Ipv6Address::IsMatchingType (m_peerAddress))
	{
		peerAddressStringStream << Ipv6Address::ConvertFrom (m_peerAddress);
	}

	if(m_IPIndex == 0)
	{
		if ((m_socket->Send (p)) >= 0)
		{
			// Added by Y.B.
			m_udje.PrepareTx(p);
			m_fdje.PrepareEqTime(p);

			++m_sent;

			NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " Uid: "
					<< p->GetUid () << " Time: "
					<< (Simulator::Now ()).GetSeconds () << " From: "
					<< GetNode()->GetId());
			NS_LOG_UNCOND ("TraceDelay TX " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " Uid: "
					<< p->GetUid () << " Time: "
					<< (Simulator::Now ()).GetSeconds () << " From: "
					<< GetNode()->GetId() << " With IP:"
					<< this->GetNode()->GetObject<Ipv4>()->GetAddress(1,0) << " Port: "
					<< m_peerPort);


		}
		else
		{
			NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " from "
					<< GetNode()->GetId());
			NS_LOG_UNCOND ("" << Simulator::Now() << ": Error while sending " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " from "
					<< GetNode()->GetId());
			//NS_LOG_UNCOND ("Error while sending " << m_size << " bytes to "
			//                                         << peerAddressStringStream.str ());
		}

		//NS_LOG_UNCOND ("cancelEvent: " << cancelEvent);
		if (m_sent < m_count)// && cancelEvent == 0)
		{
			m_sendEvent = Simulator::Schedule (m_interval, &UdpClient::Send, this);
		}
	}
	else
	{
		if ((m_socket->SendTo (p,0,m_SendToAddress)) >= 0)
		{
			// Added by Y.B.
			m_udje.PrepareTx(p);
			m_fdje.PrepareEqTime(p);

			++m_sent;

			NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " Uid: "
					<< p->GetUid () << " Time: "
					<< (Simulator::Now ()).GetSeconds () << " From: "
					<< GetNode()->GetId());
			NS_LOG_UNCOND ("TraceDelay TX " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " Uid: "
					<< p->GetUid () << " Time: "
					<< (Simulator::Now ()).GetSeconds () << " From: "
					<< GetNode()->GetId() << " With IP:"
					<< this->GetNode()->GetObject<Ipv4>()->GetAddress(1,0) << " Port: "
					<< m_peerPort);


		}
		else
		{
			NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " from "
					<< GetNode()->GetId());
			NS_LOG_UNCOND ("" << Simulator::Now() << ": Error while sending " << m_size << " bytes to "
					<< peerAddressStringStream.str () << " from "
					<< GetNode()->GetId());
			//NS_LOG_UNCOND ("Error while sending " << m_size << " bytes to "
			//                                         << peerAddressStringStream.str ());
		}

		//NS_LOG_UNCOND ("cancelEvent: " << cancelEvent);
		if (m_sent < m_count)// && cancelEvent == 0)
		{
			m_sendEvent = Simulator::Schedule (m_interval, &UdpClient::Send, this);
		}

	}

}

void
UdpClient::HandleRead (Ptr<Socket> socket)
{
	NS_LOG_FUNCTION (this << socket);

	//NS_LOG_UNCOND ("HANDLE READ CALLED");

	Ptr<Packet> packet;
	Address from;
	FakePktTagger m_fpt;
	StopTrafficTagger m_stt;
	ScanTagger m_scanT;

	while ((packet = socket->RecvFrom (from)))
	{
		NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": ENTERED INTO RECVFROM");

		m_stt.RecordStopTrafficValue(packet);
		m_ot.RecordOpValue(packet);
		m_scanT.RecordScanValue(packet);

		if(m_scanT.GetLastScan() == 3)
		{
			NS_LOG_UNCOND("" << Simulator::Now() << ": I received scan tag 3 packet and I am " << GetNode()->GetId() << " and my port is " << m_peerPort);

			//read decision carried by this packet
			m_dt.RecordDecisionValue(packet);
			m_ScanIndex=m_dt.GetLastDecision();
			Simulator::ScheduleNow(&UdpClient::ScheduleAPSwitch,this,Seconds(0));
			//m_ScanIndex=0;
			//m_ScanTagValue=0;

		}


		if(m_stt.GetLastStopTraffic() != 0)
		{
			NS_LOG_UNCOND("" << Simulator::Now() << ": I Got A Packet with stop traffic tag " << m_stt.GetLastStopTraffic() << " and it comes from " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " and I am Node " << GetNode()->GetId());
			//int stopTrafficValue=m_stt.GetLastStopTraffic();
			if(m_stt.GetLastStopTraffic() == 1) //DjMax: stop!
			{
				Simulator::Cancel (m_sendEvent);

			}
			else if(m_stt.GetLastStopTraffic() == 2)
			{
				m_sendEvent = Simulator::Schedule (Seconds(0), &UdpClient::Send, this);
			}


			/*NS_LOG_UNCOND("Done.");
	      packet->RemoveAllPacketTags ();
		  NS_LOG_UNCOND("Done.");
	      packet->RemoveAllByteTags ();
		  NS_LOG_UNCOND("Done.");

	      m_stt.WriteStopTrafficValue(packet,stopTrafficValue);
		  NS_LOG_UNCOND("Done.");

	      //SeqTsHeader seqTsTmp;
	      seqTsTmp.SetSeq (m_sent);
	      packet->AddHeader (seqTsTmp);
	      if(m_socket->Send (packet) >= 0)
	      {
	    	  ++m_sent;
	    	  NS_LOG_UNCOND("" << Simulator::Now() << ": I sent a stop traffic packet with tag value " << stopTrafficValue);
	      }//

	      int tmp;
		  NS_LOG_UNCOND("Done.");
	      tmp=socket->SendTo (packet, 0, from);
		  NS_LOG_UNCOND("Done.");
	      NS_LOG_UNCOND("" << Simulator::Now() << ": I called SendTo() with Return Value " << tmp);  //Send Packet Back*/
		}


		if(m_ot.GetLastOp() == INITIAL_PROBE_TRAIN_LENGTH) //use m_ot to capture how many packets to send - from now on train length of APP2 should be same as length of train
		{
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_ot=" << m_ot.GetLastOp() << " - a new train started.");
			m_sent=0;
			m_count=m_ot.GetLastOp();
			Simulator::Cancel (m_sendEvent);
			m_sendEvent = Simulator::Schedule (Seconds(0), &UdpClient::Send, this);

		}

		if(m_ot.GetLastOp() == 3 && cancelEvent == 0) //switch to another path
		{
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_ot==3 - Freeze on App with Port Number=" << m_peerPort);
			Simulator::Cancel (m_sendEvent);

			//Correction
			cancelEvent = 1;
			//m_rebootEvent = Simulator::ScheduleNow (&UdpClient::StopApplication, this);
			//m_rebootEvent = Simulator::Schedule (m_rebootInterval, &UdpClient::StartApplication, this);
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " : SEND EVENT CANCELLED With cancelEvent Value " << cancelEvent);
			return;
		}
		else if(m_ot.GetLastOp() == 4 && cancelEvent == 1)
		{
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_ot==4 - DeFreeze on App with Port Number=" << m_peerPort);
			//m_rebootEvent = Simulator::ScheduleNow (&UdpClient::StopApplication, this);
			//m_rebootEvent = Simulator::Schedule (m_rebootInterval, &UdpClient::StartApplication, this);
			NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": " << InetSocketAddress::ConvertFrom (from).GetIpv4 () << " : SEND EVENT RESTORED With cancelEvent Value " << cancelEvent);
			m_sendEvent = Simulator::Schedule (Seconds (0.0), &UdpClient::Send, this);
			cancelEvent = 0;

			return;
		}
		// Added by Y.B.
		else
		{
			m_udje.RecordRx(packet);
			//NS_LOG_UNCOND (InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << packet->GetSize() << "," << ",UDP Delay," << m_udje.GetLastDelay());
			//NS_LOG_UNCOND (InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << packet->GetSize() << "," << ",UDP Jitter," << m_udje.GetLastJitter());

			m_fpt.RecordFakePktValue(packet);
			packet->PeekPacketTag(m_qtRead);
			int tmp = m_qtRead.GetTid();

			if (InetSocketAddress::IsMatchingType (from))
			{
				NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
						InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
						InetSocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
						m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter());
				NS_LOG_UNCOND ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
						InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
						InetSocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
						m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter() << " and qtTag " << tmp);
			}
			else if (Inet6SocketAddress::IsMatchingType (from))
			{
				NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
						Inet6SocketAddress::ConvertFrom (from).GetIpv6 () << " port " <<
						Inet6SocketAddress::ConvertFrom (from).GetPort () << " with delay " <<
						m_udje.GetLastDelay() << " and jitters " << m_udje.GetLastJitter());
			}
		}

	}
}



} // Namespace ns3
