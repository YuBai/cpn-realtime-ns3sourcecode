/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 */

#include "sta-wifi-mac.h"
#include "ns3/wifi-net-device.h"
#include "ns3/node.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/ipv4.h"
#include "ns3/ipv4-interface-address.h"
//#include "ns3/internet-module.h"
#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv4-l3-protocol.h"

#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include "ns3/boolean.h"
#include "ns3/trace-source-accessor.h"
#include "qos-tag.h"
#include "mac-low.h"
#include "dcf-manager.h"
#include "mac-rx-middle.h"
#include "mac-tx-middle.h"
#include "wifi-mac-header.h"
#include "msdu-aggregator.h"
#include "amsdu-subframe-header.h"
#include "mgt-headers.h"
#include "ht-capabilities.h"
#include "vht-capabilities.h"

/*
 * The state machine for this STA is:
 --------------                                          -----------
 | Associated |   <--------------------      ------->    | Refused |
 --------------                        \    /            -----------
    \                                   \  /
     \    -----------------     -----------------------------
      \-> | Beacon Missed | --> | Wait Association Response |
          -----------------     -----------------------------
                \                       ^
                 \                      |
                  \    -----------------------
                   \-> | Wait Probe Response |
                       -----------------------
 */

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("StaWifiMac");

NS_OBJECT_ENSURE_REGISTERED (StaWifiMac);

TypeId
StaWifiMac::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::StaWifiMac")
    .SetParent<RegularWifiMac> ()
    .SetGroupName ("Wifi")
    .AddConstructor<StaWifiMac> ()
    .AddAttribute ("ProbeRequestTimeout", "The interval between two consecutive probe request attempts.",
                   TimeValue (Seconds (0.05)),
                   MakeTimeAccessor (&StaWifiMac::m_probeRequestTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("AssocRequestTimeout", "The interval between two consecutive assoc request attempts.",
                   TimeValue (Seconds (0.5)),
                   MakeTimeAccessor (&StaWifiMac::m_assocRequestTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("MaxMissedBeacons",
                   "Number of beacons which much be consecutively missed before "
                   "we attempt to restart association.",
                   UintegerValue (10),
                   MakeUintegerAccessor (&StaWifiMac::m_maxMissedBeacons),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("ActiveProbing",
                   "If true, we send probe requests. If false, we don't."
                   "NOTE: if more than one STA in your simulation is using active probing, "
                   "you should enable it at a different simulation time for each STA, "
                   "otherwise all the STAs will start sending probes at the same time resulting in collisions. "
                   "See bug 1060 for more info.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&StaWifiMac::SetActiveProbing, &StaWifiMac::GetActiveProbing),
                   MakeBooleanChecker ())
    .AddTraceSource ("Assoc", "Associated with an access point.",
                     MakeTraceSourceAccessor (&StaWifiMac::m_assocLogger),
                     "ns3::Mac48Address::TracedCallback")
    .AddTraceSource ("DeAssoc", "Association with an access point lost.",
                     MakeTraceSourceAccessor (&StaWifiMac::m_deAssocLogger),
                     "ns3::Mac48Address::TracedCallback")
  ;
  return tid;
}

StaWifiMac::StaWifiMac ()
  : m_state (BEACON_MISSED),
    m_probeRequestEvent (),
    m_assocRequestEvent (),
    m_beaconWatchdogEnd (Seconds (0.0)),
	m_LastIndex (0),
	m_pst (0.0),
	m_oneFilled (0),
	m_twoFilled (0),
	m_probeOrNot(0),
	m_alterOrNot(0),
	m_endTrainOrNot (0),
	m_endTrainOrNot_ACK (0),
	m_feedback_ACK (0),
	m_feedback_ACK_ACK (0),
	m_macPathOne ("00:00:00:00:00:05"),
	m_macPathTwo ("00:00:00:00:00:06"),
	m_macPathOne_channel (1),
	m_macPathTwo_channel (6),
	m_macPathOne_IPRouting (1),
	m_macPathTwo_IPRouting (2),
	//m_macPathResult ("00:00:00:00:00:05"),
	//m_macPathResult ("00:00:00:00:00:06"), //Subnet Script
	m_macPathResult ("00:00:00:00:00:00"),
	probeTrainCounter(0),
	pktAmountSample (0),
	throughputThreshold (1500),
	m_triggerOrNot (0),
	m_theChosenOne ("00:00:00:00:00:00"), //07 for APsAND50Nodes
	//m_theChosenOne ("00:00:00:00:00:00"), //Comparison Group
	startTime (99999999), //original: 5
	startTimeP (99999999), //start from 7s
	periodP (99999999), //originally trigger algorithm each 60s
	eofSendTime(Seconds(0.01)),
	FSDTriggeringDuration(Seconds(3)),
	probeTime (0),
	triggeringTime (0),
	appTriggeringState (0),
	ACTUAL_PROBE_TRAIN_LENGTH (INITIAL_PROBE_TRAIN_LENGTH),
	feedbackPktCurrentRun_one (0),
	feedbackPktCurrentRun_two (0),
	app1From ("00:00:00:00:00:00"),
	app1To ("00:00:00:00:00:00"),
	app2From ("00:00:00:00:00:00"),
	app2To ("00:00:00:00:00:00"),
	app3From ("00:00:00:00:00:00"),
	app3To ("00:00:00:00:00:00"),
	scanningPeriod (Seconds(0)),
	kk_triggeringTime_one(-1),
	kk_probeTime_one(-1),
	kk_triggeringTime_two(-1),
	kk_probeTime_two(-1),
	currentAP1TrainLen(0),
	currentAP2TrainLen(0),
	ssidOne (Ssid("ns-3-ssid")),
	ssidTwo (Ssid("ns-3-ssid1")),
	path1SNRTimes(0),
	path2SNRTimes(0),
	path1SNRSum (0.0),
	path2SNRSum (0.0),
	windowLength (Seconds(5.0)),
	SNRAP1 ("00:00:00:00:00:05"),
	SNRAP2 ("00:00:00:00:00:06"),
	lastCompTime (Seconds(0.0)),
	path1AvgSNR (0.0),
	path2AvgSNR (0.0),
	//SNRSchemeChosenOne ("00:00:00:00:00:05")
	SNRSchemeChosenOne ("00:00:00:00:00:07")
{
  NS_LOG_FUNCTION (this);
  memset(m_samples_one,0,sizeof(m_samples_one));
  memset(m_samples_two,0,sizeof(m_samples_two));
  memset(m_totalSamples_one,0,sizeof(m_totalSamples_one));
  memset(m_totalSamples_two,0,sizeof(m_totalSamples_two));
  app1Copy=0;
  app2Copy=0;
  tmpQueueAPP1 = CreateObject<WifiMacQueue> ();
  tmpQueueAPP2 = CreateObject<WifiMacQueue> ();
  tmpQueueAPP3 = CreateObject<WifiMacQueue> ();
  tmpQueueAPP1->SetMaxSize(40000000);
  tmpQueueAPP2->SetMaxSize(40000000);
  tmpQueueAPP3->SetMaxSize(40000000);
  //Mac48Address APList[2];
  APList[1]="00:00:00:00:00:05";
  APList[2]="00:00:00:00:00:06";
  ScanRequestCopyFrom="00:00:00:00:00:00";
  lastScanRequestTime=Seconds(0);
  m_ScanningOrNot=0;
  m_startID=0;
  m_timeOut=20; //Assuming 10s is sufficient
  m_scanTimeOut=1;
 //m_probeRequestEvent = Simulator::Schedule (m_probeRequestTimeout,
    //                                         &StaWifiMac::ProbeRequestTimeout, this);

  // Let the lower layers know that we are acting as a non-AP STA in
  // an infrastructure BSS.
  SetTypeOfStation (STA);
}

StaWifiMac::~StaWifiMac ()
{
  NS_LOG_FUNCTION (this);
}

globalAPTable&
StaWifiMac::GetGAT(void)
{
	return m_gat;
}

void 
StaWifiMac::ReportECParameters(void)
{
  
  if(m_dca->GetQueue()->GetSize() != 0)
	{

		WifiMacHeader headerTmp;
		NS_LOG_UNCOND_YB ("" << Simulator::Now() << " - " << GetAddress() << ": Current State is " << appTriggeringState << " With Queue Length " <<  m_dca->GetQueue()->GetSize() << " With Packet Being Served As " << m_dca->GetQueue()->Peek(&headerTmp)->GetUid());

	}
  else
	{
		NS_LOG_UNCOND_YB ("" << Simulator::Now() << " - " << GetAddress() << ": Current State is " << appTriggeringState << " With Queue Length " <<  m_dca->GetQueue()->GetSize() << " No Packet In Service.");

	}

  m_reportECParameters = Simulator::Schedule (Seconds(0.01), &StaWifiMac::ReportECParameters, this);
}

void
StaWifiMac::TriggerFSD(void)
{

  //Added by Y.B.
  if(m_triggerOrNot == 0
			   && m_probeOrNot == 0
			   && app1Copy != 0 && app1From != "00:00:00:00:00:00" && app1To != "00:00:00:00:00:00"
			   && app2Copy != 0 && app2From != "00:00:00:00:00:00" && app2To != "00:00:00:00:00:00"
			   && appTriggeringState == 1
			   && Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)   //200 seconds previously
			   //&& app3Copy != 0 && app3From != 0 && app3To !=0) //guaranteeing that FSD algorithm can start only when we have all upward messagers ready
	   {
		   FSDTriggeringDuration=Seconds(10000);
		   m_probeOrNot = 1;
		   m_alterOrNot = 0;
		   m_triggerOrNot = 1;
		   currentAP1TrainLen=0;
		   currentAP2TrainLen=0;

		   NS_LOG_UNCOND("" << Simulator::Now() << "TriggerFSD triggered.");
 

		   //Mac48Address macPathResultPre=m_macPathResult;
		   //NS_LOG_UNCOND("macPathResultPre is " << macPathResultPre);

//           if(m_macPathResult==m_macPathTwo)
//           {
//        	   NS_LOG_UNCOND("I have m_macPathResult as " << m_macPathResult);
//
//	           m_macPathResult=m_macPathOne;
//			   app1CopyTmp = app1Copy->Copy();
//
//			   m_ot.WriteOpValue(app1CopyTmp, 1);
//		       ForwardUp (app1CopyTmp, app1From, app1To);
//	           SetState(BEACON_MISSED);
//	           SetBssid(m_macPathResult);
//	    	   SetSsid (ssidOne);
//
//			   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathOne_channel);
//
//	           TryToEnsureAssociated();
//
//           }
   		   //m_ot.WriteOpValue(packet,1);
   		   //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

		   //stop app1, start app2
	       NS_LOG_UNCOND ("" << Simulator::Now() << "start appTriggering State: 1->2.");
		   appTriggeringState = 2;
		   //dequeue all packets from m_dca's queue into tmpAPPQueue3
		   m_dca_queue=m_dca->GetQueue();
	           while(!m_dca_queue->IsEmpty())
	           {
				  NS_LOG_UNCOND ("" << Simulator::Now() << " 14 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
				  WifiMacHeader hdr_tmp;
				  Ptr<const Packet> pkt_tmp;
				  EnqueueTimeTagger m_ettg;
				  //NS_LOG_UNCOND ("dequeueing.");
				  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
				  //NS_LOG_UNCOND ("dequeued.");

				  PortTagger m_ptg_tmpt;
				  m_ptg_tmpt.RecordPortValue(pkt_tmp);
				  if(m_ptg_tmpt.GetLastPort() == 11)
				  {
					  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": I found a packet from port 11 - but I do not care now so I will ignore it.");
					  continue;
				  }
				  hdr_tmp.SetAddr1 (GetBssid ());
				  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
	
				  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

				  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());

				  tmpQueueAPP1->Enqueue(pkt_tmp, hdr_tmp_tmp);
	           }


		   //app1CopyTmp = app1Copy->Copy();
		   app2CopyTmp = app2Copy->Copy();
		   //m_ot.WriteOpValue(app1CopyTmp, 3);
	           //ForwardUp (app1CopyTmp, app1From, app1To);
	           m_ot.WriteOpValue(app2CopyTmp, 4);
	           ForwardUp (app2CopyTmp, app2From, app2To);
	           NS_LOG_UNCOND ("" << Simulator::Now() << "appTriggering State: 1->2.");

		   lastPktAmountSample=pktAmountSample;
		   lastTriggerTime = Simulator::Now();
		   //m_pathDecision = 0;
		   //m_oneFilled = 0;
		   //m_pst.ResetLastProbeSeq();
		   //m_pst1.ResetLastProbeSeq();
		   //m_pst2.ResetLastProbeSeq();
		   //m_pst_tmp.ResetLastProbeSeq();
		   //memset(m_samples_one, 0, sizeof(m_samples_one));
		   //memset(m_samples_two, 0, sizeof(m_samples_two));

		   //if(macPathResultPre==m_macPathOne)
		   //{
	           //newly added
		       app2CopyTmp = app2Copy->Copy();
	           m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
	           ForwardUp (app2CopyTmp, app2From, app2To);
	           NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train since triggered: current m_macPathOne is " << m_macPathResult);

		   //}

	           //newly added to stop downlink traffic when FSD algorithm is probing
	    	   Ptr<Packet> stPkt_tmp=stopTrafficPkt->Copy();
	    	   m_stt.WriteStopTrafficValue(stPkt_tmp,1);//1: stop, 2:continue
	           ForwardUp (stPkt_tmp, stopTrafficFrom, stopTrafficTo);
	           NS_LOG_UNCOND("TriggerFSD Done");

	    	   //NS_LOG_UNCOND("" << Simulator::Now() << ": Put stop traffic packet pkt with " << stPkt_tmp->GetUid() << " into queue.");
	           //m_dca->Queue (stPkt_tmp, stopTrafficPktHdr);

	           NS_LOG_UNCOND("" << Simulator::Now() << ": All Parameter Values - m_oneFilled: " << m_oneFilled << ", m_twoFilled: " << m_twoFilled << ", m_probeOrNot: " << m_probeOrNot << ", m_alterOrNot: " << m_alterOrNot
	         		  << ", m_endTrainOrNot: " << m_endTrainOrNot << ", m_endTrainOrNot_ACK: " << m_endTrainOrNot_ACK << ", m_feedback_ACK: " << m_feedback_ACK << ", m_feedback_ACK_ACK: " << m_feedback_ACK_ACK
	         		  << ", m_macPathOne: " << m_macPathOne << ", m_macPathTwo: " << m_macPathTwo << ", m_macPathOne_channel: " << m_macPathOne_channel << ", m_macPathTwo_channel: " << m_macPathTwo_channel
	         		  << ", m_macPathOne_IPRouting: " << m_macPathOne_IPRouting << ", m_macPathTwo_IPRouting: " << m_macPathTwo_IPRouting << ", m_macPathResult: " << m_macPathResult << ", probeTrainCounter: " << probeTrainCounter
	         		  << ", pktAmountSample: " << pktAmountSample << ", throughputThreshold: " << throughputThreshold << ", m_triggerOrNot: " << m_triggerOrNot << ", m_theChosenOne: " << m_theChosenOne << ", startTime: " << startTime
	         		  << ", startTimeP: " << startTimeP << ", periodP: " << periodP << ", eofSendTime: " << eofSendTime << ", FSDTriggeringDuration: " << FSDTriggeringDuration << ", probeTime: " << probeTime << ", triggeringTime: " << triggeringTime
	         		  << ", appTriggeringState: " << appTriggeringState << ", ACTUAL_PROBE_TRAIN_LENGTH: " << ACTUAL_PROBE_TRAIN_LENGTH << ", feedbackPktCurrentRun_one: " << feedbackPktCurrentRun_one << ", feedbackPktCurrentRun_two: " << feedbackPktCurrentRun_two
	         		  << ", app1From: " << app1From << ", app1To: " << app1To << ", app2From: " << app2From << ", app2To: " << app2To << ", app3From: " << app3From << ", app3To: " << app3To << ", kk_triggeringTime_one: " << kk_triggeringTime_one << ", kk_probeTime_one: " << kk_probeTime_one
	         		  << ", kk_triggeringTime_two: " << kk_triggeringTime_two << ", kk_probeTime_two: " << kk_probeTime_two << ", currentAP1TrainLen: " << currentAP1TrainLen << ", currentAP2TrainLen: " << currentAP2TrainLen << ", ssidOne: " << ssidOne << ", ssidTwo: " << ssidTwo);

	   }

	   /*if((m_ht.GetLastHop() == 3 || m_ht.GetLastHop() == 4) && Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne) //means ACK for Fake Pkt 1 received
	   {
		   m_ot.WriteOpValue(packet,1);
   		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
		   m_feedback_ACK_ACK = 1;
		   NS_LOG_UNCOND("" << Simulator::Now() << ": m_feedback_ACK_ACK set to 1.");
		   SetState(BEACON_MISSED);
		   NS_LOG_UNCOND ("BEACON_MISSED DONE.");
		   SetBssid (m_macPathResult);
		   NS_LOG_UNCOND ("SET BSSID DONE.");
		   TryToEnsureAssociated();
		   NS_LOG_UNCOND ("TryToEnsureAssociated DONE.");
		   m_feedback_ACK_ACK = 0;
		   m_feedback_ACK = 0;
	   }*/

	/*m_oneFilled (0),
	m_twoFilled (0),
	m_probeOrNot(0),
	m_alterOrNot(0),
	m_endTrainOrNot (0),
	m_endTrainOrNot_ACK (0),
	m_feedback_ACK (0),
	m_feedback_ACK_ACK (0),
	m_macPathOne ("00:00:00:00:00:05"),
	m_macPathTwo ("00:00:00:00:00:06"),
	m_macPathOne_channel (1),
	m_macPathTwo_channel (6),
	m_macPathOne_IPRouting (1),
	m_macPathTwo_IPRouting (2),
	m_macPathResult ("00:00:00:00:00:05"),
	//m_macPathResult ("00:00:00:00:00:06"), //Subnet Script
	//m_macPathResult ("00:00:00:00:00:00"),
	probeTrainCounter(0),
	pktAmountSample (0),
	throughputThreshold (1500),
	m_triggerOrNot (0),
	m_theChosenOne ("00:00:00:00:00:07"), //07 for APsAND50Nodes
	//m_theChosenOne ("00:00:00:00:00:00"), //Comparison Group
	startTime (5),
	startTimeP (6), //start from 10s
	periodP (60), //trigger algorithm each 60s
	eofSendTime(Seconds(0.01)),
	FSDTriggeringDuration(Seconds(3)),
	probeTime (0),
	triggeringTime (0),
	appTriggeringState (0),
	ACTUAL_PROBE_TRAIN_LENGTH (INITIAL_PROBE_TRAIN_LENGTH),
	feedbackPktCurrentRun_one (0),
	feedbackPktCurrentRun_two (0),
	app1From ("00:00:00:00:00:00"),
	app1To ("00:00:00:00:00:00"),
	app2From ("00:00:00:00:00:00"),
	app2To ("00:00:00:00:00:00"),
	app3From ("00:00:00:00:00:00"),
	app3To ("00:00:00:00:00:00"),
	kk_triggeringTime_one(-1),
	kk_probeTime_one(-1),
	kk_triggeringTime_two(-1),
	kk_probeTime_two(-1),
	currentAP1TrainLen(0),
	currentAP2TrainLen(0),
	ssidOne (Ssid("ns-3-ssid")),
	ssidTwo (Ssid("ns-3-ssid1")),
*/


  //m_periodicTriggering =
  Simulator::ScheduleWithContext (3,Seconds(periodP),&StaWifiMac::TriggerFSD,this);
}

//void
//StaWifiMac::testFunction()
//{
//			   app1CopyTmp = app1Copy->Copy();
//			   app2CopyTmp = app2Copy->Copy();
//				   m_ot.WriteOpValue(app1CopyTmp, 3);
//		           ForwardUp (app1CopyTmp, app1From, app1To);
//		           m_ot.WriteOpValue(app2CopyTmp, 4);
//		           ForwardUp (app2CopyTmp, app2From, app2To);
//		           NS_LOG_UNCOND ("" << Simulator::Now() << "appTriggering State: 1->2.");
//
//			   lastPktAmountSample=pktAmountSample;
//			   lastTriggerTime = Simulator::Now();
//			   //m_pathDecision = 0;
//			   //m_oneFilled = 0;
//			   //m_pst.ResetLastProbeSeq();
//			   //m_pst1.ResetLastProbeSeq();
//			   //m_pst2.ResetLastProbeSeq();
//			   //m_pst_tmp.ResetLastProbeSeq();
//			   //memset(m_samples_one, 0, sizeof(m_samples_one));
//			   //memset(m_samples_two, 0, sizeof(m_samples_two));
//
//		           //newly added
//			   app2CopyTmp = app2Copy->Copy();
//		           m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
//		           ForwardUp (app2CopyTmp, app2From, app2To);
//		           NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");
//
//		           //newly added to stop downlink traffic when FSD algorithm is probing
//		    	   Ptr<Packet> stPkt_tmp=stopTrafficPkt->Copy();
//		    	   m_stt.WriteStopTrafficValue(stPkt_tmp,1);//1: stop, 2:continue
//		           ForwardUp (stPkt_tmp, stopTrafficFrom, stopTrafficTo);
//
//}


void
StaWifiMac::resortSamples(double* samples, int sampleLen)
{
	int i=0;
	int step=0;
	double targetValue=std::numeric_limits<double>::max();

	while(i+step<=sampleLen)
	{
		if(samples[i] == targetValue)
		{
			step=step+1;
		}
		else
		{
			i=i+1;
		}
		samples[i]=samples[i+step];
	}
	i=i+1;
	for(i=sampleLen-step+1;i<=sampleLen;i++)
	{
		samples[i]=targetValue;
	}

	//debugging
	for(int xx=0;xx<=sampleLen;xx++)
	{
		NS_LOG_UNCOND_YB("" << Simulator::Now() << "Now resorted sample[" << xx << "] becomes " << samples[xx]);
	}

}

void
StaWifiMac::Timeout (void)
{
  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and CACA Timeout is triggered.");
  m_aboutToResetScanningOrNot = 1;

  SetChannelNumber (m_originalChannel);
  if(m_originalChannel == 1)
  {
    SetBssid(m_gat.getMacAddrByAPID(0));
  }
  else
  {
    SetBssid(m_gat.getMacAddrByAPID(1));
  }

  //stupid modification
  if(m_originalChannel == 1)
  {
   SetSsid(ssidOne);
  }
  else
  {
   SetSsid(ssidTwo);
  }

  NS_LOG_UNCOND("" << Simulator::Now() << "CACA Timeout: Bssid is " << GetBssid() << " and Ssid is " << GetSsid() << " and I am " << GetAddress());  

  SetState(BEACON_MISSED);
  TryToEnsureAssociated();

  //m_ScanningOrNot=0;
}

void
StaWifiMac::ScanTimeout (void)
{
  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and CACA Scan Timeout is triggered.");
  //m_aboutToResetScanningOrNot = 1;

  SetChannelNumber (m_originalChannel);
  if(m_originalChannel == 1)
  {
    SetBssid(m_gat.getMacAddrByAPID(0));
  }
  else
  {
    SetBssid(m_gat.getMacAddrByAPID(1));
  }

  //stupid modification
  if(m_originalChannel == 1)
  {
   SetSsid(ssidOne);
  }
  else
  {
   SetSsid(ssidTwo);
  }

  NS_LOG_UNCOND("" << Simulator::Now() << "CACA Scan Timeout: Bssid is " << GetBssid() << " and Ssid is " << GetSsid() << " and I am " << GetAddress());  

  SetState(BEACON_MISSED);
  TryToEnsureAssociated();

  scanDone=1;
}

void
StaWifiMac::SetMaxMissedBeacons (uint32_t missed)
{
  NS_LOG_FUNCTION (this << missed);
  m_maxMissedBeacons = missed;
}

void
StaWifiMac::SetProbeRequestTimeout (Time timeout)
{
  NS_LOG_FUNCTION (this << timeout);
  m_probeRequestTimeout = timeout;
}

void
StaWifiMac::SetAssocRequestTimeout (Time timeout)
{
  NS_LOG_FUNCTION (this << timeout);
  m_assocRequestTimeout = timeout;
}

void
StaWifiMac::StartActiveAssociation (void)
{
  NS_LOG_FUNCTION (this);
  TryToEnsureAssociated ();
}

void
StaWifiMac::SetActiveProbing (bool enable)
{
  NS_LOG_FUNCTION (this << enable);
  if (enable)
    {
      Simulator::ScheduleNow (&StaWifiMac::TryToEnsureAssociated, this);
    }
  else
    {
      m_probeRequestEvent.Cancel ();
    }
  m_activeProbing = enable;

  NS_LOG_UNCOND_YB ("" << GetAddress() << " is starting to report its EC parameters.");
  //m_reportECParameters = Simulator::Schedule (Seconds(1), &StaWifiMac::ReportECParameters, this);

  //if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
  //{
	  //m_periodicTriggering =
	  Simulator::ScheduleWithContext (3,Seconds(startTimeP), &StaWifiMac::TriggerFSD, this);
  //}
  //m_theChosenOne=GetAddress();
}

bool StaWifiMac::GetActiveProbing (void) const
{
  return m_activeProbing;
}

void
StaWifiMac::SendProbeRequest (void)
{
  NS_LOG_FUNCTION (this);
  WifiMacHeader hdr;
  hdr.SetProbeReq ();
  hdr.SetAddr1 (Mac48Address::GetBroadcast ());
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (Mac48Address::GetBroadcast ());
  hdr.SetDsNotFrom ();
  hdr.SetDsNotTo ();
  Ptr<Packet> packet = Create<Packet> ();
  MgtProbeRequestHeader probe;
  probe.SetSsid (GetSsid ());
  probe.SetSupportedRates (GetSupportedRates ());
  if (m_htSupported || m_vhtSupported)
    {
      probe.SetHtCapabilities (GetHtCapabilities ());
      hdr.SetNoOrder ();
    }
  if (m_vhtSupported)
    {
      probe.SetVhtCapabilities (GetVhtCapabilities ());
    }
  packet->AddHeader (probe);

  //The standard is not clear on the correct queue for management
  //frames if we are a QoS AP. The approach taken here is to always
  //use the DCF for these regardless of whether we have a QoS
  //association or not.
  m_dca->Queue (packet, hdr);

  if (m_probeRequestEvent.IsRunning ())
    {
      m_probeRequestEvent.Cancel ();
    }
  m_probeRequestEvent = Simulator::Schedule (m_probeRequestTimeout,
                                             &StaWifiMac::ProbeRequestTimeout, this);
}

void
StaWifiMac::SendAssociationRequest (void)
{
  NS_LOG_FUNCTION (this << GetBssid ());
  WifiMacHeader hdr;
  hdr.SetAssocReq ();
  hdr.SetAddr1 (GetBssid ());
  hdr.SetAddr2 (GetAddress ());
  hdr.SetAddr3 (GetBssid ());
  hdr.SetDsNotFrom ();
  hdr.SetDsNotTo ();
  Ptr<Packet> packet = Create<Packet> ();
  MgtAssocRequestHeader assoc;
  assoc.SetSsid (GetSsid ());
  assoc.SetSupportedRates (GetSupportedRates ());
  if (m_htSupported || m_vhtSupported)
    {
      assoc.SetHtCapabilities (GetHtCapabilities ());
      hdr.SetNoOrder ();
    }
  if (m_vhtSupported)
    {
      assoc.SetVhtCapabilities (GetVhtCapabilities ());
    }
  packet->AddHeader (assoc);

  //The standard is not clear on the correct queue for management
  //frames if we are a QoS AP. The approach taken here is to always
  //use the DCF for these regardless of whether we have a QoS
  //association or not.
  m_dca->Queue (packet, hdr);
  NS_LOG_UNCOND_YB("" << Simulator::Now()  << "I am " << GetAddress() << " and now I am in SendAssociationRequest function with UID " << packet->GetUid());

  if (m_assocRequestEvent.IsRunning ())
    {
      m_assocRequestEvent.Cancel ();
      NS_LOG_UNCOND_YB("" << Simulator::Now()  << "I am " << GetAddress() << " and m_assocRequestEvent is cancelled.");

    }
  m_assocRequestEvent = Simulator::Schedule (m_assocRequestTimeout,
                                             &StaWifiMac::AssocRequestTimeout, this);
}

void
StaWifiMac::TryToEnsureAssociated (void)
{
  NS_LOG_FUNCTION (this);
  switch (m_state)
    {
    case ASSOCIATED:
      return;
      break;
    case WAIT_PROBE_RESP:
      /* we have sent a probe request earlier so we
         do not need to re-send a probe request immediately.
         We just need to wait until probe-request-timeout
         or until we get a probe response
       */
      break;
    case BEACON_MISSED:
      /* we were associated but we missed a bunch of beacons
       * so we should assume we are not associated anymore.
       * We try to initiate a probe request now.
       */
      m_linkDown ();
      if (m_activeProbing)
        {
          SetState (WAIT_PROBE_RESP);
          SendProbeRequest ();
        }
      break;
    case WAIT_ASSOC_RESP:
      /* we have sent an assoc request so we do not need to
         re-send an assoc request right now. We just need to
         wait until either assoc-request-timeout or until
         we get an assoc response.
       */
      break;
    case REFUSED:
      /* we have sent an assoc request and received a negative
         assoc resp. We wait until someone restarts an
         association with a given ssid.
       */
      break;
    }
}

void
StaWifiMac::AssocRequestTimeout (void)
{
  NS_LOG_FUNCTION (this);
  SetState (WAIT_ASSOC_RESP);
  SendAssociationRequest ();
}

void
StaWifiMac::ProbeRequestTimeout (void)
{
  NS_LOG_FUNCTION (this);
  SetState (WAIT_PROBE_RESP);
  SendProbeRequest ();
}

void
StaWifiMac::MissedBeacons (void)
{
  NS_LOG_FUNCTION (this);
  if (m_beaconWatchdogEnd > Simulator::Now ())
    {
      if (m_beaconWatchdog.IsRunning ())
        {
          m_beaconWatchdog.Cancel ();
        }
      m_beaconWatchdog = Simulator::Schedule (m_beaconWatchdogEnd - Simulator::Now (),
                                              &StaWifiMac::MissedBeacons, this);
      return;
    }
  NS_LOG_DEBUG ("beacon missed");
  SetState (BEACON_MISSED);
  TryToEnsureAssociated ();
}

void
StaWifiMac::RestartBeaconWatchdog (Time delay)
{
  NS_LOG_FUNCTION (this << delay);
  m_beaconWatchdogEnd = std::max (Simulator::Now () + delay, m_beaconWatchdogEnd);
  if (Simulator::GetDelayLeft (m_beaconWatchdog) < delay
      && m_beaconWatchdog.IsExpired ())
    {
      NS_LOG_DEBUG ("really restart watchdog.");
      m_beaconWatchdog = Simulator::Schedule (delay, &StaWifiMac::MissedBeacons, this);
    }
}

bool
StaWifiMac::IsAssociated (void) const
{
  return m_state == ASSOCIATED;
}

bool
StaWifiMac::IsWaitAssocResp (void) const
{
  return m_state == WAIT_ASSOC_RESP;
}

void
StaWifiMac::Scan (uint16_t startId, int originalChannel)
{
  NS_LOG_FUNCTION(this);
  SetState (BEACON_MISSED);
  uint16_t channel = m_gat.getChannelNumberByAPID((startId + 1) % m_gat.getSize());//(startId + 1) % 12;
  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and We switched to channel " << channel);
  if (channel == originalChannel)
    {
      //channel++;
	  	  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " - Branch 0");
	  	  scanDone=1;
	  	  SetChannelNumber (originalChannel);
		  //Simulator::Cancel(m_timeOutEvent);
		  //m_timeOutEvent=Simulator::Schedule(Seconds(m_timeOut),&StaWifiMac::Timeout,this);

	  	  if(originalChannel == 1)
	  	  {
	  		  SetBssid(m_gat.getMacAddrByAPID(0));
	  	  }
	  	  else
	  	  {
	  		  SetBssid(m_gat.getMacAddrByAPID(1));
	  	  }

	  	  //stupid modification
	  	  if(originalChannel == 1)
	  	  {
	  		  SetSsid(ssidOne);
	  	  }
	  	  else
	  	  {
	  		  SetSsid(ssidTwo);
	  	  }

	  	  //SetState(BEACON_MISSED);
  		  TryToEnsureAssociated();

	  	  return; //we only want to scan all channel once
    }

  Simulator::Cancel(m_scanTimeOutEvent);
  m_scanTimeOutEvent=Simulator::Schedule(Seconds(m_scanTimeOut),&StaWifiMac::ScanTimeout,this); 
  SetChannelNumber (channel);
  SetBssid(m_gat.getMacAddrByAPID(startId+1));

  //stupid modification
  if(channel == 1)
  {
	  SetSsid(ssidOne);
  }
  else
  {
	  SetSsid(ssidTwo);
  }

  if (m_activeProbing)
    {
      SetState (WAIT_PROBE_RESP);
      NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " -- branch 1");
      SendProbeRequest ();
    }
  else
    {
    }
}

void
StaWifiMac::SetChannelNumber (uint16_t id)
{
  NS_LOG_FUNCTION(this);
  DynamicCast<YansWifiPhy> (m_phy)->SetChannelNumber (id);
}


void
StaWifiMac::Enqueue (Ptr<const Packet> packet, Mac48Address to)
{
  NS_LOG_FUNCTION (this << packet << to);
  //NS_LOG_UNCOND ("" << Simulator::Now() << ": Packet with ");

  //SetActiveProbing(true);

  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": I am " << GetAddress() << " and to is " << to << " and isAssociate status is " << IsAssociated ());
  PortTagger m_ptg_tmp;
  StopTrafficTagger m_stt_tmp;
  ScanTagger m_scanT;
  m_ptg_tmp.RecordPortValue(packet);
  m_stt_tmp.RecordStopTrafficValue(packet);
  m_sat.RecordSwitchAPValue(packet);
  m_scanT.RecordScanValue(packet);

  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": appTriggeringState now is " << appTriggeringState);

  //NS_LOG_UNCOND ("This is node " << this->GetWifiPhy()->);
  /*Ptr<YansWifiPhy> tmpPhy = DynamicCast <YansWifiPhy> (this->GetWifiPhy());
  Ptr<WifiNetDevice> tmpWifi;
  //NS_LOG_UNCOND ("DynamicCast done with address " << tmpPhy);
  if (tmpPhy)
  {
	  //NS_LOG_UNCOND ("This is node " << tmpPhy->GetDevice()->GetObject<Node>());
	  tmpWifi = DynamicCast <WifiNetDevice> (tmpPhy->GetDevice());
	  //NS_LOG_UNCOND ("DynamicCast done with address " << tmpWifi);
	  if(tmpWifi)
	  {
		  //NS_LOG_UNCOND ("This is node " << tmpWifi->GetNode()->GetId() << " with IP address " << tmpWifi->GetNode()->GetObject<Ipv4>());
		  Ptr<Ipv4L3Protocol> tmpIp = tmpWifi->GetNode()->GetObject<Ipv4L3Protocol>();
		  if(tmpIp)
		  {
			  //NS_LOG_UNCOND ("tmpAddr:" << tmpIp->GetAddress(1,0).GetLocal());
		  }
		  //Ipv4Address tmpAddr = tmpIp->GetAddress(1,0).GetLocal();
		  //NS_LOG_UNCOND ("tmpAddr:" << Ipv4Address::ConvertFrom(tmpAddr));
	  }
  }*/


  //trying to see whether this pre-caching could solve problem
  //if (Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne && m_ptg_tmp.GetLastPort() == 9 && !IsAssociated ()) //only trigger this when !IsAssociated
  //  {

		

  //  }

  if(m_LastIndex != m_sat.GetLastSwitchAP()
		  && m_sat.GetLastSwitchAP() != 0)
  {
	  m_LastIndex = m_sat.GetLastSwitchAP();
	  NS_LOG_UNCOND_YB("ENTERED INTO M_LASTINDEX.");
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": I am " << GetAddress() << " and I have refreshed my index as " << m_LastIndex << " and I am going to associate with AP with Mac Address " << APList[m_LastIndex]);

  	   SetState(BEACON_MISSED);
 	   SetBssid (APList[m_LastIndex-1]);
 	   //SetSsid (APList[m_LastIndex]);
 	   SwitchAPCopyTmp=SwitchAPCopy->Copy();
 	   if(m_LastIndex == 1)
 	   {
 		   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathOne_channel);
 		   SetSsid(ssidOne);
   		   m_ot.WriteOpValue(SwitchAPCopyTmp,m_macPathOne_IPRouting);
   		   ForwardUp (SwitchAPCopyTmp, SwitchAPCopyFrom, SwitchAPCopyTo);
   		   //m_ot.WriteOpValue(app1Copy, 3);
	       //ForwardUp (app1Copy, app1From, app1To);


 	   }
 	   else
 	   {
 		   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathTwo_channel);
 		   SetSsid(ssidTwo);
   		   m_ot.WriteOpValue(SwitchAPCopyTmp,m_macPathTwo_IPRouting);
   		   ForwardUp (SwitchAPCopyTmp, SwitchAPCopyFrom, SwitchAPCopyTo);

 	   }
	   TryToEnsureAssociated();
	   NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and I have refreshed my index as " << m_LastIndex << " and I am going to associate with AP with Mac Address " << APList[m_LastIndex] << " and I am trying to re-associate");

  }


  //NS_LOG_UNCOND ("Enqueue Entered.");
  if (!IsAssociated ()
	&& m_ScanningOrNot == 0) 
	//&& Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne 
	//&& m_probeOrNot == 0
	//&& m_endTrainOrNot == 0 
	//&& m_endTrainOrNot_ACK == 0) //want this work only when we are not probing
    {
      NotifyTxDrop (packet);
      NS_LOG_UNCOND ("" << Simulator::Now() << ": Packet Dropped in Enqueue Due To !IsAssociated: BSSID I want to associate with is " << GetBssid() << " and I am " << GetAddress() << " and UID: " << packet->GetUid());
      if(m_ScanTagValue != 2)
      {
	TryToEnsureAssociated ();
      }
      return;
    }
  WifiMacHeader hdr;

  //If we are not a QoS AP then we definitely want to use AC_BE to
  //transmit the packet. A TID of zero will map to AC_BE (through \c
  //QosUtilsMapTidToAc()), so we use that as our default here.
  uint8_t tid = 0;
  uint8_t tidQ = 0; //tid for queued packets

  //For now, an AP that supports QoS does not support non-QoS
  //associations, and vice versa. In future the AP model should
  //support simultaneously associated QoS and non-QoS STAs, at which
  //point there will need to be per-association QoS state maintained
  //by the association state machine, and consulted here.
  if (m_qosSupported)
    {
      hdr.SetType (WIFI_MAC_QOSDATA);
      hdr.SetQosAckPolicy (WifiMacHeader::NORMAL_ACK);
      hdr.SetQosNoEosp ();
      hdr.SetQosNoAmsdu ();
      //Transmission of multiple frames in the same TXOP is not
      //supported for now
      hdr.SetQosTxopLimit (0);

      //Fill in the QoS control field in the MAC header
      tid = QosUtilsGetTidForPacket (packet);
      //Any value greater than 7 is invalid and likely indicates that
      //the packet had no QoS tag, so we revert to zero, which'll
      //mean that AC_BE is used.
      if (tid > 7)
        {
          tid = 0;
        }
      hdr.SetQosTid (tid);
    }
  else
    {
      hdr.SetTypeData ();
    }
  if (m_htSupported || m_vhtSupported)
    {
      hdr.SetNoOrder ();
    }

  hdr.SetAddr1 (GetBssid ());
  hdr.SetAddr2 (m_low->GetAddress ());
  hdr.SetAddr3 (to);
  hdr.SetDsNotFrom ();
  hdr.SetDsTo ();

  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Enqueue(): this packet with UID: " << packet->GetUid() << " has hdr.Addr1=" << GetBssid() << " hdr.Addr2=" << m_low->GetAddress() << " hdr.Addr3=" << to << " m_sat.LastIndex " << m_sat.GetLastSwitchAP());

  //Added by Y.B.
  //m_qdje.RecordDqTime(packet);

  FollowdownDelayEstimation m_fdje;
  SeqTagger m_st;

  //Debugging - works
  //EnqueueTimeTagger m_ett_tmp;
  //EnqueueTimeCompensateTagger m_etct_tmp;
  //if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
  //{
//	  m_ett_tmp.PrepareEqTime(packet, Simulator::Now());
//	  m_etct_tmp.PrepareEqTime(packet,Simulator::Now());
//	  m_ett_tmp.RecordEqTime(packet);
//	  m_etct_tmp.RecordEqTime(packet);
//	  NS_LOG_UNCOND ("" << Simulator::Now() << ": Packet with m_ett_tmp: " << m_ett_tmp.GetLastRecordedEnqueueTime() << " m_etct_tmp: " << m_etct_tmp.GetLastRecordedCompensateTime());

//  }


  if((ScanRequestCopy == 0 || ScanRequestCopyTo != to) &&
		  packet->GetSize() > 54 &&
		  m_ptg_tmp.GetLastPort() != 11)
  {
	  ScanRequestCopy=packet->Copy();
	  QosTag m_qt_tmp;
	  //packet->PeekPacketTag(m_qt_tmp);
	  ScanRequestCopy->RemovePacketTag(m_qt_tmp);
	  m_qt_tmp.SetTid(7);
	  ScanRequestCopy->AddPacketTag(m_qt_tmp);
	  //int tmp=m_qt_tmp.GetTid();
	  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": ScanRequestCopy copied on " << GetAddress() << " with UID: " << ScanRequestCopy->GetUid());
	  //ScanRequestCopyFrom=hdr->GetAddr3();
	  ScanRequestCopyTo=to;
	  //NS_LOG_UNCOND("" << Simulator::Now() << ": ScanRequestCopy 4AddrHead: " << hdr->GetAddr1() << ", " << hdr->GetAddr2() << ", " << hdr->GetAddr3() << ", " << hdr->GetAddr4());
  }

  //m_fdje.RecordDqTime(packet);
  if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
  {
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "CHOSEN DETECTED: 54 VS " << packet->GetSize());

  }  

  if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne && packet->GetSize() > 54)
  {
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "" << GetAddress() <<": " << packet->GetSize()<< " : CHOSEN VALUE WRITTEN WITH CURRENT probeTrainCounter " << probeTrainCounter << " AND m_probeOrNot " << m_probeOrNot);
	  m_ct.WriteChosenValue(packet,1);
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << hdr.GetAddr1() << "," << hdr.GetAddr2() << "," << packet->GetSize() << ",UID" << packet->GetUid() << "," << hdr.GetSequenceNumber() << ",Followdown Delay on Sta-Wifi-Mac," << m_fdje.GetLastQDelay());

  }


  //NS_LOG_UNCOND (hdr.GetAddr1() << "," << hdr.GetAddr2() << "," << packet->GetSize() << "," << hdr.GetSequenceNumber() << ",Followdown Delay on Sta-Wifi-Mac," << m_fdje.GetLastQDelay());
  if(packet->GetSize() > 54
	&& probeTrainCounter < ACTUAL_PROBE_TRAIN_LENGTH-1
	&& m_probeOrNot == 1
	&& Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne
	&& m_ptg_tmp.GetLastPort() == 11
	&& (appTriggeringState == 2 || appTriggeringState == 3)) //Further We need to change this into something that we could switch
  {
	  m_pt.WriteProbeValue(packet, m_probeOrNot);
	  m_apt.WriteAlterPathValue(packet,m_alterOrNot);
	  m_ptlt.WriteProbeTrainLengthValue(packet,ACTUAL_PROBE_TRAIN_LENGTH);
	  m_pst.WriteProbeSeqValue(packet);
	  m_st.RecordSeqValue(packet);
	  m_ptlt.RecordProbeTrainLengthValue(packet);
	  m_ptt.WriteProbeTimeValue(packet,triggeringTime);
	  m_ptt.RecordProbeTimeValue(packet);
  //m_pst.RecordProbeSeqValue(packet);
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "90909090: m_pst:" << m_pst.GetLastProbeSeq() << " BSSID: " << GetBssid() << " SEQ NUM OF PKT:" << m_st.GetLastSeq() << " m_ptlt: " << m_ptlt.GetLastProbeTrainLength() << " triggeringTime: " << m_ptt.GetLastProbeTime() << " UID: " << packet->GetUid());
	  probeTrainCounter = m_pst.GetLastProbeSeq();
  }
  else if(packet->GetSize() > 54
		  && probeTrainCounter == ACTUAL_PROBE_TRAIN_LENGTH-1
		  && m_probeOrNot == 1
		  && Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne
		  && m_ptg_tmp.GetLastPort() == 11
		  && (appTriggeringState == 2 || appTriggeringState == 3)) //Further We need to change this into something that we could switch
  {	
	  m_endTrainOrNot = 1.0;
	  m_pt.WriteProbeValue(packet, m_probeOrNot);
	  m_apt.WriteAlterPathValue(packet,m_alterOrNot);
	  m_ptlt.WriteProbeTrainLengthValue(packet,ACTUAL_PROBE_TRAIN_LENGTH);
	  m_pst.SetLastProbeSeq(ACTUAL_PROBE_TRAIN_LENGTH-1);
	  m_pst.WriteProbeSeqValue(packet);

	  m_ptlt.RecordProbeTrainLengthValue(packet);
	  m_ptt.WriteProbeTimeValue(packet,triggeringTime);
	  m_ptt.RecordProbeTimeValue(packet);
	  probeTrainCounter = m_pst.GetLastProbeSeq();

	  m_ett.WriteEndOfTrainValue(packet, m_endTrainOrNot);
	  m_ett.RecordEndOfTrainValue(packet);

	  //NS_LOG_UNCOND ("xx: m_pst:" << m_pst.GetLastProbeSeq() << " BSSID: " << GetBssid() << " m_ptlt: " << m_ptlt.GetLastProbeTrainLength() << " triggeringTime: " << m_ptt.GetLastProbeTime());
	  //NS_LOG_UNCOND ("Now m_endTrainOrNot=" << m_endTrainOrNot);
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":END OF TRAIN PACKET TAGGED WITH m_ett: " << m_ett.GetLastEndOfTrain() << " ,m_pt: " << m_pt.GetLastProbe());

	  //endOfTrainCopy = packet->Copy();
	  //m_ett.WriteEndOfTrainValue(endOfTrainCopy, 1);
	  //m_eofTo = to;
	  //Simulator::Schedule(eofSendTime, &StaWifiMac::sendEOF, this);
  }
  /*else if(packet->GetSize() > 1000 && m_endTrainOrNot == 1 && m_endTrainOrNot_ACK == 1 && Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne && m_feedback_ACK == 0)
  {
	  m_pt.WriteProbeValue(packet, m_probeOrNot);
	  m_apt.WriteAlterPathValue(packet,m_alterOrNot);
	  m_ptlt.WriteProbeTrainLengthValue(packet,ACTUAL_PROBE_TRAIN_LENGTH);
	  m_ptt.WriteProbeTimeValue(packet,triggeringTime);

	  //m_ett.WriteEndOfTrainValue(packet, m_endTrainOrNot);
	  //m_ett.RecordEndOfTrainValue(packet);
	  m_pt.RecordProbeValue(packet);
	  m_eofTo = to;
	  //m_pt.WriteProbeValue(packet, m_probeOrNot);
	  //m_pst.WriteProbeSeqValue(packet);
	  //m_finipt.WriteFinishProbeValue(packet,1);
  }*/

  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_feedback_ack in enqueue() is " << m_feedback_ACK << " with pkt len " << packet->GetSize() << " UID " << packet->GetUid());
  if(packet->GetSize() > 54
	&& Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne
	&& m_feedback_ACK != 0 && m_endTrainOrNot_ACK == 1
	&& m_ptg_tmp.GetLastPort() == 11
	&& (appTriggeringState == 2 || appTriggeringState == 3)) //Further We need to change this into something that we could switch
  {
	  m_finipt.WriteFinishProbeValue(packet,m_feedback_ACK);
	  m_finipt.RecordFinishProbeValue(packet);
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_feedback_ack: " << m_finipt.GetLastFinishProbe() << " written into packet");
  }

  if (m_qosSupported)
    {
	  if(m_scanT.GetLastScan() != 0)
	  {
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": this is " << GetAddress() << " and m_scanT.GetLastScan is " << m_scanT.GetLastScan() << " and UID " << packet->GetUid());
	  }

      //Sanity check that the TID is valid
      NS_ASSERT (tid < 8);
	  if(m_ScanningOrNot == 1 && m_ptg_tmp.GetLastPort() != 11 && m_scanT.GetLastScan() == 0) //meaning this does not come from APP2
	  {
		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": Minus1 appTriggeringState now is " << appTriggeringState << " so app1's packets are put into tmpQueueAPP1. tmpQueueAPP1 Len: " << tmpQueueAPP1->GetSize());

		  EnqueueTimeTagger m_ettg;
		  m_ettg.PrepareEqTime(packet, Simulator::Now());

		  tmpQueueAPP1->Enqueue(packet,hdr);
		  return;
	  }
	  else if(m_ScanningOrNot == 0)//newly added to guarantee that port10 packet will not be discarded
	  {
		  //NS_LOG_UNCOND("" << Simulator::Now() << ": 7 appTriggeringState now is " << appTriggeringState << " so app2's packets are put into m_dca's queue. tmpQueueAPP2 Len: " << tmpQueueAPP2->GetSize());
		  while(!tmpQueueAPP1->IsEmpty())
		  {
			  WifiMacHeader hdr_tmp;
			  Ptr<const Packet> pkt_tmp;
			  EnqueueTimeTagger m_ettg;
			  EnqueueTimeCompensateTagger m_etct;

			  pkt_tmp=tmpQueueAPP1->Dequeue(&hdr_tmp);
			  hdr_tmp.SetAddr1 (GetBssid ());
			  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
			  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

			  m_ettg.RecordEqTime(pkt_tmp);
			  m_etct.PrepareEqTime(pkt_tmp, Simulator::Now()-m_ettg.GetLastRecordedEnqueueTime());

			  EnqueueTimeTagger m_ettg_tmp;
			  EnqueueTimeCompensateTagger m_etct_tmp;
			  m_ettg_tmp.RecordEqTime(pkt_tmp);
			  m_etct_tmp.RecordEqTime(pkt_tmp);

		      tidQ = QosUtilsGetTidForPacket (pkt_tmp);
		      m_edca[QosUtilsMapTidToAc (tidQ)]->Queue (pkt_tmp, hdr_tmp_tmp);
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 0 appTriggeringState now is " << appTriggeringState << " and One Packet From tmpQueueAPP1 Goes into m_dca->Queue() with UID " << pkt_tmp->GetUid() << " m_ettg_tmp: " << m_ettg_tmp.GetLastRecordedEnqueueTime() << " m_etct_tmp: " << m_etct_tmp.GetLastRecordedCompensateTime());
		  }
		  //NS_LOG_UNCOND("" << Simulator::Now() << "tid to be used for packet with UID: " << packet->GetUid() << " is " << (int)tid << " and corresponding queue length is " << m_edca[QosUtilsMapTidToAc (tid)]->GetEdcaQueue()->GetSize());
	      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
	  }
	  else
	  {
	      if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
		  {
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":" << GetAddress() << ": Packet with Port Number " << m_ptg_tmp.GetLastPort() << "and UID: " << packet->GetUid() << " Entered.");
			  //state 1
			  if(appTriggeringState == 0)
			  {
				  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 1 appTriggeringState now is " << appTriggeringState << " so everyone could put its packet into it");
			      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
			  }
			  else if(appTriggeringState == 1)
			  {
				  if(m_ptg_tmp.GetLastPort() == 9) //meaning this comes from APP1
				  {
					  NS_LOG_UNCOND_YB("" << Simulator::Now() << ": 2 appTriggeringState now is " << appTriggeringState << " so app1's packets are put into m_dca's queue. tmpQueueAPP1 Len: " << tmpQueueAPP1->GetSize());
					  while(!tmpQueueAPP1->IsEmpty())
					  {
						  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 3 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from tmpQueueAPP1.");
						  WifiMacHeader hdr_tmp;
						  Ptr<const Packet> pkt_tmp;
						  EnqueueTimeTagger m_ettg;
						  EnqueueTimeCompensateTagger m_etct;
						  pkt_tmp=tmpQueueAPP1->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
						  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

						  m_ettg.RecordEqTime(pkt_tmp);
						  m_etct.PrepareEqTime(pkt_tmp, Simulator::Now()-m_ettg.GetLastRecordedEnqueueTime());

					      tidQ = QosUtilsGetTidForPacket (pkt_tmp);
					      m_edca[QosUtilsMapTidToAc (tidQ)]->Queue (pkt_tmp, hdr_tmp_tmp);
						  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 4 appTriggeringState now is " << appTriggeringState << " and One Packet From tmpQueueAPP1 Goes into m_dca->Queue()");
					  }
				      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
				  }
				  else if(m_ptg_tmp.GetLastPort() == 11)
				  {
					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 5 appTriggeringState now is " << appTriggeringState << " so app2's packets are put into tmpQueueAPP2. tmpQueueAPP2 Len: " << tmpQueueAPP2->GetSize());

					  EnqueueTimeTagger m_ettg;
					  m_ettg.PrepareEqTime(packet, Simulator::Now());

					  tmpQueueAPP2->Enqueue(packet,hdr);
				  }
				  /*else if(m_ptg_tmp.GetLastPort() == 12)
				  {
					  NS_LOG_UNCOND("" << Simulator::Now() << ": appTriggeringState now is " << appTriggeringState << " so app3's packets are put into tmpQueueAPP3.");
					  tmpQueueAPP3->Enqueue(packet,hdr);
				  }*/
				  else
				  {
					  NS_LOG_UNCOND ("appTriggeringState==1: Something goes Wrong.");
				  }
			  }
			  //state 2
			  else if(appTriggeringState == 2)
			  {

				  if(m_ptg_tmp.GetLastPort() == 9) //meaning this comes from APP1
				  {
					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 6 appTriggeringState now is " << appTriggeringState << " so app1's packets are put into tmpQueueAPP1. tmpQueueAPP1 Len: " << tmpQueueAPP1->GetSize());

					  EnqueueTimeTagger m_ettg;
					  m_ettg.PrepareEqTime(packet, Simulator::Now());

					  tmpQueueAPP1->Enqueue(packet,hdr);
				  }
				  else if(m_ptg_tmp.GetLastPort() == 11 || m_stt_tmp.GetLastStopTraffic() != 0) //newly added to guarantee that port10 packet will not be discarded
				  {
					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 7 appTriggeringState now is " << appTriggeringState << " so app2's packets are put into m_dca's queue. tmpQueueAPP2 Len: " << tmpQueueAPP2->GetSize());
					  while(!tmpQueueAPP2->IsEmpty())
					  {
						  WifiMacHeader hdr_tmp;
						  Ptr<const Packet> pkt_tmp;
						  EnqueueTimeTagger m_ettg;
						  EnqueueTimeCompensateTagger m_etct;

						  pkt_tmp=tmpQueueAPP2->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
						  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

						  m_ettg.RecordEqTime(pkt_tmp);
						  m_etct.PrepareEqTime(pkt_tmp, Simulator::Now()-m_ettg.GetLastRecordedEnqueueTime());

						  EnqueueTimeTagger m_ettg_tmp;
						  EnqueueTimeCompensateTagger m_etct_tmp;
						  m_ettg_tmp.RecordEqTime(pkt_tmp);
						  m_etct_tmp.RecordEqTime(pkt_tmp);

					      tidQ = QosUtilsGetTidForPacket (pkt_tmp);
					      m_edca[QosUtilsMapTidToAc (tidQ)]->Queue (pkt_tmp, hdr_tmp_tmp);
						  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 9 appTriggeringState now is " << appTriggeringState << " and One Packet From tmpQueueAPP2 Goes into m_dca->Queue() with UID " << pkt_tmp->GetUid() << " m_ettg_tmp: " << m_ettg_tmp.GetLastRecordedEnqueueTime() << " m_etct_tmp: " << m_etct_tmp.GetLastRecordedCompensateTime());
					  }
					  //NS_LOG_UNCOND("" << Simulator::Now() << "tid to be used for packet with UID: " << packet->GetUid() << " is " << (int)tid << " and corresponding queue length is " << m_edca[QosUtilsMapTidToAc (tid)]->GetEdcaQueue()->GetSize());
				      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
				  }
				  /*else if(m_ptg_tmp.GetLastPort() == 12)
				  {
					  NS_LOG_UNCOND("" << Simulator::Now() << ": appTriggeringState now is " << appTriggeringState << " so app3's packets are put into tmpQueueAPP3.");
					  tmpQueueAPP3->Enqueue(packet,hdr);
				  }*/
				  else
				  {
					  NS_LOG_UNCOND ("appTriggeringState==2: Something goes Wrong.");
				  }
			  }
			  //state 3
			  else if(appTriggeringState == 3)
			  {
				  if(m_ptg_tmp.GetLastPort() == 9) //meaning this comes from APP1
				  {
					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 10 appTriggeringState now is " << appTriggeringState << " so app1's packets are put into tmpQueueAPP1. tmpQueueAPP1 Len: " << tmpQueueAPP1->GetSize());

					  EnqueueTimeTagger m_ettg;
					  m_ettg.PrepareEqTime(packet, Simulator::Now());

					  tmpQueueAPP1->Enqueue(packet,hdr);
				  }
				  /*else if(m_ptg_tmp.GetLastPort() == 11)
				  {
					  NS_LOG_UNCOND("" << Simulator::Now() << ": appTriggeringState now is " << appTriggeringState << " so app2's packets are put into tmpQueueAPP2.");
					  tmpQueueAPP2->Enqueue(packet,hdr);

				  }*/
				  else if(m_ptg_tmp.GetLastPort() == 11)
				  {
					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": 11 appTriggeringState now is " << appTriggeringState << " so app3's packets are put into m_dca's queue. tmpQueueAPP3 Len: " << tmpQueueAPP3->GetSize());
					  while(!tmpQueueAPP3->IsEmpty())
					  {
						  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 12 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from tmpQueueAPP3.");
						  WifiMacHeader hdr_tmp;
						  Ptr<const Packet> pkt_tmp;
						  EnqueueTimeTagger m_ettg;
						  EnqueueTimeCompensateTagger m_etct;
						  pkt_tmp=tmpQueueAPP3->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
						  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

						  m_ettg.RecordEqTime(pkt_tmp);
						  m_etct.PrepareEqTime(pkt_tmp, Simulator::Now()-m_ettg.GetLastRecordedEnqueueTime());

						  EnqueueTimeTagger m_ettg_tmp;
						  EnqueueTimeCompensateTagger m_etct_tmp;
						  m_ettg_tmp.RecordEqTime(pkt_tmp);
						  m_etct_tmp.RecordEqTime(pkt_tmp);

					      tidQ = QosUtilsGetTidForPacket (pkt_tmp);
					      m_edca[QosUtilsMapTidToAc (tidQ)]->Queue (pkt_tmp, hdr_tmp_tmp);
						  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 13 appTriggeringState now is " << appTriggeringState << " and One Packet From tmpQueueAPP3 Goes into m_dca->Queue() with UID "  << pkt_tmp->GetUid() << " m_ettg_tmp: " << m_ettg_tmp.GetLastRecordedEnqueueTime() << " m_etct_tmp: " << m_etct_tmp.GetLastRecordedCompensateTime());
					  }
				      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
				  }
				  else
				  {
					  NS_LOG_UNCOND ("" << Simulator::Now() << "appTriggeringState==3: Something goes Wrong.");
				  }
			  }
			  else
			  {
				  NS_LOG_UNCOND ("" << Simulator::Now() << "State unrecognized - something goes wrong.");
			  }

		  }
		  else
		  {
		      m_edca[QosUtilsMapTidToAc (tid)]->Queue (packet, hdr);
		  }
	  }

    }
  else
    {
	  m_dca->Queue (packet, hdr);

    }
}

void
StaWifiMac::Receive (Ptr<Packet> packet, const WifiMacHeader *hdr)
{
  //MacTagger m_mt;
  //Added by Y.B. for SNR Based Comparison
  //NS_LOG_UNCOND ("Receive Entered.");
  int xxxx;

  SetActiveProbing(true);

  MySnrTagger mst (0.0);
  mst.RecordMySnrValue(packet);
  m_ptg.RecordPortValue(packet);
  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": I am " << GetAddress() << " and I receive a packet with SNR=" << mst.GetLastMySnr() << " FROM " << hdr->GetAddr2() << " TO " << hdr->GetAddr1() << " UID " << packet->GetUid());

  PortTagger m_ptg_tmp;
  m_ptg_tmp.RecordPortValue(packet);

  ScanTagger m_scanT;
  m_scanT.RecordScanValue(packet);

  /*if(GetActiveProbing() == false)
  {

	SetActiveProbing(true);

  }*/

  //newly added for copying pkts for stopping downlink traffic
  if(m_ptg_tmp.GetLastPort() == 10 &&
	 Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne &&
	 packet->GetSize() > 54 &&
	 stopTrafficPkt == 0)
  {
	  stopTrafficPkt=packet->Copy();
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": stopTrafficPkt copied on " << GetAddress());
	  stopTrafficFrom=hdr->GetAddr3();
	  stopTrafficTo=hdr->GetAddr1();

  }

  //newly added for updating modulation information
/*  if (hdr->GetAddr2 () == GetBssid () && (hdr->GetAddr1 () == GetAddress () || (hdr->GetAddr1 () == "ff:ff:ff:ff:ff:ff")) && hdr->GetAddr3 () != GetAddress ())
    {
	  int tmpID=m_gat.getAPIDByMacAddr(hdr->GetAddr2());
	  ModulationTagger m_mt_tmp;
	  m_mt_tmp.RecordModulationValue(packet);
	  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and m_gat.getAPIDByMacAddr returns " << tmpID << " and hdr->GetAddr2() is " << hdr->GetAddr2() << " and value to be written is " << 1/m_mt_tmp.GetLastModulation());
	  m_gat.setLoad(tmpID,1/m_mt_tmp.GetLastModulation());

    }
*/
//  if(m_scanT.GetLastScan() == 3) //meaning it is time to recover connection - we turn m_ScanningOrNot next time we successfully associate with an AP
//  {
//	  NS_LOG_UNCOND("" << Simulator::Now() << ": m_aboutToResetScanningOrNot is set to 1 on node with address " << GetAddress());
//	  m_aboutToResetScanningOrNot = 1;
//  }

  if(AVERAGE_SNR_SCHEME == 1 && Mac48Address::ConvertFrom(GetAddress()) == SNRSchemeChosenOne && Simulator::Now() > startTime)
  {
	  if(hdr->GetAddr2() == SNRAP1)	//Very Dirty Hack - Need to Know Server Addresses First
	  {
		  path1SNRTimes = path1SNRTimes + 1;
		  path1SNRSum = path1SNRSum + mst.GetLastMySnr();
	  }
	  else if(hdr->GetAddr2() == SNRAP2)
	  {
		  path2SNRTimes = path2SNRTimes + 1;
		  path2SNRSum = path2SNRSum + mst.GetLastMySnr();
	  }

	  if(Simulator::Now() >= lastCompTime + windowLength)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Time is Up For AVG SNR Comparison.");
		  lastCompTime = Simulator::Now();
		  path1AvgSNR = path1SNRSum/path1SNRTimes;
		  path2AvgSNR = path2SNRSum/path2SNRTimes;

		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": path1AvgSNR is " << path1AvgSNR << " path2AvgSNR is " << path2AvgSNR);

		  if(path1AvgSNR > path2AvgSNR)
		  {
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "" << path1AvgSNR << " versus " << path2AvgSNR << ": path1 is better.");
			  m_macPathResult = SNRAP1;

			  //switch channel - newly added in Feb.23rd
			  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
			  //wifiPhyTmp->SetChannelNumber(1);
			  SetState(BEACON_MISSED);
			  SetBssid (m_macPathResult);
			  DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(1);

			  TryToEnsureAssociated();
			  path1AvgSNR = 0;
			  path2AvgSNR = 0;
			  path1SNRTimes = 0;
			  path2SNRTimes = 0;
			  path1SNRSum = 0;
			  path2SNRSum = 0;

		  }
		  else if(path1AvgSNR < path2AvgSNR)
		  {
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "" << path1AvgSNR << " versus " << path2AvgSNR << ": path2 is better.");
			  m_macPathResult = SNRAP2;

			  //switch channel - newly added in Feb.23rd
			  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
			  //wifiPhyTmp->SetChannelNumber(6);
			  SetState(BEACON_MISSED);
			  SetBssid (m_macPathResult);
			  DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(6);

			  TryToEnsureAssociated();
			  path1AvgSNR = 0;
			  path2AvgSNR = 0;
			  path1SNRTimes = 0;
			  path2SNRTimes = 0;
			  path1SNRSum = 0;
			  path2SNRSum = 0;

		  }
	  }
  }
  else if(AVERAGE_SNR_SCHEME == 2 && Mac48Address::ConvertFrom(GetAddress()) == SNRSchemeChosenOne && Simulator::Now() > startTime)
  {
	  if(hdr->GetAddr2() == SNRAP1)
	  {
		  path1SNRSum = mst.GetLastMySnr();
	  }
	  else if(hdr->GetAddr2() == SNRAP2)
	  {
		  path2SNRSum = mst.GetLastMySnr();
	  }

	  if(Simulator::Now() >= lastCompTime + windowLength)
	  {
		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Time is Up For Latest SNR Comparison.");
		  lastCompTime = Simulator::Now();

		  if(path1SNRSum > path2SNRSum)
		  {
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "" << path1SNRSum << " versus " << path2SNRSum << ": path1 is better.");
			  m_macPathResult = SNRAP1;

			  //switch channel - newly added in Feb.23rd
			  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
			  //wifiPhyTmp->SetChannelNumber(1);
			  SetState(BEACON_MISSED);
			  SetBssid (m_macPathResult);
			  DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(1);

			  TryToEnsureAssociated();
			  path1AvgSNR = 0;
			  path2AvgSNR = 0;
			  path1SNRTimes = 0;
			  path2SNRTimes = 0;
			  path1SNRSum = 0;
			  path2SNRSum = 0;

		  }

		  else if(path1SNRSum < path2SNRSum)
		  {
			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "" << path1SNRSum << " versus " << path2SNRSum << ": path2 is better.");
			  m_macPathResult = SNRAP2;

			  //switch channel - newly added in Feb.23rd
			  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
			  //wifiPhyTmp->SetChannelNumber(6);
			  SetState(BEACON_MISSED);
			  SetBssid (m_macPathResult);
			  DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(6);

			  TryToEnsureAssociated();
			  path1AvgSNR = 0;
			  path2AvgSNR = 0;
			  path1SNRTimes = 0;
			  path2SNRTimes = 0;
			  path1SNRSum = 0;
			  path2SNRSum = 0;

		  }


	  }
  }

  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": RECEIVE A PKT FROM " << hdr->GetAddr2());
  NS_LOG_FUNCTION (this << packet << hdr);
  NS_ASSERT (!hdr->IsCtl ());
  if (hdr->GetAddr3 () == GetAddress ())
    {
      NS_LOG_LOGIC ("packet sent by us.");
      NS_LOG_UNCOND ("packet sent by us.");
      return;
    }
  else if (hdr->GetAddr1 () != GetAddress ()
           && !hdr->GetAddr1 ().IsGroup ())
    {
      NS_LOG_LOGIC ("packet is not for us");
      NS_LOG_UNCOND ("packet is not for us");
      NotifyRxDrop (packet);
      return;
    }
  else if (hdr->IsData ())
    {
      if (!IsAssociated ())
        {
          NS_LOG_LOGIC ("Received data frame while not associated: ignore");
          NS_LOG_UNCOND ("Received data frame while not associated: ignore");
          NotifyRxDrop (packet);
          return;
        }
      if (!(hdr->IsFromDs () && !hdr->IsToDs ()))
        {
          NS_LOG_LOGIC ("Received data frame not from the DS: ignore");
          NS_LOG_UNCOND ("Received data frame not from the DS: ignore");
          NotifyRxDrop (packet);
          return;
        }
      if (hdr->GetAddr2 () != GetBssid ())
        {
          NS_LOG_LOGIC ("Received data frame not from the BSS we are associated with: ignore");
          NotifyRxDrop (packet);
          return;
        }
      if (hdr->IsQosData ())
        {
    	  if (hdr->IsQosAmsdu ())
            {
              NS_ASSERT (hdr->GetAddr3 () == GetBssid ());
              DeaggregateAmsduAndForward (packet, hdr);
              packet = 0;
            }
          else
            {
                int tmpID=m_gat.getAPIDByMacAddr(hdr->GetAddr2());
                ModulationTagger m_mt_tmp;
                m_mt_tmp.RecordModulationValue(packet);
                NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": I am " << GetAddress() << " and m_gat.getAPIDByMacAddr returns " << tmpID << " and hdr->GetAddr2() is " << hdr->GetAddr2() << " and value to be written is " << 1/m_mt_tmp.GetLastModulation());
                m_gat.setLoad(tmpID,1/m_mt_tmp.GetLastModulation());

        	  //request scan debugging
        	  //port number tag debugging
//        	  if(ScanRequestCopy == 0 || ScanRequestCopyFrom != hdr->GetAddr3())
//        	  {
//        		  ScanRequestCopy=packet->Copy();
//        		  NS_LOG_UNCOND("" << Simulator::Now() << ": ScanRequestCopy copied on " << GetAddress());
//        		  ScanRequestCopyFrom=hdr->GetAddr3();
//        		  ScanRequestCopyTo=hdr->GetAddr1();
//        		  NS_LOG_UNCOND("" << Simulator::Now() << ": ScanRequestCopy 4AddrHead: " << hdr->GetAddr1() << ", " << hdr->GetAddr2() << ", " << hdr->GetAddr3() << ", " << hdr->GetAddr4());
//        	  }
        	  if(m_scanT.GetLastScan() == 1 &&
        			  ScanRequestCopy != 0 &&
					  Simulator::Now() >= lastScanRequestTime)// + scanningPeriod)
        	  {

        		  NS_LOG_UNCOND_YB(""<<Simulator::Now()<<": I have entered into m_ScanTagValue==2 branch and I am " << GetAddress());
        		  m_ScanTagValue=2;
			  m_startID=m_gat.getAPIDByMacAddr(GetBssid());
        		  //DynamicCast<YansWifiPhy> (m_phy)->SetChannelNumber (id);
        		  m_originalChannel=DynamicCast<YansWifiPhy> (m_phy)->GetChannelNumber();
        		  m_scanEvent=Simulator::ScheduleNow(&StaWifiMac::Scan,this,m_startID, m_originalChannel);
			  Simulator::Cancel(m_timeOutEvent);
			  m_timeOutEvent=Simulator::Schedule(Seconds(m_timeOut),&StaWifiMac::Timeout,this);

//        	  	  Ptr<Packet> ScanRequestCopyTmp=ScanRequestCopy->Copy();
//        	  	  ScanTagger m_scT;
//        	  	  m_scT.WriteScanValue(ScanRequestCopyTmp,2);
//        	  	  int ret=m_scT.RecordScanValue(ScanRequestCopyTmp);
//        	  	  NS_LOG_UNCOND("" << Simulator::Now() << ": Now this ScanRequestCopyTmp is " << ret << " and it has tag value " << m_scT.GetLastScan() << " and channel I am going to re-associate is " << m_originalChannel);
//        	  	  Simulator::Schedule(Seconds(0),&StaWifiMac::Enqueue,this,ScanRequestCopyTmp,ScanRequestCopyTo);//Enqueue(ScanRequestCopyTmp,ScanRequestCopyTo);

        		  m_ScanningOrNot = 1;
//        		  lastScanRequestTime=Simulator::Now();
//        		  scanningPeriod=Seconds(2);

        	  }
        	  else if(m_scanT.GetLastScan() == 3) //meaning it is time to recover connection - we turn m_ScanningOrNot next time we successfully associate with an AP
        	  {
        		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_aboutToResetScanningOrNot is set to 1.");
        		  m_aboutToResetScanningOrNot = 1;
        	  }
        	  //port number tag debugging
        	  if(SwitchAPCopy == 0)
        	  {
        		  SwitchAPCopy=packet->Copy();
        		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": SwitchAPCopy copied on " << GetAddress());
        		  SwitchAPCopyFrom=hdr->GetAddr3();
        		  SwitchAPCopyTo=hdr->GetAddr1();
        	  }
        	  if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
        	  {
        		  m_fpt_tmp.RecordFakePktValue(packet);
    			  m_fmt_tmp.RecordFakeMacValue(packet);
    			  m_apt_tmp.RecordAlterPathValue(packet);
    			  //m_dst.RecordDelaySamplesValue(packet,tmp);
    			  m_ptlt_checker_tmp.RecordProbeTrainLengthValue(packet);
    			  m_ptt_checker_tmp.RecordProbeTimeValue(packet);

        		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Received A Packet with Port Tag: " << m_ptg.GetLastPort() << " From " << hdr->GetAddr2() << " m_fpt: " << m_fpt_tmp.GetLastFakePkt() << " m_fmt: " << m_fmt_tmp.GetLastFakeMac() << " m_apt: " << m_apt_tmp.GetLastAlterPath() << "m_ptlt_checker: " << m_ptlt_checker_tmp.GetLastProbeTrainLength() << " m_ptt_checker: " << m_ptt_checker_tmp.GetLastProbeTime() << " UID" << packet->GetUid());
        		  if(m_ptg.GetLastPort() == 9 && app1Copy == 0)
        		  {
        			  app1Copy=packet->Copy();
        			  app1From =hdr->GetAddr3();
        			  app1To=hdr->GetAddr1();
        			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "app1Copy Copied. Current context is " << Simulator::GetContext());
        			  //m_ot.WriteOpValue(app1Copy, 3);
        	          //ForwardUp (app1Copy, app1From, app1To);
        		  }
        		  //stop app2 and app3 temporarily
        		  else if(m_ptg.GetLastPort() == 11 && appTriggeringState == 0)
        		  {
        			  app2Copy=packet->Copy();
        			  app2From =hdr->GetAddr3();
        			  app2To=hdr->GetAddr1();
        			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "app2Copy Copied. Current context is " << Simulator::GetContext());
        			  app2CopyTmp=app2Copy->Copy();
        			  m_ot.WriteOpValue(app2CopyTmp, 3);
        	          ForwardUp (app2CopyTmp, app2From, app2To);
        		  }
        		  /*else if(m_ptg.GetLastPort() == 12)
        		  {
        			  app3Copy=packet->Copy();
        			  app3From =hdr->GetAddr3();
        			  app3To=hdr->GetAddr1();
        			  NS_LOG_UNCOND ("app3Copy Copied.");
        			  app3CopyTmp=app3Copy->Copy();
        			  m_ot.WriteOpValue(app3CopyTmp, 3);
        	          ForwardUp (app3CopyTmp, app3From, app3To);
        		  }*/
        		  //if(app2Copy != 0 && app3Copy != 0)
        		  if(app2Copy != 0 && appTriggeringState == 0)
        		  {
        			  //it is time to go to state 1 now.
        			  //stop app2, stop app3, resume app1
        	          //NS_LOG_UNCOND ("" << Simulator::Now() << "start appTriggering State: 0->1.");
        			  appTriggeringState = 1;
        			  //clean everything here, guaranteeing that when FSD starts, there is no residual APP2 packets
        			  //dequeue everything first, then refill queue with packets from APP1
        			  m_dca_queue=m_dca->GetQueue();
        			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "filtering all packets in m_dca's queue and queue length is " << m_dca_queue->GetSize());
        	          while(!m_dca_queue->IsEmpty())
        	           {
    					  WifiMacHeader hdr_tmp;
    					  Ptr<const Packet> pkt_tmp;
    					  PortTagger m_ptg_tmp;
    					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
					  hdr_tmp.SetAddr1 (GetBssid ());	
 					  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
   					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

    					  m_ptg_tmp.RecordPortValue(pkt_tmp);
    					  if(m_ptg_tmp.GetLastPort() != 9) //we need to ignore this packet
    					  {
    						  continue;
    					  }
    					  else
    					  {
    						  tmpQueueAPP1->Enqueue(pkt_tmp, hdr_tmp_tmp);
    					  }

        	           }
        	          while(!tmpQueueAPP1->IsEmpty())
        	          {
    					  WifiMacHeader hdr_tmp;
    					  Ptr<const Packet> pkt_tmp;
    					  PortTagger m_ptg_tmp;
    					  pkt_tmp=tmpQueueAPP1->Dequeue(&hdr_tmp);
					  hdr_tmp.SetAddr1 (GetBssid ());	
 					  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
   					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;
    					  m_dca_queue->Enqueue(pkt_tmp, hdr_tmp_tmp);

        	          }
        			  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "filtering all packets in m_dca's queue and now queue length is " << m_dca_queue->GetSize());

        			  app2CopyTmp = app2Copy->Copy();
        			  //app3CopyTmp = app3Copy->Copy();
        			  m_ot.WriteOpValue(app2CopyTmp, 3);
        	          ForwardUp (app2CopyTmp, app2From, app2To);
        	          //m_ot.WriteOpValue(app3CopyTmp, 3);
        	          //ForwardUp (app3CopyTmp, app3From, app3To);
        	          NS_LOG_UNCOND_YB ("" << Simulator::Now() << "appTriggering State: 0->1.");
        		  }

        	  }
        	  if(Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
        	  {
    //        	  //Added by Y.B.
            	   m_fpt.RecordFakePktValue(packet);
            	   m_stat.RecordStatisticsValue(packet);
            	   m_pt.RecordProbeValue(packet);
            	   m_ht.RecordHopValue(packet);
            	   //m_sht.RecordSecondHopValue(packet);
            	   NS_LOG_UNCOND_YB ("Entered into Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne.");

            	   /*if(m_sht.GetLastSecondHop() != 0)
            	   {
            		   NS_LOG_UNCOND("END-OF-TRAIN-ACK RECEIVED ON RECEIVER END.");
            		   //m_endTrainOrNot_ACK = 1;
            	   }*/

            	   if(m_fpt.GetLastFakePkt() != 0) //It is feedback packet
            	   {
            		   //uint64_t tmp[2];
            		   m_fmt.RecordFakeMacValue(packet);
            		   m_apt.RecordAlterPathValue(packet);
            		   //m_dst.RecordDelaySamplesValue(packet,tmp);
            		   m_ptlt_checker.RecordProbeTrainLengthValue(packet);
            		   m_ptt_checker.RecordProbeTimeValue(packet);

            		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": local triggering time " << triggeringTime << " and m_ptt_checker is " << m_ptt_checker.GetLastProbeTime());
            		   if(m_ptlt_checker.GetLastProbeTrainLength() != ACTUAL_PROBE_TRAIN_LENGTH
            				   || m_ptt_checker.GetLastProbeTime() != triggeringTime)
            		   {
            			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Received Out Of Date Feedback Packets: m_fmt: " << m_fmt.GetLastFakeMac() << " m_apt: " << m_apt.GetLastAlterPath() << " m_ptlt_checker: " << m_ptlt_checker.GetLastProbeTrainLength() << " m_ptt_checker: " << m_ptt_checker.GetLastProbeTime());
            			   return;
            		   }
            		   //m_endTrainOrNot_ACK = 1;
            		   //NS_LOG_UNCOND ("END TRAIN RESET.");
            		   //NS_LOG_UNCOND ("Received Feedback Packets: m_fmt: " << m_fmt.GetLastFakeMac() << " m_apt: " << m_apt.GetLastAlterPath() << " m_ptlt_checker: " << m_ptlt_checker.GetLastProbeTrainLength() << " m_ptt_checker: " << m_ptt_checker.GetLastProbeTime());


            		   NS_LOG_UNCOND_YB ("ENTEREND INTO m_fpt.GetLastFakePkt() != 0 PHASE: " << m_fmt.GetLastFakeMac());
            	       if(m_apt.GetLastAlterPath() == 0)
            		   //if(hdr->GetAddr2() == m_macPathOne)
            	       {
            	    	   //NS_LOG_UNCOND ("m_dst:" << tmp[0] << " " << tmp[1]);
            	    	   if(feedbackPktCurrentRun_one == 1)
            	    	   {
            	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Already Got This Feedback Packet for Path 1. Ignore.");
            	    		   return;
            	    	   }
            	    	   else
            	    	   {
            	    		   m_endTrainOrNot_ACK = 1;
            	    	   }
            	    	   feedbackPktCurrentRun_one = 1;
            	    	   m_dst.RecordDelaySamplesValue(packet,m_samples_one); //original path

            	    	   NS_LOG_UNCOND_YB("" << Simulator::Now() << "XXXX: AP1" << currentAP1TrainLen+ACTUAL_PROBE_TRAIN_LENGTH << "\n");
            	    	   for(xxxx=currentAP1TrainLen;xxxx<currentAP1TrainLen+ACTUAL_PROBE_TRAIN_LENGTH;xxxx++)
            	    	   {
            	    		   m_totalSamples_one[xxxx]=m_samples_one[xxxx-currentAP1TrainLen];
            	    		   NS_LOG_UNCOND_YB("" << Simulator::Now() << "XXXX:m_totalSamples_one[" << xxxx << "]:" << m_totalSamples_one[xxxx] << "\n");
            	    	   }
            	    	   currentAP1TrainLen=currentAP1TrainLen+ACTUAL_PROBE_TRAIN_LENGTH;

            	    	   m_oneFilled=1;
            	    	   //m_macPathOne = m_fmt.GetLastFakeMac();
            	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FEEDBACK PACKET FOR ASSOCIATED PATH RECEIVED WITH MAC ADDR: " << m_macPathOne << " AND PKTLEN=" << packet->GetSize() << " AND MARKS:" << m_oneFilled << " " << m_twoFilled << " UID " << packet->GetUid());
            	    	   //NS_LOG_UNCOND ("FSDTurn:" << probeTime << ": case1: m_dst:" << m_samples_one[0] << " " << m_samples_one[1] << " " << m_samples_one[2] << " " << m_samples_one[3] << " " << m_samples_one[4] << " " << m_samples_one[5]);
            	    	   if(kk_triggeringTime_one != triggeringTime || kk_probeTime_one != probeTime)
            	    	   {
                	    	   for(int kk = 0; kk < ACTUAL_PROBE_TRAIN_LENGTH; kk++)
                	    	   {
                	    		   NS_LOG_UNCOND_YB ("FSDTurn:" << triggeringTime << "-" << probeTime << ": case1: m_dst1 result: " << m_samples_one[kk]);
                	    	   }
                	    	   kk_triggeringTime_one = triggeringTime;
                	    	   kk_probeTime_one = probeTime;

            	    	   }
            	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "AlterPath is 0: FSDTurn: " << triggeringTime << "-" << probeTime << ": m_macPathResult is "<< m_macPathResult);
            	    	   m_macPathResult = m_macPathTwo;
            	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_twoFilled==0: FeedbackPkt: m_macPathResult becomes" << m_macPathResult);
           	    		   m_alterOrNot = 1;
           	    		   m_feedback_ACK = 1;
           	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_feedback_ACK now becomes " << m_feedback_ACK);
           	    		   if(m_ht.GetLastHop() == 1)
           	    		   {
    						   //switch channel - newly added in Feb.23rd
    						   //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
    						   //wifiPhyTmp->SetChannelNumber(6);
           	    	    	   SetState(BEACON_MISSED);
           	   	    		   SetBssid (m_macPathResult);
           	   	    		   SetSsid (ssidTwo);
           	    			   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathTwo_channel);

           	    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":Wired I am called for m_ht.GetLastHop()==1 here");
           	   	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": m_ht.GetLastHop() == 1: AlterPath is 0: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
           	   	    		   TryToEnsureAssociated();
           	   	    		   //m_endTrainOrNot = 0;
           	   	    		   //m_endTrainOrNot_ACK = 0;
           	   	    		   //m_feedback_ACK_ACK = 0;
           	   	    		   //m_feedback_ACK = 0;

           	    		   }
           	    		   //Correction
           	    		   //m_ot.WriteOpValue(packet,1);
           	    		   m_ot.WriteOpValue(packet,m_macPathTwo_IPRouting);
           	    		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

            			   //stop app2, start app3
            			   appTriggeringState = 3;
             	           //Get m_dca's queue, and put all packets within it into tmpQueue2
             	           m_dca_queue=m_dca->GetQueue();
             	           NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 15 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
             	           while(!m_dca_queue->IsEmpty())
             	           {
         					  WifiMacHeader hdr_tmp;
         					  Ptr<const Packet> pkt_tmp;
         					  EnqueueTimeTagger m_ettg;
         					  EnqueueTimeTagger m_ettg_tmp;
         					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
         					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

         					  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());
         					  m_ettg_tmp.RecordEqTime(pkt_tmp);

         					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 16 appTriggeringState now is " << appTriggeringState << " and we are about to insert one packet to tmpQueueAPP2 with UID " << pkt_tmp->GetUid() << " EnqueueTime " << m_ettg_tmp.GetLastRecordedEnqueueTime());
         					  tmpQueueAPP2->Enqueue(pkt_tmp, hdr_tmp_tmp);
             	           }

            			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "appTriggering State: 2->3.");

             	           //newly added
            			   //app2CopyTmp = app2Copy->Copy();
             	           //m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
             	           //ForwardUp (app2CopyTmp, app2From, app2To);
             	           //NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");

           	    		   //m_dca->m_queue->Flush();
           	    		   //Ptr<WifiMacQueue>tmp = m_dca->GetObject<WifiMacQueue>();
           	    		   //tmp->Flush();

            	       }
            	       else //if(hdr->GetAddr2() == m_macPathTwo)
            	       {
            	    	   //NS_LOG_UNCOND ("m_dst:" << tmp[0] << " " << tmp[1]);
            	    	   if(feedbackPktCurrentRun_two == 1)
            	    	   {
            	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": Already Got This Feedback Packet for Path 2. Ignore.");
            	    		   return;
            	    	   }
            	    	   else
            	    	   {
            	    		   m_endTrainOrNot_ACK = 1;
            	    	   }

            	    	   feedbackPktCurrentRun_two = 1;
                		   //m_endTrainOrNot_ACK = 1;
              	    	   m_dst.RecordDelaySamplesValue(packet,m_samples_two);

            	    	   NS_LOG_UNCOND_YB("" << Simulator::Now() << "XXXX: AP2" << currentAP2TrainLen+ACTUAL_PROBE_TRAIN_LENGTH << "\n");
            	    	   for(xxxx=currentAP2TrainLen;xxxx<currentAP2TrainLen+ACTUAL_PROBE_TRAIN_LENGTH;xxxx++)
            	    	   {
            	    		   m_totalSamples_two[xxxx]=m_samples_two[xxxx-currentAP2TrainLen];
            	    		   NS_LOG_UNCOND_YB("" << Simulator::Now() << "XXXX:m_totalSamples_two[" << xxxx << "]:" << m_totalSamples_two[xxxx] << "\n");
            	    	   }
              	    	   currentAP2TrainLen=currentAP2TrainLen+ACTUAL_PROBE_TRAIN_LENGTH;

               	    	   m_twoFilled=1;
               	    	   //m_macPathTwo = m_fmt.GetLastFakeMac();
            	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FEEDBACK PACKET FOR ALTERNATIVE PATH RECEIVED WITH MAC ADDR: " << m_macPathTwo << " AND PKTLEN=" << packet->GetSize() << " AND MARKS:" << m_oneFilled << " " << m_twoFilled);
             	    	   //NS_LOG_UNCOND ("FSDTurn:" << probeTime << ": case2: m_dst:" << m_samples_two[0] << " " << m_samples_two[1] << " " << m_samples_two[2] << " " << m_samples_two[3] << " " << m_samples_two[4] << " " << m_samples_two[5]);
             	    	  if(kk_triggeringTime_two != triggeringTime || kk_probeTime_two != probeTime)
             	    	  {
                	    	  for(int gg = 0; gg < ACTUAL_PROBE_TRAIN_LENGTH; gg++)
                 	    	  {
                 	    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "AlterPath is 1: FSDTurn:" << triggeringTime << "-" << probeTime << ": case2: m_dst2 result: " << m_samples_two[gg]);
                 	    	  }
                	    	  kk_triggeringTime_two = triggeringTime;
                	    	  kk_probeTime_two = probeTime;

             	    	  }
             	    	  /*Mac48Address macTmp;
             	    	  if(GetBssid() == "00:00:00:00:00:05") //vert dirty hack - need to improve in the future
             	    	  {
             	    		  macTmp = "00:00:00:00:00:06";
             	    		  NS_LOG_UNCOND ("" << Simulator::Now() << "AlterPath=1: FeedbackPkt: macTmp becomes 00:00:00:00:00:06");

             	    	  }
             	    	  else
             	    	  {
             	    		  macTmp = "00:00:00:00:00:05";
             	    		  NS_LOG_UNCOND ("" << Simulator::Now() << "AlterPath=1: FeedbackPkt: macTmp becomes 00:00:00:00:00:05");

             	    	  }
             	    	  m_macPathResult = macTmp;*/
             	    	  /*if(m_macPathResult == "00:00:00:00:00:06")
             	    	  {
             	    		  m_macPathResult = "00:00:00:00:00:05";
             	    		  NS_LOG_UNCOND ("" << Simulator::Now() << "AlterPath=1: FeedbackPkt: m_macPathResult becomes " << m_macPathResult);
             	    	  }*/
             	    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FSDTurn: " << triggeringTime << "-" << probeTime << ": m_macPathResult is "<< m_macPathResult);
             	    	  //m_ot.WriteOpValue(packet,1);
         	    		  //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
          	    		  //if(m_oneFilled == 0)
          	    		  //{
          	   	    		  //probeTrainCounter = 0;
             	    		  m_macPathResult = m_macPathOne;
             	    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_oneFilled: FeedbackPkt: m_macPathResult becomes " << m_macPathResult);
          	    		  //}
          	    		  //m_pst.ResetLastProbeSeq();
             	    	  m_alterOrNot = 0;
             	    	  m_feedback_ACK = 2;
             	    	  NS_LOG_UNCOND_YB ("m_feedback_ACK now becomes " << m_feedback_ACK);
             	    	  if(m_ht.GetLastHop() == 1)
             	    	  {
             				  //switch channel - newly added in Feb.23rd
             				  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
             				  //wifiPhyTmp->SetChannelNumber(1);

                 	    	  SetState(BEACON_MISSED);
              	    		  SetBssid (m_macPathResult);
              	    		  SetSsid (ssidOne);
             				  //DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(1);

              	    		  NS_LOG_UNCOND_YB("" << Simulator::Now() << ":Wired I am called for m_ht.GetLastHop()==1 there");
              	    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Alter Path is Not 0: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
              	    		  TryToEnsureAssociated();
              	    		  //m_endTrainOrNot = 0;
          	   	    		  //m_endTrainOrNot_ACK = 0;
              	    		  //m_feedback_ACK_ACK = 0;
          	   	    		  //m_feedback_ACK = 0;

             	    	  }
             	    	  //Since 06 is always the second one to be probed, we do not need to re-associate now.
             	    	  //m_ot.WriteOpValue(packet,1);
             	    	  //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
             	    	  //Ptr<WifiMacQueue>tmp = m_dca->GetObject<WifiMacQueue>();
             	    	  //tmp->Flush();
             	    	  //m_dca->m_queue->Flush();


            	       }
               	       if(m_oneFilled == 1 && m_twoFilled == 1)
               	       {
               	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "ALL SAMPLES FROM TWO PATHS GATHERED. START FSD ALGORITHM: CurrentAP1TrainLen: " << currentAP1TrainLen << " CurrentAP2TrainLen: " << currentAP2TrainLen);
               	    	   //m_pathDecision = m_fdc.CalculateFSDRelationship(m_samples_one, m_samples_two);
               	    	   //m_probeOrNot = 0;
               	    	   int size[1];

               	    	   //size[0] = ACTUAL_PROBE_TRAIN_LENGTH;
               	    	   size[0] = currentAP1TrainLen > currentAP2TrainLen ? currentAP2TrainLen: currentAP1TrainLen;

               	    	   //newly added to handle with packet loss - -1 since it starts from 0
               	    	   resortSamples(m_totalSamples_one,currentAP1TrainLen-1);
               	    	   resortSamples(m_totalSamples_two,currentAP2TrainLen-1);

               	    	   //m_pathDecision = m_fdc.CalculateFSDRelationship(m_samples_one, m_samples_two, size, 15, 0.05, 20); //1610612735.625 - infty value //1500000 and 3500000 //60370000
               	    	   m_pathDecision = m_fdc.CalculateFSDRelationship(m_totalSamples_one, m_totalSamples_two, size, 18980000, 0.05, 10); //129750000 //18980000 //1610612735.625 - infty value //1500000 and 3500000 //60370000

               	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_pathDecision now becomes " << m_pathDecision);
               	    	   probeTime = probeTime + 1;

               	    	   if(m_pathDecision == 0)
               	    	   {
               	    		   /*for(int kk = 0; kk < ACTUAL_PROBE_TRAIN_LENGTH; kk++)
               	    		   {
               	    			   NS_LOG_UNCOND ("" << Simulator::Now() << "FSDTurn:" << triggeringTime << "-" << probeTime << ": case1: m_dst1 result: " << m_samples_one[kk]);
               	    		   }
               	    		   for(int gg = 0; gg < ACTUAL_PROBE_TRAIN_LENGTH; gg++)
               	    		   {
               	    		       NS_LOG_UNCOND ("" << Simulator::Now() << "FSDTurn:" << triggeringTime << "-" << probeTime << ": case2: m_dst2 result: " << m_samples_two[gg]);
               	    		   }
               	    		   */
               	    		   //NS_LOG_UNCOND ("" << Simulator::Now() << "FSDTurn: " << triggeringTime << "-" << probeTime << ": ALTERNATIVE PATH" << m_macPathTwo << " NOT FSD ORIGINAL PATH" << m_macPathOne <<".");

    				NS_LOG_UNCOND_YB ("" << Simulator::Now() << "INITIAL_PROBE_TRAIN_LENGTH: " << INITIAL_PROBE_TRAIN_LENGTH << ", INCREASE_STEP: " << INCREASE_STEP << "MAX_PROBE_TRAIN_LENGTH:" << MAX_PROBE_TRAIN_LENGTH << "START TIME:" << lastTriggerTime << "FSDTurn: " << triggeringTime << "-" << probeTime << ": ALTERNATIVE PATH" << m_macPathTwo << "NOT FSD ORIGINAL PATH" << m_macPathOne << " WITH M_MACPATHRESULT: " << m_macPathResult);


               	    		   m_probeOrNot = 0;
    //				   Simulator::Stop();


               	    		   //Correction
               	    		   //m_ot.WriteOpValue(packet,1);
               	    		   m_ot.WriteOpValue(packet,m_macPathOne_IPRouting);
               	    		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

               	    		   m_macPathResult = m_macPathOne;

               				   //switch channel - newly added in Feb.23rd
               				   //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
               				   //wifiPhyTmp->SetChannelNumber(1);

           	    	    	   SetState(BEACON_MISSED);
               	    		   SetBssid (m_macPathResult);
               	    		   SetSsid (ssidOne);
                			   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathOne_channel);

               	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Alter Path is Not 0: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
               	    		   TryToEnsureAssociated();

               	    		   //probeTrainCounter = 0;
               	    		   //m_pst.ResetLastProbeSeq();
               	    		   m_triggerOrNot = 0;
               	    		   memset(m_samples_one,0,sizeof(m_samples_one));
               	    		   memset(m_samples_two,0,sizeof(m_samples_two));
               	    		   m_oneFilled = 0;
               	    		   m_twoFilled = 0;

               	    		   //Mac48Address macTmp;
               	    		   //m_macPathOne = "00:00:00:00:00:00";
               	    		   //m_macPathTwo = "00:00:00:00:00:00";

               	    		   ACTUAL_PROBE_TRAIN_LENGTH = INITIAL_PROBE_TRAIN_LENGTH;
               	    		   probeTime = 0;
               	    		   triggeringTime = (triggeringTime+1) * 10;
               	    		   m_feedback_ACK = 0;
               	    		   //m_feedback_ACK_ACK = 0;
               	    		   kk_triggeringTime_one = -1;
               	    		   kk_probeTime_one = -1;
               	    		   kk_triggeringTime_two = -1;
               	    		   kk_probeTime_two = -1;

               	    		   feedbackPktCurrentRun_one = 0;
               	    		   feedbackPktCurrentRun_two = 0;

                			   //stop app3, resume app1
                			   appTriggeringState = 1;
                			   //dequeue all packets from m_dca's queue into tmpAPPQueue3
                			   m_dca_queue=m_dca->GetQueue();
                			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 17 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
                 	           while(!m_dca_queue->IsEmpty())
                 	           {
             					  WifiMacHeader hdr_tmp;
             					  Ptr<const Packet> pkt_tmp;
             					  EnqueueTimeTagger m_ettg;
             					  EnqueueTimeTagger m_ettg_tmp;
             					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
             					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

             					  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());
             					  m_ettg_tmp.RecordEqTime(pkt_tmp);

             					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 18 appTriggeringState now is " << appTriggeringState << " and we are about to insert one packet to tmpQueueAPP2 with UID " << pkt_tmp->GetUid() << " EnqueueTime " << m_ettg_tmp.GetLastRecordedEnqueueTime());

             					  tmpQueueAPP3->Enqueue(pkt_tmp, hdr_tmp_tmp);
                 	           }

                			   //NS_LOG_UNCOND ("" << Simulator::Now() << "appTriggering State: 2->3.");


                			   //app1CopyTmp = app1Copy->Copy();
                			   app2CopyTmp = app2Copy->Copy();
                			   //app3CopyTmp = app3Copy->Copy();
                 			   //m_ot.WriteOpValue(app1CopyTmp, 4);
                 			   //ForwardUp(app1CopyTmp, "00:00:00:00:00:06", "00:00:00:00:00:07");
                 			   m_ot.WriteOpValue(app2CopyTmp, 3);
                 	           //ForwardUp (app2CopyTmp, app2From, app2To);
                 			   ForwardUp(app2CopyTmp, "00:00:00:00:00:06", "00:00:00:00:00:07");
                 			   //m_ot.WriteOpValue(app3CopyTmp, 3);
                 	           //ForwardUp (app3CopyTmp, app3From, app3To);
                 	           NS_LOG_UNCOND_YB ("" << Simulator::Now() << "appTriggering State: 3->1.");

                 	           //newly added to stop downlink traffic when FSD algorithm is probing
                 	    	   Ptr<Packet> stPkt_tmp=stopTrafficPkt->Copy();
                 	    	   m_stt.WriteStopTrafficValue(stPkt_tmp,2);//1: pause, 2:resume
                   	           ForwardUp (stPkt_tmp, stopTrafficFrom, stopTrafficTo);

                 	           //newly added
                			   //app2CopyTmp = app2Copy->Copy();
                 	           //m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
                 	           //ForwardUp (app2CopyTmp, app2From, app2To);
                 	           //NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");


               	    	   }
               	    	   else if(m_pathDecision == 1)
               	    	   {
               	    		//HwmpProtocol::RenewRoutingNextHop (Mac48Address destination, Mac48Address retransmitter, uint32_t interface,
               	    		//   	    		          uint32_t metric, Time lifetime, uint32_t seqnum)
               	    		   //Mac48Address destination = "00:00:00:00:00:02"; //Very Dirty Hacking - Need to be Improved In the Future
               	    		   //Time lifetime = Seconds(1000);
               	    		   //ForwardUp
               	    		   for(int kk = 0; kk < ACTUAL_PROBE_TRAIN_LENGTH; kk++)
               	    		   {
               	    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FSDTurn:" << triggeringTime << "-" << probeTime << ": case1: m_dst1 result: " << m_samples_one[kk]);
               	    		   }
               	    		   for(int gg = 0; gg < ACTUAL_PROBE_TRAIN_LENGTH; gg++)
               	    		   {
               	    		       NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FSDTurn:" << triggeringTime << "-" << probeTime << ": case2: m_dst2 result: " << m_samples_two[gg]);
               	    		   }

               	    		   //m_ot.WriteOpValue(packet,1);
               	    		   //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
               	    		   //m_macPathResult = m_macPathTwo;
               	    		   //NS_LOG_UNCOND ("" << Simulator::Now() << "FSDTurn: " << triggeringTime << "-" << probeTime << ": ALTERNATIVE PATH" << m_macPathTwo << " FSD ORIGINAL PATH" << m_macPathOne << " WITH M_MACPATHRESULT: " << m_macPathResult);
    NS_LOG_UNCOND_YB ("" << Simulator::Now() << "INITIAL_PROBE_TRAIN_LENGTH: " << INITIAL_PROBE_TRAIN_LENGTH << ", INCREASE_STEP: " << INCREASE_STEP << "MAX_PROBE_TRAIN_LENGTH:" << MAX_PROBE_TRAIN_LENGTH << "START TIME:" << lastTriggerTime << "FSDTurn: " << triggeringTime << "-" << probeTime << ": ALTERNATIVE PATH" << m_macPathTwo << " FSD ORIGINAL PATH" << m_macPathOne << " WITH M_MACPATHRESULT: " << m_macPathResult);


               	    		   m_probeOrNot = 0;
    //				   Simulator::Stop();
               	    		   //Correction
               	    		   //m_ot.WriteOpValue(packet,1);
               	    		   m_ot.WriteOpValue(packet,m_macPathTwo_IPRouting);
               	    		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

               	    		   m_macPathResult = m_macPathTwo;

               				  //switch channel - newly added in Feb.23rd
               				  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
               				  //wifiPhyTmp->SetChannelNumber(6);

           	    	    	   SetState(BEACON_MISSED);
               	    		   SetBssid (m_macPathResult);
               	    		   SetSsid (ssidTwo);
                			   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathTwo_channel);

               	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "Alter Path is Not 0: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
               	    		   TryToEnsureAssociated();

               	    		   //probeTrainCounter = 0;
               	    		   //m_pst.ResetLastProbeSeq();
               	    		   m_triggerOrNot = 0;
               	    		   memset(m_samples_one,0,sizeof(m_samples_one));
               	    		   memset(m_samples_two,0,sizeof(m_samples_two));
               	    		   m_oneFilled = 0;
               	    		   m_twoFilled = 0;

               	    		   //switch parameters for two paths
               	    		   Mac48Address macTmp;
               	    		   macTmp=m_macPathOne;
               	    		   m_macPathOne=m_macPathTwo;
               	    		   m_macPathTwo=macTmp;

               	    		   int channelTmp;
               	    		   channelTmp=m_macPathOne_channel;
               	    		   m_macPathOne_channel=m_macPathTwo_channel;
               	    		   m_macPathTwo_channel=channelTmp;

               	    		   Ssid ssidTmp;
               	    		   ssidTmp=ssidOne;
               	    		   ssidOne=ssidTwo;
               	    		   ssidTwo=ssidTmp;

               	    		   int ipRoutingTmp;
               	    		   ipRoutingTmp=m_macPathOne_IPRouting;
               	    		   m_macPathOne_IPRouting=m_macPathTwo_IPRouting;
               	    		   m_macPathTwo_IPRouting=ipRoutingTmp;

               	    		   //m_macPathOne = "00:00:00:00:00:00";
               	    		   //m_macPathTwo = "00:00:00:00:00:00";

               	    		   ACTUAL_PROBE_TRAIN_LENGTH = INITIAL_PROBE_TRAIN_LENGTH;
               	    		   //SetState(BEACON_MISSED);
               	    		   //SetBssid (m_macPathResult);
               	    		   //SetSsid (ssidTwo);
           	   	    		   //NS_LOG_UNCOND ("FSDResult: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
               	    		   //TryToEnsureAssociated();
               	    		   //m_ot.WriteOpValue(packet,1);
               	    		   //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
               	    		   probeTime = 0;
               	    		   triggeringTime = (triggeringTime+1) * 10;
               	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": PROBE VALUE RESET. BEACON_MISSED SET. PROBETRAINCOUNTER RESET. BSSID NOW IS " << GetBssid());
               	    		   m_feedback_ACK = 0;
               	    		   //m_feedback_ACK_ACK = 0;
               	    		   kk_triggeringTime_one = -1;
               	    		   kk_probeTime_one = -1;
               	    		   kk_triggeringTime_two = -1;
               	    		   kk_probeTime_two = -1;
               	    		   //m_ot.WriteOpValue(packet,1);
               	    		   //ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
               	    		   //m_dca->m_queue->Flush();
               	    		   //Ptr<WifiMacQueue>tmp = m_dca->GetObject<WifiMacQueue>();
               	    		   //tmp->Flush();
               	    		   feedbackPktCurrentRun_one = 0;
               	    		   feedbackPktCurrentRun_two = 0;

                			   //stop app3, resume app1
                			   appTriggeringState = 1;
                			   //dequeue all packets from m_dca's queue into tmpAPPQueue3
                			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 19 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
                			   m_dca_queue=m_dca->GetQueue();
                 	           while(!m_dca_queue->IsEmpty())
                 	           {
             					  WifiMacHeader hdr_tmp;
             					  Ptr<const Packet> pkt_tmp;
             					  EnqueueTimeTagger m_ettg;
             					  EnqueueTimeTagger m_ettg_tmp;
             					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
						  hdr_tmp.SetAddr1 (GetBssid ());
						  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
             					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

             					  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());
             					  m_ettg_tmp.RecordEqTime(pkt_tmp);

             					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 20 appTriggeringState now is " << appTriggeringState << " and we are about to insert one packet to tmpQueueAPP2 with UID " << pkt_tmp->GetUid() << " EnqueueTime " << m_ettg_tmp.GetLastRecordedEnqueueTime());

             					  tmpQueueAPP3->Enqueue(pkt_tmp, hdr_tmp_tmp);
                 	           }

                			   //app1CopyTmp = app1Copy->Copy();
                			   app2CopyTmp = app2Copy->Copy();
                			   //app3CopyTmp = app3Copy->Copy();
                 			   //m_ot.WriteOpValue(app1CopyTmp, 4);
                 			   //ForwardUp(app1CopyTmp, "00:00:00:00:00:06", "00:00:00:00:00:07");
                 			   m_ot.WriteOpValue(app2CopyTmp, 3);
                 	           //ForwardUp (app2CopyTmp, app2From, app2To);
                 			   ForwardUp(app2CopyTmp, "00:00:00:00:00:06", "00:00:00:00:00:07");
                 	           NS_LOG_UNCOND_YB ("" << Simulator::Now() << "m_pathDecision==1: packets forwarded.");
                 	           //m_ot.WriteOpValue(app3CopyTmp, 3);
                 	           //ForwardUp (app3CopyTmp, app3From, app3To);

                 	           //newly added to stop downlink traffic when FSD algorithm is probing
                 	    	   Ptr<Packet> stPkt_tmp=stopTrafficPkt->Copy();
                 	    	   m_stt.WriteStopTrafficValue(stPkt_tmp,2);//1: pause, 2:resume
                   	           ForwardUp (stPkt_tmp, stopTrafficFrom, stopTrafficTo);


                 	           //newly added
                			   //app2CopyTmp = app2Copy->Copy();
                 	           //m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
                 	           //ForwardUp (app2CopyTmp, app2From, app2To);
                 	           //NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");


               	    	   }
               	    	   else
               	    	   {
               	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "CANNOT REACH TO A FSD RESULT: NEED MORE DATA.");
               	    		   //if(ACTUAL_PROBE_TRAIN_LENGTH + INCREASE_STEP < MAX_PROBE_TRAIN_LENGTH)
    				   if(currentAP1TrainLen+ACTUAL_PROBE_TRAIN_LENGTH < MAX_BUFFER_LENGTH && currentAP2TrainLen+ACTUAL_PROBE_TRAIN_LENGTH < MAX_BUFFER_LENGTH)
               	    		   {
               	    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "FSDTurn: " << triggeringTime << "-" << probeTime << ACTUAL_PROBE_TRAIN_LENGTH << " VS " << MAX_PROBE_TRAIN_LENGTH << ": START A NEW PROBING");
                   	    		   ACTUAL_PROBE_TRAIN_LENGTH = ACTUAL_PROBE_TRAIN_LENGTH + INCREASE_STEP;
                   	    		   m_probeOrNot = 1;
                   	    		   triggeringTime = triggeringTime+1;

                   	    		   //Correction
                   	    		   //m_ot.WriteOpValue(packet,1);
                   	    		   m_ot.WriteOpValue(packet,m_macPathOne_IPRouting);
                   	    		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

                     	           //newly added
                    			   //app2CopyTmp = app2Copy->Copy();
                     	           //m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
                     	           //ForwardUp (app2CopyTmp, app2From, app2To);
                     	           //NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");

                   	    		   m_macPathResult = m_macPathOne;

                   				  //switch channel - newly added in Feb.23rd
                   				  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
                   				  //wifiPhyTmp->SetChannelNumber(1);

                   	    		   SetState(BEACON_MISSED);
                   	    		   SetBssid (m_macPathOne); //very dirty hack
                   	    		   SetSsid (ssidOne);
                    			   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathOne_channel);

                   	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "NEED MORE DATA: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
                   	    		   TryToEnsureAssociated();

                   	    		   //probeTrainCounter = 0;
                   	    		   //m_pst.ResetLastProbeSeq();

                   	    		   m_triggerOrNot = 1;
                   	    		   memset(m_samples_one,0,sizeof(m_samples_one));
                   	    		   memset(m_samples_two,0,sizeof(m_samples_two));
                   	    		   m_oneFilled = 0;
                   	    		   m_twoFilled = 0;

                   	    		   feedbackPktCurrentRun_one = 0;
                   	    		   feedbackPktCurrentRun_two = 0;

               	   	    		   m_feedback_ACK = 0;
                   	    		   //m_feedback_ACK_ACK = 0;
                   	    		   //m_macPathOne = "00:00:00:00:00:00";
                   	    		   //m_macPathTwo = "00:00:00:00:00:00";

                    			   //stop app3, start app2
                    			   appTriggeringState = 2;

                    			   //dequeue all packets from m_dca's queue into tmpAPPQueue3
                    			   m_dca_queue=m_dca->GetQueue();
                    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 21 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
                     	           while(!m_dca_queue->IsEmpty())
                     	           {
                 					  WifiMacHeader hdr_tmp;
                 					  Ptr<const Packet> pkt_tmp;
                 					  EnqueueTimeTagger m_ettg;
                 					  EnqueueTimeTagger m_ettg_tmp;
                 					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
							  hdr_tmp.SetAddr1 (GetBssid ());
							  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
                 					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

                 					  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());
                 					  m_ettg_tmp.RecordEqTime(pkt_tmp);

                 					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 22 appTriggeringState now is " << appTriggeringState << " and we are about to insert one packet to tmpQueueAPP2 with UID " << pkt_tmp->GetUid() << " EnqueueTime " << m_ettg_tmp.GetLastRecordedEnqueueTime());

                 					  tmpQueueAPP3->Enqueue(pkt_tmp, hdr_tmp_tmp);
                     	           }

                    			   //app2CopyTmp = app2Copy->Copy();
                    			   //app3CopyTmp = app3Copy->Copy();
                     			   //m_ot.WriteOpValue(app3CopyTmp, 3);
                     	           //ForwardUp (app3CopyTmp, app3From, app3To);
                     	           //m_ot.WriteOpValue(app2CopyTmp, 4);
                     	           //ForwardUp (app2CopyTmp, app2From, app2To);
                     	           NS_LOG_UNCOND_YB ("" << Simulator::Now() << "appTriggering State: 3->2.");


               	    		   }
               	    		   else
               	    		   {
               	    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":" << ACTUAL_PROBE_TRAIN_LENGTH << " VS " << MAX_PROBE_TRAIN_LENGTH << ": WAIT FOR NEXT TIME.");
               	    			   m_probeOrNot = 0;
    //					   Simulator::Stop();
            				   triggeringTime = triggeringTime+1;
       	    			   	   //probeTrainCounter = 0;
               	    			   //m_pst.ResetLastProbeSeq();

                   	    		   //Correction
                   	    		   //m_ot.WriteOpValue(packet,1);
                   	    		   m_ot.WriteOpValue(packet,m_macPathOne_IPRouting);
                   	    		   ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());

               	    			   m_macPathResult = m_macPathOne;

               	  			  //switch channel - newly added in Feb.23rd
               	  			  //wifiPhyTmp=ns3::DynamicCast<YansWifiPhy>(m_phy);
               	  			  //wifiPhyTmp->SetChannelNumber(1);

               	    	    	   SetState(BEACON_MISSED);
               	   	    		   SetBssid (m_macPathOne); //very dirty hack
               	   	    		   SetSsid (ssidOne);
                    	  		   DynamicCast<YansWifiPhy>(m_phy)->SetChannelNumber(m_macPathOne_channel);

               	   	    		   NS_LOG_UNCOND_YB ("" << Simulator::Now() << "WAIT FOR NEXT TIME: m_macPathResult BSSID: " << GetBssid() << " SSID: " << GetSsid());
               	   	    		   TryToEnsureAssociated();

               	    			   m_triggerOrNot = 0;
               	    			   memset(m_samples_one,0,sizeof(m_samples_one));
               	    			   memset(m_samples_two,0,sizeof(m_samples_two));
               	    			   m_oneFilled = 0;
               	    			   m_twoFilled = 0;
               	    			   //m_macPathOne = "00:00:00:00:00:00";
               	    			   //m_macPathTwo = "00:00:00:00:00:00";
               	    			   ACTUAL_PROBE_TRAIN_LENGTH = INITIAL_PROBE_TRAIN_LENGTH;

               	    			   m_feedback_ACK = 0;
               	    			   //m_feedback_ACK_ACK = 0;
                   	    		   kk_triggeringTime_one = -1;
                   	    		   kk_probeTime_one = -1;
                   	    		   kk_triggeringTime_two = -1;
                   	    		   kk_probeTime_two = -1;

                   	    		   feedbackPktCurrentRun_one = 0;
                   	    		   feedbackPktCurrentRun_two = 0;

                    			   //stop app3, resume app1
                    			   appTriggeringState = 1;
                    			   //dequeue all packets from m_dca's queue into tmpAPPQueue3
                    			   m_dca_queue=m_dca->GetQueue();
                    			   NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 23 appTriggeringState now is " << appTriggeringState << " and we are about to extract one packet from m_dca's queue and queue length is " << m_dca_queue->GetSize());
                     	           while(!m_dca_queue->IsEmpty())
                     	           {
                 					  WifiMacHeader hdr_tmp;
                 					  Ptr<const Packet> pkt_tmp;
                 					  EnqueueTimeTagger m_ettg;
                 					  EnqueueTimeTagger m_ettg_tmp;
                 					  pkt_tmp=m_dca_queue->Dequeue(&hdr_tmp);
							  hdr_tmp.SetAddr1 (GetBssid ());
							  hdr_tmp.SetAddr3 (GetBssid ());//utilize parameter to here!
                 					  const WifiMacHeader hdr_tmp_tmp = hdr_tmp;

                 					  m_ettg.PrepareEqTime(pkt_tmp, Simulator::Now());
                 					  m_ettg_tmp.RecordEqTime(pkt_tmp);

                 					  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " 24 appTriggeringState now is " << appTriggeringState << " and we are about to insert one packet to tmpQueueAPP2 with UID " << pkt_tmp->GetUid() << " EnqueueTime " << m_ettg_tmp.GetLastRecordedEnqueueTime());
                 					  tmpQueueAPP3->Enqueue(pkt_tmp, hdr_tmp_tmp);
                     	           }

                    			   //app1CopyTmp = app1Copy->Copy();
                    			   app2CopyTmp = app2Copy->Copy();
                    			   //app3CopyTmp = app3Copy->Copy();
                     			   //m_ot.WriteOpValue(app1CopyTmp, 4);
                     	           //ForwardUp (app1CopyTmp, app1From, app1To);
                     			   m_ot.WriteOpValue(app2CopyTmp, 3);
                     	           ForwardUp (app2CopyTmp, app2From, app2To);
                     	           //m_ot.WriteOpValue(app3CopyTmp, 3);
                     	           //ForwardUp (app3CopyTmp, app3From, app3To);

                     	           //newly added to stop downlink traffic when FSD algorithm is probing
                     	    	   Ptr<Packet> stPkt_tmp=stopTrafficPkt->Copy();
                     	    	   m_stt.WriteStopTrafficValue(stPkt_tmp,2);//1: pause, 2:resume
                       	           ForwardUp (stPkt_tmp, stopTrafficFrom, stopTrafficTo);

                     	           //newly added
                    			   //app2CopyTmp = app2Copy->Copy();
                     	           //m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
                     	           //ForwardUp (app2CopyTmp, app2From, app2To);
                     	           //NS_LOG_UNCOND ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train.");

               	    		   }

               	    	   }
               	       }
               	       else return;
            	    }
            	    else
            	    {
            	    	   NS_LOG_UNCOND_YB ("" << Simulator::Now() << ":" << GetAddress() << ": FORWARDUP CALLED.");
            	           ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
            	    }
        	  }
        	  else
        	  {
                  ForwardUp (packet, hdr->GetAddr3 (), hdr->GetAddr1 ());
        	  }

            }
        }
      else
        {
    	  ForwardUp(packet, hdr->GetAddr3(),hdr->GetAddr1());
        }
      return;
    }
  else if (hdr->IsProbeReq ()
           || hdr->IsAssocReq ())
    {
      //This is a frame aimed at an AP, so we can safely ignore it.
      NotifyRxDrop (packet);
      return;
    }
  else if (hdr->IsBeacon ())
    {
      MgtBeaconHeader beacon;
      packet->RemoveHeader (beacon);

      NS_LOG_UNCOND_YB ("" << Simulator::Now() << "I am " << GetAddress() << " and I receive BEACON FROM ADDRESS " << hdr->GetAddr3());
      /*if(hdr->GetAddr3() == GetBssid())
      {
    	  NS_LOG_UNCOND ("");
      }*/

      bool goodBeacon = false;
      if (GetSsid ().IsBroadcast ()
          || beacon.GetSsid ().IsEqual (GetSsid ()))
        {
          goodBeacon = true;
          NS_LOG_UNCOND_YB ("" << Simulator::Now()  << "I am " << GetAddress() << " and I think Beacon is a good beacon");
        }
      SupportedRates rates = beacon.GetSupportedRates ();
      for (uint32_t i = 0; i < m_phy->GetNBssMembershipSelectors (); i++)
        {
          uint32_t selector = m_phy->GetBssMembershipSelector (i);
          if (!rates.IsSupportedRate (selector))
            {
              goodBeacon = false;
            }
        }
      if ((IsWaitAssocResp () || IsAssociated ()) && hdr->GetAddr3 () != GetBssid ())
        {
          goodBeacon = false;
        }
      if (goodBeacon)
        {
          Time delay = MicroSeconds (beacon.GetBeaconIntervalUs () * m_maxMissedBeacons);
          RestartBeaconWatchdog (delay);
          SetBssid (hdr->GetAddr3 ());
          NS_LOG_UNCOND_YB ("" << Simulator::Now()  << "I am " << GetAddress() << " and I am trying to reset my Bssid.");
        }
      //Added by Y.B. For Path Switching
      //Original Version
      /*if (goodBeacon && m_state == BEACON_MISSED)
              {
                SetState (WAIT_ASSOC_RESP);
                SendAssociationRequest ();
              }*/
      //Modified Version
      if (goodBeacon && m_state == BEACON_MISSED)
        {
    	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " GOODBEACON AND BEACON_MISSED ");

    	  //Added by Y.B.
    	  if(m_macPathResult != "00:00:00:00:00:00" && hdr->GetAddr3() == m_macPathResult)
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << GetAddress() << "ENTER INTO CASE 1");
              SetState (WAIT_ASSOC_RESP);
              SendAssociationRequest ();
              //m_macPathResult = "00:00:00:00:00:00";
    	  }
    	  else if(m_macPathResult != "00:00:00:00:00:00" && hdr->GetAddr3() != m_macPathResult)
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << GetAddress() << "ENTER INTO CASE 2");
    		  return;
    	  }
    	  else if(m_macPathResult == "00:00:00:00:00:00" && hdr->GetAddr3() == m_macPathResult)
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << GetAddress() << "ENTER INTO CASE 3");
              SetState (WAIT_ASSOC_RESP);
              SendAssociationRequest ();
    	  }
    	  else if(m_macPathResult == "00:00:00:00:00:00" && hdr->GetAddr3() != m_macPathResult)
    	  {
    		  NS_LOG_UNCOND_YB ("" << Simulator::Now() << GetAddress() << "ENTER INTO CASE 4");
    		  SetState (WAIT_ASSOC_RESP);
    		  SendAssociationRequest ();
    	  }

        }
      return;
    }
  else if (hdr->IsProbeResp ())
    {
	  if (m_state == WAIT_PROBE_RESP)
        {
          MgtProbeResponseHeader probeResp;
          packet->RemoveHeader (probeResp);
          NS_LOG_UNCOND_YB ("" << Simulator::Now() << " PROBERESP: " << probeResp.GetSsid() << " : OUR SSID: " << GetSsid() << " : Address: " << hdr->GetAddr3() << " : My Address: " << GetAddress());

         //if (!probeResp.GetSsid ().IsEqual (GetSsid ())) // original version
          if (!probeResp.GetSsid ().IsEqual (GetSsid ()))
            {
              //not a probe resp for our ssid.
              return;
            }
          if(m_macPathResult != "00:00:00:00:00:00" && hdr->GetAddr3() != m_macPathResult && Mac48Address::ConvertFrom(GetAddress()) == m_theChosenOne)
          {
        	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " NOT SATISFY PROBERESP: " << probeResp.GetSsid() << " : OUR SSID: " << GetSsid() << " : Address: " << hdr->GetAddr3() << " : Path Two: " << m_macPathResult);
        	  return;
          }

          //Added by Y.B.
          NS_LOG_UNCOND_YB ("" << Simulator::Now() << " SATISFIED PROBERESP: " << probeResp.GetSsid() << " : OUR SSID: " << GetSsid() << " : Address: " << hdr->GetAddr3() << " : Path Two: " << m_macPathResult);
          /*if(m_macPathTwo != "00:00:00:00:00:00")
          {
        	  m_macPathTwo = "00:00:00:00:00:00";
          }*/

          SupportedRates rates = probeResp.GetSupportedRates ();
          for (uint32_t i = 0; i < m_phy->GetNBssMembershipSelectors (); i++)
            {
              uint32_t selector = m_phy->GetBssMembershipSelector (i);
              if (!rates.IsSupportedRate (selector))
                {
                  return;
                }
            }
          SetBssid (hdr->GetAddr3 ());
          Time delay = MicroSeconds (probeResp.GetBeaconIntervalUs () * m_maxMissedBeacons);
          RestartBeaconWatchdog (delay);
          if (m_probeRequestEvent.IsRunning ())
            {
              m_probeRequestEvent.Cancel ();
            }

          /*int tmp=GetChannelNumber();
          if(m_ProbROrNot == 1 && ChannelID[m_startID] == tmp)
          {
                         NS_LOG_UNCOND("" << Simulator::Now() << ": I have received prob_resp in channel " << tmp << " so I am going to call scan again.");
                         m_startID=m_startID+1;
                     m_scanEvent = Simulator::Schedule (Time("5ms"), &StaWifiMac::Scan, this,
                                 m_startID,0);
          }
          else
          {
              SetState (WAIT_ASSOC_RESP);
              SendAssociationRequest ();
          }*/
	 
          SetState (WAIT_ASSOC_RESP);
          SendAssociationRequest ();
        }
      return;
    }
  else if (hdr->IsAssocResp ())
    {
	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << " IS ASSOC RESP FROM " << hdr->GetAddr3() << " WHILE M_MACPATHRESULT IS " << m_macPathResult << " AND I AM " << GetAddress() << " AND I AM IN STATE " << m_state);
      if (m_state == WAIT_ASSOC_RESP)
        {
          MgtAssocResponseHeader assocResp;
          packet->RemoveHeader (assocResp);
          if (m_assocRequestEvent.IsRunning ())
            {
              m_assocRequestEvent.Cancel ();
            }
          if (assocResp.GetStatusCode ().IsSuccess ())
            {
		int tmpID=m_gat.getAPIDByMacAddr(hdr->GetAddr2());
		ModulationTagger m_mt_tmp;
		m_mt_tmp.RecordModulationValue(packet);
		NS_LOG_UNCOND("" << Simulator::Now() << ": AssocResponse: I am " << GetAddress() << " and m_gat.getAPIDByMacAddr returns " << tmpID << " and hdr->GetAddr2() is " << hdr->GetAddr2() << " and value to be written is " << 1/m_mt_tmp.GetLastModulation());
		m_gat.setLoad(tmpID,1/m_mt_tmp.GetLastModulation());

		Simulator::Cancel(m_scanTimeOutEvent);

		if(m_ScanTagValue == 2 && m_gat.getChannelNumberByAPID((m_startID + 1) % m_gat.getSize()) != m_originalChannel) //meaning we are still in the state of scanning
	  	{
        	 m_startID=m_startID+1;
               	 m_scanEvent=Simulator::ScheduleNow(&StaWifiMac::Scan,this,m_startID, m_originalChannel);	
		 return;
	  	}

              SetState (ASSOCIATED);
              NS_LOG_DEBUG ("assoc completed");
              NS_LOG_UNCOND ("" << Simulator::Now() << ": Assoc Completed on " << GetAddress());

              //If whole re-optimization procedure is done
              if(m_aboutToResetScanningOrNot == 1)
              {
            	  m_ScanningOrNot=0;
            	  NS_LOG_UNCOND("" << Simulator::Now() << ": I am " << GetAddress() << " and m_ScanningOrNot is reset.");
                  Simulator::Cancel(m_timeOutEvent);
              }

              //If channel scan is done
              if(scanDone == 1)
              {
        	  	  Ptr<Packet> ScanRequestCopyTmp=ScanRequestCopy->Copy();
        	  	  ScanTagger m_scT;
        	  	  m_scT.WriteScanValue(ScanRequestCopyTmp,2);
        	  	  int ret=m_scT.RecordScanValue(ScanRequestCopyTmp);
        	  	  NS_LOG_UNCOND("" << Simulator::Now() << ": Now this ScanRequestCopyTmp is " << ret << " and it has tag value " << m_scT.GetLastScan());

        	  	  //write load information stored in the m_gat table into this feedback packet
        	  	  double tmpLoad[m_gat.getSize()];
        	  	  for(int fk=0;fk<m_gat.getSize();fk++)
        	  	  {
        	  		  tmpLoad[fk]=m_gat.getLoadByAPID(fk);
        	  	  }
        	  	  m_mst.WriteModulationSamplesValue(ScanRequestCopyTmp,tmpLoad);

        	  	  //debugging
        	  	  double tmptmp[m_gat.getSize()];
        	  	  m_mst.RecordModulationSamplesValue(ScanRequestCopyTmp,tmptmp);
        	  	  for(int fk=0;fk<m_gat.getSize();fk++)
        	  	  {
        	  		  NS_LOG_UNCOND("" << Simulator::Now() << ": This is " << GetAddress() << " and modulation sample[" << fk << "] is " << tmptmp[fk]);
        	  	  }

        	  	  Simulator::Schedule(Seconds(0),&StaWifiMac::Enqueue,this,ScanRequestCopyTmp,ScanRequestCopyTo);//Enqueue(ScanRequestCopyTmp,ScanRequestCopyTo);
			  //Simulator::Cancel(m_timeOutEvent);
			  //m_timeOutEvent=Simulator::Schedule(Seconds(m_timeOut),&StaWifiMac::Timeout,this);
        	  	  scanDone=0;
              }

              if(m_macPathResult != "00:00:00:00:00:00" && m_endTrainOrNot == 1 && m_endTrainOrNot_ACK == 1)
              {
            	  //NS_LOG_UNCOND("NOW RESET M_MACPATHRESULT FROM " << m_macPathResult << " TO 00:00:00:00:00:00");
            	  //m_macPathResult = "00:00:00:00:00:00";
            	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": EVERYTHING RESET: m_endTrainOrNot=" << m_endTrainOrNot << " m_endTrainOrNot_ACK=" << m_endTrainOrNot << " m_feedback_ACK_ACK=" << m_feedback_ACK_ACK << " probeTrainCounter=" << probeTrainCounter);
            	  m_endTrainOrNot = 0;
            	  m_endTrainOrNot_ACK = 0;
            	  m_feedback_ACK_ACK = 0;
            	  //m_feedback_ACK = 0;
            	  probeTrainCounter = 0;
            	  m_pst.ResetLastProbeSeq();
            	  NS_LOG_UNCOND_YB ("" << Simulator::Now() << ": EVERYTHING RESET NOW: m_endTrainOrNot=" << m_endTrainOrNot << " m_endTrainOrNot_ACK=" << m_endTrainOrNot << " m_feedback_ACK_ACK=" << m_feedback_ACK_ACK << " probeTrainCounter=" << probeTrainCounter);

    	          //newly added
            	  if(m_probeOrNot == 1)
            	  {
                	  app2CopyTmp = app2Copy->Copy();
        	          m_ot.WriteOpValue(app2CopyTmp, INITIAL_PROBE_TRAIN_LENGTH);
        	          ForwardUp (app2CopyTmp, app2From, app2To);
        	          //m_macPathResult = "00:00:00:00:00:05";
        	          NS_LOG_UNCOND_YB ("" << Simulator::Now() << "send another packet with tag=" << INITIAL_PROBE_TRAIN_LENGTH << " for new train since associated: current m_macPathOne is " << m_macPathResult);
            	  }

              }
              SupportedRates rates = assocResp.GetSupportedRates ();
              if (m_htSupported)
                {
                  HtCapabilities htcapabilities = assocResp.GetHtCapabilities ();
                  m_stationManager->AddStationHtCapabilities (hdr->GetAddr2 (),htcapabilities);
                }
              if (m_vhtSupported)
                {
                  VhtCapabilities vhtcapabilities = assocResp.GetVhtCapabilities ();
                  m_stationManager->AddStationVhtCapabilities (hdr->GetAddr2 (), vhtcapabilities);
                }

              for (uint32_t i = 0; i < m_phy->GetNModes (); i++)
                {
                  WifiMode mode = m_phy->GetMode (i);
                  if (rates.IsSupportedRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1)))
                    {
                      m_stationManager->AddSupportedMode (hdr->GetAddr2 (), mode);
                      if (rates.IsBasicRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1)))
                        {
                          m_stationManager->AddBasicMode (mode);
                        }
                    }
                }
              if (m_htSupported)
                {
                  HtCapabilities htcapabilities = assocResp.GetHtCapabilities ();
                  for (uint32_t i = 0; i < m_phy->GetNMcs (); i++)
                    {
                      WifiMode mcs = m_phy->GetMcs (i);
                      if (mcs.GetModulationClass () == WIFI_MOD_CLASS_HT && htcapabilities.IsSupportedMcs (mcs.GetMcsValue ()))
                        {
                          m_stationManager->AddSupportedMcs (hdr->GetAddr2 (), mcs);
                          //here should add a control to add basic MCS when it is implemented
                        }
                    }
                }
              if (m_vhtSupported)
                {
                  VhtCapabilities vhtcapabilities = assocResp.GetVhtCapabilities ();
                  for (uint32_t i = 0; i < m_phy->GetNMcs (); i++)
                    {
                      WifiMode mcs = m_phy->GetMcs (i);
                      if (mcs.GetModulationClass () == WIFI_MOD_CLASS_VHT && vhtcapabilities.IsSupportedTxMcs (mcs.GetMcsValue ()))
                        {
                          m_stationManager->AddSupportedMcs (hdr->GetAddr2 (), mcs);
                          //here should add a control to add basic MCS when it is implemented
                        }
                    }
                }
              if (!m_linkUp.IsNull ())
                {
                  m_linkUp ();
                }
            }
          else
            {
              NS_LOG_DEBUG ("assoc refused");
              NS_LOG_UNCOND ("" << Simulator::Now() << ": assoc refused on " << GetAddress());
              SetState (REFUSED);
            }
        }
      return;
    }

  //Invoke the receive handler of our parent class to deal with any
  //other frames. Specifically, this will handle Block Ack-related
  //Management Action frames.
  RegularWifiMac::Receive (packet, hdr);
}

SupportedRates
StaWifiMac::GetSupportedRates (void) const
{
  SupportedRates rates;
  if (m_htSupported || m_vhtSupported)
    {
      for (uint32_t i = 0; i < m_phy->GetNBssMembershipSelectors (); i++)
        {
          rates.SetBasicRate (m_phy->GetBssMembershipSelector (i));
        }
    }
  for (uint32_t i = 0; i < m_phy->GetNModes (); i++)
    {
      WifiMode mode = m_phy->GetMode (i);
      rates.AddSupportedRate (mode.GetDataRate (m_phy->GetChannelWidth (), false, 1));
    }
  return rates;
}

HtCapabilities
StaWifiMac::GetHtCapabilities (void) const
{
  HtCapabilities capabilities;
  capabilities.SetHtSupported (1);
  if (m_htSupported)
    {
      capabilities.SetLdpc (m_phy->GetLdpc ());
      capabilities.SetSupportedChannelWidth (m_phy->GetChannelWidth () == 40);
      capabilities.SetShortGuardInterval20 (m_phy->GetGuardInterval ());
      capabilities.SetShortGuardInterval40 (m_phy->GetChannelWidth () == 40 && m_phy->GetGuardInterval ());
      capabilities.SetGreenfield (m_phy->GetGreenfield ());
      capabilities.SetMaxAmsduLength (1); //hardcoded for now (TBD)
      capabilities.SetLSigProtectionSupport (!m_phy->GetGreenfield ());
      capabilities.SetMaxAmpduLength (3); //hardcoded for now (TBD)
      uint64_t maxSupportedRate = 0; //in bit/s
      for (uint8_t i = 0; i < m_phy->GetNMcs (); i++)
        {
          WifiMode mcs = m_phy->GetMcs (i);
          capabilities.SetRxMcsBitmask (mcs.GetMcsValue ());
          if (mcs.GetDataRate (m_phy->GetGuardInterval (), m_phy->GetGuardInterval (), 1) > maxSupportedRate)
            {
              maxSupportedRate = mcs.GetDataRate (m_phy->GetGuardInterval (), m_phy->GetGuardInterval (), 1);
            }
        }
      capabilities.SetRxHighestSupportedDataRate (maxSupportedRate / 1e6); //in Mbit/s
      capabilities.SetTxMcsSetDefined (m_phy->GetNMcs () > 0);
      capabilities.SetTxMaxNSpatialStreams (m_phy->GetNumberOfTransmitAntennas ());
    }
  return capabilities;
}

VhtCapabilities
StaWifiMac::GetVhtCapabilities (void) const
{
  VhtCapabilities capabilities;
  capabilities.SetVhtSupported (1);
  if (m_vhtSupported)
    {
      if (m_phy->GetChannelWidth () == 160)
        {
          capabilities.SetSupportedChannelWidthSet (1);
        }
      else
        {
          capabilities.SetSupportedChannelWidthSet (0);
        }
      capabilities.SetMaxMpduLength (2); //hardcoded for now (TBD)
      capabilities.SetRxLdpc (m_phy->GetLdpc ());
      capabilities.SetShortGuardIntervalFor80Mhz ((m_phy->GetChannelWidth () == 80) && m_phy->GetGuardInterval ());
      capabilities.SetShortGuardIntervalFor160Mhz ((m_phy->GetChannelWidth () == 160) && m_phy->GetGuardInterval ());
      capabilities.SetMaxAmpduLengthExponent (7); //hardcoded for now (TBD)
      uint8_t maxMcs = 0;
      for (uint8_t i = 0; i < m_phy->GetNMcs (); i++)
        {
          WifiMode mcs = m_phy->GetMcs (i);
          if (mcs.GetMcsValue () > maxMcs)
            {
              maxMcs = mcs.GetMcsValue ();
            }
        }
      capabilities.SetRxMcsMap (maxMcs, 1); //Only 1 SS is currently supported
      capabilities.SetTxMcsMap (maxMcs, 1); //Only 1 SS is currently supported
    }
  return capabilities;
}

void
StaWifiMac::SetState (MacState value)
{
  if (value == ASSOCIATED
      && m_state != ASSOCIATED)
    {
      m_assocLogger (GetBssid ());
    }
  else if (value != ASSOCIATED
           && m_state == ASSOCIATED)
    {
      m_deAssocLogger (GetBssid ());
    }
  m_state = value;
}

} //namespace ns3
