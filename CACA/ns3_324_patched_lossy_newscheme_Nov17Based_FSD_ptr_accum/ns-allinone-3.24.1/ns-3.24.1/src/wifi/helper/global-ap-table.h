#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/mac48-address.h"
#include "ns3/ipv4-address.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/ssid.h"
#include "ns3/wifi-information-element.h"

#ifndef GLOBAL_AP_TABLE
#define GLOBAL_AP_TABLE 1

namespace ns3{

	class globalAPTable{

		public:

			typedef struct{

				Mac48Address apAddr;
				int channelNumber;
				Ssid ssid;
				double load;

			} apTable;

			globalAPTable();
			globalAPTable(int size);
			void reSize(int size);
			int getSize(void);

			void setMacAddr(int _apID, Mac48Address _macAddr);
			int getAPIDByMacAddr(Mac48Address _macAddr);
			Mac48Address getMacAddrByAPID(int _apID);

			void setChannelNumber(int _apID, int _channelNumber);
			int getChannelNumberByAPID(int _apID);

			void setSsid(int _apID, Ssid _ssid);
			Ssid getSsidByAPID(int _apID);

			void setLoad(int _apID, double _load);
			double getLoadByAPID(int _apID);

			int getNext(int _apID);

		private:

			void initAPTable(int size);

			std::vector<apTable>m_gat;

	};


}

#endif
