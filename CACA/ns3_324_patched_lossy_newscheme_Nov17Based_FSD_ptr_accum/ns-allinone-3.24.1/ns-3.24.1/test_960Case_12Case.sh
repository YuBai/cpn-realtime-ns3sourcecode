#!/bin/sh

#SBATCH --time=48:00:00		#Run Time in hh:mm:ss
#SBATCH --mem-per-cpu=8192 	#Maximum memory required per CPU
#SBATCH --job-name=testElephantAP1LosslessPtr

sbatch 12111_test960Case.slurm
sbatch 12112_test960Case.slurm
sbatch 12113_test960Case.slurm
sbatch 12114_test960Case.slurm
sbatch 12121_test960Case.slurm
sbatch 12122_test960Case.slurm
sbatch 12123_test960Case.slurm
sbatch 12124_test960Case.slurm
sbatch 12211_test960Case.slurm
sbatch 12212_test960Case.slurm
sbatch 12213_test960Case.slurm
sbatch 12214_test960Case.slurm
sbatch 12221_test960Case.slurm
sbatch 12222_test960Case.slurm
sbatch 12223_test960Case.slurm
sbatch 12224_test960Case.slurm

