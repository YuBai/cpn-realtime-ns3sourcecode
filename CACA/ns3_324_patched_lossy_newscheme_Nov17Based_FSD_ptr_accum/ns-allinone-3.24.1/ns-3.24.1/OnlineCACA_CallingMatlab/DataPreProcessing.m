%read locations, uplink traffic and downlink traffic files
%generate testC,A,S,X

%return cell array storing schedule info
function scheduleInfo=DataPreProcessing(appName,firstGroupOffset,secondGroupOffset,homoValue)

%ATTENTION: All files should be of same amount of rows to guarantee that
%data for each node are matched correctly! To achieve this, fill entries
%for AP in downlinkTraffic and uplinkTraffic files with anything (since
%they will not be considered in the initialization loop).

%clear all;
%/Users/zerdocross/Downloads/testOnSuperComputer/April26/CVC_Case
%pathAppendix='/Users/zerdocross/Downloads/testOnSuperComputer/July4/12Case/';
%'/Users/zerdocross/Downloads/testOnSuperComputer/April26/CVC_Case/';
%'/Users/zerdocross/Downloads/testOnSuperComputer/April26/CVC_Case/';
%pathAppendix='/Volumes/BackupDisk/zerdocross/Downloads/testOnSuperComputer/April26/CVC_Case/';
%location='locations';
%downlinkTraffic='DownlinkTraffic';
%uplinkTraffic='UplinkTraffic';

AFile=[appName,'_testA'];
CFile=[appName,'_testC'];
%SFile=[appName,'_testS'];
XFile=[appName,'_testX'];

locationR=[appName,'_location'];
uplinkTraffic=[appName,'_location_uplinkTraffic'];

%locations,downlinkTraffic, and uplinkTraffic
fileName1=locationR;%strcat(pathAppendix,caseAppendix,location);
%fileName2=strcat(pathAppendix,caseAppendix,downlinkTraffic);
fileName3=uplinkTraffic;%strcat(pathAppendix,caseAppendix,uplinkTraffic);

fileName4=[appName,'_location_uplinkTraffic_testS'];
solutionR=[appName,'_location_uplinkTraffic_testS_solution'];
SFile=fileName4;

fprintf('fileName1: %s\n',fileName1);
fprintf('fileName3: %s\n',fileName3);
fprintf('fileName4: %s\n',fileName4);


homogeneousOrNot=1; %homogeneous migration cost or not

APID=[1,2];
%%

firstGroupOffsetX=[3,firstGroupOffset+3-1];
secondGroupOffsetX=[firstGroupOffset+3,secondGroupOffset+2];

firstGroupOffset=firstGroupOffsetX;
secondGroupOffset=secondGroupOffsetX;

data1=csvread(fileName1);
data3=csvread(fileName3);
data4=csvread(fileName4);

%prepare testA
APInfo=zeros(size(APID,2),4); %row:each AP; column:x axis and y axis values
APInfo(:,3)=1:1:size(APID,2); %set ID
APInfo(1:1:size(APID,2),1)=data1(APID(1,:),2);
APInfo(1:1:size(APID,2),2)=data1(APID(1,:),3);

ClientInfo=zeros(size(data1,1)-size(APID,2),6); %row: each client; column: as defined in testC
XInfo=zeros(size(data1,1)-size(APID,2),size(APID,2)); %row: each client; column: each AP

%prepare testC
for xixi=1:1:size(data1,1)
    
    %if(find(xixi,APID))
    if(xixi == APID(1,1) || xixi == APID(1,2))
        
        continue;
        
    else
        
        if(xixi>=firstGroupOffset(1,1) && xixi<=firstGroupOffset(1,2))
            
            ClientInfo(xixi,1:1:2)=data1(xixi,2:1:3); %location info
            ClientInfo(xixi,5)=data3(xixi-firstGroupOffset(1,1)+1,4); %packet len info
            
            ClientInfo(xixi,4)=1; %which AP it associates
            if(homogeneousOrNot == 1)
                ClientInfo(xixi,6)=homoValue; %migration cost
            end
                        
            ClientInfo(xixi,3)=ceil(1/data4(xixi-2,1));

        elseif(xixi>=secondGroupOffset(1,1) && xixi<=secondGroupOffset(1,2))
            
            ClientInfo(xixi,1:1:2)=data1(xixi,2:1:3); %location info
            ClientInfo(xixi,5)=data3(xixi-secondGroupOffset(1,1)+1,4); %packet len info
            
            ClientInfo(xixi,4)=2; %which AP it associates
            if(homogeneousOrNot == 1)
                ClientInfo(xixi,6)=homoValue; %migration cost
            end
                        
            ClientInfo(xixi,3)=ceil(1/data4(xixi-2,2));

        end
        
    end
    
end

%prepare testX
if(homogeneousOrNot==1)
    
    XInfo(firstGroupOffset(1,1):1:size(data1,1),:)=homoValue;
    
end


%remove blank lines
TF1=APInfo(:,1)==0;
TF2=ClientInfo(:,1)==0;
TF4=XInfo(:,1)==0;

APInfoPrint=APInfo;
APInfoPrint(TF1,:)=[];

ClientInfoPrint=ClientInfo;
ClientInfoPrint(TF2,:)=[];

XInfoPrint=XInfo;
XInfoPrint(TF4,:)=[];

dlmwrite(AFile,APInfoPrint);
dlmwrite(CFile,ClientInfoPrint);
dlmwrite(XFile,XInfoPrint);


%%
%start to call offline method
[~,lembdaI]=OfflineAlgorithmCalculation(CFile,AFile,XFile,SFile,homoValue*(size(data1,1)-size(APID,2))*0.25); %need to remove all AP entries from data1

if(isempty(lembdaI))
    fprintf('no optimization exist!');
    scheduleInfo=[];
    return;
end

%shape back into matrix showing which one to re-associate
solution=reshape(lembdaI,[size(ClientInfoPrint,1) size(APID,2)])';

count=1;
counta=1;
ToBeHandledNodes=zeros(size(find(solution),1),1);
ToBeHandledNodesOriginalAP=zeros(size(find(solution),1),1);
ToBeHandledNodesTargetAP=zeros(size(find(solution),1),1);

%update corresponding client info
for xixi=1:1:size(solution,1)
    
    for xixix=1:1:size(solution,2)
        
        if(solution(xixi,xixix) ~= xixi && solution(xixi,xixix) ~= 0) %meaning it should go to a new AP
            
            fprintf('%d associates from AP %d to AP %d\n',xixix,xixi,sum(1:1:size(solution,1))-xixi);%something not quite right here: assign to alternative AP temporarily
            ClientInfoPrint(xixix,4)=sum(1:1:size(solution,1))-ClientInfoPrint(xixix,4);
            ClientInfoPrint(xixix,3)=ceil(1/data4(xixix,ClientInfoPrint(xixix,4)));

            ToBeHandledNodes(counta,1)=xixix;
            ToBeHandledNodesOriginalAP(counta,1)=xixi;
            ToBeHandledNodesTargetAP(counta,1)=sum(1:1:size(solution,1))-xixi;
            counta=counta+1;
        end
        count=count+1;
        
    end
    
end

dlmwrite(CFile,ClientInfoPrint);

%write solution file
Solution=[ToBeHandledNodes,ToBeHandledNodesTargetAP];
dlmwrite(solutionR,Solution);

end
