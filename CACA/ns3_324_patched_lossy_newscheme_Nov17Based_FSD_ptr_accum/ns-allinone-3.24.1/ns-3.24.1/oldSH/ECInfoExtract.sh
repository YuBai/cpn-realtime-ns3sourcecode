#!/bin/bash

macPrefix1="00:00:00:00:00:0"
macPrefix2="00:00:00:00:00:"

for f in *.err
	do

		echo ECInfo_{$f}
		cat "$f" | grep "Queue Length" > ECResult/ECInfo_{$f}

		#only for 00:00:00:00:00:07 - handle with others in the future
		for ((i=7; i<8; i++))
			do

				cat ECResult/ECInfo_{$f} | grep $macPrefix1$i > ECResult/ECInfo_{$f}_{$macPrefix1}{$i}_tmp_tmp
				sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,-1/g" < ECResult/ECInfo_{$f}_{$macPrefix1}{$i}_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}{$i}_tmp
				sed -e "s/://g" -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}{$i}_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}{$1}
			done
		
	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}a > ECResult/ECInfo_{$f}_{$macPrefix1}a_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}a_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}a_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}a_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}a

	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}b > ECResult/ECInfo_{$f}_{$macPrefix1}b_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}b_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}b_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}b_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}b

	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}c > ECResult/ECInfo_{$f}_{$macPrefix1}c_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}c_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}c_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}c_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}c

	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}d > ECResult/ECInfo_{$f}_{$macPrefix1}d_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}d_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}d_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}d_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}d

	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}e > ECResult/ECInfo_{$f}_{$macPrefix1}e_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}e_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}e_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}e_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}e

	#cat ECResult/ECInfo_{$f} | grep ${macPrefix1}f > ECResult/ECInfo_{$f}_{$macPrefix1}f_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix1}f_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}f_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix1}f_tmp > ECResult/ECInfo_{$f}_{$macPrefix1}f

	#	for ((i=11; i<19; i++))
	#		do

	#			cat ECResult/ECInfo_{$f} | grep $macPrefix2$i > ECResult/ECInfo_{$f}_{$macPrefix2}{$i}_tmp_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ No Packet In Service./,x/g" < ECResult/ECInfo_{$f}_{$macPrefix2}{$i}_tmp_tmp > ECResult/ECInfo_{$f}_{$macPrefix2}{$i}_tmp
	#			sed -e "s/ns - /,/g" -e "s/: Current State is /,/g" -e "s/ With Queue Length /,/g" -e "s/ With Packet Being Served As /,/g" < ECResult/ECInfo_{$f}_{$macPrefix2}{$i}_tmp > ECResult/ECInfo_{$f}_{$macPrefix2}{$1}
	#		done
	
	done


for f in *.err
	do
		cat "$f" | grep "AccessRequestTime" > ECResult/ECInfoAccTime_{$f}_tmp
		sed -e "s/f/15/g" -e "s/e/14/g" -e "s/d/13/g" -e "s/c/12/g" -e "s/b/11/g" -e "s/a/10/g" -e "s/Node//g" -e "s/PacketTagger//g" -e "s/Packet UID//g" -e "s/AccessRequestTime//g" -e "s/ns//g" < ECResult/ECInfoAccTime_{$f}_tmp > ECResult/ECInfoAccTime_{$f}


	done




