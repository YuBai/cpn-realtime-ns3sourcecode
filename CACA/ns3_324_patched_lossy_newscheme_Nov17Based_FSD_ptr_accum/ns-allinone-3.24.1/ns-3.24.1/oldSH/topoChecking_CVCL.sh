#!/bin/bash

#iValue=( 16 16 17 16 16 16 16 16 16 16 16 16 )
#jValue=( 0 1 0 0 0 0 1 1 2 2 3 4 )
#kValue=( 0 0 0 1 0 0 0 0 0 0 0 0 )
#lValue=( 0 0 0 0 1 2 0 1 0 1 0 0 )
#mValue=( 4 3 3 3 3 2 3 2 2 1 1 0 )

iValue=( 16 17 17 16 17 18 16 17 16 17 16 16 17 18 16 16 17 16 16 16 16 16 17 18 18 16 16 17 16 16 16 16 16 16 16 16 16 16 )
jValue=( 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1 1 0 1 1 1 1 1 1 2 2 2 2 )
kValue=( 0 0 1 0 0 2 1 1 0 2 1 0 0 0 2 0 0 1 0 1 0 0 0 0 1 0 0 0 0 1 2 0 0 0 0 0 2 )
lValue=( 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 1 1 1 1 1 0 2 0 0 0 0 2 0 0 0 0 1 1 0 0 0 0 )
mValue=( 2 2 1 3 3 0 2 2 4 1 3 1 1 1 2 2 2 1 3 2 1 1 1 1 0 2 2 2 3 2 1 1 2 0 1 2 0 )

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL.cc"

for((xx=0;xx<36;xx++))
	do
		i=${iValue[xx]}
		j=${jValue[xx]}
		k=${kValue[xx]}
		l=${lValue[xx]}
		m=${mValue[xx]}

		echo $i $j $k $l $m

		sed -i "s/17,1,1,1,0/$i,$j,$k,$l,$m/g" scratch/"$fileName"
		
                ./waf build	
                ./waf --run PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL > logggTest_contentionVScontention_iIs"$i"_jIs"$j"_kIs"$k"_lIs"$l"_mIs"$m" 2>&1

		sed -i "s/$i,$j,$k,$l,$m/17,1,1,1,0/g" scratch/"$fileName"


done
