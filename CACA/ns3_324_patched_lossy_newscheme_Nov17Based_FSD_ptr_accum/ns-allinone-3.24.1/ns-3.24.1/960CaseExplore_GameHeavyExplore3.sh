#!/bin/bash

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case.cc"
slurmName="test960Case.slurm"

#Program Arguments:
#    --caseMode:        Choose between CVCL (1) and CVC (2) [0]
#    --congestionMode:  Choose between AP1 (1) and AP2 (2) [-1938129670]
#    --patternMode:     Choose between Game (1) and Video (2) (FSD Node) [32548]
#    --planMode:        Choose between Low Load (1) and High Load (2) (Background Traffic) [-1936210440]
#    --planID:          Choose From Slightest Load (1) to Heaviest Load (4) (Background Traffic) [32548]


#./waf --run "PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case --caseMode=1 --congestionMode=2 --patternMode=1 --planMode=1 --planID=1" > logggTest 2>&1

#speed=( 0.002691 0.003765 0.004839 0.005913 0.006987 0.008061 0.009135 0.0010209 0.0011283 )
speed=( 0.0021 0.0024 0.0027 0.003 0.0033 0.0036 0.0042 0.0045 )


for((xx=0;xx<8;xx++))
	do	
		xm=${speed[xx]}
		for((i=2;i<3;i++))
			do
				for((j=2;j<3;j++))
					do
						for((k=1;k<3;k++))
							do
								for((l=2;l<3;l++))
									do
										for((m=3;m<4;m++))
											do

												echo $xm $i $j $k $l $m
												cp scratch/$fileName scratch/"$xx"_"$i""$j""$k""$l""$m"_$fileName
												cp $slurmName "$xx"_"$i""$j""$k""$l""$m"_$slurmName	

												sed -i "s/=0.045/=$xm/g" scratch/"$xx"_"$i""$j""$k""$l""$m"_$fileName									

												echo "./waf --run \""$xx"_"$i""$j""$k""$l""$m"_PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case --caseMode=$i --congestionMode=$j --patternMode=$k --planMode=$l --planID=$m\" > logggTest_"$xx"_iIs"$i"_jIs"$j"_kIs"$k"_lIs"$l"_mIs"$m"_960Case.err 2>&1" >> "$xx"_"$i""$j""$k""$l""$m"_$slurmName
												echo "sbatch "$xx"_"$i""$j""$k""$l""$m"_"$slurmName"" >> test_960Case_GameExplore3.sh

											done


									done
					

							done


					done
		


			done



	done


./waf build
