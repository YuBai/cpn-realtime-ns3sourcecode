#!/bin/bash

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case.cc"
slurmName="test960Case.slurm"

#Program Arguments:
#    --caseMode:        Choose between CVCL (1) and CVC (2) [0]
#    --congestionMode:  Choose between AP1 (1) and AP2 (2) [-1938129670]
#    --patternMode:     Choose between Game (1) and Video (2) (FSD Node) [32548]
#    --planMode:        Choose between Low Load (1) and High Load (2) (Background Traffic) [-1936210440]
#    --planID:          Choose From Slightest Load (1) to Heaviest Load (4) (Background Traffic) [32548]


#./waf --run "PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case --caseMode=1 --congestionMode=2 --patternMode=1 --planMode=1 --planID=1" > logggTest 2>&1


for((i=1;i<3;i++))
	do
		for((j=1;j<3;j++))
			do
				for((k=1;k<3;k++))
					do
						for((l=1;l<3;l++))
							do
								for((m=1;m<5;m++))
									do

										echo $i $j $k $l $m
										cp scratch/$fileName scratch/"$i""$j""$k""$l""$m"_$fileName
										cp $slurmName "$i""$j""$k""$l""$m"_$slurmName	
										echo "./waf --run \"PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_960Case --caseMode=$i --congestionMode=$j --patternMode=$k --planMode=$l --planID=$m\" > logggTest_iIs"$i"_jIs"$j"_kIs"$k"_lIs"$l"_mIs"$m"_960Case.err 2>&1" >> "$i""$j""$k""$l""$m"_$slurmName
										echo "sbatch "$i""$j""$k""$l""$m"_"$slurmName"" >> test_960Case.slurm 

									done


							done
					

					done


			done
		


	done
./waf build
