function retValue=LPMRAP(D,G,S)

    %D is (Iij)
    %G is (lembda_{ij})
    %X (dij)
    %we call linprog to get result directly
    %but first we need to generate matrices for lingprog
    
    %DOriginal=D;
    %kk=find(DOriginal);
    
    GSize=size(G,1);
    %DSize=size(D,1);
    
    S=reshape(S,size(D,1)*size(G,1),1)';
    D=reshape(D,size(D,1)*size(G,1),1)';    
    
    kfk=find(D); %however this is only half of data required - we need to enlarge k to get other half
    k=zeros(1,2*size(kfk,2));
    k(1,1:1:size(kfk,2))=kfk(1,:);
    for xixi=size(kfk,2)+1:1:2*size(kfk,2)
        
        if(k(1,xixi-size(kfk,2))>=0 && k(1,xixi-size(kfk,2))<=size(D,2)/GSize) %meaning this belongs to AP1 originally
            
            k(1,xixi)=k(1,xixi-size(kfk,2))+size(D,2)/GSize; %put its counter part into formula
            
        else %meaning this belongs to AP2 originally
            
            k(1,xixi)=k(1,xixi-size(kfk,2))-size(D,2)/GSize;
            
        end
                
    end
    k=sort(k);
    
    
    A=zeros(size(G,1)+size(k,2)/GSize,size(k,2)+1); %first size(G,1): constraint 9; seconed size(G,1): constraint 10; 2*size(D,1): relaxed constraint 11
    B=zeros(size(G,1)+size(k,2)/GSize,1); %similar
    
    lb=zeros(1,size(k,2)+1);
    ub=zeros(1,size(k,2)+1);
    
    lb(1,1)=-inf;
    ub(1,1)=inf;
    
    lb(1,2:1:size(k,2)+1)=0;
    ub(1,2:1:size(k,2)+1)=1;
    
    %A:fill elements for constraint 9
    A(1:1:size(G,1),1)=-1;
    for xixi=1:1:size(G,1)
        
        for xixix=(xixi-1)*size(k,2)/GSize+1:1:xixi*size(k,2)/GSize
                
                A(xixi,xixix-(xixi-1)*size(k,2)/GSize+(xixi-1)*size(k,2)/GSize+1)=...
                S(1,k(1,xixix));%*D(1,k(1,xixix));                   
               %S(1,k(1,xixix-(xixi-1)*size(k,2)/GSize));%*D(1,k(1,xixix));
                
        end
                
    end
    
    %A:fill elements for constraint 10
    for xixi=size(G,1)+1:1:size(G,1)+size(k,2)/GSize
        
        A(xixi,xixi-size(G,1)+1)=1;
        A(xixi,xixi-size(G,1)+size(k,2)/GSize+1)=1;
        
    end
    
%     %A:fill elements for constraint 11 - stupid method but it should work
%     for xixix=size(G,1)+size(kk,1)+1:1:size(G,1)+size(kk,1)+size(k,2)
%         
%         A(xixix,k(1,xixix-size(G,1)-size(kk,1))+1)=1;
%         A(xixix+size(k,2),k(1,xixix-size(G,1)-size(kk,1))+1)=-1;
%         
%     end
    
    %B:fill elements for constraint 9
    B(1:1:size(G,1),1)=-G(1:1:size(G,1),1);
    
    %B:fill elements for constraint 10
    B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,1)=1;
    
%     %B:fill elements for constraint 11
%     B(size(G,1)+size(kk,1)+1:1:size(G,1)+size(kk,1)+size(k,2),1)=1;
%     B(size(G,1)+size(kk,1)+size(k,2)+1:1:size(G,1)+size(kk,1)+2*size(k,2),1)=0;        
   
    %A:remove unnecessary columns
    %A( :, ~any(A,1) ) = []; 
    
    %move constraint 10 into Aeq and Beq
    Aeq=A(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:);
    A(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:)=[];
    Beq=B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,1);
    B(size(G,1)+1:1:size(G,1)+size(k,2)/GSize,:)=[];
    
    %remove redundant lines
    
    otherValues=zeros(1,size(k,2));
    f=[1 otherValues];
    
    %calculate result in a compact format
    retValue=linprog(f,A,B,Aeq,Beq,lb,ub);
    
    %change current result into a standard format
    retValueTmp=zeros(1,size(D,2)+1);
    kP=k+1; %since first element is H
    retValueTmp(1,1)=retValue(1,1);
    for xixixi=1:1:size(kP,2)
        
        retValueTmp(1,kP(1,xixixi))=retValue(xixixi,1);
        
    end
        
    retValue=retValueTmp;

end