#!/bin/bash

fileName="PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL.cc"
slurmName="testCVC.slurm"


for((i=20;i>15;i--))
	do
		for((j=20-i;j>=0;j--))
			do

				for((k=20-i-j;k>=0;k--))
					do

						for((l=20-i-j-k;l>=0;l--))
							do

								for((m=20-i-j-k-l;m>=0;m--))

									do

											echo $i $j $k $l
											cp scratch/$fileName scratch/"$i""$j""$k""$l""$m"_$fileName
											cp $slurmName "$i""$j""$k""$l""$m"_$slurmName	

											sed -i "s/17,1,1,1,0/$i,$j,$k,$l,$m/g" scratch/"$i""$j""$k""$l""$m"_$fileName

											echo "./waf --run "$i""$j""$k""$l""$m"_PerformanceEvaluation_APsAnd50Nodes_Assoc_New_Subnets_Rat_DualDirFlow_3APP_AP1_contentionVScontention_sameTopoWithCVCL > logggTest_iIs"$i"_jIs"$j"_kIs"$k"_lIs"$l"_mIs"$m"_contentionVScontention_sameTopoWithCVCL.err 2>&1" >> "$i""$j""$k""$l""$m"_$slurmName 
											
											echo "sbatch "$i""$j""$k""$l""$m"_"$slurmName"" >> test_CVC.slurm 
	
									done



							done



					done


			done



	done

./waf build
#sbatch test_CVC.slurm
